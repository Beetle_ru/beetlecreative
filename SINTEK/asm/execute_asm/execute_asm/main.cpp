#include <stdio.h>
 

extern "C" void sayHello();

void main () {
    printf("Hello, what is your name?\n");
	sayHello();
}

extern "C" void* readName() {
    char name[255];        
    scanf("%s", &name);
    return &name;
}