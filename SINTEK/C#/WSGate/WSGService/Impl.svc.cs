﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SQLite;
using Spire.Doc;
using Pdfizer;

namespace WSGService {
    public class Service1 : IImpl {
        private SQLiteConnection _dbConnection;
        private const string DBName = "main.db";

        public byte[] GetContent(int id, string type) {
            if (_dbConnection == null) if (!CreateDBConn(DBName)) return null;

            var sHtml = GetRecord(id);

            byte[] result = null;

            switch (type) {
                case "html":
                    result = GetHtml(sHtml);
                    break;

                case "pdf":
                    result = GetPdf(sHtml);
                    break;

                case "word":
                    result = GetDoc(sHtml);
                    break;
            }

            return result;
        }

        private bool CreateDBConn(string dbName) {
            var path = System.AppDomain.CurrentDomain.BaseDirectory;
            var source = String.Format("{0}{1}", path, dbName);

            if (!File.Exists(source)) return false;

            var connectionString = String.Format("Data Source = {0}", source);
            _dbConnection = new SQLiteConnection(connectionString);
            _dbConnection.Open();

            return _dbConnection != null;
        }

        private string GetRecord(int id) {
            var sbHtml = new System.Text.StringBuilder();
            using (var command = _dbConnection.CreateCommand()) {
                command.CommandText = String.Format("SELECT * FROM Documents WHERE id = {0}", id);

                try {
                    using (var reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            sbHtml.Append(reader[1]);
                        }
                    }
                }
                catch (Exception) {
                    return null;
                }
            }

            return sbHtml.ToString();
        }

        private byte[] GetHtml(string html) {
            return System.Text.Encoding.ASCII.GetBytes(html);
        }

        private byte[] GetPdf(string html) {
            var ms = new MemoryStream();

            var converter = new Pdfizer.HtmlToPdfConverter();
            converter.Open(ms);
            converter.Run(html);
            converter.Close();

            return ms.GetBuffer();
        }

        private byte[] GetDoc(string html) {
            var ms = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(html));
            var doc = new Document();
            doc.LoadFromStream(ms, FileFormat.Html);

            ms = new MemoryStream();
            doc.SaveToStream(ms, FileFormat.Docx);

            return ms.GetBuffer();
        }
    }
}