﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace exp4_tree.Tree {
    internal enum VarTypeE {
        Undefined
        ,SINT
        ,INT
        ,DINT
        ,LINT
        ,USINT
        ,UINT
        ,UDINT
        ,ULINT
        ,REAL
        ,LREAL
        ,WORD
        ,DWORD
        ,LWORD
        ,BOOL
        ,BYTE
        ,STRING
        ,WSTRING
        ,TIME
        ,DATE
        ,TOD
        ,DT
        ,TIME_OF_DAY
        ,DATE_AND_TIME
    }

    internal class Var {
        public string Name;
        public ValueType Type;
        public string ValueInit;

        public static string[] VarTypesStr = {
                                                 "\0" //Undefined
                                                 ,"sint"
                                                 ,"int"
                                                 ,"dint"
                                                 ,"lint"
                                                 ,"usint"
                                                 ,"uint"
                                                 ,"udint"
                                                 ,"ulint"
                                                 ,"real"
                                                 ,"lreal"
                                                 ,"word"
                                                 ,"dword"
                                                 ,"lword"
                                                 ,"bool"
                                                 ,"byte"
                                                 ,"string"
                                                 ,"wstring"
                                                 ,"time"
                                                 ,"date"
                                                 ,"tod"
                                                 ,"DT"
                                                 ,"time_of_day"
                                                 ,"date_and_time"
                                             };
    }
}