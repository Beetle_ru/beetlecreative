﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using exp4_tree.Tree;

namespace exp4_tree {
    public class Lexer {
        private String _fileBuffer;
        private int _charIndex;
        private Token _lastToken;
        private char _chrForDebug; // remoove it later

        public Lexer() {
            _lastToken = new Token(_fileBuffer);
        }

        public void SetBuffer(string buffer) {
            _fileBuffer = buffer;
            _charIndex = 0;
            //GetNexToken();
        }

        public char Look() {
            _chrForDebug = _charIndex < _fileBuffer.Length ? _fileBuffer[_charIndex] : '\0';
            return _chrForDebug;
        }

        private char Next() {
            _charIndex = _charIndex < _fileBuffer.Length ? _charIndex + 1 : _charIndex;
            return Look();
        }

        private char Preview(int next = 1) {
            return _charIndex + next < _fileBuffer.Length ? _fileBuffer[_charIndex + next] : '\0';
        }

        public Token GetNexToken() {
            SkipWhite();
            Token t;
            if (IsAlpha(Look()) || IsUnderscape(Look())) t = GetIdent();
            else if (IsDigit(Look())) t = GetNum();
            else if (IsOp(Look())) t = GetOp();
            else {
                if (_charIndex >= _fileBuffer.Length) return null;
                t = Token.CreateToken(_fileBuffer, _charIndex, 1, TokenType.SpecSymbl);
                Next();
            }

            SkipWhite();
            return t;
        }

        //public Token GetNexToken() {
        //    SkipWhite();
        //    Token t = new Token(_fileBuffer) { Index = _charIndex, Type = TokenType.Undefine };

        //    var strBuf = "";

        //    var loop = true;
        //    while (loop) {
        //        strBuf += Look();
        //        t.Length++;
        //        Next();
        //        IsNumericLiteral(t);

        //    }

        //    SkipWhite();
        //    return t;
        //}

        //public bool IsNumericLiteral(Token t) {
        //    var str = t.GetString();
        //    var splt = str.Split('#');
        //    if (splt.Count() == 2) {
        //        if (!IsContain(Var.VarTypesStr, splt[0])) return false;
        //        if (!IsNumber(splt[1])) return false;
        //    }
        //    else if (splt.Count() == 1) {
        //        if (!IsNumber(str)) return false;
        //    }
        //    return false;
        //}

        //public bool IsNumber(string str) {
        //    var isRealDetected = false;
        //    for (int i = 0; i < str.Length; i++) {
        //        var c = str[i];
        //        if (c == '.') {
        //            if (isRealDetected) return false;
        //            isRealDetected = true;
        //            continue;
        //        }
        //        var cond1 = ((i == 0) && (IsDigit(c) || (c == '+') || (c == '-')));
        //        var cond2 = ((i > 0) && IsDigit(c));
        //        if (!(cond1 || cond2)) return false;
        //    }

        //    return true;
        //}

        //public bool IsContain(string[] dic, string str) {
        //    foreach (var s in dic) {
        //        if (s == str) return true;
        //    }
        //    return false;
        //}

        public Token GetIdent() {
            var t = new Token(_fileBuffer) {Index = _charIndex, Type = TokenType.Ident};

            if (!(IsAlpha(Look()) || IsUnderscape(Look())))
                Logger.Expected(t, "Identifier");
            while (IsAlNumUnd(Look())) {
                Next();
                t.Length++;
            }

            SkipWhite();

            _lastToken = t;
            return _lastToken;
        }

        public Token GetNum() {
            var t = new Token(_fileBuffer) {Index = _charIndex, Type = TokenType.Number};
            var isDotBeen = false;

            // prefix
            if ((Look() == '+') || (Look() == '-')) Next();

            if (!IsDigit(Look())) Logger.Expected(t, "Integer");

            while (IsDigit(Look()) || (Look() == '.')) {
                if (Look() == '.') {
                    if (isDotBeen) Logger.Abort(t, "Detected second dot symbol");
                    isDotBeen = true;
                }
                Next();
                t.Length++;
            }

            SkipWhite();

            _lastToken = t;
            return _lastToken;
        }

        public Token GetOp() {
            var t = new Token(_fileBuffer) {Index = _charIndex, Type = TokenType.Operator};
            if (!IsOp(Look())) Logger.Expected(t, "Operator");
            while (IsOp(Look())) {
                Next();
                t.Length++;
            }

            SkipWhite();

            _lastToken = t;
            return _lastToken;
        }

        public void SkipWhite() {
            while (IsWhite(Look())) Next();
        }

        public bool IsWhite(char c) {
            const string dic = "\n\r\t ";
            return dic.Any(chr => c == chr);
        }

        public bool IsDigit(char c) {
            const string dic = "01234567890";
            return dic.Any(chr => c == chr);
        }

        public bool IsAlpha(char c) {
            const string dic = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            return dic.Any(chr => c == chr);
        }

        public bool IsAlNumUnd(char c) {
            return IsAlpha(c) || IsDigit(c) || IsUnderscape(c);
        }

        public bool IsUnderscape(char c) {
            return c == '_';
        }

        public bool IsOp(char c) {
            const string dic = @"+-*/<>:=";
            return dic.Any(chr => c == chr);
        }
    }
}