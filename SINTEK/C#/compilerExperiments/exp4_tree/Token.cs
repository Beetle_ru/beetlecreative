﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace exp4_tree {
    public enum TokenType { Undefine, Ident, Operator, SpecSymbl, Number }

    public class Token {
        

        public int Index;
        public int Length;
        public TokenType Type;
        private int _hash;
        private int _lastI;
        private int _lastL;
        private string _str;

        private string _buffer; // pointer

        public Token(string buffer) {
            _buffer = buffer;
        }

        static public Token CreateToken(string buffer, int index, int length = 0, TokenType type = TokenType.Undefine) {
            return new Token(buffer) {Index = index, Length = length, Type = type};
        }

        public string GetString() {
            if ((_lastI != Index) || (_lastL != Length)) {
                BuildString();
                _lastI = Index;
                _lastL = Length;
            }
            return _str;
        }

        private void BuildString() {
            if ((Index + Length) > _buffer.Length) {
                _str = "";
                return;
            }
            _str = _buffer.Substring(Index, Length);
        }

        public int Hash() {
            if (_hash == 0) CreateHash();
            return _hash;
        }

        static public bool Equal(Token t1, Token t2) {
            if (t1.Hash() != t2.Hash()) return false;
            return t1.GetString() == t2.GetString();
        }

        private void CreateHash() {
            _hash = GetHash(GetString());
        }

        public static int GetHash(string str) {
            return str.GetHashCode();
        }

        public string GetBuffer() {
            return _buffer;
        }
    }
}
