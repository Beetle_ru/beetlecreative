﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace exp4_tree {
    public class Logger {
        public static void Expected(Token t, string str) {
            str += " expected";
            Abort(t, str);
        }

        public static void Undefine(Token t, string str) {
            Abort(t, String.Format("Undefined identifier '{0}'", str));
        }

        public static void Abort(Token t, string str) {
            Console.Error.WriteLine("\nERROR: \"{0}\"", str);
            Console.Error.WriteLine("Symbol: {0}", t.Index);

            var lineI = GetLineNumber(t, t.Index);
            Console.Error.WriteLine("Line: {0}", lineI + 1);
            Console.Error.WriteLine("\"{0}\"", GetLine(t, lineI));
            Environment.Exit(1);
        }

        private static int GetLineNumber(Token t, int symbol) {
            var lineI = 0;
            var buffer = t.GetBuffer();
            for (var i = 0; i < symbol; i++) {
                if (buffer[i] == '\n') {
                    lineI++;
                    if (i + 1 < buffer.Length) {
                        if (buffer[i + 1] == '\r') i++;
                    }
                }

                if (buffer[i] == '\r') {
                    lineI++;
                    if (i + 1 < buffer.Length) {
                        if (buffer[i + 1] == '\n') i++;
                    }
                }
            }
            return lineI;
        }

        private static string GetLine(Token t, int linNum) {
            var lines = t.GetBuffer().Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            return lines[linNum];
        }
    }
}