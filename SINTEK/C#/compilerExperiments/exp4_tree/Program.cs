﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using exp4_tree.Syntax;

namespace exp4_tree
{
    class Program {
        static void Main(string[] args) {
            var lex = new Lexer();
            lex.SetBuffer(File.ReadAllText(@"D:\GIT\BeetleCreative\SINTEK\C#\compilerExperiments\exp4_tree\bin\Debug\script.txt").ToLower());
            //var synth = new Syntaxer(lex);
            //synth.Parse();
            var t = lex.GetNexToken();
            while (t != null)
            {
                Console.WriteLine(t.GetString());
                t = lex.GetNexToken();
            }

        }
    }
}
