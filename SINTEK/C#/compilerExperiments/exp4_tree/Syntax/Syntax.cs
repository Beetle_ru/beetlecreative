﻿using System.Collections.Generic;
using exp4_tree;
using exp4_tree.Tree;

namespace exp4_tree.Syntax {
    internal partial class Syntaxer {
        private Lexer _lex;
        private Token _t;
        private MainProgram _mProgram;

        public Syntaxer(Lexer lex) {
            _lex = lex;
            Next();
        }

        public void Parse() {
            Prog();
        }

        //**************************************************************************
        //                                                                        **
        //        program_declaration ::= ’PROGRAM’ program_type_name {           **
        //                io_var_declarations                                     **
        //                | other_var_declarations                                **
        //                | located_var_declarations                              **
        //                | program_access_decls                                  ** 
        //            }                                                           **
        //            function_block_body                                         **
        //            ’END_PROGRAM’                                               **
        //                                                                        **
        //**************************************************************************
        private void Prog() {
            Match("program");
            _mProgram = new MainProgram();
            _mProgram.Name = MatchType(TokenType.Ident).GetString();
            VarDeclarations();
            Match("end_program");
        }

        private void VarDeclarations() {
            while (Is("var") || Is("var_input") || Is("var_output")) {
                IoVarDeclarations(); // TODO
            }
        }

        //**************************************************************************
        //                                                                        **
        //          io_var_declarations ::= input_declarations |                  **
        //              output_declarations |                                     **
        //              input_output_declarations                                 **
        //                                                                        **
        //**************************************************************************
        private void IoVarDeclarations() {
            switch (Look().GetString()) {
                case "var_input":
                    InputDeclarations();
                    break;
                case "var_output":
                    OutputDeclarations();
                    break;
                case "var_in_out":
                    InputOutputDeclarations();
                    break;
            }
        }

        //**************************************************************************
        //                                                                        **
        //        input_declarations ::= ’VAR_INPUT’ [’RETAIN’ | ’NON_RETAIN’]    **
        //            input_declaration ’;’                                       **
        //            {input_declaration ’;’}                                     **
        //            ’END_VAR’                                                   **
        //                                                                        **
        //**************************************************************************
        private void InputDeclarations() {
            Match("var_input");

            switch (Look().GetString()) {
                case "retain":
                    // TODO
                    Next();
                    break;
                case "non_retain":
                    // TODO
                    break;
            }

            while (!Is("end_var")) {
                InputDeclaration();
                Semi();
            }
            Match("end_var");
        }

        //**************************************************************************
        //                                                                        **
        //          input_declaration ::= var_init_decl | edge_declaration        **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          var1_init_decl ::= var1_list ’:’ (                            **
        //                  simple_spec_init                                      **
        //                  | subrange_spec_init                                  **
        //                  | enumerated_spec_init                                **
        //              )                                                         **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          edge_declaration ::= var1_list ’:’                            **
        //              ’BOOL’ (’R_EDGE’ | ’F_EDGE’)                              **
        //                                                                        **
        //**************************************************************************
        private void InputDeclaration() {
            var vl = Var1List();
            Match(":");
            SimpleSpecInit(); // TODO
        }


        //**************************************************************************
        //                                                                        **
        //          var1_list ::= variable_name {’,’ variable_name}               **
        //                                                                        **
        //**************************************************************************
        private List<Var> Var1List() {
            var vl = new List<Var>();

            var variableName = MatchType(TokenType.Ident);
            vl.Add(new Var(){Name = variableName.GetString()});

            while (Is(",")) {
                variableName = MatchType(TokenType.Ident);
                vl.Add(new Var() { Name = variableName.GetString() });

            }

            return vl;
        }

        //**************************************************************************
        //                                                                        **
        //          simple_spec_init ::= simple_specification [’:=’ constant]     **
        //                                                                        **
        //**************************************************************************
        private void SimpleSpecInit() {
            var type = SimpleSpecification();
            Match(":=");
            // TODO CONSTANT
        }

        //**************************************************************************
        //                                                                        **
        //          simple_specification ::= elementary_type_name                 **
        //              | simple_type_name                                        **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          elementary_type_name ::= numeric_type_name                    **
        //              | date_type_name                                          **
        //              | bit_string_type_name                                    **
        //              | ’STRING’ | ’WSTRING’ | ’TIME’                           **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          numeric_type_name ::= integer_type_name | real_type_name      **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          integer_type_name ::= signed_integer_type_name                **
        //              | unsigned_integer_type_name                              **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          signed_integer_type_name ::= ’SINT’ | ’INT’                   **
        //              | ’DINT’ | ’LINT’                                         **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          unsigned_integer_type_name ::= ’USINT’                        **
        //              | ’UINT’ | ’UDINT’ | ’ULINT’                              **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          real_type_name ::= ’REAL’ | ’LREAL’                           **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          date_type_name ::= ’DATE’                                     **
        //            | ’TIME_OF_DAY’ | ’TOD’                                     **
        //            | ’DATE_AND_TIME’ | ’DT’                                    **
        //                                                                        **
        //------------------------------------------------------------------------**
        //                                                                        **
        //          bit_string_type_name ::= ’BOOL’ | ’BYTE’                      **
        //              | ’WORD’ | ’DWORD’ | ’LWORD’                              **
        //                                                                        **
        //**************************************************************************
        private VarTypeE SimpleSpecification() {
            var vt = (VarTypeE)MatchArr(Var.VarTypesStr, "simple type specification");
            return vt;
        }


        //**************************************************************************
        //                                                                        **
        //          output_declarations ::= ’VAR_OUTPUT’                          **
        //              [’RETAIN’ | ’NON_RETAIN’]                                 **
        //              var_init_decl ’;’                                         **
        //              {var_init_decl ’;’}                                       **
        //              ’END_VAR’                                                 **
        //                                                                        **
        //**************************************************************************
        private void OutputDeclarations() {
            
        }

        //**************************************************************************
        //                                                                        **
        //          input_output_declarations ::= ’VAR_IN_OUT’                    **
        //              var_declaration ’;’                                       **
        //              {var_declaration ’;’}                                     **
        //              ’END_VAR’                                                 **
        //                                                                        **
        //**************************************************************************
        private void InputOutputDeclarations() {
            
        }

    }
}