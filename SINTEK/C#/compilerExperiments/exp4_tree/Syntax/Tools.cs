﻿using System.Collections.Generic;
using exp4_tree;

namespace exp4_tree.Syntax {
    internal partial class Syntaxer {
        private Token Next() {
            _t = _lex.GetNexToken();
            return _t;
        }

        private Token Look() {
            return _t;
        }

        private Token Match(string str) {
            var t = Look();
            if (t.GetString() != str) Expected(str);
            Next();
            return t;
        }

        private int MatchArr(string[] arr, string expectedMessag) {
            var index = GetIndex(arr);
            if (index < 0) Expected(expectedMessag);

            Next();

            return index;
        }

        private int GetIndex(string[] arr) {
            var str = Look().GetString();
            for (var i = 0; i < arr.Length; i++) {
                if (arr[i] == str) return i;
            }
            return -1;
        }

        private Token MatchType(TokenType tt) {
            var t = Look();
            if (t.Type != tt) Expected(tt.ToString());
            Next();
            return t;
        }

        private bool QueryCurrnetToken(TokenType tt) {
            return Look().Type == tt;
        }

        private void Semi() {
            Match(";");
        }

        private bool Is(string str) {
            return Look().GetString() == str;
        }

        private void Expected(string str) {
            Logger.Expected(Look(), str);
        }

        private void Undefine(string str) {
            Logger.Undefine(Look(), str);
        }

        private void Abort(string str) {
            Logger.Abort(Look(), str);
        }
    }
}