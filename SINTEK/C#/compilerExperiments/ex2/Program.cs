﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ex2 {
    internal class Program {
        private static string _text;
        public static char Look;
        private static int _lookIndex;


        private static void Main(string[] args) {
            //Load(@"D:\GIT\BeetleCreative\SINTEK\C#\compilerExperiments\ex2\bin\Debug\script.txt");

            //GetName();
            var isRun = true;
            do {
                LoadStdIn();
                while (Look != '\0') {
                    var token = Scan();
                    if (token.ToLower() == "end_program") isRun = false;
                    Console.WriteLine("Token -> {0}", token);
                }
            } while (isRun);
        }

        private static void Load(string filename) {
            _text = File.ReadAllText(filename);
            _lookIndex = 0;
            if (!String.IsNullOrEmpty(_text)) Look = _text[_lookIndex];
        }

        private static void LoadStdIn() {
            _text = Console.ReadLine();
            _lookIndex = 0;
            if (!String.IsNullOrEmpty(_text)) Look = _text[_lookIndex];
        }

        private static void GetChar() {
            Look = _lookIndex + 1 < _text.Length ? _text[++_lookIndex] : '\0';
        }

        private static bool IsWhite(char c) {
            return (c == '\n') || (c == '\r') || (c == '\t') || (c == ' ');
        }


        private static bool IsDigit(char c) {
            // 216
            const string dic = "01234567890";
            return dic.Any(chr => c == chr);
        }

        private static bool IsAlpha(char c) {
            const string dic = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            return dic.Any(chr => c == chr);
        }

        private static bool IsAlNum(char c) {
            return IsAlpha(c) || IsDigit(c) || IsUnderscape(c);
        }

        private static bool IsUnderscape(char c) {
            return c == '_';
        }

        private static bool IsOp(char c) {
            const string dic = @"+-*/<>:=";
            return dic.Any(chr => c == chr);
        }

        private static string GetName() {
            var str = "";
            if (!(IsAlpha(Look) || IsUnderscape(Look))) Expected("Name");
            while (IsAlNum(Look)) {
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        private static string GetNum() {
            var str = "";
            if (!IsDigit(Look)) Expected("Integer");
            while (IsDigit(Look)) {
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        private static string GetOp() {
            var str = "";
            if (!IsOp(Look)) Expected("Operator");
            while (IsOp(Look)) {
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        private static void SkipWhite() {
            while (IsWhite(Look)) GetChar();
        }

        private static void SkipComma() {
            SkipWhite();
            if (Look == ',') {
                GetChar();
                SkipWhite();
            }
        }

        private static string Scan() {
            var str = "";

            if (IsAlpha(Look) || IsUnderscape(Look)) str = GetName();
            else if (IsDigit(Look)) str = GetNum();
            else if (IsOp(Look)) str = GetOp();
            else
            {
                str += Look;
                GetChar();
            }

            SkipComma();
            return str;
        }

        private static void Expected(string str) {
            str += " expected";
            Abort(str);
        }

        private static void Abort(string str) {
            Console.WriteLine("ERROR: \"{0}\"", str);
            Environment.Exit(1);
        }
    }
}