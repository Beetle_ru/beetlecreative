﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace compilerExperiments {
    internal class Program {
        private static List<string> _lex;
        private static Stack<string> _sStack;

        private static void Main(string[] args) {
            // 125
            Init();
            Load(@"D:\GIT\BeetleCreative\SINTEK\C#\compilerExperiments\compilerExperiments\bin\Debug\script.txt");
            Translate();
        }

        private static void Translate() {
            foreach (var item in _lex) {
                //if (IsNum(item)) {
                //Expression(item);
                Exp(item);
                //}
            }
        }

        private static void Exp(string str) {
            var operators = new Dictionary<string, int> {{"*", 3}, {"/", 3}, {"+", 2}, {"-", 2}, {"(", 1}, {")", 1}};
            //var bkt = new Dictionary<string, int> { { "(", 1 }, { ")", 1 } };


            if (IsNum(str) || IsIdent(str)) {
                Console.WriteLine("PUSH {0}", str);
            }
            else if (operators.ContainsKey(str)) {
                if (str == ";") {
                    while(_sStack.Any()) {
                        Console.WriteLine("op {0}", _sStack.Pop());
                    }
                }

                if (!_sStack.Any()) {
                    _sStack.Push(str);
                    return;
                }

                if (operators[_sStack.Peek()] < operators[str]) {
                    _sStack.Push(str);
                    return;
                }
                
                if (str == "(") {
                    _sStack.Push(str);
                    return;
                }

                if (str == ")") {
                    while ((_sStack.Any()) && (_sStack.Peek() != "(")) {
                        Console.WriteLine("op {0}", _sStack.Pop());
                    }
                    _sStack.Pop();
                    return;
                }

                while ((!_sStack.Any()) && (operators[_sStack.Peek()] >= operators[str])) {
                    Console.WriteLine("PUSH {0}", _sStack.Pop());
                }
            }
        }

        private static void Expression(string str) {
            Term(str);
            Console.WriteLine("MOVE D0 D1");

            switch (str) {
                case "+":
                    break;
                case "-":
                    break;
            }
        }

        //static void Add()

        private static void Term(string str) {
            Console.WriteLine("MOVE {0} D0", str);
        }

        private static bool IsIdent(string str) {
            for (int i = 0; i < str.Length; i++) {
                var c = str[i];
                if (IsLiteral(c) || c == '_') continue;
                else if (IsDigit(c) && i > 0) continue;
                else return false;
            }
            return true;
        }

        private static bool IsNum(string str) {
            int res;
            double resd;

            if (int.TryParse(str, out res)) return true;

            str = str.Replace('.', ',');
            if (double.TryParse(str, out resd)) return true;

            return false;
        }

        private static void Init() {
            _lex = new List<string>();
            _sStack = new Stack<string>();
        }


        private enum StateE {
            Undef,
            Rem,
            Num,
            Lit,
            Spc
        };

        private static void Load(string filename) {
            var text = File.ReadAllText(filename);
            text += "\n";
            var promStr = "";

            var prevState = new StateE();
            var newState = new StateE();

            foreach (var chr in text) {
                newState = GetNewState(chr);

                if (prevState != newState) {
                    switch (prevState) {
                        case StateE.Lit:
                        case StateE.Num:
                            if ((newState == StateE.Rem) || (newState == StateE.Spc)) {
                                if ((prevState == StateE.Num) && (chr == '.')) {
                                    // for float
                                    promStr += chr;
                                    continue;
                                }

                                _lex.Add(promStr);
                                promStr = "";
                                prevState = newState;
                            }
                            break;

                        case StateE.Spc:
                            if ((newState == StateE.Rem) || (newState == StateE.Num) || (newState == StateE.Lit)) {
                                _lex.Add(promStr);
                                promStr = "";
                                prevState = newState;
                            }
                            break;

                        case StateE.Rem:
                        case StateE.Undef:
                            prevState = newState;
                            break;
                    }
                }

                if (newState != StateE.Rem) {
                    promStr += chr;
                }
            }
        }

        private static bool IsRem(char chr) {
            return (chr == '\n') || (chr == '\a') || (chr == '\t') || (chr == ' ');
        }

        private static bool IsDigit(char chr) {
            var dic = "01234567890".ToCharArray();
            return dic.Any(c => c == chr);
        }

        private static bool IsLiteral(char chr) {
            var dic = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".ToCharArray();
            return dic.Any(c => c == chr);
        }

        private static bool IsSpec(char chr) {
            var dic = "~!@#$%^&*()+-={}:;\"',./?\\|".ToCharArray();
            return dic.Any(c => c == chr);
        }

        private static StateE GetNewState(char chr) {
            if (IsRem(chr)) return StateE.Rem;
            else if (IsDigit(chr)) return StateE.Num;
            else if (IsLiteral(chr)) return StateE.Lit;
            else if (IsSpec(chr)) return StateE.Spc;
            else return StateE.Undef;
        }
    }
}