﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO; // 577

namespace ex3 {
    internal class Program {
        private static void Main(string[] args) {
            var lex = new Lexer(File.ReadAllText(@"D:\GIT\BeetleCreative\SINTEK\C#\compilerExperiments\ex3\bin\Debug\script.txt").ToLower());
            var syntax = new Syntax(lex);

            syntax.Start();

            //while (lex.Look != '\0') { // 340
            //    var lexem = lex.Scan();
            //    Console.WriteLine("-> {0}", lexem);
            //}
        }
    }
}