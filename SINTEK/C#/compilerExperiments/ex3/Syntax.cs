﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; //514

namespace ex3 {
    internal class Syntax {
        private readonly Lexer _lex;
        public string Token;
        private int _lCount;

        public void Start() {
            Prog();
        }

        private void Prog() {
            const string name = "main";
            var st = new SymbolsTable();
            if (st.InTable(name)) Abort("Second definition of program");
            else st.Add(name, SymbolClass.Program);
            MatchString("program");

            Header();
            TopDecl(st);
            Prolog();
            //FunctionProlog(st.GetIdent(name));
            Block(st);
            MatchString("end_program");
            Epilog();
            //FunctionEpilog(st.GetIdent(name));
        }

        private void DoFunction(SymbolsTable rootSt) {
            MatchString("function");
            var name = GetCName();

            if (rootSt.InTable(name)) Abort(String.Format("Duplicate function name '{0}'", name));
            else rootSt.Add(name, SymbolClass.Function);

            var st = new SymbolsTable(rootSt);

            FunctionProlog(st.GetIdent(name));
            //TopDecl(st);
            FunctDecl(st);
            Block(st);
            MatchString("end_function");
            FunctionEpilog(st.GetIdent(name));
        }

        private void FunctionProlog(string s) {
            EmitLn(String.Format("JMP END_FUNCT_{0}", s));
            PostLabel(s);
            EmitLn("PUSH EBP");
            EmitLn("MOV EBP, ESP");
        }

        private void FunctionEpilog(string s) {
            //EmitLn("DC WARMST");
            //EmitLn(String.Format("END {0}", s));
            EmitLn("MOV ESP, EBP");
            EmitLn("POP EBP");
            EmitLn("RET");
            PostLabel(String.Format("END_FUNCT_{0}", s));
        }


        private void Block(SymbolsTable st) {
            while (!(Is("end_program") || Is("end_if") || Is("end_while") || Is("else") || Is("end_function"))) {
                switch (Token) {
                    case "if":
                        DoIf(st);
                        break;

                    case "while":
                        DoWhile(st);
                        break;

                    case "read":
                        DoRead(st);
                        break;

                    case "write":
                        DoWrite(st);
                        break;

                    case "function":
                        DoFunction(st);
                        break;

                    default:
                        AssignOrFuct(st);
                        Semi();
                        break;
                }
            }
        }

        private void Decl(SymbolsTable st) {
            MatchString("var");
            while (!Is("end_var")) {
                Alloc(st, GetCName());
                while (Is(",")) {
                    GetNextToken();
                    Alloc(st, GetCName());
                }
                Semi();
            }
            MatchString("end_var");
        }

        private void LocalDecl(SymbolsTable st) {
            MatchString("var");
            var shift = 0;
            while (!Is("end_var")) {
                shift = LocalAlloc(st, GetCName(), shift);
                while (Is(",")) {
                    GetNextToken();
                    shift = LocalAlloc(st, GetCName(), shift);
                }
                Semi();
            }
            MatchString("end_var");
        }

        private void VarInputDecl(SymbolsTable st) {
            MatchString("var_input");
            var shift = 8;
            while (!Is("end_var")) {
                shift = ParamLoad(st, GetCName(), shift);
                while (Is(",")) {
                    GetNextToken();
                    shift = ParamLoad(st, GetCName(), shift);
                }
                Semi();
            }
            MatchString("end_var");
        }


        private void TopDecl(SymbolsTable st) {
            while (Is("var") || Is("var_input") || Is("var_output")) {
                switch (Token) {
                    case "var":
                        DataSection();
                        Decl(st);
                        break;
                    default:
                        Abort(String.Format("Unrecognized Keyword '{0}'", Token));
                        break;
                }
            }
        }

        private void FunctDecl(SymbolsTable st) {
            while (Is("var") || Is("var_input") || Is("var_output")) {
                switch (Token) {
                    case "var":
                        LocalDecl(st);
                        break;
                    case "var_input":
                        VarInputDecl(st);
                        break;
                    default:
                        Abort(String.Format("Unrecognized Keyword '{0}'", Token));
                        break;
                }
            }
        }

        private void Alloc(SymbolsTable st, string varName) {
            if (st.InTable(varName)) Abort(String.Format("Duplicate variable name '{0}'", varName));
            else st.Add(varName, SymbolClass.Variable);

            Console.Write("{0}\tDD ", st.GetIdent(varName));

            if (Is("=")) {
                Console.WriteLine(GetNum());
                GetNextToken();
            }
            else Console.WriteLine(0);
        }

        private int LocalAlloc(SymbolsTable st, string varName, int prevShift) {
            if (st.InTable(varName)) Abort(String.Format("Duplicate variable name '{0}'", varName));
            else st.Add(varName, SymbolClass.Variable, true);

            var s = st.GetSymbolByName(varName);

            prevShift -= s.Len;
            s.Shift = prevShift;
         

            st.SetSymbol(s);
            
            EmitLn(String.Format("SUB ESP, {0}", s.Len));

            if (Is("=")) {
                EmitLn(String.Format("MOV EAX, {0}", GetNum()));
                GetNextToken();
            }
            else EmitLn(String.Format("MOV EAX, 0"));

            EmitLn(String.Format("MOV [EBP + {0}], EAX", s.Shift));

            return prevShift;
        }

        private int ParamLoad(SymbolsTable st, string varName, int prevShift) {
            if (st.InTable(varName)) Abort(String.Format("Duplicate variable name '{0}'", varName));
            else st.Add(varName, SymbolClass.Variable, true);

            var s = st.GetSymbolByName(varName);

            s.Shift = prevShift;
            prevShift += s.Len;

            st.SetSymbol(s);

            return prevShift;
        }


        private void Header() {
            Console.WriteLine("WARMST\tEQU $A01E");
            //EmitLn("LIB RUNTIMELIB"); // load runtime library(to future)
            Console.WriteLine(".686");
            Console.WriteLine(".MODEL FLAT, C");
            Console.WriteLine(".STACK");
        }

        private void Prolog() {
            //PostLabel("MAIN");
            Console.WriteLine(".CODE");
            Console.WriteLine("prog PROC");
        }

        private void Epilog() {
            //EmitLn("DC WARMST");
            //EmitLn("END MAIN");
            Console.WriteLine("prog ENDP");
            Console.WriteLine("END");
        }

        #region operators

        private void DoIf(SymbolsTable st) {
            var l1 = "";
            var l2 = "";

            MatchString("if");
            BoolExpression(st);
            l1 = NewLabel();
            l2 = l1;
            BranchFalse(l1);
            Block(st);
            if (Is("else")) {
                MatchString("else");
                l2 = NewLabel();
                Branch(l2);
                PostLabel(l1);
                Block(st);
            }
            PostLabel(l2);
            MatchString("end_if");
        }

        private void DoWhile(SymbolsTable st) {
            // 385
            var l1 = "";
            var l2 = "";

            MatchString("while");
            l1 = NewLabel();
            l2 = NewLabel();
            PostLabel(l1);
            BoolExpression(st);
            BranchFalse(l2);
            Block(st);
            MatchString("end_while");
            Branch(l1);
            PostLabel(l2);
        }

        private void DoRead(SymbolsTable st) {
            MatchString("read");
            MatchString("(");
            ReadVar(st, st.GetIdent(GetCName()));

            while (Is(",")) {
                MatchString(",");
                ReadVar(st, st.GetIdent(GetCName()));
            }

            MatchString(")");
        }

        private void DoWrite(SymbolsTable st) {
            MatchString("write");
            MatchString("(");
            Expression(st);
            WriteVar();
            while (Is(",")) {
                MatchString(",");
                Expression(st);
                WriteVar();
            }
            MatchString(")");
        }

        #endregion

        //376

        #region BoolAlgebra 

        private bool IsOrop(string str) {
            return str == "or";
        }

        private bool IsRelop(string str) {
            string[] dic = {"=", "<>", "<", ">", ">=", "<="};
            return dic.Any(s => str == s);
        }

        private void Equals(SymbolsTable st) {
            MatchString("=");
            Expression(st);
            PopCompare();
            SetEqual();
        }

        private void NotEquals(SymbolsTable st) {
            MatchString("<>");
            Expression(st);
            PopCompare();
            SetNEqual();
        }

        private void Less(SymbolsTable st) {
            MatchString("<");
            Expression(st);
            PopCompare();
            SetLess();
        }

        private void Greater(SymbolsTable st) {
            MatchString(">");
            Expression(st);
            PopCompare();
            SetGreater();
        }

        private void LessOrEqual(SymbolsTable st) {
            MatchString("<=");
            Expression(st);
            PopCompare();
            SetLessOrEqual();
        }

        private void GreaterOrEqual(SymbolsTable st) {
            MatchString(">=");
            Expression(st);
            PopCompare();
            SetGreaterOrEqual();
        }

        private void Relation(SymbolsTable st) {
            Expression(st);
            if (IsRelop(Token)) {
                Push();

                switch (Token) {
                    case "=":
                        Equals(st);
                        break;

                    case "<>":
                        NotEquals(st);
                        break;

                    case "<":
                        Less(st);
                        break;

                    case ">":
                        Greater(st);
                        break;

                    case ">=":
                        GreaterOrEqual(st);
                        break;

                    case "<=":
                        LessOrEqual(st);
                        break;
                }
            }
        }

        private void NotFactor(SymbolsTable st) {
            if (Is("not")) {
                MatchString("not");
                Relation(st);
                Notlt();
            }
            else {
                Relation(st);
            }
        }

        private void BoolTerm(SymbolsTable st) {
            NotFactor(st);
            while (Is("and")) {
                Push();
                MatchString("and");
                NotFactor(st);
                PopAnd();
            }
        }

        private void BoolOr(SymbolsTable st) {
            MatchString("or");
            BoolTerm(st);
            PopOr();
        }

        private void BoolXor(SymbolsTable st) {
            MatchString("xor");
            BoolTerm(st);
            PopXor();
        }

        private void BoolExpression(SymbolsTable st) {
            BoolTerm(st);
            while (IsOrop(Token)) {
                Push();
                switch (Token) {
                    case "or":
                        BoolOr(st);
                        break;
                    case "xor":
                        BoolXor(st);
                        break;
                }
            }
        }

        #endregion

        #region Assigment

        private void Factor(SymbolsTable st) {
            if (Is("(")) {
                MatchString("(");
                BoolExpression(st);
                MatchString(")");
            }
            else if (_lex.IsName(Token)) {
                LoadVar(st, _lex.MatchName(Token));
                GetNextToken();
            }
            else {
                LoadConst(_lex.MatchNum(Token));
                GetNextToken();
            }
        }

        private void NegFactor(SymbolsTable st) {
            MatchString("-");
            if (_lex.IsNum(Token)) {
                LoadConst("-" + _lex.MatchNum(Token));
                GetNextToken();
            }
            else {
                Factor(st);
                Negate();
            }
        }

        private void FirstFactor(SymbolsTable st) {
            switch (Token) {
                case "+":
                    MatchString("+");
                    Factor(st);
                    break;

                case "-":
                    NegFactor(st);
                    break;

                default:
                    Factor(st);
                    break;
            }
        }

        private void Multiply(SymbolsTable st) {
            MatchString("*");
            Factor(st);
            PopMul();
        }

        private void Devide(SymbolsTable st) {
            MatchString("/");
            Factor(st);
            PopDiv();
        }

        private void Term1(SymbolsTable st) {
            while (_lex.IsMulOp(Token)) {
                Push();
                switch (Token) {
                    case "*":
                        Multiply(st);
                        break;
                    case "/":
                        Devide(st);
                        break;
                }
            }
        }

        private void Term(SymbolsTable st) {
            Factor(st);
            Term1(st);
        }

        private void FirstTerm(SymbolsTable st) {
            FirstFactor(st);
            Term1(st);
        }

        private void Add(SymbolsTable st) {
            MatchString("+");
            FirstTerm(st);
            PopAdd();
        }

        private void Substract(SymbolsTable st) {
            MatchString("-");
            FirstTerm(st);
            PopSub();
        }

        private void Expression(SymbolsTable st) {
            FirstTerm(st);

            while (_lex.IsAddOp(Token)) {
                Push();
                switch (Token) {
                    case "+":
                        Add(st);
                        break;
                    case "-":
                        Substract(st);
                        break;
                }
            }
        }

        private void Assigment(SymbolsTable st) {
            var name = _lex.MatchName(Token);
            GetNextToken();
            MatchString(":=");
            BoolExpression(st);
            Store(st, name);
        }

        private void DoCallFunction(SymbolsTable st, string name) {
            GetNextToken();
            var allocateBytes =FormalList(st);
            CallFunct(st.GetIdent(name));
            RemoveParams(allocateBytes);
        }

        private int FormalList(SymbolsTable st) {
            var bytesAlloc = 0;

            if (Is("(")) {
                MatchString("(");

                if (!Is(")")) {
                    bytesAlloc += Param(st);

                    while (Is(",")) {
                        GetNextToken();
                        bytesAlloc += Param(st);
                    }
                }

                MatchString(")");
            }

            return bytesAlloc;
        }

        private int Param(SymbolsTable st) {
            var name = GetCName();
            //ToEAX(st.GetIdent(name));
            LoadVar(st, name);
            Push();
            return st.GetSymbolByName(name).Len;
        }

        private void AssignOrFuct(SymbolsTable st) {
            foreach (var symbol in st.GetStList()) {
                if (symbol.Name == Token) {
                    switch (symbol.SClass) {
                        case SymbolClass.Variable:
                            Assigment(st);
                            return;
                            break;
                        case SymbolClass.Function:
                            DoCallFunction(st, symbol.Name);
                            return;
                            break;
                    }
                }
            }
            Undefine(Token);
        }

        #endregion

        #region Tools

        public Syntax(Lexer lex) {
            _lex = lex;
            GetNextToken();
        }

        private string GetNextToken() {
            Token = _lex.Scan();
            return Token;
        }

        private string GetName() {
            Token = _lex.GetName();
            return Token;
        }

        private string GetNum() {
            Token = _lex.GetNum();
            return Token;
        }

        private string GetOp() {
            Token = _lex.GetOp();
            return Token;
        }

        private void MatchString(string x) {
            if (Token == x) GetNextToken();
            else Expected(x);
        }

        private void Semi() {
            MatchString(";");
        }

        private string GetCName() {
            var n = _lex.MatchName(Token);
            GetNextToken();
            return n;
        }

        private string GetCNum() {
            var n = _lex.MatchNum(Token);
            GetNextToken();
            return n;
        }

        private void Test(string x) {
            if (Token != x) Expected(x);
        }

        private bool Is(string x) {
            return x == Token;
        }

        private string NewLabel() {
            return String.Format("L{0}", _lCount++);
        }

        private void PostLabel(string lbl) {
            Console.WriteLine("{0}:", lbl);
        }

        private void Emit(string str) {
            Console.Write("\t{0}", str);
        }

        private void EmitLn(string str) {
            Emit(str);
            Console.WriteLine("");
        }

        private void Expected(string str) {
            _lex.SetPosPrevWord();
            _lex.Expected(str);
        }

        private void Abort(string str) {
            _lex.SetPosPrevWord();
            _lex.Abort(str);
        }

        private void Undefine(string str) {
            Abort(String.Format("Undefined identifier '{0}'", str));
        }

        #endregion

        #region CodeGenerators

        private void DataSection() {
            Console.WriteLine(".data");
        }

        private void Clear() {
            EmitLn("XOR EAX, EAX");
        }

        private void Negate() {
            EmitLn("NEG EAX");
        }

        private void LoadConst(string val) {
            //Emit("MOV #");
            //Console.WriteLine("{0} D0", val);
            EmitLn(String.Format("MOV EAX, {0}", val));
        }

        private void LoadVar(SymbolsTable st, string name) {
            if (!st.InTable(name)) Undefine(name);

            //EmitLn(String.Format("MOVE {0}(PC),D0", name));
            var s = st.GetSymbolByName(name);

            if (s.IsLocal) {
                EmitLn(String.Format("MOV EAX, [EBP + {0}]", s.Shift));
            }
            else {
                EmitLn(String.Format("LEA EBX, {0}", s.Ident));
                EmitLn(String.Format("MOV EAX, [EBX]"));
            }
        }

        private void Push() {
            //EmitLn("MOVE D0,-(SP)");
            EmitLn("PUSH EAX");
        }

        private void PopAdd() {
            //EmitLn("ADD (SP)+,D0");
            EmitLn("POP EDX");
            EmitLn("ADD EAX, EDX");
        }

        private void PopSub() {
            //EmitLn("SUB (SP)+,D0");
            //EmitLn("NEG D0");
            EmitLn("POP EDX");
            EmitLn("SUB EDX, EAX");
            EmitLn("MOV EAX, EDX");
        }

        private void PopMul() {
            //EmitLn("MULS (SP)+,D0");
            EmitLn("POP EDX");
            EmitLn("IMUL EAX, EDX");
        }

        private void PopDiv() {
            //EmitLn("MOVE (SP)+,D7");
            //EmitLn("EXT.L D7");
            //EmitLn("DIVS D0,D7");
            //EmitLn("MOVE D7,D0");
            EmitLn("POP EDX");
            EmitLn("MOV EBX, EAX");
            EmitLn("MOV EAX, EDX");
            EmitLn("XOR EDX, EDX");
            EmitLn("IDIV EBX");
        }

        private void Store(SymbolsTable st, string name) {
            if (!st.InTable(name)) Undefine(name);

            var s = st.GetSymbolByName(name);

            if (s.IsLocal) {
                EmitLn(String.Format("MOV [EBP + {0}], EAX", s.Shift));
            }
            else {
                EmitLn(String.Format("LEA EBX, {0}", s.Ident));
                EmitLn("MOV [EBX], EAX");
            }
            //EmitLn(String.Format("LEA {0}(PC),A0", st.GetIdent(name)));
            //EmitLn("MOVE D0,(A0)");
        }

        //////////   bool

        private void Notlt() {
            EmitLn("NOT EAX");
        }

        private void PopAnd() {
            //EmitLn("AND (SP)+, D0");
            EmitLn("POP EDX");
            EmitLn("AND EAX, EDX");
        }

        private void PopOr() {
            //EmitLn("OR (SP)+,D0");
            EmitLn("POP EDX");
            EmitLn("OR EAX, EDX");
        }

        private void PopXor() {
            //EmitLn("EOR (SP)+,D0");
            EmitLn("POP EDX");
            EmitLn("XOR EAX, EDX");
        }

        private void PopCompare() {
            //EmitLn("CMP (SP)+, D0");
            EmitLn("POP EDX");
            //EmitLn("CMP EAX, EDX");
            EmitLn("CMP EDX, EAX");
        }

        private void SetEqual() {
            //EmitLn("SEQ EAX");
            //EmitLn("EXT EAX");
            EmitLn("MOV EAX, -1");
            EmitLn("JE @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        private void SetNEqual() {
            //EmitLn("SNE D0");
            //EmitLn("EXT D0");
            EmitLn("MOV EAX, -1");
            EmitLn("JNE @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        private void SetGreater() {
            //EmitLn("SLT D0");
            //EmitLn("EXT D0");
            EmitLn("MOV EAX, -1");
            EmitLn("JG @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        private void SetLess() {
            //EmitLn("SGT D0");
            //EmitLn("EXT D0");
            EmitLn("MOV EAX, -1");
            EmitLn("JL @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        private void SetLessOrEqual() {
            //EmitLn("SGE D0");
            //EmitLn("EXT D0");
            EmitLn("MOV EAX, -1");
            EmitLn("JGE @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        private void SetGreaterOrEqual() {
            //EmitLn("SLE D0");
            //EmitLn("SLE D0");
            EmitLn("MOV EAX, -1");
            EmitLn("JLE @F");
            EmitLn("XOR EAX, EAX");
            Console.WriteLine("@@:");
        }

        // if while, etc

        private void Branch(string l) {
            EmitLn(String.Format("JMP {0}", l));
        }

        private void BranchFalse(string l) {
            //EmitLn("TST D0");
            //EmitLn(String.Format("BEQ {0}", l));
            EmitLn("MOV ECX, EAX");
            EmitLn(String.Format("JECXZ {0}", l));
        }

        // IO

        private void ReadVar(SymbolsTable st, string valueName) {
            EmitLn("BSR READ");
            Store(st, valueName);
        }

        private void WriteVar() {
            EmitLn("BSR WRITE");
        }

        private void CallFunct(string s) {
            EmitLn(String.Format("CALL {0}", s));
        }

        private void RemoveParams(int bytes) {
            EmitLn(String.Format("ADD ESP, {0}", bytes));
        }

        private void ToEAX(string val) {
            EmitLn(String.Format("MOV EAX {0}", val));
        }

        #endregion
    }
}