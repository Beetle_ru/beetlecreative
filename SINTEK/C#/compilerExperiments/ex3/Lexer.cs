﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ex3 {
    internal class Lexer {
        private readonly string _text;
        public char Look;
        private int _lookIndex;
        private int _prevWordIndex;

        public Lexer(string src) {
            _text = src;
            _lookIndex = 0;
            if (!String.IsNullOrEmpty(_text)) Look = _text[_lookIndex];
        }

        public string Scan() {
            SkipWhite();
            var str = "";

            if (IsAlpha(Look) || IsUnderscape(Look)) str = GetName();
            else if (IsDigit(Look)) str = GetNum();
            else if (IsOp(Look)) str = GetOp();
            else {
                str += Look;
                GetChar();
            }

            //SkipComma();
            SkipWhite();
            return str;
        }

        private void GetChar() {
            SkipComment();
            Look = _lookIndex + 1 < _text.Length ? _text[++_lookIndex] : '\0';
        }

        public bool IsWhite(char c) {
            return (c == '\n') || (c == '\r') || (c == '\t') || (c == ' ');
        }

        public bool IsDigit(char c) {
            // 216
            const string dic = "01234567890";
            return dic.Any(chr => c == chr);
        }

        public bool IsAlpha(char c) {
            const string dic = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            return dic.Any(chr => c == chr);
        }

        public bool IsAlNumUnd(char c) {
            return IsAlpha(c) || IsDigit(c) || IsUnderscape(c);
        }

        public bool IsUnderscape(char c) {
            return c == '_';
        }

        public bool IsOp(char c) {
            const string dic = @"+-*/<>:=";
            return dic.Any(chr => c == chr);
        }

        public bool IsMulOp(string str) {
            if (str.Length != 1) return false;
            const string dic = @"*/";
            return dic.Any(chr => str[0] == chr);
        }

        public bool IsAddOp(string str) {
            if (str.Length != 1) return false;
            const string dic = @"+-";
            return dic.Any(chr => str[0] == chr);
        }

        public bool IsName(string name) {
            if (name.Length < 1) return false;

            if (!(IsAlpha(name[0]) || IsUnderscape(name[0]))) return false;

            foreach (var chr in name) {
                if (!IsAlNumUnd(chr)) return false;
            }
            return true;
        }

        public string MatchName(string name) {
            if (!IsName(name)) Expected("identifier");
            return name;
        }

        public string GetName() {
            _prevWordIndex = _lookIndex;
            var str = "";
            if (!(IsAlpha(Look) || IsUnderscape(Look))) Expected("Name");
            while (IsAlNumUnd(Look)) {
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        public bool IsNum(string val) {
            for (int i = 0; i < val.Length; i++) {
                var chr = val[i];
                var isDigit = IsDigit(chr);
                var fp = ((i == 0) && (chr != '+'));
                var fm = ((i == 0) && (chr != '-'));
                if (!(isDigit || fp || fm)) return false;
            }

            return true;
        }

        public string MatchNum(string val) {
            if (!IsNum(val)) Expected("Number");
            return val;
        }

        public string GetNum() {
            _prevWordIndex = _lookIndex;
            var str = "";
            var isDotBeen = false;

            if ((Look == '+') || (Look == '-')) {
                str += Look;
                GetChar();
            }

            if (!IsDigit(Look)) Expected("Integer");

            while (IsDigit(Look) || (Look == '.')) {
                if (Look == '.') {
                    if (isDotBeen) Abort("Detected second dot symbol");
                    isDotBeen = true;
                }
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        public string GetOp() {
            _prevWordIndex = _lookIndex;
            var str = "";
            if (!IsOp(Look)) Expected("Operator");
            while (IsOp(Look)) {
                str += Look;
                GetChar();
            }
            SkipWhite();
            return str;
        }

        public void SkipWhite() {
            while (IsWhite(Look)) GetChar();
        }

        public void SkipComma() {
            SkipWhite();
            if (Look == ',') {
                GetChar();
                SkipWhite();
            }
        }

        public void SkipComment() {
            if ((_lookIndex + 2) < _text.Length) {
                if (_text[_lookIndex + 1] == '(') {
                    if (_text[_lookIndex + 2] == '*') {
                        _lookIndex += 2;

                        while (true) {
                            if ((_lookIndex + 2) >= _text.Length) {
                                Expected("*)");
                            }

                            SkipComment();

                            if (_text[_lookIndex++] == '*') {
                                if (_text[_lookIndex++] == ')') {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SetPosPrevWord() {
            _lookIndex = _prevWordIndex;
        }

        public void Expected(string str) {
            str += " expected";
            Abort(str);
        }

        public void Abort(string str) {
            Console.Error.WriteLine("\nERROR: \"{0}\"", str);
            Console.Error.WriteLine("Symbol: {0}", _lookIndex);

            var lineI = GetLineNumber(_lookIndex);
            Console.Error.WriteLine("Line: {0}", lineI);
            Console.Error.WriteLine("\"{0}\"", GetLine(lineI));
            Environment.Exit(1);
        }


        private int GetLineNumber(int symbol) {
            var lineI = 0;
            for (var i = 0; i < symbol; i++) {
                if (_text[i] == '\n') {
                    lineI++;
                    if (i + 1 < _text.Length) {
                        if (_text[i + 1] == '\r') i++;
                    }
                }

                if (_text[i] == '\r') {
                    lineI++;
                    if (i + 1 < _text.Length) {
                        if (_text[i + 1] == '\n') i++;
                    }
                }
            }
            return lineI;
        }

        private string GetLine(int linNum) {
            var lines = _text.Split(new string[] {"\r\n", "\n"}, StringSplitOptions.None);

            return lines[linNum];
        }
    }
}