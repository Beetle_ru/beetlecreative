﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ex3
{
    internal enum SymbolClass
    {
        Undef,
        Variable,
        Function,
        Program
    }

    internal struct Symbol {
        public string Name;
        public string Ident;
        public SymbolClass SClass;
        public bool IsLocal;
        public int Len;
        public int Shift;
    }

    class SymbolsTable {
        private List<Symbol> _st;
        private static int _lCount;

        public SymbolsTable() {
            _st = new List<Symbol>();
        }

        public SymbolsTable(SymbolsTable rootSt) {
            _st = new List<Symbol>();

            foreach (var symbol in rootSt.GetStList()) {
                _st.Add(symbol);
            }
        }

        public bool InTable(string name) {
            foreach (var symbol in _st) {
                if (symbol.Name == name) return true;
            }
            return false;
        }

        public void Add(string name, SymbolClass sClass, bool isLocal = false) {
            _st.Add(new Symbol() { Name = name, SClass = sClass, Ident = GenerateIdent(), Len = 4, IsLocal = isLocal });
        }

        private string GenerateIdent() {
            return String.Format("S{0}", _lCount++);
        }

        public string GetIdent(string name) {
            foreach (var symbol in _st) {
                if (symbol.Name == name) return symbol.Ident;
            }
            return "ERROR: SYMBOL NOT FOUND -> " + name;
        }

        public Symbol GetSymbolByName(string name) {
            foreach (var symbol in _st) {
                if (name == symbol.Name) return symbol;
            }
            return new Symbol();
        }

        public void SetSymbol(Symbol s) {
            for (int i = 0; i < _st.Count; i++) {
                if (_st[i].Ident == s.Ident) {
                    _st[i] = s;
                }
            }
        }

        public Symbol GetPrevSymbol(Symbol cs) {
            for (var i = 0; i < _st.Count; i++ ) {
                if ((_st[i].Ident == cs.Ident) && (i > 0)) {
                    return _st[i - 1];
                }
            }
                return new Symbol();
        }

        public List<Symbol> GetStList() {
            return _st;
        }
        
    }
}
