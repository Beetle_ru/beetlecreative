﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace websocketTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new TcpListener(IPAddress.Loopback, 8181);
            listener.Start();
            while (true) {
                var client = listener.AcceptTcpClient();
                Console.WriteLine("Connected!");

                var stream = client.GetStream();

                var bytes = new Byte[256];
                int i;

                if (client.Connected) {
                    while (stream.CanRead){
                        try {
                            i = stream.Read(bytes, 0, bytes.Length);
                            if (i == 0) break;
                            // Translate data bytes to a ASCII string.
                            var data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                            Console.WriteLine("Received: {0}", data);

                            // Process the data sent by the client.
                            //data = data.ToUpper();

                            //byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                            // Send back a response.
                            //stream.Write(msg, 0, msg.Length);
                            //Console.WriteLine("Sent: {0}", data);
                        }
                        catch (Exception) {
                            break;
                        }
                    }
                }

                client.Close();
            }
            
            //using (var client = listener.AcceptTcpClient())
            //using (var stream = client.GetStream())
            //using (var reader = new StreamReader(stream))
            //using (var writer = new StreamWriter(stream))
            //{
            //    writer.WriteLine("HTTP/1.1 101 Web Socket Protocol Handshake");
            //    writer.WriteLine("Upgrade: WebSocket");
            //    writer.WriteLine("Connection: Upgrade");
            //    writer.WriteLine("WebSocket-Origin: http://localhost:8080");
            //    writer.WriteLine("WebSocket-Location: ws://localhost:8181/websession");
            //    writer.WriteLine("");
            //}
            //listener.Stop();
        }
    }
}
