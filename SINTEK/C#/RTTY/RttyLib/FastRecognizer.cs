﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib
{
    public class FastRecognizer {
        private float[] _pattern;
        private float[] _buffer;
        private int _bufferIndex;
        public FastRecognizer(float[] pattern) {
            _pattern = pattern;
            _buffer = new float[pattern.Count()];
            _bufferIndex = 0;
        }

        public void Next(float newVal) {
            var oldVal = _buffer[_bufferIndex];
            _buffer[_bufferIndex] = newVal;

            _bufferIndex++;
            if (_bufferIndex >= _buffer.Count()) _bufferIndex = 0;
        }
    }
}
