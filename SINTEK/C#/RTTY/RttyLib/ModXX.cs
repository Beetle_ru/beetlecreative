﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace RttyLib {
    public class ModXX : WaveProvider32 {
        private int _impulseIndex;
        private readonly List<byte> _data;
        private readonly List<byte> _synchroImpulse;
        private int _dataIndex;
        private bool _isTransfered;
        private int _needOffset;
        private int _hightLen = 50;
        private int _lowLen = 25;
        private bool _isBlockBegin;

        private float[] _lSample;
        private float[] _hSample;
        private int _sampleIndex;


        public override int Read(float[] buffer, int offset, int sampleCount) {
            if (_isTransfered) {
                buffer[0] = 0;
                return 1;
            }
            if (!_data.Any()) {
                buffer[0] = 0;
                return 1;
            }

            var r = new Random();

            for (int n = 0; n < sampleCount; n++) {
                var val = 0.0f;

                if (_dataIndex <= GetBitCount()) {
                    if (_dataIndex >= GetBitCount()){
                        _dataIndex++;
                        buffer[n + offset] = 1;
                        continue;
                    }

                    //var bit = GetBit(_dataIndex);
                    var currentSample = GetBit(_dataIndex) == 0 ? _lSample : _hSample;
                    val = currentSample[_sampleIndex++];

                    if (_sampleIndex >= currentSample.Count()) {
                        _sampleIndex = 0;
                        _dataIndex++;
                    }
                }
                else {
                    //if (_isBlockBegin) {
                    //    _impulseIndex = 0;
                    //    _dataIndex = 0;
                    //    _isBlockBegin = false;
                    //}
                    //else {
                    //    _isTransfered = true;
                    //    return n;
                    //}
                    _isTransfered = true;
                    return n;
                }

                buffer[n + offset] = val;// +(float)(r.NextDouble() - 0.5) * 1.7f;
            }
            return sampleCount;
        }


        public ModXX(int bitrate) {
            _data = new List<byte>();

            _synchroImpulse = new List<byte>();
            for (int i = 0; i < 31; i++) {
            //for (int i = 0; i < 150; i++) {
                _synchroImpulse.Add(0);
            }
            _synchroImpulse.Add(1); // 0000000000000000000000010000000 -> data

            SetBitRate(bitrate);
        }

        private int GetBit(int index) {
            int bitI;
            var byteI = Math.DivRem(index, 8, out bitI);
            //if (_isBlockBegin) return ((_synchroImpulse[byteI] >> bitI) & 1);
            //else return ((_data[byteI] >> bitI) & 1);
            return ((_data[byteI] >> bitI) & 1);
        }

        public void SetBitRate(int bitrate) {
            var sampleRate = WaveFormat.SampleRate;
            _lowLen = (int) Math.Round((1.0/bitrate)*sampleRate);
            _hightLen = (int) Math.Round(_lowLen*0.5);

            _lSample = SampleGenerator.SinGen(_lowLen, 1);
            _hSample = SampleGenerator.SinGen(_hightLen, 1);
            //_lSample = SampleGenerator.ImpsGen(_lowLen, 1);
            //_hSample = SampleGenerator.ImpsGen(_hightLen, 1);
        }

        private int GetBitCount() {
            //if (_isBlockBegin) return _synchroImpulse.Count*8;
            //else return _data.Count*8;
            return _data.Count * 8;
        }

        public void SetData(List<byte> bList) {
            _data.Clear();

            foreach (var b in _synchroImpulse) { // add synchro bytes
                _data.Add(b);
            }

            foreach (var b in bList) {
                _data.Add(b);
            }

            _dataIndex = 0;
            _needOffset = 0;
            _sampleIndex = 0;
            _isTransfered = false;
            _isBlockBegin = true;
        }



    }
}