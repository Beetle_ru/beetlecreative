﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib
{
    class Frame {
        public float[] Buff;
        public int Index;
        public Frame(int len) {
            Buff = new float[len];
            Index = 0;
        }

        public void Push(float v) {
            Index++;
           
            if (Index >= Buff.Count()) Index = 0;
            
            Buff[Index] = v;
        }

        public float GetMax() {
            return Buff.Max();
        }
    }
}
