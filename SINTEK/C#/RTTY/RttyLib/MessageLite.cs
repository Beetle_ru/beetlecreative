﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib {
    public class MessageLite {
        public string Key;
        public string Value;
        public string Type;
        public char Separator = ':';

        public MessageLite() {
        }

        public MessageLite(string dataKey, object dataVal) {
            Key = dataKey;
            Value = dataVal.ToString();
            Type = dataVal.GetType().ToString();
        }

        public MessageLite(byte[] buffer) {
            Deser(buffer);
        }

        public void Deser(byte[] buffer) {
            var len = 0;

            while (true) {
                if ((len >= buffer.Length) || (buffer[len] == 0)) break;
                len++;
            }

            var str = Encoding.ASCII.GetString(buffer, 0, len);

            var itms = str.Split(Separator);
            if (itms.Count() < 3) return;
            Key = Base64Decode(itms[0]);
            Value = Base64Decode(itms[1]);
            Type = Base64Decode(itms[2]);
        }

        public byte[] Serialize() {
            var str = String.Format("{1}{0}{2}{0}{3}", Separator, Base64Encode(Key), Base64Encode(Value),
                                    Base64Encode(Type));
            return Encoding.ASCII.GetBytes(str);
        }

        public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}