﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib
{
    class SignalQueue
    {
        //public float[] Buff;
        public List<float> Buff;
        public SignalQueue(int len) {
            //Buff = new float[len];
            Buff = new List<float>();
            for (int i = 0; i < len; i++) {
                Buff.Add(0.0f);
            }
        }

        public void Push(float v) {
            //for (int i = 1; i < Buff.Count(); i++) {
            //    Buff[i - 1] = Buff[i];
            //}
            //Buff[Buff.Count() - 1] = v;
            Buff.Add(v);
            Buff.RemoveAt(0);
        }
    }
}
