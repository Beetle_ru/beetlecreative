﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace RttyLib {
    public delegate void DataReceiveDelegate(List<byte> data);

    public class DemXX {
        private int _tickCount;
        private bool _isImpulse;
        private bool _isSignalLost;
        private bool _isMinus;
        private float _odd;
        public int BitRate;
        private bool _isInited;
        private int _samplerate;
        private int _averageLen = 38;
        private int _hightLen = 50;
        private int _lowLen = 25;
        private bool _isSynchronized;
        private int _syncCount;
        public int SyncImpulseCountTreshold = 128;
        private List<byte> _data;
        private int _bitIndex;
        private readonly DataReceiveDelegate _dataReceiveCallback;
        private SignalQueue _sigQueue;
        private Frame _lFrame;
        private Frame _hFrame;
        private float[] _lSample;
        private float[] _hSample;
        public const double SignalTreshold = 0.1;
        private RollAverage _averageSignal;

        private bool isPrint;

        public void DataAvailable(object sender, WaveInEventArgs e)
        {
            var wlc = (WasapiLoopbackCapture)sender;
            var bitsPerSample = wlc.WaveFormat.BitsPerSample;
            var sampleCount = e.BytesRecorded / (bitsPerSample / 8);

            if (!_isInited)
            {
                _samplerate = wlc.WaveFormat.SampleRate;
                Init();
                _isInited = true;
            }
            //Console.WriteLine("q");

            var signalMax = 0.0f;
            var signalMin = 0.0f;
            var isMinus = false;

            for (int n = 0; n < sampleCount; n++)
            {
                var val = BitConverter.ToSingle(e.Buffer, n * 4);
                if ((n % 2) == 0)
                {
                    var centerVal = (_odd + val) * 0.5f;

                    var signalVal = _averageSignal.Average(centerVal);

                    signalVal = signalVal * 100;

                    signalVal = signalVal > 1 ? 1 : signalVal;
                    signalVal = signalVal < -1 ? -1 : signalVal;

                    //_tickCount++;

                    var isLow = 0;
                    var isHight = 0;

                    if (signalVal > 0.5) {
                        _isImpulse = true;
                    }

                    if (_isMinus && (signalVal > -0.5) && _isImpulse) {
                        _isImpulse = false;
                    }

                    _isMinus = signalVal < -0.5;


                    if (_isImpulse) {
                        _tickCount++;
                        if ((_tickCount > _lowLen * 2)) {
                            if (_isSynchronized) {
                                Console.WriteLine(":: SIGNAL LOST 2 :: --> {0}", _tickCount);
                                DataReceived();
                                _isSignalLost = true;
                                _isSynchronized = false;
                                _isImpulse = false;
                            }
                        }
                    } else {
                        if ((_tickCount != 0) && ((_tickCount > _lowLen * 2) || (_tickCount < _hightLen * 0.5))) {
                            if (_isSynchronized) {
                                Console.WriteLine(":: SIGNAL LOST 1 :: --> {0}", _tickCount);
                                DataReceived();
                                _isSignalLost = true;
                                _isSynchronized = false;
                            }
                        }
                        else {
                            if (_tickCount > _averageLen) {
                                isLow = 1;
                                BitReceived(0);
                            }
                            else {
                                isHight = 1;
                                BitReceived(1);
                            }
                            _isSignalLost = false;
                        }
                        _tickCount = 0;
                    }


                }
                else
                {
                    _odd = val;
                }

            }
        }

        //public void DataAvailable(object sender, WaveInEventArgs e) {
        //    var wlc = (WasapiLoopbackCapture) sender;
        //    var bitsPerSample = wlc.WaveFormat.BitsPerSample;
        //    var sampleCount = e.BytesRecorded/(bitsPerSample/8);

        //    if (!_isInited) {
        //        _samplerate = wlc.WaveFormat.SampleRate;
        //        Init();
        //        _isInited = true;
        //    }
        //    //Console.WriteLine("q");

        //    var signalMax = 0.0f;
        //    var signalMin = 0.0f;
        //    var isMinus = false;

        //    for (int n = 0; n < sampleCount; n++) {
        //        var val = BitConverter.ToSingle(e.Buffer, n*4);
        //        if ((n%2) == 0) {
        //            var centerVal = (_odd + val)*0.5f;

        //            var signalVal = _averageSignal.Average(centerVal);

        //            signalVal = signalVal*100;

        //            signalVal = signalVal > 1 ? 1 : signalVal;
        //            signalVal = signalVal < -1 ? -1 : signalVal;

        //            _tickCount++;

        //            var isLow = 0;
        //            var isHight = 0;

        //            if (signalVal >= 1) {
        //                if (isMinus) {
        //                    if ((_tickCount > _averageLen) && (_tickCount < _lowLen * 2)) {
        //                        isLow = 1;
        //                        BitReceived(0);
        //                        //Console.WriteLine(-1);
        //                        _isSignalLost = false;
        //                    } else if ((_tickCount < _averageLen)&& (_tickCount > _hightLen * 0.5)) {
        //                        isHight = 1;
        //                        BitReceived(1);
        //                        //Console.WriteLine(1);
        //                        _isSignalLost = false;
        //                    }
        //                    else {
        //                        if (!_isSignalLost) {
        //                            Console.WriteLine(":: SIGNAL LOST 1 :: --> {0}", _tickCount);
        //                            _isSignalLost = true;
        //                            _isSynchronized = false;
        //                            DataReceived();
        //                        }
        //                    }
        //                    _tickCount = 0;
        //                    isMinus = false;
        //                }
                        

        //            }
        //            else if (signalVal <= -1) {
        //                isMinus = true;
        //            }

        //            if ((_tickCount > _lowLen * 2) && !_isSignalLost) {
        //                Console.WriteLine(":: SIGNAL LOST 2 :: --> {0}", _tickCount);
        //                _isSignalLost = true;
        //                _isSynchronized = false;
        //                _tickCount = 0;
        //                DataReceived();
        //            }

        //            //_sigQueue.Push(centerVal);
                    

        //            //var lRec = Recognition(_sigQueue.Buff, 0, _lSample);
        //            //var hRec = Recognition(_sigQueue.Buff, 0, _hSample);

        //            //_lFrame.Push(lRec);
        //            //_hFrame.Push(hRec);

        //            //var lmax = _lFrame.GetMax();
        //            //var hmax = _hFrame.GetMax();

        //            //var isLow = 0;
        //            //var isHight = 0;

        //            //if (lmax > hmax) {
        //            //    var lmd = lmax - lRec;
        //            //    if ((lmd <= SignalTreshold) && (lmax > SignalTreshold)) {
        //            //        if (!_isImpulse) {
        //            //            isLow = 1;
        //            //            BitReceived(0);
                                
        //            //        }
        //            //        _isImpulse = true;
        //            //    }
        //            //    else _isImpulse = false;
        //            //}
        //            //else {
        //            //    var hmd = hmax - hRec;
        //            //    if ((hmd <= SignalTreshold) && (hmax > SignalTreshold)) {
        //            //        if (!_isImpulse) {
        //            //            isHight = 1;
        //            //            BitReceived(1);
        //            //        }
        //            //        _isImpulse = true;
        //            //    }
        //            //    else _isImpulse = false;
        //            //}

        //            //if (centerVal != 0) isPrint = true;
        //            //else isPrint = false;

        //            //if (isPrint) {
        //            //    //Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", ';', centerVal, lRec, hRec, lmax,
        //            //    //                  hmax,
        //            //    //                  isLow, isHight);
        //            //    //Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}", ';', centerVal, signalVal, isLow, isHight);
        //            //}

        //            //if (isPrint && (lmax <= 0) && (hmax <= 0)) isPrint = false;
                    
        //        }
        //        else {
        //            _odd = val;
        //        }

        //        //if ((++_tickCount > (_lowLen * 3)) && _isSynchronized) { // SIGNAL LOST
        //        //    Console.WriteLine(":: SIGNAL LOST :: -> {0}", _tickCount);
        //        //    DataReceived();
        //        //    _isSynchronized = false;
        //        //}
        //    }
        //}

        public void BitReceived(int val) {
            if (!_isSynchronized) {
                _bitIndex = 0;

                if (val == 0) {
                    _syncCount++;
                } else {
                    if (_syncCount > SyncImpulseCountTreshold) {
                        _isSynchronized = true;
                        SetBit(_bitIndex++, (byte)val);
                    }
                    _syncCount = 0;
                }
            }
            else {
                SetBit(_bitIndex++, (byte)val);
                _tickCount = 0;
                _syncCount = 0;
            }
            
        }

        public DemXX(int bitrate, DataReceiveDelegate receiveCallback) {
            _dataReceiveCallback = receiveCallback;
            SetBitRate(bitrate);

            //Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", ';', "centerVal", "lRec", "hRec", "lmax", "hmax",
            //                          "isLow", "isHight");
        }

        private void Init() {
            _lowLen = (int) Math.Round((1.0/BitRate)*_samplerate);
            _hightLen = (int) Math.Round(_lowLen*0.5);
            _averageLen = (int) Math.Round((_lowLen + _hightLen)*0.5);

            _data = new List<byte>();

            _lSample = SampleGenerator.SinGen(_lowLen, 1);
            _hSample = SampleGenerator.SinGen(_hightLen, 1);

            _sigQueue = new SignalQueue(_lowLen);
            _lFrame = new Frame(_lowLen);
            _hFrame = new Frame(_hightLen);

            _averageSignal = new RollAverage((int)Math.Round(Math.Max(_lowLen, _hightLen) * 0.25));

            
        }

        private void SetBit(int index, byte bitValue) {
            int bitI;
            var byteI = Math.DivRem(index, 8, out bitI);

            while (_data.Count <= byteI) {
                _data.Add(0);
            }
            _data[byteI] = (byte) (_data[byteI] | (bitValue << bitI));
        }

        public void DataReceived() {
            if (!_data.Any()) return;
            _data.RemoveAt(0);
            if (!_data.Any()) return;

            _dataReceiveCallback(_data);
            //Console.WriteLine("RECEIVED -> {0} bytes", _data.Count);

            _data.Clear();
        }


        public void SetBitRate(int bitrate) {
            BitRate = bitrate;
            Init();
        }

        private static float Recognition(List<float> signal, int startIndex, float[] pattern) {
            var result = 0.0f;
            var patternInex = 0;
            for (var i = startIndex; (patternInex < pattern.Count()) && (i < signal.Count()); i++) {
                result += signal[i]*pattern[patternInex++];
            }
            result = result/pattern.Count();
            result = result < 0.0 ? 0.0f : result;
            return result;
        }
    }
}