﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace RttyLib {
    //public delegate void DataReceiveDelegate(List<byte> data);

    public class Demodulator {
        private int _tickCount;
        private bool _isImpulse;
        private float _odd;
        public int BitRate;
        private bool _isInited;
        private int _samplerate;
        private int _averageLen = 38;
        private int _hightLen = 50;
        private int _lowLen = 25;
        private bool _isSynchronized;
        private int _syncCount;
        public int SyncImpulseCountTreshold = 128;
        private List<byte> _data;
        private int _bitIndex;
        private readonly DataReceiveDelegate _dataReceiveCallback;

        public Demodulator(int bitrate, DataReceiveDelegate receiveCallback) {
            _dataReceiveCallback = receiveCallback;
            SetBitRate(bitrate);
        }

        private void Init() {
            _lowLen = (int) Math.Round((1.0/BitRate)*_samplerate);
            _hightLen = (int) Math.Round(_lowLen*0.5);
            _averageLen = (int) Math.Round((_lowLen + _hightLen)*0.5);

            _data = new List<byte>();
        }

        private void SetBit(int index, byte bitValue) {
            int bitI;
            var byteI = Math.DivRem(index, 8, out bitI);

            while (_data.Count <= byteI) {
                _data.Add(0);
            }
            _data[byteI] = (byte) (_data[byteI] | (bitValue << bitI));
        }

        public void DataReceived() {
            if (!_data.Any()) return;
            _data.RemoveAt(0);
            if (!_data.Any()) return;

            _dataReceiveCallback(_data);
            //Console.WriteLine("RECEIVED -> {0} bytes", _data.Count);

            _data.Clear();
        }

        public void DataAvailable(object sender, WaveInEventArgs e) {
            var wlc = (WasapiLoopbackCapture) sender;
            var bitsPerSample = wlc.WaveFormat.BitsPerSample;
            var sampleCount = e.BytesRecorded/(bitsPerSample/8);

            if (!_isInited) {
                _samplerate = wlc.WaveFormat.SampleRate;
                Init();
                _isInited = true;
            }

            for (int n = 0; n < sampleCount; n++) {
                var val = BitConverter.ToSingle(e.Buffer, n*4);
                if ((n%2) == 0) {
                    var centerVal = (_odd + val)*0.5f;
                    if (centerVal > 0) {
                        if (!_isImpulse) {
                            _isImpulse = true;
                            if ((_tickCount > _lowLen*3) || (_tickCount < _hightLen*0.3)) {
                                //Console.WriteLine("::: ERROR IMPULSE LEN = {0} :::", _tickCount); // TODO make callback
                                _isSynchronized = false;
                                _syncCount = 0;
                                _bitIndex = 0;
                                DataReceived();
                            }
                            else {
                                if (_isSynchronized) {
                                    if (_tickCount < _averageLen) {
                                        //Console.WriteLine("*** -> {0}", 1);
                                        SetBit(_bitIndex, 1);
                                    }
                                    else {
                                        //Console.WriteLine("--- -> {0}", 0);
                                    }
                                    _bitIndex++;
                                }
                                else {
                                    if (_tickCount < _averageLen) {
                                        if (_syncCount > SyncImpulseCountTreshold) {
                                            _isSynchronized = true;
                                        }
                                        _syncCount = 0;
                                        _bitIndex = 0;
                                        _tickCount = 0;
                                    }
                                    else _syncCount++;
                                }
                            }
                            _tickCount = 0;
                        }
                        else {
                            _tickCount++;
                        }
                    }
                    else {
                        _tickCount++;
                        _isImpulse = false;
                    }
                }
                else {
                    _odd = val;
                }
            }
        }

        public void SetBitRate(int bitrate) {
            BitRate = bitrate;
            Init();
        }
    }
}