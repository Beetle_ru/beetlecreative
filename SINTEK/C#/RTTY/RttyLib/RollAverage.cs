﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib
{
    public class RollAverage {
        private readonly float[] _buff;
        private int _index;
        private float _summ;
        private float _devider;


        public RollAverage(int len) {
            _buff = new float[len];
            _devider = (float)(1.0/len);
        }

        public float Average( float newVal) {
            _summ = _summ - _buff[_index] + newVal;
            _buff[_index] = newVal;
            _index++;
            if (_index >= _buff.Count()) _index = 0;
            return _summ * _devider;
        }
    }
}
