﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System.Threading;

namespace RttyLib {
    public enum BitRates {
        Bps50 = 50,
        Bps100 = 100,
        Bps110 = 110,
        Bps150 = 150,
        Bps300 = 300,
        Bps600 = 600,
        Bps1200 = 1200,
        Bps1600 = 1600,
        Bps2400 = 2400,
        Bps3200 = 3200,
        Bps4800 = 4800
    }

    public class RTTYTXRX {
        public ModXX Mod;
        //public Modulator Mod;
        //public Demodulator Demod;
        public DemXX Demod;
        public WaveOut WO;
        public WasapiLoopbackCapture WLC;
        private readonly DataReceiveDelegate _dataReceiveCallback;

        public RTTYTXRX(int bitrate, DataReceiveDelegate receiveCallback = null) {
            Mod = new ModXX(bitrate);
           // Mod = new Modulator(bitrate);

            WO = new WaveOut();

            WO.Init(Mod);
            WO.Play();

            if (receiveCallback != null) {
                _dataReceiveCallback = receiveCallback;
                //Demod = new Demodulator(bitrate, receiveCallback);
                Demod = new DemXX(bitrate, receiveCallback);
                WLC = new WasapiLoopbackCapture();
                WLC.DataAvailable += new EventHandler<WaveInEventArgs>(Demod.DataAvailable);
                WLC.StartRecording();

                //System.Threading.Thread.Sleep(500);
            }
        }

        public void StopAll() {
            WO.Stop();
            if (_dataReceiveCallback != null) {
                WLC.StopRecording();
            }
        }


        public void SetBitRate(int bitrate) {
            Mod.SetBitRate(bitrate);
            if (_dataReceiveCallback != null) {
                Demod.SetBitRate(bitrate);
            }
        }

        public void SetData(List<byte> data) {
            Mod.SetData(data);
        }
    }
}