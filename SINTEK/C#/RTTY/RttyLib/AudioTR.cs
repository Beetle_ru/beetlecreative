﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib {
    public delegate void DRDelegate(string cmd, string msg);

    public class AudioTR {
        public RTTYTXRX Transmitter;
        private DRDelegate _receiveCallback;

        public AudioTR(int bitrate, DRDelegate receiveCallback = null) {
            DataReceiveDelegate drc = null;

            if (receiveCallback != null) drc = DataReceive;

            Transmitter = new RTTYTXRX(bitrate, drc);
            _receiveCallback = receiveCallback;
        }

        public void Send(string cmd, string message) {
            var data = new List<byte>();
            data.Add(170); // start byte

            var msgDat = new MessageLite(cmd, message).Serialize();

            Int32 length = msgDat.Count();
            for (int i = 0; i < sizeof (Int32); i++) {
                data.Add((byte) ((length >> (i << 3)) & 0xff));
            }

            data.AddRange(msgDat);

            Transmitter.SetData(data);
        }

        public void StopAll() {
            Transmitter.StopAll();
        }

        private void DataReceive(List<byte> data) {
            if (_receiveCallback == null) return;

            Int32 length = 0;
            int datP = 0;

            if (data[datP++] != 170) return;

            if (data.Count < sizeof(Int32) + 1) return;

            for (int i = 0; i < sizeof (Int32); i++) {
                length = length | (data[datP++] << (i << 3));
            }

            var msgDat = new List<byte>();
            for (int i = datP; i < data.Count; i++) {
                datP++;
                msgDat.Add(data[i]);
            }

            if (length != msgDat.Count) return;
            
            var msgl = new MessageLite(msgDat.ToArray());
            _receiveCallback(msgl.Key, msgl.Value);
        }
    }
}