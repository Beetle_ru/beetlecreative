﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace RttyLib {
    public class Modulator : WaveProvider32 {
        private int _impulseIndex;
        private readonly List<byte> _data;
        private readonly List<byte> _synchroImpulse;
        private int _dataIndex;
        private bool _isTransfered;
        private int _needOffset;
        private int _hightLen = 50;
        private int _lowLen = 25;
        private bool _isBlockBegin;

        public Modulator(int bitrate) {
            _data = new List<byte>();

            _synchroImpulse = new List<byte>();
            for (int i = 0; i < 31; i++) {
                _synchroImpulse.Add(0);
            }
            _synchroImpulse.Add(1); // 0000000000000000000000010000000 -> data


            SetBitRate(bitrate);
        }


        private int GetBit(int index) {
            int bitI;
            var byteI = Math.DivRem(index, 8, out bitI);
            if (_isBlockBegin) return ((_synchroImpulse[byteI] >> bitI) & 1);
            else return ((_data[byteI] >> bitI) & 1);
        }

        public void SetBitRate(int bitrate) {
            var sampleRate = WaveFormat.SampleRate;
            _lowLen = (int) Math.Round((1.0/bitrate)*sampleRate);
            _hightLen = (int) Math.Round(_lowLen*0.5);
        }

        private int GetBitCount() {
            if (_isBlockBegin) return _synchroImpulse.Count*8;
            else return _data.Count*8;
        }

        public void SetData(List<byte> bList) {
            _data.Clear();
            foreach (var b in bList) {
                _data.Add(b);
            }

            _dataIndex = 0;
            _needOffset = 0;
            _isTransfered = false;
            _isBlockBegin = true;
        }

        public override int Read(float[] buffer, int offset, int sampleCount) {
            return SquareMod(buffer, offset, sampleCount);
        }

        public int SquareMod(float[] buffer, int offset, int sampleCount) {
            if (_isTransfered) {
                buffer[0] = 0;
                return 1;
            }
            if (!_data.Any()) {
                buffer[0] = 0;
                return 1;
            }

            for (int n = 0; n < sampleCount; n++) {
                if (_dataIndex <= GetBitCount()) {
                    if ((_impulseIndex > _needOffset)) {
                        if (_dataIndex >= GetBitCount()) {
                            _dataIndex++;
                            continue;
                        }
                        _needOffset = GetBit(_dataIndex++) == 1 ? _hightLen : _lowLen;
                        _impulseIndex = 0;
                    }

                    var val = 0;
                    if (_impulseIndex < (_needOffset*0.5)) {
                        val = 1;
                    }

                    else {
                        val = -1;
                    }
                    buffer[n + offset] = val;
                    _impulseIndex++;
                }
                else {
                    if (_isBlockBegin) {
                        _impulseIndex = 0;
                        _dataIndex = 0;
                        _isBlockBegin = false;
                    }
                    else {
                        _isTransfered = true;
                        return n;
                    }
                }
            }

            return sampleCount;
        }

        //public int SinMod(float[] buffer, int offset, int sampleCount) {
        //    if (_isTransfered) return 0;
        //    if (!_pDt.Any()) return 0;

        //    int sampleRate = WaveFormat.SampleRate;

        //    double f1 = 2000;
        //    double f2 = 1000;
        //    double impulseLen = 1.0/128.0;
        //    var needOffset = (int) Math.Round(sampleRate*impulseLen);

        //    var value = 0.0;
        //    var is2Pi = false;

        //    for (int n = 0; n < sampleCount; n++) {
        //        //if ((!_isStarted) || (_impulseIndex > needOffset) && (Math.Abs(value) < 0.001)) {
        //        if ((!_isStarted) || (_impulseIndex > needOffset) && is2Pi) {
        //            if (!_isStarted) _isStarted = true;

        //            if (_dataIndex >= _pDt.Count) {
        //                _isTransfered = true;
        //                _impulseIndex = 0;
        //                _bitIndex = 0;
        //                _dataIndex = 0;
        //                return n;
        //            }

        //            if (_dataIndex < _pDt.Count) _frequency = _pDt[_dataIndex++] == 1 ? f1 : f2;

        //            _impulseIndex = 0;
        //        }

        //        value = Math.Sin((2*Math.PI*_sample*_frequency)/sampleRate);
        //        var x = ((2*Math.PI*_sample*f1)/sampleRate);
        //        var x2 = ((2*Math.PI*_sample*f2)/sampleRate);
        //        var xx = x%(2*Math.PI);
        //        var xx2 = x2%(2*Math.PI);
        //        is2Pi = (xx < 0.3) && (xx2 < 0.3);
        //        buffer[n + offset] = (float) value;
        //        _impulseIndex++;
        //        _sample++;

        //        if (_sample >= sampleRate) _sample = 0;
        //    }

        //    return sampleCount;
        //}
    }
}

//for (int n = 0; n < sampleCount; n++)
//{
//    buffer[n + offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate));
//    sample++;
//    if (sample >= sampleRate) sample = 0;
//}
//return sampleCount;