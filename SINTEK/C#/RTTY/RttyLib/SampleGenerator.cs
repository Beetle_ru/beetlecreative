﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RttyLib
{
    public static class SampleGenerator
    {
        static public float[] ImpsGen(int len, int periods)
        {
            var impulse = new float[len];
            var plen = len / periods;
            for (var p = 0; p < periods; p++)
            {
                for (int i = 0; i < plen; i++)
                {
                    var implShift = i + p * plen;
                    if (i < plen * 0.5)
                    {
                        impulse[implShift] = 1.0f;
                    }
                    else
                    {
                        impulse[implShift] = -1.0f;
                    }
                }
            }
            return impulse;
        }

        static public float[] SinGen(int len, int periods)
        {
            var impulse = new float[len];
            var w = 2 * Math.PI * periods; // угловая частота
            var wstep = w / len; // шаг на импульс

            for (int i = 0; i < len; i++)
            {
                impulse[i] = (float)Math.Sin(i * wstep);
            }

            return impulse;
        }
    }
}
