﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlternateModulators {
    internal class Program {
        private static void Main(string[] args) {
            const int sampleLen = 50;

            //var hightSamle = ImpsGen(sampleLen / 2, 1);
            //var lowSamle = ImpsGen(sampleLen, 1);

            var hightSamle = SinGen(sampleLen / 2, 1);
            var lowSamle = SinGen(sampleLen, 1);

            //PrintSample(hightSamle);
            //PrintSample(lowSamle);

            var data = new List<byte>();
            data.Add(0);
            data.Add(0);
            data.Add(1);
            data.Add(1);

            var flow = new float[355];

            #region generateFlow

            var datIndex = 0;
            var sampleIndex = 0;
            for (var i = 0; i < flow.Count(); i++) {
                var val = 0.0f;

                if (i < 50) continue;

                if (datIndex < data.Count) {
                    var currentSample = data[datIndex] == 0 ? lowSamle : hightSamle;
                    val = currentSample[sampleIndex++];

                    if (sampleIndex >= currentSample.Count()) {
                        sampleIndex = 0;
                        datIndex++;
                    }
                }

                flow[i] = val;
            }

            //PrintSample(flow);

            #endregion

            var r = new Random();
            for (int i = 0; i < flow.Count(); i++) {
                //flow[i] = (float)(flow[i] * 1.0 + (r.NextDouble() * 1.0));
            }

            var lf = new Frame(sampleLen);
            var hf = new Frame(sampleLen / 2);
            var impulseBackCount = 0;
            var isImpulse = false;
            var signalTreshold = 0.3;


            Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", ';', "signal", "isLow", "isHight", "lowRec",
                                  "hightRec", "lmax", "hmax");


            for (int i = 0; i < flow.Count(); i++) {
                var lowRec = Recognition(flow, i, lowSamle);
                var hightRec = Recognition(flow, i, hightSamle);

                lf.Push(lowRec);
                hf.Push(hightRec);

                var isLow = 0;
                var isHight = 0;

                var lmax = lf.GetMax();
                var hmax = hf.GetMax();

                if (lmax > hmax) {
                    var lmd = lmax - lowRec;
                    if ((lmd <= signalTreshold) && (lmax > signalTreshold))
                    {
                        if (!isImpulse) isLow = 1;
                        isImpulse = true;
                    } else isImpulse = false;
                }
                else {
                    var hmd = hmax - hightRec;
                    if ((hmd <= signalTreshold) && (hmax > signalTreshold))
                    {
                        if (!isImpulse) isHight = 1;
                        isImpulse = true;
                    } else isImpulse = false;
                }


                //var lmd = lmax - lowRec;
                //var hmd = hmax - hightRec;

                //if ((lmd < 0.1) || (hmd < 0.1)) {
                //    if (impulseBackCount <= 0) {
                //        if (lmd > hmd) {
                //            isHight = 1;
                //        }
                //        else {
                //            isLow = 1;
                //        }
                //        impulseBackCount = (int)Math.Round(sampleLen / 1.5);
                //    }
                //}

                //impulseBackCount--;

                //var diff = lmax - hmax;

                //if ((Math.Abs(diff) > 0.1) && (impulseBackCount <= 0))
                //{
                //    if (diff > 0) isLow = 1;
                //    else isHight = 1;
                //    impulseBackCount = sampleLen;
                //}

                //if (impulseBackCount > 0) impulseBackCount--;

                //var chr = ' ';

                //Console.WriteLine("{2:000} -> l {0:0.00}  ---  h {1:0.00}  {3} ; {4}", lowRec, hightRec, i, chr, 0);
                //Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}", ';', flow[i], lowRec, hightRec, lf.GetMax(), hf.GetMax());
                //Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", ';', flow[i], isLow, isHight, lmax, hmax, lowRec, hightRec);
                Console.WriteLine("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}", ';', flow[i], isLow, isHight, lowRec,
                                  hightRec, lmax, hmax);
            }
        }

        private static float Recognition(float[] signal, int startIndex, float[] pattern) {
            var result = 0.0f;
            var patternInex = 0;
            for (var i = startIndex; (patternInex < pattern.Count()) && (i < signal.Count()); i++) {
                result += signal[i]*pattern[patternInex++];
            }
            result = result/pattern.Count();
            result = result < 0.0 ? 0.0f : result;
            return result;
        }

        private static void PrintSample(float[] sample) {
            foreach (var f in sample) {
                Console.WriteLine(f);
            }
            Console.WriteLine("----------");
        }

        private static float[] ImpsGen(int len, int periods) {
            var impulse = new float[len];
            var plen = len/periods;
            for (var p = 0; p < periods; p++) {
                for (int i = 0; i < plen; i++) {
                    var implShift = i + p*plen;
                    if (i < plen*0.5) {
                        impulse[implShift] = 1.0f;
                    }
                    else {
                        impulse[implShift] = -1.0f;
                    }
                }
            }
            return impulse;
        }

        private static float[] SinGen(int len, int periods) {
            var impulse = new float[len];
            var w = 2*Math.PI*periods; // угловая частота
            var wstep = w/len; // шаг на импульс

            for (int i = 0; i < len; i++) {
                impulse[i] = (float) Math.Sin(i*wstep);
            }

            return impulse;
        }
    }
}