﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RttyLib;

namespace tester
{
    class Program
    {
        static void Main(string[] args)
        {
            //var atr = new AudioTR(Global.Constants.bitRate, Receiver);
            var rxtx = new RTTYTXRX(Global.Constants.bitRate, DataReceive);
            var data = new List<byte>();

            for (int i = 0; i < 2; i++) {
                data.Add(255);
                data.Add(0);
            }

            //System.Threading.Thread.Sleep(3000);
            rxtx.SetData(data);
            
            System.Threading.Thread.Sleep(1000);
            data.Clear();
            data.Add(170);
            data.Add(170);
            data.Add(170);

            //rxtx.SetData(data);

            //atr.Send("test", "test");
            Console.ReadLine();
           // atr.StopAll();
            rxtx.StopAll();
        }

        private static void DataReceive(List<byte> data) {
            foreach (var b in data) {
                Console.WriteLine(b);
            }
        }

        private static void Receiver(string cmd, string msg)
        {
            Console.WriteLine("Command -> {0}: \n{1}", cmd, msg);
        }
    }
}
