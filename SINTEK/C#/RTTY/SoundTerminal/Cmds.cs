﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoundTerminal
{
    public static class Cmds {
        public const string ExecuteCmd = "ExecuteCmd";
        public const string TestResponse = "TestResponse";
        public const string TestRequest = "TestRequest";
        public const string RemoteQuit = "RemoteQuit";
        public const string ErrorMsg = "ErrorMsg";
        public const string Cmd = "Cmd";
        public const string CmdResult = "CmdResult";

        public static class SubCmds {
            public const string Ls = "ls";
            public const string Cd = "cd";
            public const string Pwd = "pwd";
        }
    }
}
