﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RttyLib;
using System.Diagnostics;
using System.IO;

namespace SoundTerminal
{
    class Program {
        public static AudioTR ATR;
        public static string AppAddr;
        static void Main(string[] args) {
            AppAddr = System.AppDomain.CurrentDomain.FriendlyName.Split('.').First();
            var r = new Random();
            AppAddr = String.Format("{0}_{1}", AppAddr, r.Next());

            ATR = new AudioTR(Global.Constants.bitRate, Receiver);

            while (true) {
                var input = Console.ReadLine();
                if (String.IsNullOrWhiteSpace(input)) continue;

                if (input.StartsWith(":")) {
                    input = input.Remove(0, 1);

                    switch (input) {
                        case "exit":
                        case "quit":
                        case "q":
                            Exit();
                            break;
                        case "Q":
                        case "qq":
                        case "remotequit":
                        case "remoteexit":
                            Send(Cmds.RemoteQuit, "now");
                            //Exit();
                            break;
                        case "test":
                        case "t":
                            Send(Cmds.TestRequest, "Request");
                            break;
                        case "exec" :
                        case "e":
                        case "run":
                            Console.WriteLine("Please write command for remoute Execute");
                            var procname = Console.ReadLine();
                            Send(Cmds.ExecuteCmd, procname);
                            break;
                        default :
                            Send(Cmds.Cmd, "define");
                            break;
                    }
                }
                else {
                    Send(Cmds.Cmd, input);
                }
            }
        }

        private static void Send(string cmd, string msg) {
            cmd = String.Format("{0}:{1}", AppAddr, cmd);
            ATR.Send(cmd, msg);
        }

        private static void Exit() {
            ATR.StopAll();
            Environment.Exit(0);
        }

        private static void Receiver(string cmd, string msg) {
            var cmdsplt = cmd.Split(':');
            
            if (cmdsplt.Count() < 2) return;

            var from = cmdsplt[0];
            cmd = cmdsplt[1];

            if (from == AppAddr) {
                Console.WriteLine("... ok");
                return;
            }

            Console.WriteLine("from \"{0}\" cmd:\"{1}\" \n* -> \"{2}\"", from, cmd, msg);

            switch (cmd) {
                case Cmds.ExecuteCmd:
                    try {
                        Process.Start(msg);
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                        Send(Cmds.ErrorMsg, e.Message);
                    }
                    break;
                case Cmds.TestRequest:
                    Send(Cmds.TestResponse, "Response");
                    break;
                case Cmds.RemoteQuit:
                    Exit();
                    break;

                case Cmds.Cmd:
                    Commands(msg);
                    break;
            }
        }

        public static void Commands(string subcmd) {
            var args = subcmd.Split(' ');

            if (!args.Any()) {
                Send(Cmds.ErrorMsg, subcmd);
                return;
            }

            var cmd = args.First();
            var result = "";
            switch (cmd) {
                case Cmds.SubCmds.Ls:
                    var path = Directory.GetCurrentDirectory();
                    var dirs = Directory.GetDirectories(path);
                    var files = Directory.GetFiles(path);
                    
                    foreach (var dir in dirs) {
                        if (result != "") result += "\n";
                        result += dir.Split('\\').Last();
                    }
                    foreach (var file in files) {
                        if (result != "") result += "\n";
                        result += file.Split('\\').Last();
                    }
                    break;

                case Cmds.SubCmds.Cd:
                    if (args.Count() < 2) {
                        Send(Cmds.ErrorMsg, subcmd);
                        return;
                    }
                    try {
                        Directory.SetCurrentDirectory(args[1]);
                        result = args[1];
                    }
                    catch (Exception e) {
                        Send(Cmds.ErrorMsg, e.Message);
                    }
                    
                    break;

                case Cmds.SubCmds.Pwd:
                    result = Directory.GetCurrentDirectory();
                    break;
            }

            var rcmd = String.Format("{0}.{1}", Cmds.CmdResult, cmd);
            Send(rcmd, result);
        }
    }
}
