﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RttyLib;

namespace Receiver {
    internal class Program {
        private static void Main(string[] args) {
            var atr = new AudioTR(Global.Constants.bitRate, Receiver);

            Console.WriteLine("Receiver started, for exit press enter");
            Console.ReadLine();
            atr.StopAll();
        }

        private static void Receiver(string cmd, string msg) {
            Console.WriteLine("Command -> {0}: \n{1}", cmd, msg);
        }


    }
}