﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RttyLib;

namespace Sender {
    internal class Program {
        private static void Main(string[] args) {
            const string extStr = ":q";

            var atr = new AudioTR(Global.Constants.bitRate);

            Console.WriteLine("sender stated, for exit write \"{0}\" and pres enter", extStr);

            var str = "";
            //atr.Send("Sender.start", "qqq");
            while (str != extStr) {
                str = Console.ReadLine();
                atr.Send("Sender.message", str);
            }

            atr.StopAll();
        }
    }
}