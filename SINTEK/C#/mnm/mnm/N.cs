﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mnm
{
    public delegate int GetValDel();

    public delegate int ActivationDel(int val);

    public class N {
        public List<GetValDel> Inputs;
        private int _axon;

        public N() {
            Inputs = new List<GetValDel>();
        }

        public void Calc(int[] matrix, ActivationDel activation) {
            _axon = 0;
            for (int i = 0; i < Inputs.Count; i++) {
                var absiw = Math.Abs(Inputs[i]() - matrix[i]);
                var k = absiw != 0 ? matrix[i] / absiw : 0;
                _axon += k;
            }

            _axon = activation(_axon);
        }

        public int Axon() {
            return _axon;
        }
    }
}
