﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mnm
{
    public class NTube {
        public List<N> Neurons;
        public int[] Matrix;

        public int Treshold = 127;
        public int Out = 255;

        public NTube(List<int> matrix) {
            Neurons = new List<N>();
            matrix.CopyTo(Matrix);
        }

        public NTube(int size) {
            Neurons = new List<N>();
            Matrix = new int[size];
        }

        public void Calc() {
            foreach (var neuron in Neurons) {
                neuron.Calc(Matrix, Activation);
            }
        }

        public int Activation(int val) {
            return 0;
        }

    }
}
