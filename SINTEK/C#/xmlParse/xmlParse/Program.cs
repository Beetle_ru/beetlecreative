﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace xmlParse {
    internal class Program {
        private static void Main(string[] args) {
            var output = new StringBuilder();

            const string xmlString = @"<?xml version='1.0'?>
        <!-- This is a sample XML document -->
        <Items>
          <Item>test with a child element <more/> stuff</Item>
        </Items>";
            // Create an XmlReader
            using (var reader = XmlReader.Create(new StringReader(xmlString))) {
                var ws = new XmlWriterSettings();
                ws.Indent = true;
                using (var writer = XmlWriter.Create(output, ws)) {
                    // Parse the file and display each of the nodes.
                    while (reader.Read()) {
                        switch (reader.NodeType) {
                            case XmlNodeType.Element:
                                writer.WriteStartElement(reader.Name);
                                break;
                            case XmlNodeType.Text:
                                writer.WriteString(reader.Value);
                                break;
                            case XmlNodeType.XmlDeclaration:
                            case XmlNodeType.ProcessingInstruction:
                                writer.WriteProcessingInstruction(reader.Name, reader.Value);
                                break;
                            case XmlNodeType.Comment:
                                writer.WriteComment(reader.Value);
                                break;
                            case XmlNodeType.EndElement:
                                writer.WriteFullEndElement();
                                break;
                        }
                    }
                }
            }
            Console.WriteLine(output.ToString());
        }
    }
}