﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Diagnostics;
using Microsoft.Win32;

namespace processLockers {
    internal class Program {
        private static void Main(string[] args) {
            EnableAutorestart(false);

            KillRunExplorer(false);

            Console.ReadLine();

            EnableAutorestart(true);
            KillRunExplorer(true);
        }

        public static  void Admin() {
            var ident = WindowsIdentity.GetCurrent();
            var isAdministrativeRight = false;
            if (ident != null) {
                var pricipal = new WindowsPrincipal(ident);
                isAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (!isAdministrativeRight) {
                var processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas"; //указываем, что процесс должен быть запущен с правами администратора
                processInfo.FileName = Environment.GetCommandLineArgs()[0];
                try {
                    Process.Start(processInfo);
                }
                catch (Exception) {
                    //Ничего не делаем, потому что пользователь, возможно, нажал кнопку "Нет" в ответ на вопрос о запуске программы в окне предупреждения UAC (для Windows 7)
                }
                Environment.Exit(0);
            }
        }

        public static void EnableAutorestart(bool isEnabled) {
            Admin();
            var ourKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                                                 Environment.Is64BitOperatingSystem
                                                     ? RegistryView.Registry64
                                                     : RegistryView.Registry32);
            ourKey = ourKey.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", true);
            var value = isEnabled ? 1 : 0;
            if (ourKey != null) {
                var regval = ourKey.GetValue("AutoRestartShell");
                if (value != (int)regval) {
                    ourKey.SetValue("AutoRestartShell", value);
                }
            }
        }

        public static void KillRunProcesses(bool isRun, string name) {
            var explorers = Process.GetProcessesByName(name);

            if (!isRun) {
                foreach (var process in explorers) {
                    process.Kill();
                }
            }
            else {
                if (!explorers.Any()) Process.Start(name);
            }
        }

        public static void KillRunExplorer(bool isRun) {
            const string exploreName = "explorer";
            var explorers = Process.GetProcessesByName(exploreName);

            if (!isRun) {
                KillRunProcesses(false, exploreName);
            }
            else {
                if (!explorers.Any()) {
                    var windir = Environment.GetEnvironmentVariable("windir");
                    if (windir != null) Process.Start(Path.Combine(windir, exploreName + ".exe"));
                }
            }
        }
    }
}