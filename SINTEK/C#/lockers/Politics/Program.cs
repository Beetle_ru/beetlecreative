﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using Microsoft.Win32;

namespace Politics {
    internal class Program {
        public static Dictionary<string, RegKey> OptionAdresses;

        private static void Main(string[] args) {
            OptionAdresses = GetOptionAdresses();
            ChangeAll(OptionAdresses, true);
        }

        public static Dictionary<string, RegKey> GetOptionAdresses() {
            var oa = new Dictionary<string, RegKey>();

            oa.Add("DisableChangePassword", new RegKey() { BaseKey = RegistryHive.CurrentUser, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "DisableChangePassword" });
            oa.Add("DisableLockWorkstation", new RegKey() { BaseKey = RegistryHive.CurrentUser, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "DisableLockWorkstation" });
            oa.Add("NoLogoff", new RegKey() { BaseKey = RegistryHive.CurrentUser, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "NoLogoff" });
            oa.Add("DisableTaskMgr", new RegKey() { BaseKey = RegistryHive.CurrentUser, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "DisableTaskMgr" });
            oa.Add("HideUserSwitching", new RegKey() { BaseKey = RegistryHive.LocalMachine, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "HideFastUserSwitching" });
            oa.Add("ShutdownWithoutLogon", new RegKey() { BaseKey = RegistryHive.LocalMachine, SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System", Name = "ShutdownWithoutLogon" });

            return oa;
        }

        public static void Admin() {
            var ident = WindowsIdentity.GetCurrent();
            var isAdministrativeRight = false;
            if (ident != null) {
                var pricipal = new WindowsPrincipal(ident);
                isAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (!isAdministrativeRight) {
                var processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas"; //указываем, что процесс должен быть запущен с правами администратора
                processInfo.FileName = Environment.GetCommandLineArgs()[0];
                try {
                    Process.Start(processInfo);
                }
                catch (Exception) {
                    //Ничего не делаем, потому что пользователь, возможно, нажал кнопку "Нет" в ответ на вопрос о запуске программы в окне предупреждения UAC (для Windows 7)
                }
                Environment.Exit(0);
            }
        }

        public struct RegKey {
            public RegistryHive BaseKey;
            public string SubKey;
            public string Name;
        }

        public static void RegOption(RegKey key, bool isEnabled) {
            Admin();

            var ourKey = RegistryKey.OpenBaseKey(key.BaseKey,
                                                 Environment.Is64BitOperatingSystem
                                                     ? RegistryView.Registry64
                                                     : RegistryView.Registry32);
            ourKey = ourKey.CreateSubKey(key.SubKey);
            //ourKey = ourKey.OpenSubKey(key.SubKey, true);
            var value = isEnabled ? 1 : 0;
            if (ourKey != null) {
                var regval = ourKey.GetValue(key.Name);
                if ((regval == null) || ((regval is int) && (value != (int)regval)))
                    ourKey.SetValue(key.Name, value);
            }
        }

        public static void ChangeAll(Dictionary<string, RegKey> oa, bool value) {
            foreach (var regKey in oa) {
                RegOption(regKey.Value, value);
                Console.WriteLine("{0} = {1}", regKey.Key, value);
            }
            
        }
    }
}