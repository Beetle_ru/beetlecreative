﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OPC.Common;
using OPC.Data;
using OPC.Data.Interface;

namespace OpcDaWebsok {
    internal class Program {
        static public List<OPCItemDef> OpcItemsDef;

        private static void Main(string[] args) {
            var opcItemDefs = new List<OPCItemDef>();
            var opcServer = new OpcServer();
            OPCItemResult[] opcItemResults;

            opcServer.Connect("Sintek.DualSource");
            var opcGroup = opcServer.AddGroup("WebSock", false, 500);
            opcGroup.DataChanged += new DataChangeEventHandler(OpcGroupDataChanged);

            OpcItemsDef = new List<OPCItemDef>();
            var tags = GetTags("", opcServer);
            for (int i = 0; i < tags.Count; i++) {
                OpcItemsDef.Add(new OPCItemDef(tags[i], true, i, VarEnum.VT_EMPTY));
            }

            var res = opcGroup.AddItems(OpcItemsDef.ToArray(), out opcItemResults);

            Console.WriteLine("AddItems = {0}", res);
            //foreach (var opcItemResult in opcItemResults) Console.WriteLine("[err] -> {0}", opcItemResult.Error);

            opcGroup.Active = true;

            Console.WriteLine("started");
            Console.ReadLine();
        }

        private static List<string> GetTags(string item, OpcServer opcServer) {
            ArrayList branchs;
            ArrayList leafs;
            var res = new List<string>();

            opcServer.ChangeBrowsePosition(OPCBROWSEDIRECTION.OPC_BROWSE_TO, item);

            opcServer.Browse(OPCBROWSETYPE.OPC_BRANCH, out branchs);
            opcServer.Browse(OPCBROWSETYPE.OPC_LEAF, out leafs);

            if (leafs != null) res.AddRange(from object leaf in leafs select String.Format("{0}.{1}", item, leaf));

            if (branchs != null) {
                foreach (var branch in branchs) {
                    var ni = item != "" ? String.Format("{0}.{1}", item, branch) : (string) branch;
                    var subleafs = GetTags(ni, opcServer);
                    res.AddRange(subleafs);
                }
            }

            return res;
        }



        private static void OpcGroupDataChanged(object sender, DataChangeEventArgs e) {
            foreach (var opcItemState in e.sts) {
                var msg = String.Format("{0,-50} =>   {1}", OpcItemsDef[opcItemState.HandleClient].ItemID, opcItemState.DataValue);
                Console.WriteLine(msg);
            }
        }
    }
}