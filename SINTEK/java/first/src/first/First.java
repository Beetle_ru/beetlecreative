/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package first;

/**
 *
 * @author a.zhukov
 */
public class First {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.println("hello");
        
        Neuron n1 = new Neuron();
        Neuron n2 = new Neuron();
        
        n2.SetSinaps(n1);
        
        n1.axonValue = 11.33;
        
        n2.calc();
        
        System.out.format("%.3f\n", n2.axonValue);
    }
    
}
