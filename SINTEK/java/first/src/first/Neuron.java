/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package first;

/**
 *
 * @author a.zhukov
 */
public class Neuron implements INeuron{
    public INeuron Sinaps;
    public double axonValue;
    
    public void SetSinaps(INeuron sinaps) {
        Sinaps = sinaps;
    }
    
    @Override
    public double GetAxon() {
        return axonValue;
    }
    
    public void calc() {
        axonValue = Sinaps.GetAxon();
    }
}
