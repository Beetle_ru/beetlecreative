//������ ��������������� �������
//2009.07.01+2009.07.06 +2009.07.22 +2009.09.09  +2009.09.18 +2009.09.21 +2009.10.09 +2009.10.12
//+2009.11.04 +2010.01.19 +2010.06.11 +2010.08.10
unit CRT4;

interface
uses Windows;

type

TReplaceFlags = set of (rfReplaceAll, rfIgnoreCase);

Function GetSystemTimeAsDateTime:TDateTime;
Function DeleteFileS(FName:string):LongBool;
Function TempPathLong:String;
Function TempFileName:string;overload;
Function TempFileName(DoCreateFile:Boolean; NameLen:byte):string;overload;
Procedure CopyFileS(ExistingFileName, NewFileName:String);
Function DateTimeToStrMy(T:TDateTime; ShowMS:boolean=false):String;
Function DateTimeToCSVStr(T:TDateTime):String;
Function DateToStrMy(T:TDateTime):String;
Function DWordToStrMy(dw:Cardinal; Digits:byte):String;
function WindowsDirs(Folder:integer):string;
Procedure ExecuteProgram(ApplicationName:string; WaitApplication, Visible:boolean);
function MD5FromFile(FileName:String):String;
Function FileBirthday(FileName:String):TDateTime;
procedure CreateLink(const PathObj, PathLink, Desc, Param: string);
Function ChangeExt(FName, NewExt:String):string;
function StringReplace(const S, OldPattern, NewPattern: string; Flags: TReplaceFlags): string;
function DeleteSelf:boolean;
Procedure GetFileVersionD(FileName: string; var Major1, Major2, Minor1, Minor2: Integer);
Function GetFileVersionStr(FileName:string):string;

Function FileTimeToStr(ft:FileTime):string;
Function FileTimeToStrMy(ft:FileTime; ShowMS:boolean=false):string;

Function ParamStrExists(s:string):Boolean;

const CSIDL_COMMON_PROGRAMS:integer=$0017;
      CSIDL_PROGRAM_FILES = $0026; { C:\Program Files }
      CSIDL_SYSTEM = $0025; { GetSystemDirectory() }

      TicksShift:int64=504911232000000000;// = 14025312 hours = 1600 years
      TicksSecond:int64=10000000;
      TicksMinute:int64=600000000;
      TicksHour:int64=36000000000;
      TicksDay:int64=864000000000;

implementation
uses SysUtils, DateUtils, SHFolder, dialogs, IdHashMessageDigest,
     ShlObj, ComObj, ActiveX, SyncObjs, IdGlobal;

var TempFileNameCrit:TCriticalSection;

Function DateTimeToCSVStr(T:TDateTime):String;
var y,m,d,h,min,s,ms:word;
begin//21.01.2009 17:44:00
  DecodeDateTime(T,y,m,d,h,min,s,ms);
  result:=
    DWordToStrMy(d,2)+'.'+
    DWordToStrMy(m,2)+'.'+
    IntToStr(y)+'.'+
    ' '+
    DWordToStrMy(h,2)+':'+
    DWordToStrMy(min,2)+':'+
    DWordToStrMy(s,2);
  if T=0 then Result:='';
end;


Function FileTimeToStrMy(ft:FileTime; ShowMS:boolean=false):string;
var st:systemTime;
begin
  FileTimeToSystemTime(ft,st);
  Result:=DateTimeToStrMy(SystemTimeToDateTime(st), ShowMS);
end;

Function FileTimeToStr(ft:FileTime):string;
var st:systemTime;
begin
  FileTimeToSystemTime(ft,st);
  Result:=DateTimeToStr(SystemTimeToDateTime(st));
end;

Function ParamStrExists(s:string):Boolean;
var i:integer;
begin
  Result:=false;
  s:=AnsiUpperCase(s);
  if ParamCount>0 then for i:=1 to ParamCount do
    if AnsiUpperCase(ParamStr(i))=s then Result:=True;
end;

Function GetFileVersionStr(FileName:string):string;
var Major1, Major2, Minor1, Minor2: Integer;
begin
  Result:='';
  if FileExists(FileName)=false then exit;
  GetFileVersionD(FileName, Major1, Major2, Minor1, Minor2);
  Result:=IntToStr(Major1)+'.'+IntToStr(Major2)+'.'+IntToStr(Minor1)+'.'+IntToStr(Minor2);
end;

procedure GetFileVersionD(FileName: string; var Major1, Major2, Minor1, Minor2: Integer);
 var
   Info: Pointer;
   InfoSize: DWORD;
   FileInfo: PVSFixedFileInfo;
   FileInfoSize: DWORD;
   Tmp: DWORD;
 begin
   InfoSize := GetFileVersionInfoSize(PChar(FileName), Tmp);
   if InfoSize = 0 then
     //���� �� �������� ���������� � ������
   else
   begin
     GetMem(Info, InfoSize);
     try
       GetFileVersionInfo(PChar(FileName), 0, InfoSize, Info);
       VerQueryValue(Info, '\', Pointer(FileInfo), FileInfoSize);
       Major1 := FileInfo.dwFileVersionMS shr 16;
       Major2 := FileInfo.dwFileVersionMS and $FFFF;
       Minor1 := FileInfo.dwFileVersionLS shr 16;
       Minor2 := FileInfo.dwFileVersionLS and $FFFF;
     finally
       FreeMem(Info, FileInfoSize);
     end;
   end;
 end;

function GetLongPathNameW(lpszShortPath:pwidechar;lpszLongPath:pwidechar;cchBuffer:DWORD):DWORD;stdcall; external kernel32 name 'GetLongPathNameW';

function DeleteSelf:boolean;
var
  BatchFile: TextFile;
  BatchFileNameShort:String;
  BatchFileName:String;
  ExeFileNameShort:String;
  ProcessInfo: TProcessInformation;
  StartUpInfo: TStartupInfo;
begin
  Result:=false;
  BatchFileName:=ExtractFilePath(ParamStr(0))+'4selfdel.bat';
  ExeFileNameShort:=ExtractShortPathName(ParamStr(0));

  AssignFile(BatchFile, BatchFileName); Rewrite(BatchFile);
  Writeln(BatchFile,':try');
  Writeln(BatchFile,'del "'+ExeFileNameShort+'"');
  Writeln(BatchFile,'if exist "'+ExeFileNameShort+'" goto try');
  BatchFileNameShort:=ExtractShortPathName(BatchFileName);
  Writeln(BatchFile,'del "'+BatchFileNameShort+'"');
  CloseFile(BatchFile);

  FillChar(StartUpInfo, SizeOf(StartUpInfo), $00);
  StartUpInfo.dwFlags := STARTF_USESHOWWINDOW;
  StartUpInfo.wShowWindow := SW_HIDE;

  if CreateProcess(nil, PChar(BatchFileName), nil, nil,
    False, IDLE_PRIORITY_CLASS, nil, nil, StartUpInfo, ProcessInfo) then
    begin
      CloseHandle(ProcessInfo.hThread);
      CloseHandle(ProcessInfo.hProcess);
      Result:=true;
    end;
end;

function StringReplace(const S, OldPattern, NewPattern: string; Flags: TReplaceFlags): string;
var
  SearchStr, Patt, NewStr: string;
  Offset: Integer;
begin //from SysUtils
  if rfIgnoreCase in Flags then
  begin
    SearchStr := AnsiUpperCase(S);
    Patt := AnsiUpperCase(OldPattern);
  end else
  begin
    SearchStr := S;
    Patt := OldPattern;
  end;
  NewStr := S;
  Result := '';
  while SearchStr <> '' do
  begin
    Offset := AnsiPos(Patt, SearchStr);
    if Offset = 0 then
    begin
      Result := Result + NewStr;
      Break;
    end;
    Result := Result + Copy(NewStr, 1, Offset - 1) + NewPattern;
    NewStr := Copy(NewStr, Offset + Length(OldPattern), MaxInt);
    if not (rfReplaceAll in Flags) then
    begin
      Result := Result + NewStr;
      Break;
    end;
    SearchStr := Copy(SearchStr, Offset + Length(Patt), MaxInt);
  end;
end;

Function ChangeExt(FName, NewExt:String):string;
var s,d:string;
begin
  s:=ExtractFilePath(FName);
  d:=ExtractFileName(FName);
  d:=Copy(d, 1, Length(d)-Length(ExtractFileExt(d)));
  if Length(d)>0 then if d[Length(d)]='.' then Delete(d,Length(d),1);
  if Length(NewExt)>0 then d:=d+'.'+NewExt;
  Result:=s+d;
end;

procedure CreateLink(const PathObj, PathLink, Desc, Param: string);
var
  IObject: IUnknown;
  SLink: IShellLink;
  PFile: IPersistFile;
begin
  IObject := CreateComObject(CLSID_ShellLink);
  SLink := IObject as IShellLink;
  PFile := IObject as IPersistFile;
  with SLink do
  begin
    SetArguments(PChar(Param));
    SetDescription(PChar(Desc));
    SetPath(PChar(PathObj));
  end;
  PFile.Save(PWChar(WideString(PathLink)), FALSE);
end;

Function FileBirthday(FileName:String):TDateTime;
var S:TSearchRec;
begin
  if FindFirst(FileName,$3F,S)=0 then
    result:=FileDateToDateTime(s.Time) else
    result:=0;
  SysUtils.FindClose(S);
end;

function MD5FromFile(FileName:String):String;
var fd:file; Data:IdGlobal.TIdBytes;
    MD5:TIdHashMessageDigest5;
begin
  md5:=TIdHashMessageDigest5.Create;
    AssignFile(fd,FileName);
    FileMode:=0;//Set file access to read only
    Reset(fd,1);
    SetLength(Data, FileSize(fd));
    BlockRead(fd,data[0],length(data));
    CloseFile(fd);
  result:=md5.HashBytesAsHex(Data);

  SetLength(Data,0);
  md5.Free;
end;

Procedure ExecuteProgram(ApplicationName:string; WaitApplication, Visible:boolean);
var si:STARTUPINFOA; pi:PROCESS_INFORMATION;
    a:AnsiString;
begin
  FillChar(si, sizeof(si),#0);
  si.cb:=sizeof(si);
  si.dwFlags:=STARTF_USESHOWWINDOW;
  si.wShowWindow:=SW_SHOWNA;
  if UpperCase(ExtractFileExt(ApplicationName))='.BAT' then si.wShowWindow:=SW_SHOWMINNOACTIVE;
  if Visible then si.wShowWindow:=SW_SHOWDEFAULT;
  a:=ansistring(ApplicationName);
  try
    CreateProcessA(nil,pansichar(a),nil,nil,false,0,nil,nil,si,pi);
    if WaitApplication then WaitForSingleObject(pi.hProcess,INFINITE);
  finally
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
  end;
end;

function WindowsDirs(Folder:integer):string;
var path:array[0..max_path]of char;
begin
  if Succeeded( SHGetFolderPath(0, Folder, 0, SHGFP_TYPE_CURRENT, @path[0]) )then
    Result:=Path
    else
    Result:='';
end;

Function DWordToStrMy(dw:Cardinal; Digits:byte):String;
begin
  Result:=inttostr(dw);
  while Length(Result)<Digits do
    Result:='0'+Result;
end;

Function DateTimeToStrMy(T:TDateTime; ShowMS:boolean=false):String;
var y,m,d,h,min,s,ms:word;
begin//2009.09.09 17.44
  DecodeDateTime(T,y,m,d,h,min,s,ms);
  if ShowMS=false then
    result:=inttostr(y)+'.'+DWordToStrMy(m,2)+'.'+DWordToStrMy(d,2)+' '+DWordToStrMy(h,2)+'.'+DWordToStrMy(min,2) else
    result:=inttostr(y)+'.'+DWordToStrMy(m,2)+'.'+DWordToStrMy(d,2)+' '+DWordToStrMy(h,2)+'.'+DWordToStrMy(min,2)+'.'+DWordToStrMy(s,2)+'.'+DWordToStrMy(ms,3);
  if T=0 then Result:='';
end;

Function DateToStrMy(T:TDateTime):String;
var y,m,d,h,min,s,ms:word;
begin//2009.09.09
  DecodeDateTime(T,y,m,d,h,min,s,ms);
  result:=inttostr(y)+'.'+DWordToStrMy(m,2)+'.'+DWordToStrMy(d,2);
end;

Procedure CopyFileS(ExistingFileName, NewFileName:String);
var f1,f2:file; buf:array[1..10240]of byte;
    r,s:integer;
begin
  FileMode:=fmOpenRead;
  AssignFile(f1,ExistingFileName); Reset(f1,1);
  AssignFile(f2,NewFileName);      Rewrite(f2,1);
  s:=sizeof(buf);
  repeat
    BlockRead(f1,buf,s,r);
    BlockWrite(f2,buf,r);
  until r<s;
  CloseFile(f1);
  CloseFile(f2);
end;

Function GetSystemTimeAsDateTime:TDateTime;
var t:_SYSTEMTIME;
begin
  GetSystemTime(t);
  result:=SystemTimeToDateTime(t);
end;

Function DeleteFileS(FName:string):LongBool;
var f:file;
begin
  try
    AssignFile(f, fname);
    Erase(f);
    Result:=true;
  except;
    Result:=false;
  end;
end;

Function TempPathLongOld:String;
var buf,path:array[1..1024]of byte;
    i,n:integer;
begin
  result:='';
  GetTempPath(1024,@buf);
  n:=GetLongPathNameW(@buf,@path,1024);
  if (n>1)and(n<1024) then for i:=1 to n do result:=result+chr(path[i]);
end;

Function TempPathLong:String;
var Buf,Path:WideString;
    n:integer;
begin
  SetLength(Buf,1024);
  SetLength(Path,1024);
  GetTempPath(1024,@buf[1]);
  n:=GetLongPathNameW(@Buf[1],@Path[1],1024);
//  Result:=String(WideString( pwidechar(Path) ));
  SetLength(Path,n);
  Result:=String(Path);
end;

Function TempFileName(DoCreateFile:Boolean; NameLen:byte):string;//62^NameLen combinations
var s,d:string;
    i:integer;
    f:file;
const sym:array[1..62]of char=('0','1','2','3','4','5','6','7','8','9','a','A','b',
          'B','c','C','d','D','e','E','f','F','g','G','h','H','i','I','j','J','k',
          'K','l','L','m','M','n','N','o','O','p','P','q','Q','r','R','s','S','t',
          'T','u','U','v','V','w','W','x','X','y','Y','z','Z');
begin
  if NameLen<4 then NameLen:=4; if NameLen>30 then NameLen:=30;
  d:=TempPathLong;
  TempFileNameCrit.Enter;
  try
    repeat
      s:=''; for i:=1 to NameLen do s:=s+sym[random(62)+1];
      Result:=d+s+'.tmp';
    until FileSearch(Result,'')='';
    if DoCreateFile=true then
      begin
      assignfile(f,Result);
      Rewrite(f,1);
      CloseFile(f);
      end;
  finally
    TempFileNameCrit.Leave;
  end;
end;

Function TempFileName:string;//62^8=2e14 combinations
begin
  Result:=TempFileName(false, 8);
end;

initialization
  Randomize;
  TempFileNameCrit:=TCriticalSection.Create;

finalization
  TempFileNameCrit.Free;

end.
