//2011 �Nike Frolov GPL Free
//������ ��� ������ � �������� ����������������� �������
//2009.06.22 +2009.06.29 +2009.07.22 +2009.07.30 +2009.09.16(TiniCrit) +2009.10.29(Try in Save)
//2010.06.24-GetValues  2010.08.25-GetVolumes
//2010.08.26-Copy full string to Params if absent divider '='
//2011.08.10-Can correct save null value without divider '='
//2011.10.21-added default save
unit MLini;

interface
uses Classes, SyncObjs;

type
Tiniitem=record
  Volume:String;
  Param:String;
  Value:String;
end;

Tini=object
  strict private
    LastFN:string;
  public
  Data:array of Tiniitem;
  Function Load(FileName:string):boolean;
  Function Save(FileName:string):boolean;overload;
  Function Save:boolean;overload;
  Function GetValue(_Volume,_Param:String):String;
  Procedure GetParams(_Volume:String; var Params:TStringList);//new proc!!!!!!!!
  Procedure GetValues(_Volume:String; var Values:TStringList);overload;//new proc!!!!!!!!
  Procedure GetValues(_Volume:String; var Params,Values:TStringList);overload;//new proc!!!!!!!!
  Procedure GetVolumes(var Volumes:TStringList);
  Procedure DelValueEx(_Volume,_Param:String; doSortData:boolean);
  Procedure DelValue(_Volume,_Param:String);
  Procedure DelVolume(_Volume:String);
  Procedure DelVolumeEx(_Volume:String; doSortData:boolean);
  Function GetId(_Volume,_Param:String):integer;
  Function AddValue(_Volume,_Param,_Value:String):boolean;
  Procedure SortData;
  Procedure Clear;
end;

{TiniCrit=class//deprecated for UnUse ini in thread and transmit Tini in main thread
    Crit:TCriticalSection;
    Data:array of Tiniitem;
    Function Load(FileName:string):boolean;
    Function LoadEx(var Str:String):boolean;
    Function Save(FileName:string):boolean;
    Function SaveEx:String;
    Function GetValue(_Volume,_Param:String):String;
    Procedure DelValueEx(_Volume,_Param:String; doSortData:boolean);
    Procedure DelValue(_Volume,_Param:String);
    Procedure DelVolume(_Volume:String);
    Procedure DelVolumeEx(_Volume:String; doSortData:boolean);
    Function GetId(_Volume,_Param:String):integer;
    Function AddValue(_Volume,_Param,_Value:String):boolean;
    Procedure SortData;
    Procedure Clear;
    Constructor Create;
    Destructor Destroy;override;
  private
    Procedure internalSortData;
    Function internalGetId(_Volume,_Param:String):integer;
end;}

implementation
uses SysUtils, Crt2;

///////////////////// Tini /////////////////////

procedure Tini.GetVolumes(var Volumes:TStringList);
var i,j,n:integer;
begin
  if Volumes=nil then exit;
  Volumes.Clear;
  if Length(Data)=0 then Exit;
  Volumes.Add(Data[0].Volume);
  if Length(Data)>1 then for i:=1 to high(Data) do//try next
    if Data[i-1].Volume<>Data[i].Volume then
      begin
        n:=0;
        for j:=0 to Volumes.Count-1 do if Volumes[j]=Data[i].Volume then inc(n);
        if n=0 then//add new
          Volumes.Add(Data[i].Volume);
      end;
end;

Procedure Tini.Clear;
begin
  SetLength(Data, 0);
end;

Procedure Tini.SortData;
var i,n:integer; t:Tiniitem;
begin
  if Length(Data)>1 then
  repeat
    n:=0;
    for i:=1 to high(data) do
      if (data[i].Volume<data[i-1].Volume)or
         ((data[i].Volume=data[i-1].Volume)and(Data[i].Param<Data[i-1].Param))then
         begin
         t:=data[i]; Data[i]:=Data[i-1]; Data[i-1]:=t;
         inc(n);
         end;
  until n=0;
end;

Function Tini.Load(FileName:string):boolean;
var f:TextFile;
    s:string;
    c:Tiniitem;
    i,n:integer;
begin
  LastFN:=FileName;
  if FileSearch(FileName,'')='' then begin result:=false;exit;end;
  AssignFile(f, FileName); Reset(f);
  c.Volume:=''; c.Param:=''; c.Value:=''; SetLength(Data, 0);
  Repeat
    ReadLn(f,s);
    if Length(s)>2 then if (s[1]='[')and(s[length(s)]=']') then
      begin c.Volume:=Copy(s,2,Length(s)-2); s:=''; end;
    if Length(s)>1 then
      begin
      c.Param:='???';
      c.Value:='???';
      n:=-1; for i:=Length(s) downto 1 do if s[i]='=' then n:=i;
      if n=-1 then//copy full string to Param
        begin
        c.Param:=s;//must not be null parameter
        c.Value:=''; //null parameter
        end;
      if n>0 then
        begin
        c.Param:=Copy(s,1,n-1);
        c.Value:=Copy(s,n+1,Length(s)-n);//123n5678  8-4=4
        end;
      AddValue(c.Volume, c.Param, c.Value);
      end;
  Until EOF(f);
  CloseFile(f);
  Result:=True;
end;

Function Tini.Save(FileName:string):boolean;
var i:integer;
    f:TextFile;
    V:String;
begin
  LastFN:=FileName;
  SortData;
  try
  AssignFile(f,FileName); Rewrite(f);
  if Length(Data)>0 then V:=Data[0].Volume+'x';
  if Length(Data)>0 then for i:=0 to high(Data) do
    begin
    if UpperCase(v)<>UpperCase(Data[i].Volume) then
      begin
      v:=Data[i].Volume;
      WriteLn(f,'['+v+']');
      end;
    if Data[i].Value='' then WriteLn(f,Data[i].Param) else
      WriteLn(f,Data[i].Param,'=',Data[i].Value);
    end;
  CloseFile(f);
  Result:=true;
  except
    Result:=false;
  end;
end;

Function Tini.Save:boolean;
begin
  if LastFN='' then result:=false
    else Result:=Save(LastFN);
end;

Function Tini.GetId(_Volume,_Param:String):integer;
var i:integer;
begin
  Result:=-1;
  _Volume:=UpperCase(_Volume);
  _Param:=UpperCase(_Param);
  if Length(Data)>0 then for i:=0 to high(data) do
    if UpperCase(Data[i].Volume)=_Volume then
      if UpperCase(Data[i].Param)=_Param then
        begin
        Result:=i;
        Break;
        end;
end;

Function Tini.GetValue(_Volume,_Param:String):String;
var i:integer;
begin
  i:=GetId(_Volume,_Param);
  if i<0 then Result:='' else Result:=Data[i].Value;
end;

Procedure Tini.GetParams(_Volume:String; var Params:TStringList);
var i:integer;
begin
  if Params=nil then exit;
  try Params.Clear; except exit; end;
  if Length(Data)=0 then exit;
  _Volume:=uppercase(_Volume);
  for i:=0 to High(Data) do
    if _Volume=uppercase(Data[i].Volume) then
      Params.Add(Data[i].Param);
end;

Procedure Tini.GetValues(_Volume:String; var Values:TStringList);
var i:integer;
begin
  if Values=nil then exit;
  try Values.Clear; except exit; end;
  if Length(Data)=0 then exit;
  _Volume:=uppercase(_Volume);
  for i:=0 to High(Data) do
    if _Volume=uppercase(Data[i].Volume) then
      Values.Add(Data[i].Value);
end;

Procedure Tini.GetValues(_Volume:String; var Params,Values:TStringList);
var i:integer;
begin
  if Params=nil then exit;
  if Values=nil then exit;
  try Params.Clear; except exit; end;
  try Values.Clear; except exit; end;
  if Length(Data)=0 then exit;
  _Volume:=uppercase(_Volume);
  for i:=0 to High(Data) do
    if _Volume=uppercase(Data[i].Volume) then
      begin
      Params.Add(Data[i].Param);
      Values.Add(Data[i].Value);
      end;
end;

Function Tini.AddValue(_Volume,_Param,_Value:String):boolean;
var id:integer;
begin
  if _Volume='' then begin result:=false; exit;end;
  if (_Param='')and(_Value='') then begin result:=false; exit;end;
  id:=GetId(_Volume,_Param);
  if id<0 then
    begin
    SetLength(Data, Length(Data)+1);
    id:=high(Data);
    end;
  Data[id].Volume:=_Volume;
  Data[id].Param:=_Param;
  Data[id].Value:=_Value;
  Result:=true;
end;

Procedure Tini.DelValue(_Volume,_Param:String);
begin
  DelValueEx(_Volume, _Param, true);
end;

Procedure Tini.DelVolume(_Volume:String);
begin
  DelVolumeEx(_Volume, true);
end;

Procedure Tini.DelVolumeEx(_Volume:String; doSortData:boolean);
var i:integer;
begin
  if Length(Data)=0 then exit;
  _Volume:=uppercase(_Volume);
  for i:=High(Data) downto 0 do
    if _Volume=uppercase(Data[i].Volume) then
      begin//del
      if i=high(Data) then
        begin//������ � ����� ������
        SetLength(Data,Length(Data)-1);
        end else
        begin//������ � �������� ������
        Data[i].Volume:=Data[high(Data)].Volume;
        Data[i].Param:=Data[high(Data)].Param;
        Data[i].Value:=Data[high(Data)].Value;
        SetLength(Data,Length(Data)-1);
        if doSortData then SortData;
        end;
      end;
end;

Procedure Tini.DelValueEx(_Volume,_Param:String; doSortData:boolean);
var id:integer;
begin
  id:=GetId(_Volume,_Param);
  if id>-1 then
    begin
      if id=high(Data) then
        begin//������ � ����� ������
        SetLength(Data,Length(Data)-1);
        end else
        begin//������ � �������� ������
        Data[id].Volume:=Data[high(Data)].Volume;
        Data[id].Param:=Data[high(Data)].Param;
        Data[id].Value:=Data[high(Data)].Value;
        SetLength(Data,Length(Data)-1);
        if doSortData then SortData;
        end;
    end;
end;

/////////////////// TiniCrit //////////////////////
{
Constructor TiniCrit.Create;
begin
  inherited;
  Crit:=TCriticalSection.Create;
end;

Destructor TiniCrit.Destroy;
begin
  Crit.Free;
  inherited;
end;

Procedure TiniCrit.Clear;
begin
  Crit.Enter;
  SetLength(Data, 0);
  Crit.Leave;
end;

Procedure TiniCrit.internalSortData;
var i,n:integer; t:Tiniitem;
begin
  if Length(Data)>1 then
  repeat
    n:=0;
    for i:=1 to high(data) do
      if (data[i].Volume<data[i-1].Volume)or
         ((data[i].Volume=data[i-1].Volume)and(Data[i].Param<Data[i-1].Param))then
         begin
         t:=data[i]; Data[i]:=Data[i-1]; Data[i-1]:=t;
         inc(n);
         end;
  until n=0;
end;

Procedure TiniCrit.SortData;
begin
  Crit.Enter;
  internalSortData;
  Crit.Leave;
end;

Function TiniCrit.Load(FileName:string):boolean;
var f:TextFile;
    s:string;
    c:Tiniitem;
    i,n:integer;
begin
  Crit.Enter;
  Result:=true;
  if FileSearch(FileName,'')='' then result:=false;
  if result then
    begin
    AssignFile(f, FileName); Reset(f);
    c.Volume:=''; c.Param:=''; c.Value:=''; SetLength(Data, 0);
    Repeat
      ReadLn(f,s);
      if Length(s)>2 then if (s[1]='[')and(s[length(s)]=']') then
        begin c.Volume:=Copy(s,2,Length(s)-2); s:=''; end;
      if Length(s)>1 then
        begin
        c.Param:='???';
        c.Value:='???';
        n:=-1; for i:=Length(s) downto 1 do if s[i]='=' then n:=i;
        if n=-1 then//copy full string to Values
          begin
          c.Param:=s;
          c.Value:='';//null parameter
          end;
        if n>0 then
          begin
          c.Param:=Copy(s,1,n-1);
          c.Value:=Copy(s,n+1,Length(s)-n);//123n5678  8-4=4
          end;
        AddValue(c.Volume, c.Param, c.Value);
        end;
    Until EOF(f);
    CloseFile(f);
    end;
  Crit.Leave;
end;

Function TiniCrit.Save(FileName:string):boolean;
var i:integer;
    f:TextFile;
    V:String;
begin
  Crit.Enter;
  internalSortData;
  try
  AssignFile(f,FileName); Rewrite(f);
  if Length(Data)>0 then V:=Data[0].Volume+'x';
  if Length(Data)>0 then for i:=0 to high(Data) do
    begin
    if UpperCase(v)<>UpperCase(Data[i].Volume) then
      begin
      v:=Data[i].Volume;
      WriteLn(f,'['+v+']');
      end;
    WriteLn(f,Data[i].Param,'=',Data[i].Value);
    end;
  CloseFile(f);
  Result:=true;
  except
    result:=false;
  end;
  Crit.Leave;
end;

Function TiniCrit.internalGetId(_Volume,_Param:String):integer;
var i:integer;
begin
  Result:=-1;
  _Volume:=UpperCase(_Volume);
  _Param:=UpperCase(_Param);
  if Length(Data)>0 then for i:=0 to high(data) do
    if UpperCase(Data[i].Volume)=_Volume then
      if UpperCase(Data[i].Param)=_Param then
        begin
        Result:=i;
        Break;
        end;
end;

Function TiniCrit.GetId(_Volume,_Param:String):integer;
begin
  Crit.Enter;
  Result:=internalGetId(_Volume,_Param);
  Crit.Leave;
end;

Function TiniCrit.GetValue(_Volume,_Param:String):String;
var i:integer;
begin
  Crit.Enter;
  i:=internalGetId(_Volume,_Param);
  if i<0 then Result:='' else Result:=Data[i].Value;
  Crit.Leave;
end;

Function TiniCrit.AddValue(_Volume,_Param,_Value:String):boolean;
var id:integer;
begin
  Crit.Enter;
  Result:=true;
  if _Volume='' then result:=false;
  if _Param='' then result:=false;
  if result then
    begin
    id:=internalGetId(_Volume,_Param);
    if id<0 then
      begin
      SetLength(Data, Length(Data)+1);
      id:=high(Data);
      end;
    Data[id].Volume:=_Volume;
    Data[id].Param:=_Param;
    Data[id].Value:=_Value;
    end;
  Crit.Leave;
end;

Procedure TiniCrit.DelVolume(_Volume:String);
var i:integer;
begin
  Crit.Enter;
  if Length(Data)>0 then
    begin
    _Volume:=uppercase(_Volume);
    for i:=High(Data) downto 0 do
      if _Volume=uppercase(Data[i].Volume) then
        begin//del
        if i=high(Data) then
          begin//������ � ����� ������
          SetLength(Data,Length(Data)-1);
          end else
          begin//������ � �������� ������
          Data[i].Volume:=Data[high(Data)].Volume;
          Data[i].Param:=Data[high(Data)].Param;
          Data[i].Value:=Data[high(Data)].Value;
          SetLength(Data,Length(Data)-1);
          internalSortData;
          end;
        end;
    end;
  Crit.Leave;
end;

Procedure TiniCrit.DelVolumeEx(_Volume:String; doSortData:boolean);
var i:integer;
begin
  Crit.Enter;
  if Length(Data)>0 then
    begin
    _Volume:=uppercase(_Volume);
    for i:=High(Data) downto 0 do
      if _Volume=uppercase(Data[i].Volume) then
        begin//del
        if i=high(Data) then
          begin//������ � ����� ������
          SetLength(Data,Length(Data)-1);
          end else
          begin//������ � �������� ������
          Data[i].Volume:=Data[high(Data)].Volume;
          Data[i].Param:=Data[high(Data)].Param;
          Data[i].Value:=Data[high(Data)].Value;
          SetLength(Data,Length(Data)-1);
          if doSortData then InternalSortData;
          end;
        end;
    end;
  Crit.Leave;
end;

Procedure TiniCrit.DelValue(_Volume,_Param:String);
var id:integer;
begin
  Crit.Enter;
  id:=internalGetId(_Volume,_Param);
  if id>-1 then
    begin
      if id=high(Data) then
        begin//������ � ����� ������
        SetLength(Data,Length(Data)-1);
        end else
        begin//������ � �������� ������
        Data[id].Volume:=Data[high(Data)].Volume;
        Data[id].Param:=Data[high(Data)].Param;
        Data[id].Value:=Data[high(Data)].Value;
        SetLength(Data,Length(Data)-1);
        internalSortData;
        end;
    end;
  Crit.Leave;
end;

Procedure TiniCrit.DelValueEx(_Volume,_Param:String; doSortData:boolean);
var id:integer;
begin
  Crit.Enter;
  id:=internalGetId(_Volume,_Param);
  if id>-1 then
    begin
      if id=high(Data) then
        begin//������ � ����� ������
        SetLength(Data,Length(Data)-1);
        end else
        begin//������ � �������� ������
        Data[id].Volume:=Data[high(Data)].Volume;
        Data[id].Param:=Data[high(Data)].Param;
        Data[id].Value:=Data[high(Data)].Value;
        SetLength(Data,Length(Data)-1);
        if doSortData then internalSortData;
        end;
    end;
  Crit.Leave;
end;

Function TiniCrit.LoadEx(var Str:String):boolean;
var c:Tiniitem;
    k:integer;
    List,L2:TStringList;
begin//main #2 ip #2 192.168.0.1 #1
  Crit.Enter;
  List:=TStringList.Create;
  L2:=TStringList.Create;
  Result:=true;
  DelimitedStrToList(Str,List,#1);
  if List.Count<1 then result:=false;
  if result then
    begin
    c.Volume:=''; c.Param:=''; c.Value:=''; SetLength(Data, 0);
    for k:=0 to List.Count-1 do
      begin
      DelimitedStrToList(List[k], L2, #2);
      if L2.Count=3 then
        begin
        c.Volume:=L2[0]; c.Param:=L2[1]; c.Value:=L2[2];
        AddValue(c.Volume, c.Param, c.Value);
        end;
      end;
    end;
  List.Free; L2.Free;
  Crit.Leave;
end;

Function TiniCrit.SaveEx:String;
var i:integer;
begin
  Crit.Enter;
  Result:='';
  if Length(Data)>0 then for i:=0 to High(Data) do
    begin
    Result:=Result+Data[i].Volume+#2+Data[i].Param+#2+Data[i].Value;
    if i<High(Data) then Result:=Result+#1;
    end;
  Crit.Leave;
end;
}

end.
