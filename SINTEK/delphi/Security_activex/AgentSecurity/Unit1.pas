unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, SecurityLib_TLB, SECAUTOUTILLib_TLB, GENCLIENTWRAPPERLib_TLB, SysLogger;

type
  TForm1 = class(TForm)
    ButtonLogout: TButton;
    ButtonLogin: TButton;
    EditHost: TEdit;
    EditPass: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EditNode: TEdit;
    Label4: TLabel;
    EditLogin: TComboBox;
    procedure ButtonLogoutClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonLoginClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EditLoginDropDown(Sender: TObject);
    procedure EditPassKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TClaster=class
  public
    Names, Results:TStringList;
    Function CheckARM(ARM:string):Boolean;
  public
    Constructor Create;
    Destructor Destroy;override;
end;

var
  Form1: TForm1;
var //SECConfig:IConfig2;
    //SECHelper:ISECHelper;
    //SECDual:ISECDual4;
    //ServerKey:OleVariant;
    //ServerName:string;
    LocalHostName:string;
    Claster:TClaster;

implementation
uses ComObj, ActiveX, crt2, MLini, Unit2, crt4;
var Admin:record Login,Pass:string;end;
{$R *.dfm}

///////////////////////// TClaster /////////////////////////

Constructor TClaster.Create;
begin
  Names:=TStringList.Create;
  Results:=TStringList.Create;
end;

Destructor TClaster.Destroy;
begin
  Names.Free;
  Results.Free;
  inherited;
end;

Function TClaster.CheckARM(ARM:string):Boolean;
var i:integer;
begin
  result:=false;
  ARM:=UpperCase(ARM);
  if Names.Count>0 then for i:=0 to Names.Count-1 do
    if UpperCase(Names[i])=ARM then
      Result:=true;
end;

///////////////////////////////////////////////////////////

function GetHostName:string;
var s:cardinal;
    ws:WideString;
begin
  ws:=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
  s:=length(ws);
  GetComputerName(@ws[1],s);
  result:=copy(ws,1,s);
end;

Function GetSecHost:String;
var GenClient:IClient;
begin
  try
    GenClient:=CreateComObject(CLASS_Client) as IClient;
    Result:=AnsiUpperCase(GenClient.GetActiveServerNode(GC_SERVER_SECURITY));
  except
    GenClient:=nil;
    Result:='';
    Logger.Error('������! �� ������� �������� ��� ������� ������������!');
    exit;
  end;
  GenClient:=nil;
end;

procedure TForm1.ButtonLogoutClick(Sender: TObject);
var SECDual:ISECDual4; sh:string;
begin
  {$REGION '//1.read current sec server'}
  sh:=GetSecHost;
  EditHost.Text:=sh;
  {$ENDREGION}
    try
      SecDual:=CreateRemoteComObject(sh,CLASS_SECDual4) as ISECDual4;
      SecDual.Node:=LocalHostName;
    except
      SecDual:=nil;
      ShowMessage('������! �� ������� ������������ � ������� ������������!');
      exit;
    end;
    try
      SECDual.Logout(SECDual.LoggedIn);
    except
      SecDual:=nil;
      ShowMessage('������! �� ������� ��������� �������������� ������������!');
      exit;
    end;
  writeln('UserUnregistred;', EditLogin.text);
  SECDual:=nil;
end;

procedure TForm1.EditLoginDropDown(Sender: TObject);
var Config:IConfig3;
    SECHelper:ISECHelper;//local
    PasswordBlob,ServerKey:OleVariant;
    Users:IUsers3; iu:integer;
    UserInfo:IAcl3; SH:STRING;
begin//get user lists from sec server
  EditLogin.Items.Clear;
  {$REGION '//1.read current sec server'}
  sh:=GetSecHost;
  EditHost.Text:=sh;
  {$ENDREGION}
  {$REGION 'Helper;'}
  try
    SECHelper:=CreateComObject(CLASS_SECHelper)as ISECHelper;
  except
    Config:=nil; SECHelper:=nil;
    //ShowMessage('������! �� ������� ������������ � �������!');
    Logger.Error('������! �� ������� ������������ � �������!');
    exit;
  end;
  {$ENDREGION}
  {$REGION 'config;'}
  try
    Config:=CreateRemoteComObject(sh,CLASS_SECConfig3) as IConfig3;
    ServerKey:=Config.ServerPublicKey;
  except
    Config:=nil; SECHelper:=nil;
    //ShowMessage('������! �� ������� ������������ � ������� ������������!');
    Logger.Error('������! �� ������� ������������ � ������� ������������!');
    Exit;
  end;
  {$ENDREGION}
  {$REGION 'hash;'}
  try
    PasswordBlob:=SECHelper.EncryptPassword[Admin.Pass,ServerKey];
  except
    Config:=nil; SECHelper:=nil;
    //ShowMessage('������! �� ������� ��������� �����������!');
    Logger.Error('������! �� ������� ��������� �����������!');
    Exit;
  end;
  {$ENDREGION}
  {$REGION 'AdminLogin;'}
  try
    Config.AdminLogin(LocalHostName, Admin.Login, PasswordBlob);
  except
    Config:=nil; SECHelper:=nil;
    //ShowMessage('������! �� ������� ����������� �� �������!');
    Logger.Error('������! �� ������� ����������� �� �������!');
    Exit;
  end;
  {$ENDREGION}
  {$REGION 'Users;'}
  try
    Config.Users.QueryInterface(IID_IUsers3, Users);
    if Users.Count>0 then for iu:=0 to Users.Count-1 do
      begin
        Users.Item[iu].QueryInterface(IID_IAcl3, UserInfo);
        //if AnsiUpperCase(UserInfo.Name)<>AnsiUpperCase(Admin.Login) then
          EditLogin.Items.Add(UserInfo.Name);
        UserInfo:=nil;
      end;
  except
    UserInfo:=nil; Users:=nil;
    Config:=nil; SECHelper:=nil;
    //ShowMessage('������! �� ������� �������� ������ �������������!');
    Logger.Error('������! �� ������� �������� ������ �������������!');
    Exit;
  end;
  UserInfo:=nil; Users:=nil;
  Config:=nil; SECHelper:=nil;
  {$ENDREGION}
end;

procedure TForm1.EditPassKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=13 then ButtonLoginClick(Sender);
end;

procedure TForm1.ButtonLoginClick(Sender: TObject);
var PasswordBlob:OleVariant;
    ClasterMode, LastClasterMode, RemoteClasterMode, i,n:integer;
    CheckRights:integer;
var SECHelper:ISECHelper;//local
    SECDual:ISECDual4;
    ServerKey:OleVariant;
var RemoteSECDual:ISECDual4;   //for other host
    RemoteServerKey:OleVariant;
    sh,shs:string;
    //r:hResult;
begin//login
  //Allow user lists, Display last user)
  Logger.Information('Login procedure initiated');
  {$REGION '//1.read current sec server'}
  sh:=GetSecHost;
  EditHost.Text:=sh;
  Logger.Information('current sec server is "'+sh+'"');
  {$ENDREGION}
  {$REGION '//2.Connect to SecServer for Local NODE'}
    //dual
    try
      SecDual:=CreateRemoteComObject(sh,CLASS_SECDual4) as ISECDual4;
      SecDual.Node:=AnsiUpperCase(LocalHostName);
      ServerKey:=SecDual.ServerPublicKey;
    except
      SecDual:=nil;
      ShowMessage('������! �� ������� ������������ � ������� ������������!');
      exit;
    end;
    //helper
    try
      SECHelper:=CreateComObject(CLASS_SECHelper)as ISECHelper;
    except
      SECHelper:=nil;
      ShowMessage('������! �� ������� ������������ � �������!');
      exit;
    end;
{$ENDREGION}
  {$REGION '//3.read LastClasterMode'}
    LastClasterMode:=0;
    if SECDual.TestCustom['ClasterGuest'] then LastClasterMode:=3;
    if SECDual.TestCustom['ClasterDisp'] then LastClasterMode:=1;
    if SECDual.LoggedIn='' then LastClasterMode:=3;//����� �� ���������������
    Logger.Information('Last claster mode on local host is '+IntToStr(LastClasterMode)+' (3-ClasterGuest, 1-ClasterDisp)');
  {$ENDREGION}
  {$REGION '//4.check ARM with simulative login at Local NODE'}
    CheckRights:=0;
    //virtual connect
    try
      RemoteSecDual:=CreateRemoteComObject(sh,CLASS_SECDual4) as ISECDual4;
      RemoteSecDual.Node:=LocalHostName+'_VirtualLogin';
      RemoteServerKey:=RemoteSECDual.ServerPublicKey;
    except
      RemoteSecDual:=nil;
      ShowMessage('������ �� ����������� ����! �� ������� �������� ������������ � ������� ������������!');
      exit;
    end;

    try
      PasswordBlob:=SECHelper.EncryptPassword[EditPass.text,RemoteServerKey];
    except
      SECHelper:=nil;
      ShowMessage('������ �� ����������� ����! �� ������� ���������� ������!');
      exit;
    end;

    try
      RemoteSecDual.LogIn(AnsiUpperCase(EditLogin.text),PasswordBlob);
    except
      CheckRights:=-1;
      //ShowMessage('������ ����������� ������������! �������� ����� ��� ������.');//��� ����
    end;

    if (RemoteSECDual<>nil)and(CheckRights=0) then
    try
      if RemoteSecDual.TestCustom['ARM='+LocalHostName]=false then
        begin
          CheckRights:=-2;
          //ShowMessage('��� ���� �� ����������� �� ���� ���!');
        end else
        CheckRights:=1;//ok
    except
      RemoteSECDual:=nil;
      ShowMessage('������ �� ����������� ����! �� ������� ��������� ����� ������������!');
      exit;
    end;

    if RemoteSECDual<>nil then
    try
      RemoteSECDual.Logout(AnsiUpperCase(EditLogin.text));
      RemoteSECDual:=nil;
    except
    end;
    Logger.Information('CheckRights is '+IntToStr(CheckRights));

    if CheckRights=0 then
      begin
        ShowMessage('������ ����������� ������������! �� ������� ��������� �����������.');
        exit;
      end;
    if CheckRights=-1 then
      begin
        ShowMessage('������ ����������� ������������! �������� ����� ��� ������.');
        exit;
      end;
    if CheckRights=-2 then
      begin
        ShowMessage('������ ����������� ������������! ��� ���� �� ����������� �� ���� ���.');
        exit;
      end;
  {$ENDREGION}
  {$REGION '//5.Local Login'}
    try
      PasswordBlob:=SECHelper.EncryptPassword[EditPass.text,ServerKey];
    except
      PasswordBlob:=NULL;
      ShowMessage('������! �� ������� ���������� ������!');
      exit;
    end;

    if PasswordBlob<>NULL then
    try
      SECDual.LogIn(AnsiUpperCase(EditLogin.text),PasswordBlob);
    except
      ShowMessage('������ ����������� ������������! �������� ����� ��� ������.');
      exit;
    end;
  {$ENDREGION}
  writeln('UserLocalLogined;', EditLogin.text);
  //6.Check groups count
  {$REGION '//7.check ClasterMode'}
    ClasterMode:=0;
    try
      if SECDual.TestCustom['ClasterGuest'] then ClasterMode:=3;
      if SECDual.TestCustom['ClasterDisp'] then ClasterMode:=1;
    except
      ShowMessage('������! �� ������� ��������� ����� ������ ����� �������!');
      ClasterMode:=0;
    end;
    Logger.Information('Claster mode on local host is '+IntToStr(ClasterMode));
    if ClasterMode=0 then begin Form1.Close; Exit;end;
  {$ENDREGION}
  {$REGION '//8.ARM in Claster'}
    Logger.Information('ARM in claster='+BoolToStr(Claster.CheckARM(LocalHostName),true));
    if not Claster.CheckARM(LocalHostName) then begin Form1.Close; Exit;end;
  {$ENDREGION}
  {$REGION '//9.LastClasterMode'}
    if ClasterMode=3 then if LastClasterMode<>1 then
      begin
        Logger.Information('Claster mode and Last claster mode not supported for claster login');
        Exit;
      end;
  {$ENDREGION}
  {$REGION '//10.read claster'}
  //10.read claster
  {$ENDREGION}
  {$REGION '//11.ClasterLogin'}
  logger.Information('Claster names count='+IntToStr(Claster.Names.Count));
  logger.Information('LocalHostName='+LocalHostName);
    Claster.Results.Clear; Claster.Results.AddStrings(Claster.Names);
    if Claster.Names.Count>0 then
      begin
        for i:=0 to Claster.Names.Count-1 do
          if AnsiUpperCase(Claster.Names[i])=AnsiUpperCase(LocalHostName) then
            begin
              Claster.Results[i]:='';//Local ARM
            end else
          begin//login at remote ARM
            Claster.Results[i]:='';
            RemoteSECDual:=nil;
            shs:=sh; if shs='' then shs:=AnsiUpperCase(Claster.Names[i]);//���� �� ��������� ��� �������� ��������� ������ ������������, �� �� �������� ��� ��������������� ������������� ���������� ������� ������������
            logger.Information('Remote ARM='+Claster.Names[i]);
            logger.Information('Remote Server Sec Link='+shs);
            try
              RemoteSECDual:=CreateRemoteComObject(shs, CLASS_SECDual4) as ISECDual4;
            except
              RemoteSECDual:=nil;
              Claster.Results[i]:='��� �� ��������';
              Logger.Warning('Remote ARM failed at CreateRemoteComObject');
            end;
            if RemoteSecDual<>nil then
              begin
                Claster.Results[i]:='';//try
                RemoteClasterMode:=0;
                try
                  RemoteSECDual.Node:=AnsiUpperCase(Claster.Names[i]);
                  if RemoteSECDual.TestCustom['ClasterGuest'] then RemoteClasterMode:=3;//guest
                  if RemoteSECDual.TestCustom['ClasterDisp'] then RemoteClasterMode:=1;//disp
                  if RemoteSECDual.LoggedIn='' then RemoteClasterMode:=3;//����� �� ���������������
                  if RemoteClasterMode=0 then Claster.Results[i]:=RemoteSECDual.LoggedIn;
                  logger.Information('RemoteClasterMode='+inttostr(RemoteClasterMode));
                except
                  RemoteClasterMode:=0;//��� �� ��������
                  Claster.Results[i]:='��� �� ��������';
                  RemoteSECDual:=nil;
                  Logger.Warning('Remote ARM failed at TestCustom');
                end;
                if RemoteSECDual<>nil then if RemoteClasterMode>0 then//remote login
                try
                  PasswordBlob:=SECHelper.EncryptPassword[EditPass.text,RemoteSECDual.ServerPublicKey];
                  RemoteSECDual.LogIn(AnsiUpperCase(EditLogin.text),PasswordBlob);
                  Claster.Results[i]:='';//login ok
                except
                  RemoteSECDual:=nil;
                  Claster.Results[i]:='��� ����';
                end;
              end;
            RemoteSECDual:=nil;
          end;
      end;
  {$ENDREGION}
  writeln('UserClasterLogined;', EditLogin.text);
  {$REGION '//12.results'}
    Form2.StringGrid1.RowCount:=2;
    Form2.StringGrid1.Cells[0,0]:='���';
    Form2.StringGrid1.Cells[1,0]:='������������/������';
    Form2.StringGrid1.FixedRows:=1;
    n:=0;
    if Claster.Names.Count>0 then for i:=0 to Claster.Names.Count-1 do
      if Claster.Results[i]<>'' then
        begin
          inc(n);
          Form2.StringGrid1.RowCount:=n+1;
          Form2.StringGrid1.Cells[0,n]:=Claster.Names[i];//col,row
          Form2.StringGrid1.Cells[1,n]:=Claster.Results[i];//col,row
        end;
    if n>0 then StringGridAutoSizeColumns(Form2.StringGrid1);
    logger.Information('fails count='+inttostr(n));
    if n>0 then Form2.ShowModal;
    if n=0 then Form1.Close;
  {$ENDREGION}
  SECDual:=nil;
  SECHelper:=nil;
end;

procedure ReadSecServerInfo;
{var Config3:IConfig3;
    Users3:IUsers3;
    Groups3:IGroups3;
    UserInfo3:IAcl3;
    GroupInfo3:IAcl3;
    UserInEx:IIncludeExclude;
    Stations:IStrings;
    PasswordBlob:OleVariant;
    iu,i:integer;   SECHelper:ISECHelper;
    gr:string; ServerKey:OleVariant;}
begin
{  Config3:=CreateComObject(CLASS_SECConfig3) as IConfig3;
  PasswordBlob:=SECHelper.EncryptPassword['1',ServerKey];
  Config3.AdminLogin(LocalHostName, 'NIKE', PasswordBlob);
  //by user
  Config3.FindUser['NIKE'].QueryInterface(IAcl3, UserInfo3);
      //memo2.Lines.Add(' Name = '+UserInfo3.Name);
      //memo2.Lines.Add(' Description = '+UserInfo3.Description);
      //user
      UserInfo3.Stations.QueryInterface(IID_IIncludeExclude, UserInEx);
      UserInEx.Include.QueryInterface(IID_IStrings, Stations);
      if Stations.Count>0 then for i:=0 to Stations.Count-1 do
        //memo2.Lines.Add(' Stations(U) include = '+Stations.Item[i]);
      Stations:=nil;
      UserInEx:=nil;
  //group
  if UserInfo3.Count>0 then for i:=0 to UserInfo3.Count-1 do
    begin
      gr:=UserInfo3.Item[i];
      //memo2.Lines.Add('  Group = '+gr);
      Config3.FindGroup[gr].QueryInterface(IAcl3, GroupInfo3);
      //memo2.Lines.Add('  GroupDescr = '+GroupInfo3.Description);
      GroupInfo3:=nil;
    end;
  UserInfo3:=nil;
  //all users
  //memo2.Lines.Add('[Users]');
  Config3.Users.QueryInterface(IID_IUsers3, Users3);
  if Users3.Count>0 then for iu:=0 to Users3.Count-1 do
    begin
      Users3.Item[iu].QueryInterface(IID_IAcl3, UserInfo3);
      UserInfo3.Stations.QueryInterface(IID_IIncludeExclude, UserInEx);
      UserInEx.Include.QueryInterface(IID_IStrings, Stations);
      if Stations.Count>0 then for i:=0 to Stations.Count-1 do
        //memo2.Lines.Add(UserInfo3.Name+' Stations include = '+Stations.Item[i]);
      Stations:=nil;
      UserInEx:=nil;
      UserInfo3:=nil;
    end;
  Users3:=nil;
  //all groups
  //memo2.Lines.Add('[Groups]');
  Config3.Groups.QueryInterface(IID_IGroups3, Groups3);
  if Groups3.Count>0 then for iu:=0 to Groups3.Count-1 do
    begin
      Groups3.Item[iu].QueryInterface(IID_IAcl3, UserInfo3);
      UserInfo3.Stations.QueryInterface(IID_IIncludeExclude, UserInEx);
      UserInEx.Include.QueryInterface(IID_IStrings, Stations);
      if Stations.Count>0 then for i:=0 to Stations.Count-1 do
        //memo2.Lines.Add(UserInfo3.Name+' Stations include = '+Stations.Item[i]);
      Stations:=nil;
      UserInEx:=nil;
      UserInfo3:=nil;
    end;
  Users3:=nil;
  //end
  Config3:=nil;}
end;

procedure TForm1.FormCreate(Sender: TObject);
var ini,inisec:Tini;
    List:TStringList;
    path:string;
begin
  //logger
  Logger.ApplName:='AgentSec';
  Logger.LogFileEnabled:=false;
  Logger.LogApplEnabled:=true;
  Logger.LogLevel:=sletERROR;
  if ParamCount>0 then if UpperCase(ParamStr(1))='DEBUG' then
    begin
      Logger.LogLevel:=sletINFORMATION;
      Logger.Information('Debug mode on');
    end;
  //load ini file
  path:=ExtractFilePath(ParamStr(0));
  Claster:=TClaster.Create;
  List:=TStringList.Create;
  Logger.Information('ini path='+path+'agentsec.ini');
  if FileExists(path+'agentsec.ini') then
    begin
      ini.Load(path+'agentsec.ini');
      ini.GetParams('Claster',List);
      Logger.Information('ini len='+IntToStr(List.Count));
      if List.count>0 then Claster.Names.AddStrings(List);
    end;
  List.Free;
  //load security ini
  inisec.Load(WindowsDirs(CSIDL_SYSTEM)+'\agentsec.ini');
  Admin.Login:=inisec.GetValue('Admin','Login');
  if Admin.Login='' then Admin.login:='������';
  Admin.Pass:=inisec.GetValue('Admin','Pass');
  if Admin.Pass='' then Admin.Pass:='1';

  //COM
  CoInitializeEx(nil, COINIT_MULTITHREADED);
  //Server
  EditHost.Text:=GetSecHost;
  //Node
  LocalHostName:=UpperCase(GetHostName);
  EditNode.Text:=LocalHostName;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Claster.Free;
end;

end.
