program AgentSec;
{$APPTYPE CONSOLE}
uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  GENCLIENTWRAPPERLib_TLB in 'GENCLIENTWRAPPERLib_TLB.pas' {$R *.res},
  crt2 in '!!units\crt2.pas',
  CRT4 in '!!units\CRT4.pas',
  MLini in '!!units\MLini.pas',
  MLMath in '!!units\MLMath.pas',
  MLTimeZone in '!!units\MLTimeZone.pas',
  MLTypes in '!!units\MLTypes.pas',
  SysLogger in '!!units\SysLogger.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
