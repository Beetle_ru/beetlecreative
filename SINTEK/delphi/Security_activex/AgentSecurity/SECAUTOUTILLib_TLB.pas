unit SECAUTOUTILLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 16059 $
// File generated on 28.06.2011 14:15:07 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ICONICS\SecAutoUtil.dll (1)
// LIBID: {DA8A534D-8E38-11D3-A5E5-00104B0D13C3}
// LCID: 0
// Helpfile: 
// HelpString: ICONICS Security Automation Helper 2.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SECAUTOUTILLibMajorVersion = 2;
  SECAUTOUTILLibMinorVersion = 0;

  LIBID_SECAUTOUTILLib: TGUID = '{DA8A534D-8E38-11D3-A5E5-00104B0D13C3}';

  IID_ISECHelper: TGUID = '{DA8A535B-8E38-11D3-A5E5-00104B0D13C3}';
  CLASS_SECHelper: TGUID = '{DA8A535C-8E38-11D3-A5E5-00104B0D13C3}';
  IID_ISECHelper2: TGUID = '{824B49BC-0435-457C-BEE5-07319433AA4D}';
  CLASS_SECHelper2: TGUID = '{CA4E293C-A766-4291-AB0A-601979330186}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISECHelper = interface;
  ISECHelperDisp = dispinterface;
  ISECHelper2 = interface;
  ISECHelper2Disp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SECHelper = ISECHelper;
  SECHelper2 = ISECHelper2;


// *********************************************************************//
// Interface: ISECHelper
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DA8A535B-8E38-11D3-A5E5-00104B0D13C3}
// *********************************************************************//
  ISECHelper = interface(IDispatch)
    ['{DA8A535B-8E38-11D3-A5E5-00104B0D13C3}']
    function Get_EncryptPassword(const Password: WideString; ServerPublicKey: OleVariant): OleVariant; safecall;
    property EncryptPassword[const Password: WideString; ServerPublicKey: OleVariant]: OleVariant read Get_EncryptPassword;
  end;

// *********************************************************************//
// DispIntf:  ISECHelperDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DA8A535B-8E38-11D3-A5E5-00104B0D13C3}
// *********************************************************************//
  ISECHelperDisp = dispinterface
    ['{DA8A535B-8E38-11D3-A5E5-00104B0D13C3}']
    property EncryptPassword[const Password: WideString; ServerPublicKey: OleVariant]: OleVariant readonly dispid 1;
  end;

// *********************************************************************//
// Interface: ISECHelper2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {824B49BC-0435-457C-BEE5-07319433AA4D}
// *********************************************************************//
  ISECHelper2 = interface(IDispatch)
    ['{824B49BC-0435-457C-BEE5-07319433AA4D}']
    function Get_PrimaryServer: WideString; safecall;
    procedure Set_PrimaryServer(const pVal: WideString); safecall;
    function Get_BackupServer: WideString; safecall;
    procedure Set_BackupServer(const pVal: WideString); safecall;
    property PrimaryServer: WideString read Get_PrimaryServer write Set_PrimaryServer;
    property BackupServer: WideString read Get_BackupServer write Set_BackupServer;
  end;

// *********************************************************************//
// DispIntf:  ISECHelper2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {824B49BC-0435-457C-BEE5-07319433AA4D}
// *********************************************************************//
  ISECHelper2Disp = dispinterface
    ['{824B49BC-0435-457C-BEE5-07319433AA4D}']
    property PrimaryServer: WideString dispid 1;
    property BackupServer: WideString dispid 2;
  end;

// *********************************************************************//
// The Class CoSECHelper provides a Create and CreateRemote method to          
// create instances of the default interface ISECHelper exposed by              
// the CoClass SECHelper. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECHelper = class
    class function Create: ISECHelper;
    class function CreateRemote(const MachineName: string): ISECHelper;
  end;

// *********************************************************************//
// The Class CoSECHelper2 provides a Create and CreateRemote method to          
// create instances of the default interface ISECHelper2 exposed by              
// the CoClass SECHelper2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECHelper2 = class
    class function Create: ISECHelper2;
    class function CreateRemote(const MachineName: string): ISECHelper2;
  end;

implementation

uses ComObj;

class function CoSECHelper.Create: ISECHelper;
begin
  Result := CreateComObject(CLASS_SECHelper) as ISECHelper;
end;

class function CoSECHelper.CreateRemote(const MachineName: string): ISECHelper;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECHelper) as ISECHelper;
end;

class function CoSECHelper2.Create: ISECHelper2;
begin
  Result := CreateComObject(CLASS_SECHelper2) as ISECHelper2;
end;

class function CoSECHelper2.CreateRemote(const MachineName: string): ISECHelper2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECHelper2) as ISECHelper2;
end;

end.
