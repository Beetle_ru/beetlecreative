library TEstActivex;

uses
  ComServ,
  TEstActivex_TLB in 'TEstActivex_TLB.pas',
  Unit1 in 'Unit1.pas' {TestAXForm: TActiveForm} {TestAXForm: CoClass};

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
