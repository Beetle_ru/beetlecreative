unit GENCLIENTWRAPPERLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 16059 $
// File generated on 28.06.2011 14:17:59 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ICONICS\GenClientWrapper.dll (1)
// LIBID: {F4D1C576-BA64-424A-9600-229174634B35}
// LCID: 0
// Helpfile: 
// HelpString: GenClientWrapper 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GENCLIENTWRAPPERLibMajorVersion = 1;
  GENCLIENTWRAPPERLibMinorVersion = 0;

  LIBID_GENCLIENTWRAPPERLib: TGUID = '{F4D1C576-BA64-424A-9600-229174634B35}';

  DIID__IClientEvents: TGUID = '{7ABDE6A2-D75F-410B-B718-7522CA1F286C}';
  IID_IOpcHelper: TGUID = '{34C8CE9A-6024-4FF6-8582-F897F8B1A48B}';
  IID_IClient: TGUID = '{FE88D54F-EC19-4F15-B5A4-EC101634ACC4}';
  CLASS_Client: TGUID = '{98E3806C-F1ED-4246-AF7D-510BB238B4FD}';
  IID_IDataPoint: TGUID = '{52E2E604-DDFB-45E4-AA7F-7407983A65B9}';
  IID_ISecurityPoint: TGUID = '{A73AE90D-9EB8-4CD5-A51F-ABECEE545819}';
  CLASS_DataPoint: TGUID = '{D8199828-378B-47C2-8A26-E34243FA6E58}';
  CLASS_SecurityPoint: TGUID = '{732F918F-5C1A-4835-AD06-646A66197D8F}';
  CLASS_OpcHelper: TGUID = '{4CBC0320-A0D0-419D-914B-4EE1C77B86D5}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0001
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0001 = TOleEnum;
const
  GC_POINT_UNKNOWN = $00000000;
  GC_POINT_FAIL = $00000001;
  GC_POINT_OK_VIRGIN = $00000002;
  GC_POINT_OK_UPDATED = $00000003;

// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0004
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0004 = TOleEnum;
const
  GC_QUALITY_BAD = $00000000;
  GC_QUALITY_UNCERTAIN = $00000040;
  GC_QUALITY_GOOD = $000000C0;

// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0005
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0005 = TOleEnum;
const
  GC_S_OK = $00000000;
  GC_S_FALSE = $00000001;
  GC_S_CLAMP = $0004000E;
  GC_E_FAIL = $80004005;
  GC_E_BADRIGHTS = $C0040006;
  GC_E_INVALIDHANDLE = $C0040001;
  GC_E_UNKNOWNITEMID = $C0040007;
  GC_E_RANGE = $C004000B;
  GC_E_BADTYPE = $C0040004;
  GC_E_NOTSUPPORTED = $C0040406;

// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0002
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0002 = TOleEnum;
const
  GC_SECURITY_FILENAME = $00000001;
  GC_SECURITY_OPCITEM = $00000002;
  GC_SECURITY_FUNCTION = $00000003;
  GC_SECURITY_CUSTOM = $00000004;

// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0003
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0003 = TOleEnum;
const
  GC_PROPERTY_CANONICALDATATYPE = $00000001;
  GC_PROPERTY_ACCESSRIGHTS = $00000005;
  GC_PROPERTY_ENGUNITS = $00000064;
  GC_PROPERTY_DESCRIPTION = $00000065;
  GC_PROPERTY_HIGHEURANGE = $00000066;
  GC_PROPERTY_LOWEURANGE = $00000067;
  GC_PROPERTY_CLOSELABEL = $0000006A;
  GC_PROPERTY_OPENLABEL = $0000006B;

// Constants for enum __MIDL___MIDL_itf_GenClientWrapper_0000_0006
type
  __MIDL___MIDL_itf_GenClientWrapper_0000_0006 = TOleEnum;
const
  GC_SERVER_SECURITY = $00000000;
  GC_SERVER_LICENSING = $00000001;
  GC_SERVER_GLOBAL_ALIASING = $00000002;
  GC_SERVER_LANGUAGE = $00000003;
  GC_SERVER_GENEVENT = $00000004;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _IClientEvents = dispinterface;
  IOpcHelper = interface;
  IOpcHelperDisp = dispinterface;
  IClient = interface;
  IClientDisp = dispinterface;
  IDataPoint = interface;
  IDataPointDisp = dispinterface;
  ISecurityPoint = interface;
  ISecurityPointDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Client = IClient;
  DataPoint = IDataPoint;
  SecurityPoint = ISecurityPoint;
  OpcHelper = IOpcHelper;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//

  GC_POINT_STATE = __MIDL___MIDL_itf_GenClientWrapper_0000_0001; 
  GC_DATA_POINT_QUALITY = __MIDL___MIDL_itf_GenClientWrapper_0000_0004; 
  GC_RESULT = __MIDL___MIDL_itf_GenClientWrapper_0000_0005; 
  GC_SECURITY_POINT_TYPE = __MIDL___MIDL_itf_GenClientWrapper_0000_0002; 
  GC_POINT_PROPERTY = __MIDL___MIDL_itf_GenClientWrapper_0000_0003; 
  GC_SERVER_TYPE = __MIDL___MIDL_itf_GenClientWrapper_0000_0006; 

// *********************************************************************//
// DispIntf:  _IClientEvents
// Flags:     (4096) Dispatchable
// GUID:      {7ABDE6A2-D75F-410B-B718-7522CA1F286C}
// *********************************************************************//
  _IClientEvents = dispinterface
    ['{7ABDE6A2-D75F-410B-B718-7522CA1F286C}']
  end;

// *********************************************************************//
// Interface: IOpcHelper
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {34C8CE9A-6024-4FF6-8582-F897F8B1A48B}
// *********************************************************************//
  IOpcHelper = interface(IDispatch)
    ['{34C8CE9A-6024-4FF6-8582-F897F8B1A48B}']
    function Read(const pointName: WideString; out quality: OleVariant; out timestamp: OleVariant): OleVariant; safecall;
    procedure Write(const pointName: WideString; value: OleVariant); safecall;
    function Get_ScanRate: Integer; safecall;
    procedure Set_ScanRate(pVal: Integer); safecall;
    function Get_ReadTimeout: Integer; safecall;
    procedure Set_ReadTimeout(pVal: Integer); safecall;
    function Get_WriteTimeout: Integer; safecall;
    procedure Set_WriteTimeout(pVal: Integer); safecall;
    function Get_PointLifetime: Integer; safecall;
    procedure Set_PointLifetime(pVal: Integer); safecall;
    function Get_LastError: Integer; safecall;
    function GetRanges(const pointName: WideString; out loRange: OleVariant; out hiRange: OleVariant): WordBool; safecall;
    function GetAccessRights(const pointName: WideString; out isReadable: OleVariant; 
                             out isWriteable: OleVariant): WordBool; safecall;
    property ScanRate: Integer read Get_ScanRate write Set_ScanRate;
    property ReadTimeout: Integer read Get_ReadTimeout write Set_ReadTimeout;
    property WriteTimeout: Integer read Get_WriteTimeout write Set_WriteTimeout;
    property PointLifetime: Integer read Get_PointLifetime write Set_PointLifetime;
    property LastError: Integer read Get_LastError;
  end;

// *********************************************************************//
// DispIntf:  IOpcHelperDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {34C8CE9A-6024-4FF6-8582-F897F8B1A48B}
// *********************************************************************//
  IOpcHelperDisp = dispinterface
    ['{34C8CE9A-6024-4FF6-8582-F897F8B1A48B}']
    function Read(const pointName: WideString; out quality: OleVariant; out timestamp: OleVariant): OleVariant; dispid 1;
    procedure Write(const pointName: WideString; value: OleVariant); dispid 2;
    property ScanRate: Integer dispid 3;
    property ReadTimeout: Integer dispid 4;
    property WriteTimeout: Integer dispid 5;
    property PointLifetime: Integer dispid 6;
    property LastError: Integer readonly dispid 7;
    function GetRanges(const pointName: WideString; out loRange: OleVariant; out hiRange: OleVariant): WordBool; dispid 8;
    function GetAccessRights(const pointName: WideString; out isReadable: OleVariant; 
                             out isWriteable: OleVariant): WordBool; dispid 9;
  end;

// *********************************************************************//
// Interface: IClient
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FE88D54F-EC19-4F15-B5A4-EC101634ACC4}
// *********************************************************************//
  IClient = interface(IDispatch)
    ['{FE88D54F-EC19-4F15-B5A4-EC101634ACC4}']
    function RequestDataPoint(const bstrName: WideString; lScanRate: Integer; lRequestType: Integer): IDataPoint; safecall;
    function RequestSecurityPoint(SecType: GC_SECURITY_POINT_TYPE; const bstrName: WideString; 
                                  nFunctionID: Integer): ISecurityPoint; safecall;
    function GetPointProperty(const bstrPointName: WideString; nPropertyID: GC_POINT_PROPERTY): OleVariant; safecall;
    procedure ShowStatistics; safecall;
    function GetLoggedInUsers: OleVariant; safecall;
    function Succeeded(gcResult: GC_RESULT): Integer; safecall;
    function LogIn(const UserName: WideString; const Password: WideString): Integer; safecall;
    function LogOut(const UserName: WideString): Integer; safecall;
    function ChangePassword(const UserName: WideString; const OldPassword: WideString; 
                            const NewPassword: WideString): Integer; safecall;
    function SendSimpleEvent(const Source: WideString; Time: TDateTime; Severity: Integer; 
                             const Message: WideString; const Node: WideString; 
                             const Comment: WideString): Integer; safecall;
    function SendTrackingEvent(const Source: WideString; Time: TDateTime; Category: Integer; 
                               Severity: Integer; const Message: WideString; 
                               const Node: WideString; const Comment: WideString; 
                               const Param: WideString; PrevValue: OleVariant; NewValue: OleVariant): Integer; safecall;
    function SendTrackingEventEx(const Source: WideString; Time: TDateTime; Category: Integer; 
                                 Severity: Integer; const Message: WideString; 
                                 const Node: WideString; const Comment: WideString; 
                                 const User: WideString; const Param: WideString; 
                                 PrevValue: OleVariant; NewValue: OleVariant): Integer; safecall;
    function GetActiveServerNode(server: GC_SERVER_TYPE): OleVariant; safecall;
    function GetPrimaryServerNode(server: GC_SERVER_TYPE): OleVariant; safecall;
    function GetBackupServerNode(server: GC_SERVER_TYPE): OleVariant; safecall;
    function IsNodeLocal(const strNode: WideString): WordBool; safecall;
    function LocalTimeToUTC(timeLocal: OleVariant): OleVariant; safecall;
    function UTCToLocalTime(timeUTC: OleVariant): OleVariant; safecall;
    function IsWebHMI: WordBool; safecall;
    function GetLoggedInGroups: OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IClientDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FE88D54F-EC19-4F15-B5A4-EC101634ACC4}
// *********************************************************************//
  IClientDisp = dispinterface
    ['{FE88D54F-EC19-4F15-B5A4-EC101634ACC4}']
    function RequestDataPoint(const bstrName: WideString; lScanRate: Integer; lRequestType: Integer): IDataPoint; dispid 1;
    function RequestSecurityPoint(SecType: GC_SECURITY_POINT_TYPE; const bstrName: WideString; 
                                  nFunctionID: Integer): ISecurityPoint; dispid 2;
    function GetPointProperty(const bstrPointName: WideString; nPropertyID: GC_POINT_PROPERTY): OleVariant; dispid 3;
    procedure ShowStatistics; dispid 4;
    function GetLoggedInUsers: OleVariant; dispid 5;
    function Succeeded(gcResult: GC_RESULT): Integer; dispid 6;
    function LogIn(const UserName: WideString; const Password: WideString): Integer; dispid 7;
    function LogOut(const UserName: WideString): Integer; dispid 8;
    function ChangePassword(const UserName: WideString; const OldPassword: WideString; 
                            const NewPassword: WideString): Integer; dispid 9;
    function SendSimpleEvent(const Source: WideString; Time: TDateTime; Severity: Integer; 
                             const Message: WideString; const Node: WideString; 
                             const Comment: WideString): Integer; dispid 10;
    function SendTrackingEvent(const Source: WideString; Time: TDateTime; Category: Integer; 
                               Severity: Integer; const Message: WideString; 
                               const Node: WideString; const Comment: WideString; 
                               const Param: WideString; PrevValue: OleVariant; NewValue: OleVariant): Integer; dispid 11;
    function SendTrackingEventEx(const Source: WideString; Time: TDateTime; Category: Integer; 
                                 Severity: Integer; const Message: WideString; 
                                 const Node: WideString; const Comment: WideString; 
                                 const User: WideString; const Param: WideString; 
                                 PrevValue: OleVariant; NewValue: OleVariant): Integer; dispid 12;
    function GetActiveServerNode(server: GC_SERVER_TYPE): OleVariant; dispid 13;
    function GetPrimaryServerNode(server: GC_SERVER_TYPE): OleVariant; dispid 14;
    function GetBackupServerNode(server: GC_SERVER_TYPE): OleVariant; dispid 15;
    function IsNodeLocal(const strNode: WideString): WordBool; dispid 16;
    function LocalTimeToUTC(timeLocal: OleVariant): OleVariant; dispid 17;
    function UTCToLocalTime(timeUTC: OleVariant): OleVariant; dispid 18;
    function IsWebHMI: WordBool; dispid 19;
    function GetLoggedInGroups: OleVariant; dispid 20;
  end;

// *********************************************************************//
// Interface: IDataPoint
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {52E2E604-DDFB-45E4-AA7F-7407983A65B9}
// *********************************************************************//
  IDataPoint = interface(IDispatch)
    ['{52E2E604-DDFB-45E4-AA7F-7407983A65B9}']
    function Get_value: OleVariant; safecall;
    procedure Set_value(pVal: OleVariant); safecall;
    function Get_State: GC_POINT_STATE; safecall;
    function Get_quality: GC_DATA_POINT_QUALITY; safecall;
    function Get_timestamp(out pvMilliseconds: OleVariant): TDateTime; safecall;
    function Get_WritesPending: Integer; safecall;
    procedure GetValueEtc(out value: OleVariant; out quality: OleVariant; 
                          out timestamp: OleVariant; out TimestampMilliseconds: OleVariant); safecall;
    procedure SyncWrite(newVal: OleVariant; MillisecondsTimeout: Integer); safecall;
    function Get_LastWriteError: GC_RESULT; safecall;
    property value: OleVariant read Get_value write Set_value;
    property State: GC_POINT_STATE read Get_State;
    property quality: GC_DATA_POINT_QUALITY read Get_quality;
    property timestamp[out pvMilliseconds: OleVariant]: TDateTime read Get_timestamp;
    property WritesPending: Integer read Get_WritesPending;
    property LastWriteError: GC_RESULT read Get_LastWriteError;
  end;

// *********************************************************************//
// DispIntf:  IDataPointDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {52E2E604-DDFB-45E4-AA7F-7407983A65B9}
// *********************************************************************//
  IDataPointDisp = dispinterface
    ['{52E2E604-DDFB-45E4-AA7F-7407983A65B9}']
    property value: OleVariant dispid 1;
    property State: GC_POINT_STATE readonly dispid 2;
    property quality: GC_DATA_POINT_QUALITY readonly dispid 3;
    property timestamp[out pvMilliseconds: OleVariant]: TDateTime readonly dispid 4;
    property WritesPending: Integer readonly dispid 5;
    procedure GetValueEtc(out value: OleVariant; out quality: OleVariant; 
                          out timestamp: OleVariant; out TimestampMilliseconds: OleVariant); dispid 6;
    procedure SyncWrite(newVal: OleVariant; MillisecondsTimeout: Integer); dispid 7;
    property LastWriteError: GC_RESULT readonly dispid 8;
  end;

// *********************************************************************//
// Interface: ISecurityPoint
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A73AE90D-9EB8-4CD5-A51F-ABECEE545819}
// *********************************************************************//
  ISecurityPoint = interface(IDispatch)
    ['{A73AE90D-9EB8-4CD5-A51F-ABECEE545819}']
    function Get_Enabled: Integer; safecall;
    function Get_State: GC_POINT_STATE; safecall;
    property Enabled: Integer read Get_Enabled;
    property State: GC_POINT_STATE read Get_State;
  end;

// *********************************************************************//
// DispIntf:  ISecurityPointDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A73AE90D-9EB8-4CD5-A51F-ABECEE545819}
// *********************************************************************//
  ISecurityPointDisp = dispinterface
    ['{A73AE90D-9EB8-4CD5-A51F-ABECEE545819}']
    property Enabled: Integer readonly dispid 1;
    property State: GC_POINT_STATE readonly dispid 2;
  end;

// *********************************************************************//
// The Class CoClient provides a Create and CreateRemote method to          
// create instances of the default interface IClient exposed by              
// the CoClass Client. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoClient = class
    class function Create: IClient;
    class function CreateRemote(const MachineName: string): IClient;
  end;

// *********************************************************************//
// The Class CoDataPoint provides a Create and CreateRemote method to          
// create instances of the default interface IDataPoint exposed by              
// the CoClass DataPoint. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDataPoint = class
    class function Create: IDataPoint;
    class function CreateRemote(const MachineName: string): IDataPoint;
  end;

// *********************************************************************//
// The Class CoSecurityPoint provides a Create and CreateRemote method to          
// create instances of the default interface ISecurityPoint exposed by              
// the CoClass SecurityPoint. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSecurityPoint = class
    class function Create: ISecurityPoint;
    class function CreateRemote(const MachineName: string): ISecurityPoint;
  end;

// *********************************************************************//
// The Class CoOpcHelper provides a Create and CreateRemote method to          
// create instances of the default interface IOpcHelper exposed by              
// the CoClass OpcHelper. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOpcHelper = class
    class function Create: IOpcHelper;
    class function CreateRemote(const MachineName: string): IOpcHelper;
  end;

implementation

uses ComObj;

class function CoClient.Create: IClient;
begin
  Result := CreateComObject(CLASS_Client) as IClient;
end;

class function CoClient.CreateRemote(const MachineName: string): IClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Client) as IClient;
end;

class function CoDataPoint.Create: IDataPoint;
begin
  Result := CreateComObject(CLASS_DataPoint) as IDataPoint;
end;

class function CoDataPoint.CreateRemote(const MachineName: string): IDataPoint;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DataPoint) as IDataPoint;
end;

class function CoSecurityPoint.Create: ISecurityPoint;
begin
  Result := CreateComObject(CLASS_SecurityPoint) as ISecurityPoint;
end;

class function CoSecurityPoint.CreateRemote(const MachineName: string): ISecurityPoint;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SecurityPoint) as ISecurityPoint;
end;

class function CoOpcHelper.Create: IOpcHelper;
begin
  Result := CreateComObject(CLASS_OpcHelper) as IOpcHelper;
end;

class function CoOpcHelper.CreateRemote(const MachineName: string): IOpcHelper;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OpcHelper) as IOpcHelper;
end;

end.
