unit GENREGISTRARSERVERLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 16059 $
// File generated on 28.06.2011 14:17:30 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ICONICS\GenRegistrarServer.exe (1)
// LIBID: {DF77EB13-FEC4-11D1-A487-00A0244886EE}
// LCID: 0
// Helpfile: 
// HelpString: GenRegistrarServer 1.5 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  GENREGISTRARSERVERLibMajorVersion = 1;
  GENREGISTRARSERVERLibMinorVersion = 5;

  LIBID_GENREGISTRARSERVERLib: TGUID = '{DF77EB13-FEC4-11D1-A487-00A0244886EE}';

  IID_IGenRegistrar: TGUID = '{DF77EB20-FEC4-11D1-A487-00A0244886EE}';
  IID_IGenRegistrar2: TGUID = '{FF235191-5832-4DCC-887E-DC9D734382AB}';
  IID_IRegistrarLocator: TGUID = '{6613ACBA-1BE8-11D2-A4FF-00104B0D13C3}';
  IID_IRegistrarLocator2: TGUID = '{91C1D9B9-4E13-42F1-B4F2-1CA6DAFBDD76}';
  IID_IRegistrarLocator3: TGUID = '{862200A0-2C53-4915-A0F1-66904B5F7C01}';
  IID_IRegistrarLocator4: TGUID = '{13E39F56-F969-42FC-9DBE-0A236E222FE5}';
  CLASS_RegistrarLocator: TGUID = '{6613ACBB-1BE8-11D2-A4FF-00104B0D13C3}';
  IID_IPeer: TGUID = '{36FA841B-1CB0-11D2-A4FF-00104B0D13C3}';
  IID_IPeer2: TGUID = '{F32B9F04-49E4-417F-A4D7-97398E9B55E5}';
  IID_IPeer3: TGUID = '{F2926606-CE91-4F4F-9E49-57A0C0006225}';
  CLASS_Peer: TGUID = '{36FA841C-1CB0-11D2-A4FF-00104B0D13C3}';
  IID_IGenEvent: TGUID = '{066A12FF-809E-11D2-A53B-00104B0D13C3}';
  IID_IGenEvent2: TGUID = '{3ED88308-916B-11D3-A5E7-00104B0D13C3}';
  IID_IGenEvent3: TGUID = '{62389CDD-5FDB-4E3A-8149-0AC4051BEF3A}';
  CLASS_GenEvent: TGUID = '{066A1300-809E-11D2-A53B-00104B0D13C3}';
  CLASS_RegistrarLocator2: TGUID = '{9C115B21-856E-11D3-A5E0-00104B0D13C3}';
  IID_IAdmin: TGUID = '{E75FF46E-A9B6-11D3-A606-00104B0D13C3}';
  CLASS_Admin: TGUID = '{E75FF46F-A9B6-11D3-A606-00104B0D13C3}';
  IID_IRemoteRegistrarLocator: TGUID = '{72BF576A-2FE2-11D4-A635-00104B0D13C3}';
  CLASS_RemoteRegistrarLocator: TGUID = '{72BF576B-2FE2-11D4-A635-00104B0D13C3}';
  CLASS_DispatchRegistrar: TGUID = '{6904D57F-0A3B-48AE-9CB0-CF1DEDACCD38}';
  CLASS_RegistrarLocator7_0: TGUID = '{EDF723D1-2305-4FE2-AC33-FE7B1AE62E64}';
  CLASS_RemoteRegistrarLocator7_0: TGUID = '{09BD0530-884B-4295-A9D7-134AB995C7DE}';
  CLASS_Peer7_0: TGUID = '{B0FED17E-CA14-4AE7-951C-909AE4CE6698}';
  CLASS_RegistrarLocator8_0: TGUID = '{E6364F36-0F67-4782-9066-2C374DB7D991}';
  CLASS_RemoteRegistrarLocator8_0: TGUID = '{157DB789-9BAD-4D90-AF5A-45FB4D2215AC}';
  CLASS_Peer8_0: TGUID = '{89D1C5A2-6714-4389-8B3B-4029EB3433D1}';
  CLASS_RegistrarLocator9_0: TGUID = '{AF931C07-D3E3-4A98-B7D3-2F48A9047F82}';
  CLASS_RemoteRegistrarLocator9_0: TGUID = '{9DDF9E01-E227-4224-A437-9F33204C6AA0}';
  CLASS_Peer9_0: TGUID = '{F45BB1A8-20AA-45FA-965B-C83EA58DE517}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_GenRegistrarServer_0000_0001
type
  __MIDL___MIDL_itf_GenRegistrarServer_0000_0001 = TOleEnum;
const
  GREG_STATUS_LICENSED = $00000001;
  GREG_STATUS_DEMO = $00000002;
  GREG_STATUS_DEMO_TIMEOUT = $00000003;
  GREG_STATUS_NOT_CONNECTED = $00000004;

// Constants for enum __MIDL___MIDL_itf_GenRegistrarServer_0217_0001
type
  __MIDL___MIDL_itf_GenRegistrarServer_0217_0001 = TOleEnum;
const
  OPERATOR_PROCESS_CHANGE = $00000000;
  ADVANCED = $00000001;
  SYSCONFIG = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IGenRegistrar = interface;
  IGenRegistrar2 = interface;
  IRegistrarLocator = interface;
  IRegistrarLocator2 = interface;
  IRegistrarLocator3 = interface;
  IRegistrarLocator4 = interface;
  IPeer = interface;
  IPeer2 = interface;
  IPeer3 = interface;
  IGenEvent = interface;
  IGenEvent2 = interface;
  IGenEvent3 = interface;
  IAdmin = interface;
  IRemoteRegistrarLocator = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  RegistrarLocator = IRegistrarLocator;
  Peer = IPeer;
  GenEvent = IGenEvent;
  RegistrarLocator2 = IRegistrarLocator;
  Admin = IAdmin;
  RemoteRegistrarLocator = IRemoteRegistrarLocator;
  DispatchRegistrar = IGenRegistrar;
  RegistrarLocator7_0 = IRegistrarLocator;
  RemoteRegistrarLocator7_0 = IRemoteRegistrarLocator;
  Peer7_0 = IPeer;
  RegistrarLocator8_0 = IRegistrarLocator;
  RemoteRegistrarLocator8_0 = IRemoteRegistrarLocator;
  Peer8_0 = IPeer;
  RegistrarLocator9_0 = IRegistrarLocator;
  RemoteRegistrarLocator9_0 = IRemoteRegistrarLocator;
  Peer9_0 = IPeer;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PPWideChar1 = ^PWideChar; {*}
  PIDispatch1 = ^IDispatch; {*}
  PUserType1 = ^TGUID; {*}
  PUserType2 = ^GREG_SERVERSTATUS; {*}
  PHResult1 = ^HResult; {*}
  PWord1 = ^Word; {*}
  PUserType3 = ^GREG_SERVERSTATUS2; {*}

  _FILETIME = record
    dwLowDateTime: LongWord;
    dwHighDateTime: LongWord;
  end;

  __MIDL___MIDL_itf_GenRegistrarServer_0000_0002 = record
    szNode: PWideChar;
    ftStartTime: _FILETIME;
    ftCurrentTime: _FILETIME;
    ftDemoOverTime: _FILETIME;
    dwServerState: __MIDL___MIDL_itf_GenRegistrarServer_0000_0001;
    dwCUTotal: LongWord;
    dwCUIssued: LongWord;
    dwPointCount: LongWord;
    dwPointsInUse: LongWord;
    wMajorVersion: Word;
    wMinorVersion: Word;
    wBuildNumber: Word;
    dwGenLicBits: LongWord;
    lDaysRemaining: Integer;
  end;

  GREG_SERVERSTATUS = __MIDL___MIDL_itf_GenRegistrarServer_0000_0002; 

{$ALIGN 2}
  __MIDL___MIDL_itf_GenRegistrarServer_0000_0003 = record
    wPocketNodesTotal: Word;
    wPocketNodesInUse: Word;
    wMobileUsersTotal: Word;
    wMobileUsersInUse: Word;
  end;

  GREG_SERVERSTATUS2 = __MIDL___MIDL_itf_GenRegistrarServer_0000_0003; 
  TRACKING_CATEGORY = __MIDL___MIDL_itf_GenRegistrarServer_0217_0001; 

// *********************************************************************//
// Interface: IGenRegistrar
// Flags:     (0)
// GUID:      {DF77EB20-FEC4-11D1-A487-00A0244886EE}
// *********************************************************************//
  IGenRegistrar = interface(IUnknown)
    ['{DF77EB20-FEC4-11D1-A487-00A0244886EE}']
    function RegisterApp(bIsBrowser: Integer; szNode: PWideChar; szAppName: PWideChar): HResult; stdcall;
    function AddDispatch(szKey: PWideChar; const pDisp: IDispatch): HResult; stdcall;
    function RemoveDispatch(const pDisp: IDispatch): HResult; stdcall;
    function GetDispatch(szKey: PWideChar; szAppName: PWideChar; szNode: PWideChar; 
                         out ppDisp: IDispatch): HResult; stdcall;
    function QueryDispatch(szKey: PWideChar; szAppName: PWideChar; szNode: PWideChar; 
                           out pdwCount: LongWord; out ppszKeys: PPWideChar1; 
                           out ppszAppNames: PPWideChar1; out ppszNodes: PPWideChar1; 
                           out pppDispatachs: PIDispatch1): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IGenRegistrar2
// Flags:     (0)
// GUID:      {FF235191-5832-4DCC-887E-DC9D734382AB}
// *********************************************************************//
  IGenRegistrar2 = interface(IUnknown)
    ['{FF235191-5832-4DCC-887E-DC9D734382AB}']
    function SetKey(const pDisp: IDispatch; szKey: PWideChar): HResult; stdcall;
    function GetKey(const pDisp: IDispatch; out ppKey: PWideChar): HResult; stdcall;
    function QueryDispatch2(const szKey: WideString; const szAppName: WideString; 
                            const szNode: WideString; out pdwCount: OleVariant; 
                            out ppszKeys: OleVariant; out ppszAppNames: OleVariant; 
                            out ppszNodes: OleVariant; out pppDispatachs: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IRegistrarLocator
// Flags:     (0)
// GUID:      {6613ACBA-1BE8-11D2-A4FF-00104B0D13C3}
// *********************************************************************//
  IRegistrarLocator = interface(IUnknown)
    ['{6613ACBA-1BE8-11D2-A4FF-00104B0D13C3}']
    function RegisterApp(szAppName: PWideChar; var riid: TGUID; out ppUnk: IUnknown): HResult; stdcall;
    function GetStatus(out ppStatus: PUserType2): HResult; stdcall;
    function AddItems(dwNumItems: LongWord; var szItemNames: PWideChar; out ppErrors: PHResult1): HResult; stdcall;
    function RemoveItems(dwNumItems: LongWord; var szItemNames: PWideChar): HResult; stdcall;
    function SetBrowser(bIsBrowser: Integer): HResult; stdcall;
    function SetControllingNode(szNode: PWideChar): HResult; stdcall;
    function IsBrowser(out pbIsBrowser: Integer): HResult; stdcall;
    function AllocatedCUDetails(out pdwCount: LongWord; out ppwClientUnits: PWord1; 
                                out ppszAppNames: PPWideChar1; out ppszNodeNames: PPWideChar1): HResult; stdcall;
    function GetErrorString(dwError: HResult; out ppString: PWideChar): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IRegistrarLocator2
// Flags:     (0)
// GUID:      {91C1D9B9-4E13-42F1-B4F2-1CA6DAFBDD76}
// *********************************************************************//
  IRegistrarLocator2 = interface(IUnknown)
    ['{91C1D9B9-4E13-42F1-B4F2-1CA6DAFBDD76}']
    function SetBackupNode(szNode: PWideChar): HResult; stdcall;
    function GetBackupNode(out ppString: PWideChar): HResult; stdcall;
    function GetPrimaryNode(out ppString: PWideChar): HResult; stdcall;
    function SetAllowWebHMIChanges(bAllowWebHMIChanges: Integer): HResult; stdcall;
    function GetAllowWebHMIChanges(out pbAllowWebHMIChanges: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IRegistrarLocator3
// Flags:     (0)
// GUID:      {862200A0-2C53-4915-A0F1-66904B5F7C01}
// *********************************************************************//
  IRegistrarLocator3 = interface(IUnknown)
    ['{862200A0-2C53-4915-A0F1-66904B5F7C01}']
    function GetStatus2(out ppStatus2: PUserType3): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IRegistrarLocator4
// Flags:     (0)
// GUID:      {13E39F56-F969-42FC-9DBE-0A236E222FE5}
// *********************************************************************//
  IRegistrarLocator4 = interface(IUnknown)
    ['{13E39F56-F969-42FC-9DBE-0A236E222FE5}']
    function GetOptionSet2(out pwOptionSet2: Word): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IPeer
// Flags:     (0)
// GUID:      {36FA841B-1CB0-11D2-A4FF-00104B0D13C3}
// *********************************************************************//
  IPeer = interface(IUnknown)
    ['{36FA841B-1CB0-11D2-A4FF-00104B0D13C3}']
    function RegisterApp(bIsBrowser: Integer; szNode: PWideChar; szAppName: PWideChar; 
                         var riid: TGUID; out ppUnk: IUnknown): HResult; stdcall;
    function GetStatus(out ppStatus: PUserType2): HResult; stdcall;
    function AllocatedCUDetails(out pdwCount: LongWord; out ppwClientUnits: PWord1; 
                                out ppszAppNames: PPWideChar1; out ppszNodeNames: PPWideChar1): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IPeer2
// Flags:     (0)
// GUID:      {F32B9F04-49E4-417F-A4D7-97398E9B55E5}
// *********************************************************************//
  IPeer2 = interface(IUnknown)
    ['{F32B9F04-49E4-417F-A4D7-97398E9B55E5}']
    function GetStatus2(out ppStatus2: PUserType3): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IPeer3
// Flags:     (0)
// GUID:      {F2926606-CE91-4F4F-9E49-57A0C0006225}
// *********************************************************************//
  IPeer3 = interface(IUnknown)
    ['{F2926606-CE91-4F4F-9E49-57A0C0006225}']
    function GetOptionSet2(out pwOptionSet2: Word): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IGenEvent
// Flags:     (0)
// GUID:      {066A12FF-809E-11D2-A53B-00104B0D13C3}
// *********************************************************************//
  IGenEvent = interface(IUnknown)
    ['{066A12FF-809E-11D2-A53B-00104B0D13C3}']
    function ReportIconicsTrackingEvent(szSource: PWideChar; ftTime: _FILETIME; 
                                        cat: TRACKING_CATEGORY; dwSeverity: LongWord; 
                                        szMsg: PWideChar; szNode: PWideChar; szComment: PWideChar; 
                                        szParam: PWideChar; vPrevValue: OleVariant; 
                                        vNewValue: OleVariant): HResult; stdcall;
    function ReportTrackingEvent(szSource: PWideChar; ftTime: _FILETIME; cat: TRACKING_CATEGORY; 
                                 dwSeverity: LongWord; szMsg: PWideChar; szNode: PWideChar; 
                                 szUser: PWideChar; szComment: PWideChar; szParam: PWideChar; 
                                 vPrevValue: OleVariant; vNewValue: OleVariant): HResult; stdcall;
    function ReportSimpleEvent(szSource: PWideChar; ftTime: _FILETIME; dwSeverity: LongWord; 
                               szMsg: PWideChar; szNode: PWideChar; szComment: PWideChar): HResult; stdcall;
    function Get_PrimaryRelayNode(out pVal: WideString): HResult; stdcall;
    function Set_PrimaryRelayNode(const pVal: WideString): HResult; stdcall;
    function Get_BackupRelayNode(out pVal: WideString): HResult; stdcall;
    function Set_BackupRelayNode(const pVal: WideString): HResult; stdcall;
    function Get_AlwaysLogLocal(out pVal: Integer): HResult; stdcall;
    function Set_AlwaysLogLocal(pVal: Integer): HResult; stdcall;
    function Get_EventLogSeverity(out pVal: LongWord): HResult; stdcall;
    function Set_EventLogSeverity(pVal: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IGenEvent2
// Flags:     (0)
// GUID:      {3ED88308-916B-11D3-A5E7-00104B0D13C3}
// *********************************************************************//
  IGenEvent2 = interface(IUnknown)
    ['{3ED88308-916B-11D3-A5E7-00104B0D13C3}']
    function Get_NTLogApplication(szApp: PWideChar; out pVal: Integer): HResult; stdcall;
    function Set_NTLogApplication(szApp: PWideChar; pVal: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IGenEvent3
// Flags:     (0)
// GUID:      {62389CDD-5FDB-4E3A-8149-0AC4051BEF3A}
// *********************************************************************//
  IGenEvent3 = interface(IUnknown)
    ['{62389CDD-5FDB-4E3A-8149-0AC4051BEF3A}']
    function Get_ForceLocalTimestamp(out pVal: Integer): HResult; stdcall;
    function Set_ForceLocalTimestamp(pVal: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IAdmin
// Flags:     (0)
// GUID:      {E75FF46E-A9B6-11D3-A606-00104B0D13C3}
// *********************************************************************//
  IAdmin = interface(IUnknown)
    ['{E75FF46E-A9B6-11D3-A606-00104B0D13C3}']
    function RefreshLicense: HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IRemoteRegistrarLocator
// Flags:     (0)
// GUID:      {72BF576A-2FE2-11D4-A635-00104B0D13C3}
// *********************************************************************//
  IRemoteRegistrarLocator = interface(IRegistrarLocator)
    ['{72BF576A-2FE2-11D4-A635-00104B0D13C3}']
    function SetNode(szNodeName: PWideChar): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoRegistrarLocator provides a Create and CreateRemote method to          
// create instances of the default interface IRegistrarLocator exposed by              
// the CoClass RegistrarLocator. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegistrarLocator = class
    class function Create: IRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoPeer provides a Create and CreateRemote method to          
// create instances of the default interface IPeer exposed by              
// the CoClass Peer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPeer = class
    class function Create: IPeer;
    class function CreateRemote(const MachineName: string): IPeer;
  end;

// *********************************************************************//
// The Class CoGenEvent provides a Create and CreateRemote method to          
// create instances of the default interface IGenEvent exposed by              
// the CoClass GenEvent. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGenEvent = class
    class function Create: IGenEvent;
    class function CreateRemote(const MachineName: string): IGenEvent;
  end;

// *********************************************************************//
// The Class CoRegistrarLocator2 provides a Create and CreateRemote method to          
// create instances of the default interface IRegistrarLocator exposed by              
// the CoClass RegistrarLocator2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegistrarLocator2 = class
    class function Create: IRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoAdmin provides a Create and CreateRemote method to          
// create instances of the default interface IAdmin exposed by              
// the CoClass Admin. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAdmin = class
    class function Create: IAdmin;
    class function CreateRemote(const MachineName: string): IAdmin;
  end;

// *********************************************************************//
// The Class CoRemoteRegistrarLocator provides a Create and CreateRemote method to          
// create instances of the default interface IRemoteRegistrarLocator exposed by              
// the CoClass RemoteRegistrarLocator. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRemoteRegistrarLocator = class
    class function Create: IRemoteRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoDispatchRegistrar provides a Create and CreateRemote method to          
// create instances of the default interface IGenRegistrar exposed by              
// the CoClass DispatchRegistrar. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDispatchRegistrar = class
    class function Create: IGenRegistrar;
    class function CreateRemote(const MachineName: string): IGenRegistrar;
  end;

// *********************************************************************//
// The Class CoRegistrarLocator7_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRegistrarLocator exposed by              
// the CoClass RegistrarLocator7_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegistrarLocator7_0 = class
    class function Create: IRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoRemoteRegistrarLocator7_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRemoteRegistrarLocator exposed by              
// the CoClass RemoteRegistrarLocator7_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRemoteRegistrarLocator7_0 = class
    class function Create: IRemoteRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoPeer7_0 provides a Create and CreateRemote method to          
// create instances of the default interface IPeer exposed by              
// the CoClass Peer7_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPeer7_0 = class
    class function Create: IPeer;
    class function CreateRemote(const MachineName: string): IPeer;
  end;

// *********************************************************************//
// The Class CoRegistrarLocator8_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRegistrarLocator exposed by              
// the CoClass RegistrarLocator8_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegistrarLocator8_0 = class
    class function Create: IRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoRemoteRegistrarLocator8_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRemoteRegistrarLocator exposed by              
// the CoClass RemoteRegistrarLocator8_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRemoteRegistrarLocator8_0 = class
    class function Create: IRemoteRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoPeer8_0 provides a Create and CreateRemote method to          
// create instances of the default interface IPeer exposed by              
// the CoClass Peer8_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPeer8_0 = class
    class function Create: IPeer;
    class function CreateRemote(const MachineName: string): IPeer;
  end;

// *********************************************************************//
// The Class CoRegistrarLocator9_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRegistrarLocator exposed by              
// the CoClass RegistrarLocator9_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegistrarLocator9_0 = class
    class function Create: IRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoRemoteRegistrarLocator9_0 provides a Create and CreateRemote method to          
// create instances of the default interface IRemoteRegistrarLocator exposed by              
// the CoClass RemoteRegistrarLocator9_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRemoteRegistrarLocator9_0 = class
    class function Create: IRemoteRegistrarLocator;
    class function CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
  end;

// *********************************************************************//
// The Class CoPeer9_0 provides a Create and CreateRemote method to          
// create instances of the default interface IPeer exposed by              
// the CoClass Peer9_0. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPeer9_0 = class
    class function Create: IPeer;
    class function CreateRemote(const MachineName: string): IPeer;
  end;

implementation

uses ComObj;

class function CoRegistrarLocator.Create: IRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RegistrarLocator) as IRegistrarLocator;
end;

class function CoRegistrarLocator.CreateRemote(const MachineName: string): IRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegistrarLocator) as IRegistrarLocator;
end;

class function CoPeer.Create: IPeer;
begin
  Result := CreateComObject(CLASS_Peer) as IPeer;
end;

class function CoPeer.CreateRemote(const MachineName: string): IPeer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Peer) as IPeer;
end;

class function CoGenEvent.Create: IGenEvent;
begin
  Result := CreateComObject(CLASS_GenEvent) as IGenEvent;
end;

class function CoGenEvent.CreateRemote(const MachineName: string): IGenEvent;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_GenEvent) as IGenEvent;
end;

class function CoRegistrarLocator2.Create: IRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RegistrarLocator2) as IRegistrarLocator;
end;

class function CoRegistrarLocator2.CreateRemote(const MachineName: string): IRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegistrarLocator2) as IRegistrarLocator;
end;

class function CoAdmin.Create: IAdmin;
begin
  Result := CreateComObject(CLASS_Admin) as IAdmin;
end;

class function CoAdmin.CreateRemote(const MachineName: string): IAdmin;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Admin) as IAdmin;
end;

class function CoRemoteRegistrarLocator.Create: IRemoteRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RemoteRegistrarLocator) as IRemoteRegistrarLocator;
end;

class function CoRemoteRegistrarLocator.CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RemoteRegistrarLocator) as IRemoteRegistrarLocator;
end;

class function CoDispatchRegistrar.Create: IGenRegistrar;
begin
  Result := CreateComObject(CLASS_DispatchRegistrar) as IGenRegistrar;
end;

class function CoDispatchRegistrar.CreateRemote(const MachineName: string): IGenRegistrar;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DispatchRegistrar) as IGenRegistrar;
end;

class function CoRegistrarLocator7_0.Create: IRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RegistrarLocator7_0) as IRegistrarLocator;
end;

class function CoRegistrarLocator7_0.CreateRemote(const MachineName: string): IRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegistrarLocator7_0) as IRegistrarLocator;
end;

class function CoRemoteRegistrarLocator7_0.Create: IRemoteRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RemoteRegistrarLocator7_0) as IRemoteRegistrarLocator;
end;

class function CoRemoteRegistrarLocator7_0.CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RemoteRegistrarLocator7_0) as IRemoteRegistrarLocator;
end;

class function CoPeer7_0.Create: IPeer;
begin
  Result := CreateComObject(CLASS_Peer7_0) as IPeer;
end;

class function CoPeer7_0.CreateRemote(const MachineName: string): IPeer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Peer7_0) as IPeer;
end;

class function CoRegistrarLocator8_0.Create: IRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RegistrarLocator8_0) as IRegistrarLocator;
end;

class function CoRegistrarLocator8_0.CreateRemote(const MachineName: string): IRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegistrarLocator8_0) as IRegistrarLocator;
end;

class function CoRemoteRegistrarLocator8_0.Create: IRemoteRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RemoteRegistrarLocator8_0) as IRemoteRegistrarLocator;
end;

class function CoRemoteRegistrarLocator8_0.CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RemoteRegistrarLocator8_0) as IRemoteRegistrarLocator;
end;

class function CoPeer8_0.Create: IPeer;
begin
  Result := CreateComObject(CLASS_Peer8_0) as IPeer;
end;

class function CoPeer8_0.CreateRemote(const MachineName: string): IPeer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Peer8_0) as IPeer;
end;

class function CoRegistrarLocator9_0.Create: IRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RegistrarLocator9_0) as IRegistrarLocator;
end;

class function CoRegistrarLocator9_0.CreateRemote(const MachineName: string): IRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegistrarLocator9_0) as IRegistrarLocator;
end;

class function CoRemoteRegistrarLocator9_0.Create: IRemoteRegistrarLocator;
begin
  Result := CreateComObject(CLASS_RemoteRegistrarLocator9_0) as IRemoteRegistrarLocator;
end;

class function CoRemoteRegistrarLocator9_0.CreateRemote(const MachineName: string): IRemoteRegistrarLocator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RemoteRegistrarLocator9_0) as IRemoteRegistrarLocator;
end;

class function CoPeer9_0.Create: IPeer;
begin
  Result := CreateComObject(CLASS_Peer9_0) as IPeer;
end;

class function CoPeer9_0.CreateRemote(const MachineName: string): IPeer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Peer9_0) as IPeer;
end;

end.
