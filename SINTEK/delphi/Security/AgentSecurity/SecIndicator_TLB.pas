unit SecIndicator_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 16059 $
// File generated on 28.06.2011 14:15:36 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ICONICS\SecIndicator.ocx (1)
// LIBID: {82B2D809-D446-44F5-A255-FB1533C2CA04}
// LCID: 0
// Helpfile: 
// HelpString: ICONICS Security Indicator 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SecIndicatorMajorVersion = 1;
  SecIndicatorMinorVersion = 0;

  LIBID_SecIndicator: TGUID = '{82B2D809-D446-44F5-A255-FB1533C2CA04}';

  CLASS_CGeneral: TGUID = '{BF2D65F3-0F9C-4171-9692-DABB44765F64}';
  CLASS_COptions: TGUID = '{CC0A00E2-4B33-4907-9AC2-25E09FDEE1B5}';
  DIID__ISecIndEvents: TGUID = '{393B448B-6139-4276-8034-B26C25DF1666}';
  IID_ISecInd: TGUID = '{4DBAB1F5-CF85-43C0-B67C-50F60CD32B23}';
  CLASS_CSecInd: TGUID = '{C59C957C-4F1A-4D5F-9649-5519BDD1A0B1}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _ISecIndEvents = dispinterface;
  ISecInd = interface;
  ISecIndDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CGeneral = IUnknown;
  COptions = IUnknown;
  CSecInd = ISecInd;


// *********************************************************************//
// DispIntf:  _ISecIndEvents
// Flags:     (4096) Dispatchable
// GUID:      {393B448B-6139-4276-8034-B26C25DF1666}
// *********************************************************************//
  _ISecIndEvents = dispinterface
    ['{393B448B-6139-4276-8034-B26C25DF1666}']
  end;

// *********************************************************************//
// Interface: ISecInd
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DBAB1F5-CF85-43C0-B67C-50F60CD32B23}
// *********************************************************************//
  ISecInd = interface(IDispatch)
    ['{4DBAB1F5-CF85-43C0-B67C-50F60CD32B23}']
    procedure Set_AutoSize(pbool: WordBool); safecall;
    function Get_AutoSize: WordBool; safecall;
    procedure Set_BackColor(pclr: OLE_COLOR); safecall;
    function Get_BackColor: OLE_COLOR; safecall;
    procedure Set_ForeColor(pclr: OLE_COLOR); safecall;
    function Get_ForeColor: OLE_COLOR; safecall;
    procedure _Set_Font(const ppFont: IFontDisp); safecall;
    procedure Set_Font(const ppFont: IFontDisp); safecall;
    function Get_Font: IFontDisp; safecall;
    procedure Set_Caption(const pstrCaption: WideString); safecall;
    function Get_Caption: WideString; safecall;
    function Get_FileEnabled: WordBool; safecall;
    procedure Set_FileEnabled(pVal: WordBool); safecall;
    function Get_ShowNode: WordBool; safecall;
    procedure Set_ShowNode(pVal: WordBool); safecall;
    function Get_ShowUsers: WordBool; safecall;
    procedure Set_ShowUsers(pVal: WordBool); safecall;
    procedure Set_ServerPrompt(const pVal: WideString); safecall;
    function Get_ServerPrompt: WideString; safecall;
    procedure Set_UsersPrompt(const pVal: WideString); safecall;
    function Get_UsersPrompt: WideString; safecall;
    procedure Set_UpdatePeriod(pVal: Integer); safecall;
    function Get_UpdatePeriod: Integer; safecall;
    procedure Set_FileName(const pVal: WideString); safecall;
    function Get_FileName: WideString; safecall;
    procedure Set_URLPath(const pVal: WideString); safecall;
    function Get_URLPath: WideString; safecall;
    procedure FileLoad; safecall;
    procedure FileSave; safecall;
    function Get_Users: WideString; safecall;
    function Get_ServerNode: WideString; safecall;
    procedure Dispose; safecall;
    property AutoSize: WordBool read Get_AutoSize write Set_AutoSize;
    property BackColor: OLE_COLOR read Get_BackColor write Set_BackColor;
    property ForeColor: OLE_COLOR read Get_ForeColor write Set_ForeColor;
    property Font: IFontDisp read Get_Font write Set_Font;
    property Caption: WideString read Get_Caption write Set_Caption;
    property FileEnabled: WordBool read Get_FileEnabled write Set_FileEnabled;
    property ShowNode: WordBool read Get_ShowNode write Set_ShowNode;
    property ShowUsers: WordBool read Get_ShowUsers write Set_ShowUsers;
    property ServerPrompt: WideString read Get_ServerPrompt write Set_ServerPrompt;
    property UsersPrompt: WideString read Get_UsersPrompt write Set_UsersPrompt;
    property UpdatePeriod: Integer read Get_UpdatePeriod write Set_UpdatePeriod;
    property FileName: WideString read Get_FileName write Set_FileName;
    property URLPath: WideString read Get_URLPath write Set_URLPath;
    property Users: WideString read Get_Users;
    property ServerNode: WideString read Get_ServerNode;
  end;

// *********************************************************************//
// DispIntf:  ISecIndDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DBAB1F5-CF85-43C0-B67C-50F60CD32B23}
// *********************************************************************//
  ISecIndDisp = dispinterface
    ['{4DBAB1F5-CF85-43C0-B67C-50F60CD32B23}']
    property AutoSize: WordBool dispid -500;
    property BackColor: OLE_COLOR dispid -501;
    property ForeColor: OLE_COLOR dispid -513;
    property Font: IFontDisp dispid -512;
    property Caption: WideString dispid -518;
    property FileEnabled: WordBool dispid 1;
    property ShowNode: WordBool dispid 2;
    property ShowUsers: WordBool dispid 3;
    property ServerPrompt: WideString dispid 8;
    property UsersPrompt: WideString dispid 9;
    property UpdatePeriod: Integer dispid 10;
    property FileName: WideString dispid 4;
    property URLPath: WideString dispid 5;
    procedure FileLoad; dispid 6;
    procedure FileSave; dispid 7;
    property Users: WideString readonly dispid 11;
    property ServerNode: WideString readonly dispid 12;
    procedure Dispose; dispid 13;
  end;

// *********************************************************************//
// The Class CoCGeneral provides a Create and CreateRemote method to          
// create instances of the default interface IUnknown exposed by              
// the CoClass CGeneral. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCGeneral = class
    class function Create: IUnknown;
    class function CreateRemote(const MachineName: string): IUnknown;
  end;

// *********************************************************************//
// The Class CoCOptions provides a Create and CreateRemote method to          
// create instances of the default interface IUnknown exposed by              
// the CoClass COptions. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCOptions = class
    class function Create: IUnknown;
    class function CreateRemote(const MachineName: string): IUnknown;
  end;

implementation

uses ComObj;

class function CoCGeneral.Create: IUnknown;
begin
  Result := CreateComObject(CLASS_CGeneral) as IUnknown;
end;

class function CoCGeneral.CreateRemote(const MachineName: string): IUnknown;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CGeneral) as IUnknown;
end;

class function CoCOptions.Create: IUnknown;
begin
  Result := CreateComObject(CLASS_COptions) as IUnknown;
end;

class function CoCOptions.CreateRemote(const MachineName: string): IUnknown;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_COptions) as IUnknown;
end;

end.
