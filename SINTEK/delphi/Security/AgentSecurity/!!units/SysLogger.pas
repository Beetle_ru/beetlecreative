unit SysLogger;//2011 �Nike Frolov GPL Free

interface
type TSysLoggerEventType=(sletEmpty, sletERROR, sletWARNING, sletSUCCESS, sletINFORMATION, sletDEBUG);
                        //sletOFF
type TSysLogger=class
  private
    _LogFile:Boolean;
    _LogAppl:Boolean;
    _ApplName:String;
    _FileName:String;
    h:THandle;
    Procedure SetApplName(s:string);
  public
    LogLevel:TSysLoggerEventType;
    Property LogFileEnabled:Boolean read _LogFile write _LogFile;
    Property LogApplEnabled:Boolean read _LogAppl write _LogAppl;
    Property ApplName:String read _ApplName write SetApplName;
    Property FileName:String read _FileName write _FileName;
    Procedure Generate(EventType:TSysLoggerEventType; Text:string);
    constructor Create;
    destructor Destroy;override;
  public//for compatibility
    Procedure Success(Text:String);overload;
    Procedure Success(ClassName,Method,Text:String);overload;
    Procedure Error(Text:String);overload;
    Procedure Error(ClassName,Method,Text:String);overload;
    Procedure Warning(Text:String);overload;
    Procedure Warning(ClassName,Method,Text:String);overload;
    Procedure Information(Text:String);overload;
    Procedure Information(ClassName,Method,Text:String);overload;
    Procedure Debug(Text:String);overload;
    Procedure Debug(ClassName,Method,Text:String);overload;
    //Procedure ToFile(FileName, Text:String);
end;

Procedure LogToFile(FileName, Text:AnsiString);

var Logger:TSysLogger;

implementation
uses SysUtils, Windows;

Procedure LogToFile(FileName, Text:AnsiString);
var f:file;
begin
  try
    AssignFile(f, FileName); Rewrite(f,1);
    BlockWrite(f, Text[1], Length(Text));
    CloseFile(f);
  except
  end;
end;

Procedure TSysLogger.SetApplName(s:string);
begin
  _ApplName:=s;
  DeregisterEventSource(h);
  h:=RegisterEventSource('',pchar(_ApplName));
end;

constructor TSysLogger.Create;
begin
  LogLevel:=sletERROR;
  LogFileEnabled:=False;//defaults
  LogApplEnabled:=True;
  ApplName:='SysLogger';
  FileName:=ExtractFilePath(ApplName)+ExtractFileName(ApplName)+'.log';
  h:=RegisterEventSource('',pchar(ApplName));
end;

destructor TSysLogger.Destroy;
begin
  DeregisterEventSource(h);
  inherited;
end;

Procedure TSysLogger.Generate(EventType:TSysLoggerEventType; Text: string);
var ret:word; ms:string; f:textfile;
    st:_SystemTime;
    dt:TDateTime;
const
  EVENTLOG_SUCCESS          = $0000;
  EVENTLOG_ERROR_TYPE       = $0001;
  EVENTLOG_WARNING_TYPE     = $0002;
  EVENTLOG_INFORMATION_TYPE = $0004;
begin
  if EventType>LogLevel then exit;
  case EventType of
    sletSUCCESS:     begin ret:=EVENTLOG_SUCCESS;          ms:='SUCCESS'; end;
    sletERROR:       begin ret:=EVENTLOG_ERROR_TYPE;       ms:='ERROR'; end;
    sletWARNING:     begin ret:=EVENTLOG_WARNING_TYPE;     ms:='WARNING'; end;
    sletINFORMATION: begin ret:=EVENTLOG_INFORMATION_TYPE; ms:='INFORMATION'; end;
    sletDEBUG:       begin ret:=EVENTLOG_INFORMATION_TYPE; ms:='DEBUG'; end;
    else             begin ret:=EVENTLOG_INFORMATION_TYPE; ms:='empty'; end;
  end;
  if LogApplEnabled then
    try
      ReportEvent(h, ret, 0, S_False, nil, 1, 0, @Text, nil);
    Except
      LogApplEnabled:=false;
    end;
  if LogFileEnabled then
    try
      AssignFile(f,FileName);
      if FileExists(FileName) then Append(f) else Rewrite(f);
        GetSystemTime(st);
        dt:=SystemTimeToDateTime(st);
      WriteLn(f,DateTimeToStr(dt),';',ms,';',Text);
    finally
      CloseFile(f);
    end;
end;

Procedure TSysLogger.Success(Text:String);
begin
  Generate(sletSUCCESS, Text);
end;
Procedure TSysLogger.Success(ClassName,Method,Text:String);
begin
  Generate(sletSUCCESS, ClassName+'.'+Method+' '+Text);
end;

Procedure TSysLogger.Error(Text:String);
begin
  Generate(sletERROR, Text);
end;
Procedure TSysLogger.Error(ClassName,Method,Text:String);
begin
  Generate(sletERROR, ClassName+'.'+Method+' '+Text);
end;

Procedure TSysLogger.Warning(Text:String);
begin
  Generate(sletWARNING, Text);
end;
Procedure TSysLogger.Warning(ClassName,Method,Text:String);
begin
  Generate(sletWARNING, ClassName+'.'+Method+' '+Text);
end;

Procedure TSysLogger.Information(Text:String);
begin
  Generate(sletINFORMATION, Text);
end;
Procedure TSysLogger.Information(ClassName,Method,Text:String);
begin
  Generate(sletINFORMATION, ClassName+'.'+Method+' '+Text);
end;

Procedure TSysLogger.Debug(Text:String);
begin
  Generate(sletDEBUG, Text);
end;
Procedure TSysLogger.Debug(ClassName,Method,Text:String);
begin
  Generate(sletDEBUG, ClassName+'.'+Method+' '+Text);
end;

initialization
  Logger:=TSysLogger.Create;

finalization
  Logger.Destroy;
  //Logger.Free;

end.
