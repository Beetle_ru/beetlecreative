//+21.07.05 +25.09.2005 +08.02.2006-04.04.2006 +27.10.2010 +22.08.2011 +23.05.2013
unit crt2;

interface
uses classes, grids, SysUtils, Windows;

  function UpString(s:string):string;{Rus(win)&Eng}
  procedure UpString2(var s:string);{Rus(win)&Eng}
  function UpCharRus(s:char):char;
  function LowStrRus(s:string):string;
  function SortedInsert(s:string; var list:Tstrings):integer;

Procedure DelimitedStrToList(DStr:String; var List:TStringList; Delimiter:char);
function DelimitedStrLastStr(DStr:String; Delimiter:char):string;
function ListToStr(var List:TStringList; Delimiter:char):string;

Function GUIDMatch(G1,G2:TGUID):Boolean;
Function GUIDToHex(GUID:TGUID):String;
Function GUIDToText(GUID:TGUID):String;
Function HexToGUID(s:string):TGUID;
Function StrToGUID(s:string):TGUID;overload;
Function StrToGUID(s:AnsiString):TGUID;overload;
Function TryStrToGUID(s:string; id:TGUID):boolean;overload;
Function TryStrToGUID(s:AnsiString; id:TGUID):boolean;overload;

function ExportResource(ResName,ResType,ExportFileName:string):hResult;

Procedure StringGridAutoSizeColumnsByHead(var S:TStringGrid);
Procedure StringGridSetSizeColumnsByHead(var S:TStringGrid);
Procedure StringGridAutoSizeColumns(var S:TStringGrid);

Function ActualUTCTimeToStr(t:extended):string;

Function RoundFP2(e:extended):extended;

function ExtractRes(ResName,FileName:string):hResult;overload;
function ExtractRes(ResName:string):string;overload;

Function AnsiToDos(s:string):string;

Function StrToFloatDefSmart(s:string; DefValue:extended):extended;overload;

Function IntToStrAnsi(i:integer):AnsiString;overload;
Function IntToStrAnsi(i:int64):AnsiString;overload;
Function FloatToStrAnsi(i:Extended):AnsiString;overload;
Function FloatToStrAnsi(i:Extended; FormatSettings:TFormatSettings):AnsiString;overload;

function IntToStrF(i:integer; FixedLength:integer=0):string;

function DaysInMonth(Year,Month:integer):integer;

const GUID_NULL: TGUID = '{00000000-0000-0000-0000-000000000000}';//exist in ActiveX

implementation
uses MLTimeZone, CRT4, MLMath;

function IntToStrF(i:integer; FixedLength:integer=0):string;
begin
  Result:=inttostr(i);
  while Length(Result)<FixedLength do Result:='0'+Result;
end;

function DaysInMonth(Year,Month:integer):integer;
const DaysN:array[1..12]of integer=(31,28,31,30,31,30 ,30,31,30,31,30,31);
      DaysV:array[1..12]of integer=(31,29,31,30,31,30 ,30,31,30,31,30,31);
begin//��� �������� ���������� � ���� �������: ���� �� ������ 4, �� ��� ���� �� ������ 100, ���� ������ 400.
  if (Year mod 4=0)and(Year mod 100<>0)or(Year mod 400=0) then
    Result:=DaysV[Month] else
    Result:=DaysN[Month];
end;

Function FloatToStrAnsi(i:Extended):AnsiString;
begin
  Result:=AnsiString(FloatToStr(i));
end;

Function FloatToStrAnsi(i:Extended; FormatSettings:TFormatSettings):AnsiString;
begin
  Result:=AnsiString(FloatToStr(i, FormatSettings));
end;

Function IntToStrAnsi(i:integer):AnsiString;
begin
  Result:=AnsiString(IntToStr(i));
end;

Function IntToStrAnsi(i:int64):AnsiString;
begin
  Result:=AnsiString(IntToStr(i));
end;

Function StrToFloatDefSmart(s:string; DefValue:extended):extended;
var fs:TFormatSettings;
begin
  fs.DecimalSeparator:=',';
  if not TryStrToFloat(s, result, fs) then
    begin
      fs.DecimalSeparator:='.';
      if not TryStrToFloat(s, result, fs) then
        Result:=DefValue;
    end;
end;


Function GUIDMatch(G1,G2:TGUID):Boolean;
begin
  Result:=False;
  if G1.D1=G2.D1 then
  if G1.D2=G2.D2 then
  if G1.D3=G2.D3 then
  if Integer((@G1.D4[0])^)=Integer((@G2.D4[0])^) then
  if Integer((@G1.D4[4])^)=Integer((@G2.D4[4])^) then
    Result:=True;
end;

Function StrToGUID(s:string):TGUID;
begin
  Result:=GUID_NULL;
  if Length(s)<30 then exit;
  try
    if s[1]<>'{' then s:='{'+s;
    if s[Length(s)]<>'}' then s:=s+'}';
    Result:=SysUtils.StringToGUID(s);
  except
    Result:=GUID_NULL;
  end;
end;
Function StrToGUID(s:AnsiString):TGUID;
begin
  Result:=GUID_NULL;
  if Length(s)<30 then exit;
  try
    if s[1]<>'{' then s:='{'+s;
    if s[Length(s)]<>'}' then s:=s+'}';
    Result:=SysUtils.StringToGUID(String(s));
  except
    Result:=GUID_NULL;
  end;
end;

Function TryStrToGUID(s:string; id:TGUID):boolean;
begin//{FD6D5960-365E-40D7-83FF-370FA34A9198}//38
  Result:=False;
  id:=GUID_NULL;
  try
    if s[1]<>'{' then s:='{'+s;
    if s[Length(s)]<>'}' then s:=s+'}';
    id:=SysUtils.StringToGUID(s);
    Result:=True;
  except
    id:=GUID_NULL;
  end;
end;
Function TryStrToGUID(s:AnsiString; id:TGUID):boolean;
begin
  Result:=False;
  id:=GUID_NULL;
  try
    if s[1]<>'{' then s:='{'+s;
    if s[Length(s)]<>'}' then s:=s+'}';
    id:=SysUtils.StringToGUID(string(s));
    Result:=True;
  except
    id:=GUID_NULL;
  end;
end;


Function AnsiToDos(s:string):string;
var a,r:AnsiString;
begin
  a:=AnsiString(s);
  r:=a;
  AnsiToOem(PAnsiChar(a),PAnsiChar(r));
  Result:=WideString(r);
end;

function ExtractRes(ResName,FileName:string):hResult;overload;
var Resource: THandle;
    h:pointer;
    f:file;
    buf:array of byte;
begin
  Result:=S_OK;
  ResName:=UpperCase(ResName);
  Resource := LoadResource(hInstance,FindResource(hInstance, pWideChar(ResName), 'FILE'));
  if Resource>0 then
    try
      h:=LockResource(Resource);
      assignfile(f,FileName); rewrite(f,1);
      SetLength(buf,SizeofResource(hInstance,FindResource(hInstance, pWideChar(ResName), 'FILE')));
      CopyMemory(@buf[0],h,Length(buf));
      //���������� �������� ���������� ����� �� �����
      BlockWrite(f,buf[0],Length(buf));
      SetLength(buf,0);
    finally
      CloseFile(f);
      UnLockResource(Resource);
      FreeResource(Resource);
    end;
end;

function ExtractRes(ResName:string):string;
begin
  Result:=TempFileName;
  ExtractRes(ResName, Result);
end;

Function RoundFP2(e:extended):extended;
begin
  result:=Round(e*100)/100;
end;

Function ActualUTCTimeToStr(t:extended):string;
begin
  if t>24 then
    Result:=DateTimeToStr(UTCToLocal(t)) else
    Result:='';
end;

Procedure StringGridAutoSizeColumnsByHead(var S:TStringGrid);
var i,w:integer;
begin
  if s.RowCount>0 then if s.ColCount>0 then
    for i:=0 to s.ColCount-1 do
      begin
        w:=s.Canvas.TextWidth(s.Cells[i,0])+6;
        if w>S.ColWidths[i] then S.ColWidths[i]:=w;
      end;
end;

Procedure StringGridSetSizeColumnsByHead(var S:TStringGrid);
var i,w:integer;
begin
  if s.RowCount>0 then if s.ColCount>0 then
    for i:=0 to s.ColCount-1 do
      begin
        w:=s.Canvas.TextWidth(s.Cells[i,0])+6;
        S.ColWidths[i]:=w;
      end;
end;
Procedure StringGridAutoSizeColumns(var S:TStringGrid);
var col,row,w:integer;
begin
  if s.RowCount>0 then if s.ColCount>0 then
    for col:=0 to s.ColCount-1 do
    for row:=0 to s.RowCount-1 do
      begin
        w:=s.Canvas.TextWidth(s.Cells[col,row])+6;
        if w>S.ColWidths[col] then S.ColWidths[col]:=w;
      end;
end;

function ExportResource(ResName,ResType,ExportFileName:string):hResult;
var Resource: THandle;
    h:pointer;
    f:file;
    buf:array of byte;
begin
  ResName:=UpperCase(ResName);
  Result:=S_False;
  Resource := LoadResource(hInstance,FindResource(hInstance, pWideChar(ResName), pWideChar(ResType)));
  if Resource>0 then
    try
      h:=LockResource(Resource);
      assignfile(f,ExportFileName); rewrite(f,1);
      SetLength(buf,SizeofResource(hInstance,FindResource(hInstance, pWideChar(ResName), pWideChar(ResType))));
      CopyMemory(@buf[0],h,Length(buf));
      //���������� �������� ���������� ����� �� �����
      BlockWrite(f,buf[0],Length(buf));
      SetLength(buf,0);
      Result:=S_Ok;
    finally
      CloseFile(f);
      UnLockResource(Resource);
      FreeResource(Resource);
    end;
end;

Function GUIDToText(GUID:TGUID):String;
begin//1f79f20e-4d70-427b-99dd-243cc1c28d54
  Result:=IntToHex(GUID.D1,8)+
          '-'+
          IntToHex(GUID.D2,4)+
          '-'+
          IntToHex(GUID.D3,4)+
          '-'+
          IntToHex(GUID.D4[0],2)+
          IntToHex(GUID.D4[1],2)+
          '-'+
          IntToHex(GUID.D4[2],2)+
          IntToHex(GUID.D4[3],2)+
          IntToHex(GUID.D4[4],2)+
          IntToHex(GUID.D4[5],2)+
          IntToHex(GUID.D4[6],2)+
          IntToHex(GUID.D4[7],2);
end;

Function GUIDToHex(GUID:TGUID):String;
begin
  Result:=IntToHex(GUID.D1,8)+
          IntToHex(GUID.D2,4)+
          IntToHex(GUID.D3,4)+
          IntToHex(GUID.D4[0],2)+
          IntToHex(GUID.D4[1],2)+
          IntToHex(GUID.D4[2],2)+
          IntToHex(GUID.D4[3],2)+
          IntToHex(GUID.D4[4],2)+
          IntToHex(GUID.D4[5],2)+
          IntToHex(GUID.D4[6],2)+
          IntToHex(GUID.D4[7],2);
end;

Function HexToGUID(s:string):TGUID;
begin//000000001111222233445566778899AA
  if length(s)<>32 then RaiseException(EXCEPTION_FLT_DIVIDE_BY_ZERO,0,0,nil);
  //1..8 =8
  Result.D1:=HexToByte(s[1])*$10000000+
             HexToByte(s[2])*$01000000+
             HexToByte(s[3])*$00100000+
             HexToByte(s[4])*$00010000+
             HexToByte(s[5])*$00001000+
             HexToByte(s[6])*$00000100+
             HexToByte(s[7])*$00000010+
             HexToByte(s[8]){*$00000001};
  //9..12 =4
  Result.D2:=HexToByte(s[ 9])*$1000+
             HexToByte(s[10])*$0100+
             HexToByte(s[11])*$0010+
             HexToByte(s[12]){*$0001};
  //13..16=4
  Result.D3:=HexToByte(s[13])*$1000+
             HexToByte(s[14])*$0100+
             HexToByte(s[15])*$0010+
             HexToByte(s[16]){*$0001};
  //17,18=2
  Result.D4[0]:=HexToByte(s[17])*$10+
                HexToByte(s[18]){*$01};
  Result.D4[1]:=HexToByte(s[19])*$10+
                HexToByte(s[20]){*$01};
  Result.D4[2]:=HexToByte(s[21])*$10+
                HexToByte(s[22]){*$01};
  Result.D4[3]:=HexToByte(s[23])*$10+
                HexToByte(s[24]){*$01};
  Result.D4[4]:=HexToByte(s[25])*$10+
                HexToByte(s[26]){*$01};
  Result.D4[5]:=HexToByte(s[27])*$10+
                HexToByte(s[28]){*$01};
  Result.D4[6]:=HexToByte(s[29])*$10+
                HexToByte(s[30]){*$01};
  Result.D4[7]:=HexToByte(s[31])*$10+
                HexToByte(s[32]){*$01};
end;

Procedure DelimitedStrToList(DStr:String; var List:TStringList; Delimiter:char);
var s,e:integer;
begin
  List.Clear;
  if Length(DStr)<1 then exit;
  s:=1; e:=1;
  repeat
    while (e<Length(DStr))and(DStr[e]<>Delimiter) do inc(e);
    if DStr[e]=Delimiter then List.Add(Copy(DStr,s,e-s));
    if DStr[e]<>Delimiter then List.Add(Copy(DStr,s,e-s+1));
    s:=e+1;
    e:=s;
  until s>Length(DStr);
end;

function DelimitedStrLastStr(DStr:String; Delimiter:char):string;
var i,n:integer;
begin
  n:=-1;
  for i:=Length(DStr) downto 1 do
    if DStr[i]=Delimiter then
      begin
        n:=i;
        Break;
      end;
  if n=-1 then
    Result:=DStr else
    Result:=Copy(DStr,n+1,Length(DStr)-n);
end;

function ListToStr(var List:TStringList; Delimiter:char):string;
var i:integer;
begin
  Result:='';
  if List.Count>0 then Result:=List[0];
  if List.Count>1 then
    for i:=1 to List.Count-1 do
      Result:=Result+Delimiter+List[i];
end;

function SortedInsert(s:string; var list:Tstrings):integer;
  var x,x1,x2:integer;
  begin
    x1:=0; x2:=list.Count-1; result:=list.count;
    if x2<0 then list.Add(s) else
      if s>=list.Strings[x2] then list.Add(s) else//���������� �������� ��� ��������������� �������������������
      begin
      repeat
        x:=(x1+x2)div 2;
        if s>=list.Strings[x] then x1:=x else x2:=x;
      until x2-x1<=1;
      if s<list.Strings[x1] then begin list.Insert(x1,s);result:=x1; end else
        begin list.Insert(x2,s); result:=x2;end;
      end;
  end;

procedure UpString2(var s:string);{Rus(win)&Eng}
  var i:integer;
  const mas:array[0..255]of byte=(
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,
        30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,
        50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,
        70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,
        90,91,92,93,94,95,96,65,66,67,68,69,70,71,72,73,74,75,76,77,
        78,79,80,81,82,83,84,85,86,87,88,89,90,123,124,125,126,127,128,129,
        130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,
        150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,
        170,171,172,173,174,175,176,177,178,179,180,181,182,183,168,185,186,187,188,189,190,191,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223);
  begin
    for i:=1 to length(s) do s[i]:=chr(mas[ord(s[i])]);
  end;

function UpString(s:string):string;
  var i:integer;
  const mas:array[0..255]of byte=(
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,
        30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,
        50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,
        70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,
        90,91,92,93,94,95,96,65,66,67,68,69,70,71,72,73,74,75,76,77,
        78,79,80,81,82,83,84,85,86,87,88,89,90,123,124,125,126,127,128,129,
        130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,
        150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,
        170,171,172,173,174,175,176,177,178,179,180,181,182,183,168,185,186,187,188,189,190,191,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223);
  begin
    for i:=1 to length(s) do s[i]:=chr(mas[ord(s[i])]);
    UpString:=s;
  end;

function UpCharRus(s:char):char;
  const mas:array[0..255]of byte=(
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,
        30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,
        50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,
        70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,
        90,91,92,93,94,95,96,65,66,67,68,69,70,71,72,73,74,75,76,77,
        78,79,80,81,82,83,84,85,86,87,88,89,90,123,124,125,126,127,128,129,
        130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,
        150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,
        170,171,172,173,174,175,176,177,178,179,180,181,182,183,168,185,186,187,188,189,190,191,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,
        192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223);
  begin
    Result:=chr(mas[ord(s)]);
  end;

function LowStrRus(s:string):string;
  const n1:string='��������������������������������';
        n0:string='�����Ũ��������������������������';
  var i,j:integer;
  begin
    for i:=1 to length(s) do for j:=1 to length(n0)do
      if s[i]=n0[j] then s[i]:=n1[j];
    LowStrRus:=s;
  end;

end.
