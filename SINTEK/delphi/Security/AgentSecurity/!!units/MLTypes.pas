unit MLTypes;
//2012

interface
uses windows;

type
  MLT2=packed record//16-bit
    case tag:Word of
      varSmallint:(I2:SmallInt);
      varByte:    (UI1:array[0..1]of byte);
      varWord:    (UI2:word);
  end;

  MLT4=packed record//32-bit
    case tag:Word of
      varInteger: (I4:integer);
      varSingle:  (R4:Single);
      varByte:    (UI1:array[0..3]of byte);
      varWord:    (UI2:array[0..1]of word);
      varLongWord:(UI4:LongWord);
  end;

  MLT8=packed record//64-bit
    case tag:Word of
      varShortInt:(I1:array[0..7]of ShortInt);
      varSmallInt:(I2:array[0..3]of SmallInt);
      varInteger: (I4:array[0..1]of Integer);
      varDouble:  (R8:Double);
      varByte:    (UI1:array[0..7]of byte);
      varWord:    (UI2:array[0..3]of word);
      varLongWord:(UI4:array[0..1]of LongWord);
      varInt64:   (I8:int64);
      varUInt64:  (UI8:UInt64);
      $0111:      (ft:_FileTime);
  end;

type TColorEx=record
  case Byte of
    1:(Color:integer);
    2:(R,G,B,c:Byte);
  end;

  MLTPoint=record x,y:Double;end;

  MLTPointArray=array of MLTPoint;

  MLTVector=record A,B:MLTPoint;end;

  TVariantRelationship = (vrEqual, vrLessThan, vrGreaterThan, vrNotEqual);

function CrossX(Line1,Line2:MLTVector):Double;
Function Interpolate(A,B:MLTPoint; x:Double):MLTPoint;
Procedure MLTPointArrayGrow(var Mas:MLTPointArray; Point:MLTPoint);

Function VariantCompare(v1,v2:Variant):TVariantRelationship;

implementation
uses sysutils;

Function IntegerCompare(i1,i2:int64):TVariantRelationship;
begin
  if i1=i2 then Result:=vrEqual else
  if i1<i2 then Result:=vrLessThan else
                Result:=vrGreaterThan;
end;

Function DoubleCompare(d1,d2:Double):TVariantRelationship;
begin
  if d1=d2 then Result:=vrEqual else
  if d1<d2 then Result:=vrLessThan else
                Result:=vrGreaterThan;
end;

Function StringCompare(s1,s2:String):TVariantRelationship;
begin
  if s1=s2 then Result:=vrEqual else
  if s1<s2 then Result:=vrLessThan else
                Result:=vrGreaterThan;
end;

Function VariantCompare(v1,v2:Variant):TVariantRelationship;
var i1,i2:int64;
    d1,d2:Double;
    s1,s2:string;
begin
  s1:=v1; s2:=v2;
  if TryStrToInt64(s1,i1) and TryStrToInt64(s2,i2) then Result:=IntegerCompare(i1,i2) else
  if TryStrToFloat(s1,d1) and TryStrToFloat(s2,d2) then Result:=DoubleCompare(d1,d2) else
                                                        Result:=StringCompare(s1,s2);
end;

Procedure MLTPointArrayGrow(var Mas:MLTPointArray; Point:MLTPoint);
begin
  SetLength(Mas, Length(Mas)+1);
  Mas[High(Mas)]:=Point;
end;

function CrossX(Line1,Line2:MLTVector):Double;
var a1,a2,b1,b2:Double;
begin
  //precalculate
  //Line1
  if Line1.A.x=Line1.B.x then
    a1:=1e9 else
    a1:=(Line1.B.y-Line1.A.y)/(Line1.B.x-Line1.A.x);
  b1:=Line1.A.y-a1*Line1.A.x;
  //Line2
  if Line2.A.x=Line2.B.x then
    a2:=1e9 else
    a2:=(Line2.B.y-Line2.A.y)/(Line2.B.x-Line2.A.x);
  b2:=Line2.A.y-a2*Line2.A.x;
  //calculate cross X-point
  if a1=a2 then Result:=-1e99 else//параллельные не пересекаются
    Result:=(b2-b1)/(a1-a2);
end;

Function Interpolate(A,B:MLTPoint; x:Double):MLTPoint;
begin
  Result.x:=x;
  if A.x=B.x then
    Result.y:=(A.y+B.y)/2 else
    Result.y:=A.y+(x-A.x)*(B.y-A.y)/(B.x-A.x);
end;

end.
