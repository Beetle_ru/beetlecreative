unit MLTimeZone;//+20.05.2008 +19.06.2008 +03.09.2010//as unit
   // 1.000/24/3600/64=1.80845e-7
   // 15 ������ // 5 ������-���� //10 ������-���� ���
   // 99999+1900�=2173�
   // �������� ����������� 7,27595761418343E-12=0.629 �����
interface
uses SysUtils, Classes, windows;

Function UTCToLocal(T:TDateTime):TDateTime;
Function LocalToUTC(T:TDateTime):TDateTime;
function GetCurrentTimeZoneBias:integer;

Function SystemTimeToFileTimeToHex16(const st:SYSTEMTIME):string;

Function DateTimeToFileTime(dt:TDateTime):TFileTime;
Function DateTimeToStr2(t:TDateTime):string;

Function FileTimeToDateTime(ft:TFileTime):TDateTime;
Function FileTimeToLocalSystemTime(T:_FileTime):_SystemTime;
Function FileTimeToLocalDateTime(T:_FileTime):TDateTime;

var TZInfo:TIME_ZONE_INFORMATION;

implementation

//DEPRECATED!!!
//function TzSpecificLocalTimeToSystemTime(lpTimeZoneInformation: PTimeZoneInformation;
//  var lpUniversalTime, lpLocalTime: TSystemTime): BOOL; stdcall; external kernel32 name 'TzSpecificLocalTimeToSystemTime';

Function FileTimeToLocalSystemTime(T:_FileTime):_SystemTime;
var s:_SystemTime;
    T2:_FileTime;
begin
  FileTimeToLocalFileTime(T,T2);
  FileTimeToSystemTime(T2,S);
  Result:=s;
end;

Function FileTimeToLocalDateTime(T:_FileTime):TDateTime;
Begin
  Result:=SystemTimeToDateTime(FileTimeToLocalSystemTime(T));
end;

Function DateTimeToStr2(t:TDateTime):string;
var st:_SystemTime;
begin
  DateTimeToSystemTime(t,st);
  Result:=IntToStr(st.wYear)+'.'+IntToStr(st.wMonth)+'.'+IntToStr(st.wDay)+' '+
  IntToStr(st.wHour)+':'+IntToStr(st.wMinute)+':'+IntToStr(st.wSecond)+'.'+IntToStr(st.wMilliseconds);
end;

Function DateTimeToFileTime(dt:TDateTime):TFileTime;
var st:_SYSTEMTIME;
begin
  DateTimeToSystemTime(dt,st);
  SystemTimeToFileTime(st,result);
end;

Function FileTimeToDateTime(ft:TFileTime):TDateTime;
var st:_SYSTEMTIME;
begin
  try
    FileTimeToSystemTime(ft,st);
    Result:=SystemTimeToDateTime(st);
  except
    Result:=0;
  end;
end;

Function SystemTimeToFileTimeToHex16(const st:SYSTEMTIME):string;
var ft:TFileTime;
begin
  try
    SystemTimeToFileTime(st,ft);
    Result:=IntToHex(ft.dwHighDateTime,8)+IntToHex(ft.dwLowDateTime,8);
  except
    Result:='Error';
  end;
end;

Function LocalToUTC(T:TDateTime):TDateTime;
var S1,S2:TSystemTime;
    F1,F2:_FILETIME;
begin
  try
    DateTimeToSystemTime(T,S1);
    SystemTimeToFileTime(S1, F1);
    LocalFileTimeToFileTime(F1, F2);//GOOD CONVERTER 2011
    FileTimeToSystemTime(F2, S2);
    Result:=SystemTimeToDateTime(S2);
  Except
    Result:=0;
  end;
end;

Function UTCToLocal(T:TDateTime):TDateTime;
var S1,S2:TSystemTime;
    F1,F2:_FILETIME;
begin
  try
    DateTimeToSystemTime(T,S1);
    SystemTimeToFileTime(S1, F1);
    FileTimeToLocalFileTime(F1, F2);//GOOD CONVERTER 2011
    FileTimeToSystemTime(F2, S2);
    Result:=SystemTimeToDateTime(S2);
  except
    Result:=0;
  end;
end;

function GetCurrentTimeZoneBias:integer;//in minutes
var DayLight:integer;
    info:_TIME_ZONE_INFORMATION;
begin
  DayLight:=GetTimeZoneInformation(info);
  result:=0;
  if DayLight=2 then result:=info.Bias+info.DaylightBias;//TIME_ZONE_ID_DAYLIGHT
  if DayLight=1 then result:=info.Bias+info.StandardBias;//TIME_ZONE_ID_STANDARD
end;

function NowUTC:TDateTime;//old and unstable
type TValue=record
  case byte of
  0:(time:TDateTime);
  1:(b:byte);
  end;
var V:TValue;
    DayLight:integer;
    info:_TIME_ZONE_INFORMATION;
begin
  DayLight:=GetTimeZoneInformation(info);
  if DayLight=2 then
    begin//TIME_ZONE_ID_DAYLIGHT// low bit=1
    v.time:=(info.Bias+info.DaylightBias)/24/60+now;
    v.b:=v.b or $01; //add time zone
    end;
  if DayLight=1 then
    begin//TIME_ZONE_ID_STANDARD// low bit=0
    v.time:=(info.Bias+info.StandardBias)/24/60+now;
    v.b:=v.b and $FE;//clear time_zone
    end;
  result:=v.time;
end;

Procedure TimeZoneMarkAsDAYLIGHT(var time:TDateTime);
type TValue=record
  case byte of
  0:(time:TDateTime);
  1:(b:byte);
  end;
var V:^TValue;
begin
  V:=@time;
  v.b:=v.b or $01;//TIME_ZONE_ID_DAYLIGHT// low bit=1
end;

Procedure TimeZoneMarkAsSTANDARD(var time:TDateTime);
type TValue=record
  case byte of
  0:(time:TDateTime);
  1:(b:byte);
  end;
var V:^TValue;
begin
  V:=@time;
  v.b:=v.b and $FE;//TIME_ZONE_ID_STANDARD// low bit=0
end;

function TimeZoneisDAYLIGHT(time:TDateTime):boolean;
type TValue=record
  case byte of
  0:(time:TDateTime);
  1:(b:byte);
  end;
var V:TValue;
begin
  V.time:=time;
  if (V.b and $01)=1 then result:=true else result:=false;
end;

function TimeZoneisSTANDARD(time:TDateTime):boolean;
type TValue=record
  case byte of
  0:(time:TDateTime);
  1:(b:byte);
  end;
var V:TValue;
begin
  V.time:=time;
  if (V.b and $01)=0 then result:=true else result:=false;
end;

function DAYLIGHTTimeZoneBias:integer;//in minutes
begin
  result:=TZInfo.Bias+TZInfo.DaylightBias;//TIME_ZONE_ID_DAYLIGHT
end;

function STANDARDTimeZoneBias:integer;//in minutes
begin
  result:=TZInfo.Bias+TZInfo.StandardBias;//TIME_ZONE_ID_STANDARD
end;

initialization
  GetTimeZoneInformation(TZInfo);

end.
