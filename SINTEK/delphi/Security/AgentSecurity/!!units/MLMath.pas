// 30.03.2004+04.05.04+24.08.04+28.04.05+13.09.05+04.04.2006+21.05.2007+20.07.2007+05.09.2007+
// +04.10.2007 +29.02.2008 +02.07.2010  +06.07.2010  +26.08.2010 +05.10.2011 +26.04.2013
unit MLMath;

interface
type TSingleArray=array of Single;

function HexToByte(hex:char):byte;overload;
function HexToByte(hex:string):byte;overload;
function HexToWord(hex:string):Word;
function HexToDWord(hex:string):Cardinal;
function HexToInt32(hex8:string):Integer;

function ByteToHex(a:integer):string;
function WordToHex(a:integer):string;
function ByteToBit(b:byte):string;

Function StepReal(x,n:extended):extended;
Function StepInt(x:extended; n:integer):extended;
Function Step10(n:byte):integer;
Function Step2(n:byte):integer;

function StrToReal(s:string):real;
function StrToFloat2(s:string):extended;
function StrToInt2(s:string):integer;
function StrToIntShielded(s:string):integer;
function FloatToStrEx(Value: Extended): string;//renamed to ex


function Tan(x:real):real;
function Angle(x,y:real):real;
function ArcSin(x:real):real;
function ArcCos(x:real):real;

function IntToBool(i:integer):boolean;
function BoolToInt(b:boolean):integer;
function BoolToStr2(b:boolean):string;
function StrToBool2(s:string):boolean;

function SwapBits(a:byte):byte;

function HexToExtended(s:string):extended;
function ExtendedToHex(e:extended):string;

function Max(x1,x2:extended):extended;
function Min(x1,x2:extended):extended;overload;
function Min(x1,x2:double):double;overload;
function Min(x1,x2:integer):integer;overload;

Procedure HexToSingleArray(s:string; var a:TSingleArray);
Function SingleArrayToHex(var a:TSingleArray):string;


implementation
uses SysUtils, Math, Classes, Windows, MLTypes, crt2;

Procedure HexToSingleArray(s:string; var a:TSingleArray);
var i,n:integer;
    r:MLT4;
begin
  n:=Length(s) div 8;
  SetLength(a, n);
  if n>0 then
  for i:=0 to n-1 do
    begin
      r.UI1[3]:=HexToByte(s[i*8+1])*$10+
                HexToByte(s[i*8+2]);
      r.UI1[2]:=HexToByte(s[i*8+3])*$10+
                HexToByte(s[i*8+4]);
      r.UI1[1]:=HexToByte(s[i*8+5])*$10+
                HexToByte(s[i*8+6]);
      r.UI1[0]:=HexToByte(s[i*8+7])*$10+
                HexToByte(s[i*8+8]);
      a[i]:=r.R4;
    end;
end;

Function SingleArrayToHex(var a:TSingleArray):string;
var i:integer;
    r:MLT4;
begin
  Result:='';
  if Length(a)>0 then for i:=0 to High(a) do
    begin
      r.R4:=a[i];
      Result:=Result+IntToHex(r.UI1[3],2)+IntToHex(r.UI1[2],2)+IntToHex(r.UI1[1],2)+IntToHex(r.UI1[0],2);
    end;
end;

Function Min(x1,x2:integer):integer;
begin
  if x1<x2 then Result:=x1 else Result:=x2;
end;

function Max(x1,x2:extended):extended;
begin
  if x1>x2 then Result:=x1 else Result:=x2;
end;

function Min(x1,x2:extended):extended;
begin
  if x1<x2 then Result:=x1 else Result:=x2;
end;

function Min(x1,x2:double):double;
begin
  if x1<x2 then Result:=x1 else Result:=x2;
end;


function HexToExtended(s:string):extended;
type Tm=array[0..9]of byte;
var m:^Tm;
    e:extended;
    i:integer;
begin
  m:=@e;
  if Length(s)>=20 then for i:=0 to 9 do
    m^[i]:=HexToByte(s[i*2+1]+s[i*2+2]);
  Result:=e;
end;

function ExtendedToHex(e:extended):string;
type Tm=array[0..9]of byte;
var i:integer;
    m:^Tm;
begin
  Result:='';
  m:=@e;
  for i:=0 to 9 do Result:=Result+ ByteToHex( m^[i] );
end;

function StrToFloat2(s:string):extended;
var fs:TFormatSettings;
begin
  s:=StringReplace(s,',','.',[rfReplaceAll]);//set separator '.'
  fs.DecimalSeparator:='.';
  Result:=StrToFloatDef(s,0,fs);
end;

function BoolToStr2(b:boolean):string;
  begin
  if b=false then result:='false' else result:='true';
  end;

function StrToBool2(s:string):boolean;
  begin
  if uppercase(s)='TRUE' then result:=true else result:=false;
  end;

function IntToBool(i:integer):boolean;
  begin
  if i=0 then result:=false else result:=true;
  end;
function BoolToInt(b:boolean):integer;
  begin
  if b=true then result:=1 else result:=0;
  end;

function FloatToStrEx(Value: Extended): string;
var
  Buffer: array[0..63] of Char;
begin
  SetString(Result, Buffer, FloatToText(Buffer, Value, fvExtended, ffGeneral, 18, 0));// up to 18
end;

function ByteToBit(b:byte):string;
  begin
   result:=inttostr(b div 128)+inttostr(b div 64 mod 2)+inttostr(b div 32 mod 2)+inttostr(b div 16 mod 2)
   +inttostr(b div 8 mod 2)+inttostr(b div 4 mod 2)+inttostr(b div 2 mod 2)+inttostr(b mod 2);
  end;

function HexToByte_deprecated(hex:char):byte;overload;
  begin
    Hex:=UpCase(Hex);
    if hex='0'then result:=0 else
    if hex='1'then result:=1 else
    if hex='2'then result:=2 else
    if hex='3'then result:=3 else
    if hex='4'then result:=4 else
    if hex='5'then result:=5 else
    if hex='6'then result:=6 else
    if hex='7'then result:=7 else
    if hex='8'then result:=8 else
    if hex='9'then result:=9 else
    if hex='A'then result:=10 else
    if hex='B'then result:=11 else
    if hex='C'then result:=12 else
    if hex='D'then result:=13 else
    if hex='E'then result:=14 else
    if hex='F'then result:=15 else result:=0;
  end;

function HexToByte(hex:char):byte;overload;
const mas:array[0..255]of byte=(
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,1,2,3,4,5,6,7,8,9,//0-9
      0,0,0,0,0,0,0,
      10,11,12,13,14,15,//A-E
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      10,11,12,13,14,15,//abcdef
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
begin//super turbo speed
  Result:=mas[ord(hex)];
end;

function HexToByte(hex:string):byte;overload;
  begin
    if length(hex)=2 then
      RESULT:=HexToByte(hex[1])*16+HexToByte(hex[2])
      else result:=0;
  end;

function HexToWord(hex:string):Word;
  begin
    if Length(Hex)=4 then
      begin
        Result:=$100*HexToByte(Hex[1]+Hex[2]) + HexToByte(Hex[3]+Hex[4]);
      end else
      begin
        Result:=0;
        RaiseException(EXCEPTION_FLT_DIVIDE_BY_ZERO,0,0,nil);
      end;
  end;

function HexToDWord(hex:string):Cardinal;
var i:integer;
    r,s:cardinal;
begin
  r:=0;s:=1;
  for i:=length(hex) downto 1 do
    begin
      r:=r+s*HexToByte(Hex[i]);
      s:=s*16;
    end;
  result:=r;
end;

function HexToInt32(hex8:string):Integer;
var x:MLT4;
begin//11223344-high to low
  if Length(hex8)=8 then
    begin
      x.UI1[3]:=HexToByte(hex8[1])*16+HexToByte(hex8[2]);
      x.UI1[2]:=HexToByte(hex8[3])*16+HexToByte(hex8[4]);
      x.UI1[1]:=HexToByte(hex8[5])*16+HexToByte(hex8[6]);
      x.UI1[0]:=HexToByte(hex8[7])*16+HexToByte(hex8[8]);
      Result:=x.I4;
    end else
    Result:=0;
end;

function Angle(x,y:real):real;
  var a:real;
  begin
    if x<>0 then begin a:=arctan(y/x);if x<0 then a:=a+pi; end else
    if y>=0 then a:=pi/2 else a:=-pi/2;
    result:=a;
  end;

function Tan(x:real):real;
  begin Tan:=Sin(x)/Cos(x); end;

function ArcSin(x:real):real;
  begin
  if x= 1 then ArcSin:=pi/2 else
  if x=-1 then ArcSin:=-pi/2 else
  if abs(x)<1 then ArcSin:= ArcTan (x/sqrt(1-sqr (x))) else
  arcsin:=0;
  end;

function ArcCos(x:real):real;
  begin
  if abs(x)>0 then ArcCos:=ArcTan (sqrt(1-sqr(x))/x)else
  if x=0 then ArcCos:=pi/2 else
  arccos:=0;
  end;

function ByteToHex(a:integer):string;
  var s:string;
  begin
    s:='00';
    if (a div 16)<10 then s[1]:=chr(ord('0')+a div 16);
    if (a div 16)>=10 then s[1]:=chr(ord('A')-10+a div 16);
    if (a mod 16)<10 then s[2]:=chr(ord('0')+a mod 16);
    if (a mod 16)>=10 then s[2]:=chr(ord('A')-10+a mod 16);
    bytetohex:=s;
  end;

function WordToHex(a:integer):string;
  begin
    WordToHex:=ByteToHex(a div 256)+ByteToHex(a mod 256);
  end;

Function StepReal(x,n:extended):extended;
  begin
    if x=0 then result:=0 else
    if x>0 then result:=exp(n*ln(x))
    else result:=0;
  end;

Function StepInt(x:extended; n:integer):extended;
  var i:integer;
  begin
    Result:=1;
    for i:=1 to abs(n) do Result:=Result*x;
    if sign(n)=-1 then Result:=1/Result;
  end;

Function Step10(n:byte):integer;
  var i,s:integer;
  begin
    S:=1;
    for i:=1 to n do s:=s*10;
    Step10:=s;
  end;

Function Step2(n:byte):integer;
  var i,s:integer;
  const mas:array[0..7]of integer=(1,2,4,8,16,32,64,128);
  begin
    s:=1;
    if n>7 then for i:=1 to n do s:=s*2 else s:=mas[n];
    result:=s;
  end;

function StrToIntShielded(s:string):integer;
  var i,code:integer;
  begin
    val(s,i,code);
    if code=0 then result:=i else result:=0;
  end;

function StrToInt2(s:string):integer;
  var n,i,z:integer;
  begin
    {check}
    if s='' then s:='0';
    if s[1]='-' then
      begin z:=-1; delete(s,1,1);end
      else z:=1;
    if s='' then s:='0';
    n:=0;
    for i:=1 to length(s) do if (ord(s[i])>=ord('0'))and(ord(s[i])<=ord('9'))then n:=n+(ord(s[i])-48)*step10(length(s)-i);
    result:=z*n;
  end;

function StrToReal(s:string):real;
  var n,m,i,z:integer; s2:string;
  begin
    {check}
    if s='' then s:='0';
    if s[1]='-' then
      begin z:=-1; delete(s,1,1);end
      else z:=1;
    if s='' then s:='0';
    //n - ����� ����
    n:=0; for i:=1 to length(s) do if (ord(s[i])>=ord('0'))and(ord(s[i])<=ord('9'))then inc(n);
    //m-���������� �����
    m:=0; for i:=1 to length(s) do if (s[i]='.')or(s[i]=',') then inc(m);
    if (n+m<length(s))or(m>1) then s:='0';
    {n-iiceoey a?iaiie caiyoie}
    n:=0; s2:='0';
    for i:=length(s) downto 1 do if (s[i]='.')or(s[i]=',') then n:=i;
    //n=��������� �������������� �����
    if n>0 then
      begin
      s2:=copy(s,n+1,length(s)-n);
      delete(s,n,length(s)-n+1);
      end;
    if s='' then s:='0';
    if s2='' then s2:='0';
    result:=z*(strtoint(s)+strtoint(s2)/step10(length(s2)));
  end;

function SwapBits(a:byte):byte;
  begin
  result:=
     a div  1  mod 2 *128+
     a div  2  mod 2 *64+
     a div  4  mod 2 *32+
     a div  8  mod 2 *16+
     a div 16  mod 2 *8+
     a div 32  mod 2 *4+
     a div 64  mod 2 *2+
     a div 128;
  end;

End.
