unit SecurityLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 16059 $
// File generated on 28.06.2011 14:14:41 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ICONICS\security.exe (1)
// LIBID: {38B2E815-28E3-11D3-A5B5-00104B0D13C3}
// LCID: 0
// Helpfile: 
// HelpString: ICONICS Security Automation 2.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SecurityLibMajorVersion = 2;
  SecurityLibMinorVersion = 0;

  LIBID_SecurityLib: TGUID = '{38B2E815-28E3-11D3-A5B5-00104B0D13C3}';

  DIID_ISECDualEvent: TGUID = '{DAB46033-2B7E-470C-B2E5-36B778FF325D}';
  IID_ISECDual: TGUID = '{AFB6CE0D-28DF-11D3-A5B5-00104B0D13C3}';
  CLASS_SECDual: TGUID = '{AFB6CE0E-28DF-11D3-A5B5-00104B0D13C3}';
  IID_IConfig: TGUID = '{9AC7CA5F-88B1-11D3-A5E3-00104B0D13C3}';
  CLASS_SECConfig: TGUID = '{9AC7CA60-88B1-11D3-A5E3-00104B0D13C3}';
  IID_IUserAcl: TGUID = '{9AC7CA61-88B1-11D3-A5E3-00104B0D13C3}';
  IID_IAcl: TGUID = '{9AC7CA66-88B1-11D3-A5E3-00104B0D13C3}';
  IID_IGroupAcl: TGUID = '{9AC7CA63-88B1-11D3-A5E3-00104B0D13C3}';
  IID_IDefaultGroupAcl: TGUID = '{118B7D69-8CA8-11D3-A5E3-00104B0D13C3}';
  IID_IUsers: TGUID = '{2EBE91EB-88BE-11D3-A5E3-00104B0D13C3}';
  IID_IGroups: TGUID = '{2EBE91ED-88BE-11D3-A5E3-00104B0D13C3}';
  IID_IPreferences: TGUID = '{CB167133-0EEA-11D4-A62D-00104B0D13C3}';
  CLASS_Preferences: TGUID = '{CB167134-0EEA-11D4-A62D-00104B0D13C3}';
  IID_UserPrefPropertyPageCATID: TGUID = '{5CFB4543-10B2-11D4-A62E-00104B0D13C3}';
  IID_IConfig2: TGUID = '{5DFA3F07-0177-49B4-AE9C-F619677D0B03}';
  CLASS_SECConfig2: TGUID = '{B9422E9A-3D0E-44BC-BE8D-21690EA1DC1A}';
  IID_IStrings: TGUID = '{20A0BE33-94F9-4E87-96AC-F3CC89869A16}';
  IID_ILongs: TGUID = '{C417C418-E873-4ABD-80DB-6CF737C41FA9}';
  IID_IIncludeExclude: TGUID = '{3C7B795A-E187-402C-B486-E803E90BC66A}';
  IID_IHoursOfWeek: TGUID = '{F2A965C8-758A-401F-B9E4-5F123AFC2270}';
  IID_IPolicyData: TGUID = '{DDD3313B-253D-4B50-9CA0-FAD4753AC2C4}';
  IID_IAcl2: TGUID = '{C37F92EC-4ABF-4A60-A230-73CB1E335EF5}';
  IID_IGroups2: TGUID = '{30979B9C-DD5C-4C91-97E9-09A2E714F88F}';
  IID_IUsers2: TGUID = '{B7969588-4C91-4ECD-B66B-156CFB539DFF}';
  IID_IAppActionDefs: TGUID = '{29CD8C63-D158-4DD1-9E96-7ABF969A03AE}';
  IID_IAppActionDef: TGUID = '{E1CC61B2-E70B-45E9-BB63-42974B011E09}';
  CLASS_PrivateSession: TGUID = '{0A85A756-4889-4504-A1D8-0168D86B36AA}';
  IID_IConfig3: TGUID = '{30200612-288A-459D-923A-39BA7A73F89B}';
  CLASS_SECConfig3: TGUID = '{024C10B3-8AAF-46A3-A045-6B185C91F7B8}';
  IID_IPolicyData3: TGUID = '{D88A07AA-1BC9-441B-A254-7AF2BE0BBDAE}';
  IID_IAcl3: TGUID = '{000F25EB-1347-445C-B6D5-316E07AC2102}';
  IID_IGroups3: TGUID = '{DE2F5DF7-A1EE-4C34-8D5F-FA74977EB489}';
  IID_IUsers3: TGUID = '{364840A8-3A73-450F-A2FA-32E6E5DADCFA}';
  IID_IGlobalPolicy: TGUID = '{3EEFF3F2-DE2C-447F-8695-595595A7BCD9}';
  IID_ISECDual2: TGUID = '{980D14E3-4DB8-49A9-A621-05259925F597}';
  CLASS_SECDual2: TGUID = '{32530EBB-692F-49B4-975D-1251F026AC17}';
  IID_ISECDual3: TGUID = '{01840A20-562C-4B60-A158-C9FC43F665E2}';
  CLASS_SECDual3: TGUID = '{EDB04E07-5DD0-4CD6-94D1-B6AAC30236E2}';
  IID_ISECDual4: TGUID = '{BD3F34D0-3563-46B0-BD10-B428929C8284}';
  CLASS_SECDual4: TGUID = '{0E0E1208-88D1-4E74-BF34-C2F1AC840749}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum __MIDL___MIDL_itf_security_0210_0001
type
  __MIDL___MIDL_itf_security_0210_0001 = TOleEnum;
const
  secDenied = $00000000;
  secAllowed = $00000001;
  secCritical = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISECDualEvent = dispinterface;
  ISECDual = interface;
  ISECDualDisp = dispinterface;
  IConfig = interface;
  IConfigDisp = dispinterface;
  IUserAcl = interface;
  IUserAclDisp = dispinterface;
  IAcl = interface;
  IAclDisp = dispinterface;
  IGroupAcl = interface;
  IGroupAclDisp = dispinterface;
  IDefaultGroupAcl = interface;
  IDefaultGroupAclDisp = dispinterface;
  IUsers = interface;
  IUsersDisp = dispinterface;
  IGroups = interface;
  IGroupsDisp = dispinterface;
  IPreferences = interface;
  IPreferencesDisp = dispinterface;
  UserPrefPropertyPageCATID = interface;
  IConfig2 = interface;
  IConfig2Disp = dispinterface;
  IStrings = interface;
  IStringsDisp = dispinterface;
  ILongs = interface;
  ILongsDisp = dispinterface;
  IIncludeExclude = interface;
  IIncludeExcludeDisp = dispinterface;
  IHoursOfWeek = interface;
  IHoursOfWeekDisp = dispinterface;
  IPolicyData = interface;
  IPolicyDataDisp = dispinterface;
  IAcl2 = interface;
  IAcl2Disp = dispinterface;
  IGroups2 = interface;
  IGroups2Disp = dispinterface;
  IUsers2 = interface;
  IUsers2Disp = dispinterface;
  IAppActionDefs = interface;
  IAppActionDefsDisp = dispinterface;
  IAppActionDef = interface;
  IAppActionDefDisp = dispinterface;
  IConfig3 = interface;
  IConfig3Disp = dispinterface;
  IPolicyData3 = interface;
  IPolicyData3Disp = dispinterface;
  IAcl3 = interface;
  IAcl3Disp = dispinterface;
  IGroups3 = interface;
  IGroups3Disp = dispinterface;
  IUsers3 = interface;
  IUsers3Disp = dispinterface;
  IGlobalPolicy = interface;
  IGlobalPolicyDisp = dispinterface;
  ISECDual2 = interface;
  ISECDual2Disp = dispinterface;
  ISECDual3 = interface;
  ISECDual3Disp = dispinterface;
  ISECDual4 = interface;
  ISECDual4Disp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SECDual = ISECDual;
  SECConfig = IConfig;
  Preferences = IPreferences;
  SECConfig2 = IConfig2;
  PrivateSession = ISECDual;
  SECConfig3 = IConfig3;
  SECDual2 = ISECDual2;
  SECDual3 = ISECDual3;
  SECDual4 = ISECDual4;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PPSafeArray1 = ^PSafeArray; {*}
  PPSafeArray2 = ^PSafeArray; {*}

  SecRightsEnum = __MIDL___MIDL_itf_security_0210_0001; 

// *********************************************************************//
// DispIntf:  ISECDualEvent
// Flags:     (4096) Dispatchable
// GUID:      {DAB46033-2B7E-470C-B2E5-36B778FF325D}
// *********************************************************************//
  ISECDualEvent = dispinterface
    ['{DAB46033-2B7E-470C-B2E5-36B778FF325D}']
    procedure OnLogin(const ComputerName: WideString; const UserName: WideString); dispid 1;
    procedure OnLogout(const ComputerName: WideString; const UserName: WideString; 
                       AutoLogout: WordBool); dispid 2;
  end;

// *********************************************************************//
// Interface: ISECDual
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AFB6CE0D-28DF-11D3-A5B5-00104B0D13C3}
// *********************************************************************//
  ISECDual = interface(IDispatch)
    ['{AFB6CE0D-28DF-11D3-A5B5-00104B0D13C3}']
    function Get_TestFile(const strItem: WideString): WordBool; safecall;
    function Get_TestOPCItem(const strItem: WideString): WordBool; safecall;
    function Get_TestCustom(const strItem: WideString): WordBool; safecall;
    function Get_TestAppAction(AppItem: Integer): WordBool; safecall;
    function Get_Node: WideString; safecall;
    procedure Set_Node(const pVal: WideString); safecall;
    function Get_LoggedIn: WideString; safecall;
    procedure Logout(const UserName: WideString); safecall;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    property TestFile[const strItem: WideString]: WordBool read Get_TestFile;
    property TestOPCItem[const strItem: WideString]: WordBool read Get_TestOPCItem;
    property TestCustom[const strItem: WideString]: WordBool read Get_TestCustom;
    property TestAppAction[AppItem: Integer]: WordBool read Get_TestAppAction;
    property Node: WideString read Get_Node write Set_Node;
    property LoggedIn: WideString read Get_LoggedIn;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
  end;

// *********************************************************************//
// DispIntf:  ISECDualDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AFB6CE0D-28DF-11D3-A5B5-00104B0D13C3}
// *********************************************************************//
  ISECDualDisp = dispinterface
    ['{AFB6CE0D-28DF-11D3-A5B5-00104B0D13C3}']
    property TestFile[const strItem: WideString]: WordBool readonly dispid 1;
    property TestOPCItem[const strItem: WideString]: WordBool readonly dispid 2;
    property TestCustom[const strItem: WideString]: WordBool readonly dispid 3;
    property TestAppAction[AppItem: Integer]: WordBool readonly dispid 4;
    property Node: WideString dispid 5;
    property LoggedIn: WideString readonly dispid 6;
    procedure Logout(const UserName: WideString); dispid 7;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); dispid 8;
    property ServerPublicKey: OleVariant readonly dispid 9;
  end;

// *********************************************************************//
// Interface: IConfig
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA5F-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IConfig = interface(IDispatch)
    ['{9AC7CA5F-88B1-11D3-A5E3-00104B0D13C3}']
    function Get_Users: IDispatch; safecall;
    function Get_Groups: IDispatch; safecall;
    function Get_DefaultGroup: IDispatch; safecall;
    function Get_AddUser(const UserName: WideString): IDispatch; safecall;
    procedure DeleteUser(const UserName: WideString); safecall;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindUser(const UserName: WideString): IDispatch; safecall;
    function Get_AddGroup(const GroupName: WideString): IDispatch; safecall;
    procedure DeleteGroup(const GroupName: WideString); safecall;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindGroup(const GroupName: WideString): IDispatch; safecall;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure FileNew; safecall;
    procedure FileOpen(const FileName: WideString); safecall;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); safecall;
    procedure FileSave; safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); safecall;
    property Users: IDispatch read Get_Users;
    property Groups: IDispatch read Get_Groups;
    property DefaultGroup: IDispatch read Get_DefaultGroup;
    property AddUser[const UserName: WideString]: IDispatch read Get_AddUser;
    property FindUser[const UserName: WideString]: IDispatch read Get_FindUser;
    property AddGroup[const GroupName: WideString]: IDispatch read Get_AddGroup;
    property FindGroup[const GroupName: WideString]: IDispatch read Get_FindGroup;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
  end;

// *********************************************************************//
// DispIntf:  IConfigDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA5F-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IConfigDisp = dispinterface
    ['{9AC7CA5F-88B1-11D3-A5E3-00104B0D13C3}']
    property Users: IDispatch readonly dispid 1;
    property Groups: IDispatch readonly dispid 2;
    property DefaultGroup: IDispatch readonly dispid 3;
    property AddUser[const UserName: WideString]: IDispatch readonly dispid 4;
    procedure DeleteUser(const UserName: WideString); dispid 5;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); dispid 6;
    property FindUser[const UserName: WideString]: IDispatch readonly dispid 7;
    property AddGroup[const GroupName: WideString]: IDispatch readonly dispid 8;
    procedure DeleteGroup(const GroupName: WideString); dispid 9;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); dispid 10;
    property FindGroup[const GroupName: WideString]: IDispatch readonly dispid 11;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 12;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 13;
    procedure FileNew; dispid 14;
    procedure FileOpen(const FileName: WideString); dispid 15;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); dispid 16;
    procedure FileSave; dispid 17;
    property ServerPublicKey: OleVariant readonly dispid 18;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); dispid 19;
  end;

// *********************************************************************//
// Interface: IUserAcl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA61-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IUserAcl = interface(IDispatch)
    ['{9AC7CA61-88B1-11D3-A5E3-00104B0D13C3}']
    function Get_Administrator: WordBool; safecall;
    procedure Set_Administrator(pVal: WordBool); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pVal: WideString); safecall;
    function Get_ChangePassNext: WordBool; safecall;
    procedure Set_ChangePassNext(pVal: WordBool); safecall;
    function Get_ChangePassNever: WordBool; safecall;
    procedure Set_ChangePassNever(pVal: WordBool); safecall;
    function Get_AccountDisable: WordBool; safecall;
    procedure Set_AccountDisable(pVal: WordBool); safecall;
    function Get_AccountLockout: WordBool; safecall;
    procedure Set_AccountLockout(pVal: WordBool); safecall;
    property Administrator: WordBool read Get_Administrator write Set_Administrator;
    property Password: WideString read Get_Password write Set_Password;
    property ChangePassNext: WordBool read Get_ChangePassNext write Set_ChangePassNext;
    property ChangePassNever: WordBool read Get_ChangePassNever write Set_ChangePassNever;
    property AccountDisable: WordBool read Get_AccountDisable write Set_AccountDisable;
    property AccountLockout: WordBool read Get_AccountLockout write Set_AccountLockout;
  end;

// *********************************************************************//
// DispIntf:  IUserAclDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA61-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IUserAclDisp = dispinterface
    ['{9AC7CA61-88B1-11D3-A5E3-00104B0D13C3}']
    property Administrator: WordBool dispid 1;
    property Password: WideString dispid 2;
    property ChangePassNext: WordBool dispid 3;
    property ChangePassNever: WordBool dispid 4;
    property AccountDisable: WordBool dispid 5;
    property AccountLockout: WordBool dispid 6;
  end;

// *********************************************************************//
// Interface: IAcl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA66-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IAcl = interface(IDispatch)
    ['{9AC7CA66-88B1-11D3-A5E3-00104B0D13C3}']
    function Get_Name: WideString; safecall;
    function Get_Description: WideString; safecall;
    procedure Set_Description(const pVal: WideString); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): WideString; safecall;
    property Name: WideString read Get_Name;
    property Description: WideString read Get_Description write Set_Description;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: WideString read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IAclDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA66-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IAclDisp = dispinterface
    ['{9AC7CA66-88B1-11D3-A5E3-00104B0D13C3}']
    property Name: WideString readonly dispid 1;
    property Description: WideString dispid 2;
    property Count: Integer readonly dispid 3;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: WideString readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IGroupAcl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA63-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IGroupAcl = interface(IDispatch)
    ['{9AC7CA63-88B1-11D3-A5E3-00104B0D13C3}']
  end;

// *********************************************************************//
// DispIntf:  IGroupAclDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9AC7CA63-88B1-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IGroupAclDisp = dispinterface
    ['{9AC7CA63-88B1-11D3-A5E3-00104B0D13C3}']
  end;

// *********************************************************************//
// Interface: IDefaultGroupAcl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {118B7D69-8CA8-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IDefaultGroupAcl = interface(IDispatch)
    ['{118B7D69-8CA8-11D3-A5E3-00104B0D13C3}']
    function Get_SimultaneousLogins: WordBool; safecall;
    procedure Set_SimultaneousLogins(pVal: WordBool); safecall;
    property SimultaneousLogins: WordBool read Get_SimultaneousLogins write Set_SimultaneousLogins;
  end;

// *********************************************************************//
// DispIntf:  IDefaultGroupAclDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {118B7D69-8CA8-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IDefaultGroupAclDisp = dispinterface
    ['{118B7D69-8CA8-11D3-A5E3-00104B0D13C3}']
    property SimultaneousLogins: WordBool dispid 1;
  end;

// *********************************************************************//
// Interface: IUsers
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2EBE91EB-88BE-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IUsers = interface(IDispatch)
    ['{2EBE91EB-88BE-11D3-A5E3-00104B0D13C3}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IUsersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2EBE91EB-88BE-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IUsersDisp = dispinterface
    ['{2EBE91EB-88BE-11D3-A5E3-00104B0D13C3}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IGroups
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2EBE91ED-88BE-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IGroups = interface(IDispatch)
    ['{2EBE91ED-88BE-11D3-A5E3-00104B0D13C3}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IGroupsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2EBE91ED-88BE-11D3-A5E3-00104B0D13C3}
// *********************************************************************//
  IGroupsDisp = dispinterface
    ['{2EBE91ED-88BE-11D3-A5E3-00104B0D13C3}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IPreferences
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CB167133-0EEA-11D4-A62D-00104B0D13C3}
// *********************************************************************//
  IPreferences = interface(IDispatch)
    ['{CB167133-0EEA-11D4-A62D-00104B0D13C3}']
    function GetPreference(const User: WideString; const Tab: WideString; const PrefName: WideString): OleVariant; safecall;
    procedure SetPreference(const User: WideString; const Tab: WideString; 
                            const PrefName: WideString; Val: OleVariant); safecall;
    procedure GetPreferences(const User: WideString; const Tab: WideString; 
                             out PrefNames: PSafeArray; out Values: PSafeArray); safecall;
    procedure SetPreferences(const User: WideString; const Tab: WideString; 
                             var PrefNames: PSafeArray; var Values: PSafeArray); safecall;
    procedure DeletePreference(const User: WideString; const Tab: WideString; 
                               const PrefName: WideString); safecall;
    function Get_User: WideString; safecall;
    procedure Set_User(const pVal: WideString); safecall;
    property User: WideString read Get_User write Set_User;
  end;

// *********************************************************************//
// DispIntf:  IPreferencesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CB167133-0EEA-11D4-A62D-00104B0D13C3}
// *********************************************************************//
  IPreferencesDisp = dispinterface
    ['{CB167133-0EEA-11D4-A62D-00104B0D13C3}']
    function GetPreference(const User: WideString; const Tab: WideString; const PrefName: WideString): OleVariant; dispid 1;
    procedure SetPreference(const User: WideString; const Tab: WideString; 
                            const PrefName: WideString; Val: OleVariant); dispid 2;
    procedure GetPreferences(const User: WideString; const Tab: WideString; 
                             out PrefNames: {??PSafeArray}OleVariant; 
                             out Values: {??PSafeArray}OleVariant); dispid 3;
    procedure SetPreferences(const User: WideString; const Tab: WideString; 
                             var PrefNames: {??PSafeArray}OleVariant; 
                             var Values: {??PSafeArray}OleVariant); dispid 4;
    procedure DeletePreference(const User: WideString; const Tab: WideString; 
                               const PrefName: WideString); dispid 5;
    property User: WideString dispid 6;
  end;

// *********************************************************************//
// Interface: UserPrefPropertyPageCATID
// Flags:     (0)
// GUID:      {5CFB4543-10B2-11D4-A62E-00104B0D13C3}
// *********************************************************************//
  UserPrefPropertyPageCATID = interface(IUnknown)
    ['{5CFB4543-10B2-11D4-A62E-00104B0D13C3}']
  end;

// *********************************************************************//
// Interface: IConfig2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5DFA3F07-0177-49B4-AE9C-F619677D0B03}
// *********************************************************************//
  IConfig2 = interface(IDispatch)
    ['{5DFA3F07-0177-49B4-AE9C-F619677D0B03}']
    function Get_Users: IDispatch; safecall;
    function Get_Groups: IDispatch; safecall;
    function Get_DefaultGroup: IDispatch; safecall;
    function Get_AddUser(const UserName: WideString): IDispatch; safecall;
    procedure DeleteUser(const UserName: WideString); safecall;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindUser(const UserName: WideString): IDispatch; safecall;
    function Get_AddGroup(const GroupName: WideString): IDispatch; safecall;
    procedure DeleteGroup(const GroupName: WideString); safecall;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindGroup(const GroupName: WideString): IDispatch; safecall;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure FileNew; safecall;
    procedure FileOpen(const FileName: WideString); safecall;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); safecall;
    procedure FileSave; safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); safecall;
    function Get_AppActionDefs: IDispatch; safecall;
    property Users: IDispatch read Get_Users;
    property Groups: IDispatch read Get_Groups;
    property DefaultGroup: IDispatch read Get_DefaultGroup;
    property AddUser[const UserName: WideString]: IDispatch read Get_AddUser;
    property FindUser[const UserName: WideString]: IDispatch read Get_FindUser;
    property AddGroup[const GroupName: WideString]: IDispatch read Get_AddGroup;
    property FindGroup[const GroupName: WideString]: IDispatch read Get_FindGroup;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
    property AppActionDefs: IDispatch read Get_AppActionDefs;
  end;

// *********************************************************************//
// DispIntf:  IConfig2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5DFA3F07-0177-49B4-AE9C-F619677D0B03}
// *********************************************************************//
  IConfig2Disp = dispinterface
    ['{5DFA3F07-0177-49B4-AE9C-F619677D0B03}']
    property Users: IDispatch readonly dispid 1;
    property Groups: IDispatch readonly dispid 2;
    property DefaultGroup: IDispatch readonly dispid 3;
    property AddUser[const UserName: WideString]: IDispatch readonly dispid 4;
    procedure DeleteUser(const UserName: WideString); dispid 5;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); dispid 6;
    property FindUser[const UserName: WideString]: IDispatch readonly dispid 7;
    property AddGroup[const GroupName: WideString]: IDispatch readonly dispid 8;
    procedure DeleteGroup(const GroupName: WideString); dispid 9;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); dispid 10;
    property FindGroup[const GroupName: WideString]: IDispatch readonly dispid 11;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 12;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 13;
    procedure FileNew; dispid 14;
    procedure FileOpen(const FileName: WideString); dispid 15;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); dispid 16;
    procedure FileSave; dispid 17;
    property ServerPublicKey: OleVariant readonly dispid 18;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); dispid 19;
    property AppActionDefs: IDispatch readonly dispid 20;
  end;

// *********************************************************************//
// Interface: IStrings
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {20A0BE33-94F9-4E87-96AC-F3CC89869A16}
// *********************************************************************//
  IStrings = interface(IDispatch)
    ['{20A0BE33-94F9-4E87-96AC-F3CC89869A16}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): WideString; safecall;
    procedure AddTail(const Val: WideString); safecall;
    procedure Remove(const Val: WideString); safecall;
    procedure RemoveAll; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: WideString read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IStringsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {20A0BE33-94F9-4E87-96AC-F3CC89869A16}
// *********************************************************************//
  IStringsDisp = dispinterface
    ['{20A0BE33-94F9-4E87-96AC-F3CC89869A16}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: WideString readonly dispid 0; default;
    procedure AddTail(const Val: WideString); dispid 2;
    procedure Remove(const Val: WideString); dispid 3;
    procedure RemoveAll; dispid 4;
  end;

// *********************************************************************//
// Interface: ILongs
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C417C418-E873-4ABD-80DB-6CF737C41FA9}
// *********************************************************************//
  ILongs = interface(IDispatch)
    ['{C417C418-E873-4ABD-80DB-6CF737C41FA9}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): Integer; safecall;
    procedure AddTail(Val: Integer); safecall;
    procedure Remove(Val: Integer); safecall;
    procedure RemoveAll; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: Integer read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  ILongsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C417C418-E873-4ABD-80DB-6CF737C41FA9}
// *********************************************************************//
  ILongsDisp = dispinterface
    ['{C417C418-E873-4ABD-80DB-6CF737C41FA9}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: Integer readonly dispid 0; default;
    procedure AddTail(Val: Integer); dispid 2;
    procedure Remove(Val: Integer); dispid 3;
    procedure RemoveAll; dispid 4;
  end;

// *********************************************************************//
// Interface: IIncludeExclude
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3C7B795A-E187-402C-B486-E803E90BC66A}
// *********************************************************************//
  IIncludeExclude = interface(IDispatch)
    ['{3C7B795A-E187-402C-B486-E803E90BC66A}']
    function Get_Include: IDispatch; safecall;
    function Get_Exclude: IDispatch; safecall;
    property Include: IDispatch read Get_Include;
    property Exclude: IDispatch read Get_Exclude;
  end;

// *********************************************************************//
// DispIntf:  IIncludeExcludeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3C7B795A-E187-402C-B486-E803E90BC66A}
// *********************************************************************//
  IIncludeExcludeDisp = dispinterface
    ['{3C7B795A-E187-402C-B486-E803E90BC66A}']
    property Include: IDispatch readonly dispid 1;
    property Exclude: IDispatch readonly dispid 2;
  end;

// *********************************************************************//
// Interface: IHoursOfWeek
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F2A965C8-758A-401F-B9E4-5F123AFC2270}
// *********************************************************************//
  IHoursOfWeek = interface(IDispatch)
    ['{F2A965C8-758A-401F-B9E4-5F123AFC2270}']
    function Get_Value(Day: Integer; Hour: Integer): WordBool; safecall;
    procedure Set_Value(Day: Integer; Hour: Integer; pVal: WordBool); safecall;
    property Value[Day: Integer; Hour: Integer]: WordBool read Get_Value write Set_Value;
  end;

// *********************************************************************//
// DispIntf:  IHoursOfWeekDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F2A965C8-758A-401F-B9E4-5F123AFC2270}
// *********************************************************************//
  IHoursOfWeekDisp = dispinterface
    ['{F2A965C8-758A-401F-B9E4-5F123AFC2270}']
    property Value[Day: Integer; Hour: Integer]: WordBool dispid 1;
  end;

// *********************************************************************//
// Interface: IPolicyData
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DDD3313B-253D-4B50-9CA0-FAD4753AC2C4}
// *********************************************************************//
  IPolicyData = interface(IDispatch)
    ['{DDD3313B-253D-4B50-9CA0-FAD4753AC2C4}']
    function Get_Max_password_age: WordBool; safecall;
    procedure Set_Max_password_age(pVal: WordBool); safecall;
    function Get_Pass_never_expire: WordBool; safecall;
    procedure Set_Pass_never_expire(pVal: WordBool); safecall;
    function Get_Pass_days_to_expire: Integer; safecall;
    procedure Set_Pass_days_to_expire(pVal: Integer); safecall;
    function Get_Min_password_age: WordBool; safecall;
    procedure Set_Min_password_age(pVal: WordBool); safecall;
    function Get_Pass_change_immediately: WordBool; safecall;
    procedure Set_Pass_change_immediately(pVal: WordBool); safecall;
    function Get_Pass_days_to_change: Integer; safecall;
    procedure Set_Pass_days_to_change(pVal: Integer); safecall;
    function Get_Min_password_length: WordBool; safecall;
    procedure Set_Min_password_length(pVal: WordBool); safecall;
    function Get_Pass_blank_password: WordBool; safecall;
    procedure Set_Pass_blank_password(pVal: WordBool); safecall;
    function Get_Pass_number_character: Integer; safecall;
    procedure Set_Pass_number_character(pVal: Integer); safecall;
    function Get_Password_uniqueness: WordBool; safecall;
    procedure Set_Password_uniqueness(pVal: WordBool); safecall;
    function Get_Pass_never_keep_history: WordBool; safecall;
    procedure Set_Pass_never_keep_history(pVal: WordBool); safecall;
    function Get_number_passwords: Integer; safecall;
    procedure Set_number_passwords(pVal: Integer); safecall;
    function Get_Auto_logout: WordBool; safecall;
    procedure Set_Auto_logout(pVal: WordBool); safecall;
    function Get_Account_never_timeout: WordBool; safecall;
    procedure Set_Account_never_timeout(pVal: WordBool); safecall;
    function Get_Account_timeout_period: Integer; safecall;
    procedure Set_Account_timeout_period(pVal: Integer); safecall;
    function Get_Account_lockout: WordBool; safecall;
    procedure Set_Account_lockout(pVal: WordBool); safecall;
    function Get_Account_never_lockout: WordBool; safecall;
    procedure Set_Account_never_lockout(pVal: WordBool); safecall;
    function Get_Account_number_bad_attempts: Integer; safecall;
    procedure Set_Account_number_bad_attempts(pVal: Integer); safecall;
    function Get_Account_reset_time: Integer; safecall;
    procedure Set_Account_reset_time(pVal: Integer); safecall;
    function Get_Account_lockout_forever: WordBool; safecall;
    procedure Set_Account_lockout_forever(pVal: WordBool); safecall;
    function Get_Account_lockout_period: Integer; safecall;
    procedure Set_Account_lockout_period(pVal: Integer); safecall;
    property Max_password_age: WordBool read Get_Max_password_age write Set_Max_password_age;
    property Pass_never_expire: WordBool read Get_Pass_never_expire write Set_Pass_never_expire;
    property Pass_days_to_expire: Integer read Get_Pass_days_to_expire write Set_Pass_days_to_expire;
    property Min_password_age: WordBool read Get_Min_password_age write Set_Min_password_age;
    property Pass_change_immediately: WordBool read Get_Pass_change_immediately write Set_Pass_change_immediately;
    property Pass_days_to_change: Integer read Get_Pass_days_to_change write Set_Pass_days_to_change;
    property Min_password_length: WordBool read Get_Min_password_length write Set_Min_password_length;
    property Pass_blank_password: WordBool read Get_Pass_blank_password write Set_Pass_blank_password;
    property Pass_number_character: Integer read Get_Pass_number_character write Set_Pass_number_character;
    property Password_uniqueness: WordBool read Get_Password_uniqueness write Set_Password_uniqueness;
    property Pass_never_keep_history: WordBool read Get_Pass_never_keep_history write Set_Pass_never_keep_history;
    property number_passwords: Integer read Get_number_passwords write Set_number_passwords;
    property Auto_logout: WordBool read Get_Auto_logout write Set_Auto_logout;
    property Account_never_timeout: WordBool read Get_Account_never_timeout write Set_Account_never_timeout;
    property Account_timeout_period: Integer read Get_Account_timeout_period write Set_Account_timeout_period;
    property Account_lockout: WordBool read Get_Account_lockout write Set_Account_lockout;
    property Account_never_lockout: WordBool read Get_Account_never_lockout write Set_Account_never_lockout;
    property Account_number_bad_attempts: Integer read Get_Account_number_bad_attempts write Set_Account_number_bad_attempts;
    property Account_reset_time: Integer read Get_Account_reset_time write Set_Account_reset_time;
    property Account_lockout_forever: WordBool read Get_Account_lockout_forever write Set_Account_lockout_forever;
    property Account_lockout_period: Integer read Get_Account_lockout_period write Set_Account_lockout_period;
  end;

// *********************************************************************//
// DispIntf:  IPolicyDataDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DDD3313B-253D-4B50-9CA0-FAD4753AC2C4}
// *********************************************************************//
  IPolicyDataDisp = dispinterface
    ['{DDD3313B-253D-4B50-9CA0-FAD4753AC2C4}']
    property Max_password_age: WordBool dispid 1;
    property Pass_never_expire: WordBool dispid 2;
    property Pass_days_to_expire: Integer dispid 3;
    property Min_password_age: WordBool dispid 4;
    property Pass_change_immediately: WordBool dispid 5;
    property Pass_days_to_change: Integer dispid 6;
    property Min_password_length: WordBool dispid 7;
    property Pass_blank_password: WordBool dispid 8;
    property Pass_number_character: Integer dispid 9;
    property Password_uniqueness: WordBool dispid 10;
    property Pass_never_keep_history: WordBool dispid 11;
    property number_passwords: Integer dispid 12;
    property Auto_logout: WordBool dispid 13;
    property Account_never_timeout: WordBool dispid 14;
    property Account_timeout_period: Integer dispid 15;
    property Account_lockout: WordBool dispid 16;
    property Account_never_lockout: WordBool dispid 17;
    property Account_number_bad_attempts: Integer dispid 18;
    property Account_reset_time: Integer dispid 19;
    property Account_lockout_forever: WordBool dispid 20;
    property Account_lockout_period: Integer dispid 21;
  end;

// *********************************************************************//
// Interface: IAcl2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C37F92EC-4ABF-4A60-A230-73CB1E335EF5}
// *********************************************************************//
  IAcl2 = interface(IDispatch)
    ['{C37F92EC-4ABF-4A60-A230-73CB1E335EF5}']
    function Get_Name: WideString; safecall;
    function Get_Description: WideString; safecall;
    procedure Set_Description(const pVal: WideString); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): WideString; safecall;
    function Get_Files: IDispatch; safecall;
    function Get_Points: IDispatch; safecall;
    function Get_Stations: IDispatch; safecall;
    function Get_Customs: IDispatch; safecall;
    function Get_AppActions: IDispatch; safecall;
    function Get_HoursOfWeek: IDispatch; safecall;
    function Get_PolicyData: IDispatch; safecall;
    function Get_Administrator: WordBool; safecall;
    procedure Set_Administrator(pVal: WordBool); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pVal: WideString); safecall;
    function Get_ChangePassNext: WordBool; safecall;
    procedure Set_ChangePassNext(pVal: WordBool); safecall;
    function Get_ChangePassNever: WordBool; safecall;
    procedure Set_ChangePassNever(pVal: WordBool); safecall;
    function Get_AccountDisable: WordBool; safecall;
    procedure Set_AccountDisable(pVal: WordBool); safecall;
    function Get_AccountLockout: WordBool; safecall;
    procedure Set_AccountLockout(pVal: WordBool); safecall;
    function Get_SimultaneousLogins: WordBool; safecall;
    procedure Set_SimultaneousLogins(pVal: WordBool); safecall;
    property Name: WideString read Get_Name;
    property Description: WideString read Get_Description write Set_Description;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: WideString read Get_Item; default;
    property Files: IDispatch read Get_Files;
    property Points: IDispatch read Get_Points;
    property Stations: IDispatch read Get_Stations;
    property Customs: IDispatch read Get_Customs;
    property AppActions: IDispatch read Get_AppActions;
    property HoursOfWeek: IDispatch read Get_HoursOfWeek;
    property PolicyData: IDispatch read Get_PolicyData;
    property Administrator: WordBool read Get_Administrator write Set_Administrator;
    property Password: WideString read Get_Password write Set_Password;
    property ChangePassNext: WordBool read Get_ChangePassNext write Set_ChangePassNext;
    property ChangePassNever: WordBool read Get_ChangePassNever write Set_ChangePassNever;
    property AccountDisable: WordBool read Get_AccountDisable write Set_AccountDisable;
    property AccountLockout: WordBool read Get_AccountLockout write Set_AccountLockout;
    property SimultaneousLogins: WordBool read Get_SimultaneousLogins write Set_SimultaneousLogins;
  end;

// *********************************************************************//
// DispIntf:  IAcl2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C37F92EC-4ABF-4A60-A230-73CB1E335EF5}
// *********************************************************************//
  IAcl2Disp = dispinterface
    ['{C37F92EC-4ABF-4A60-A230-73CB1E335EF5}']
    property Name: WideString readonly dispid 1;
    property Description: WideString dispid 2;
    property Count: Integer readonly dispid 3;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: WideString readonly dispid 0; default;
    property Files: IDispatch readonly dispid 4;
    property Points: IDispatch readonly dispid 5;
    property Stations: IDispatch readonly dispid 6;
    property Customs: IDispatch readonly dispid 7;
    property AppActions: IDispatch readonly dispid 8;
    property HoursOfWeek: IDispatch readonly dispid 9;
    property PolicyData: IDispatch readonly dispid 10;
    property Administrator: WordBool dispid 11;
    property Password: WideString dispid 12;
    property ChangePassNext: WordBool dispid 13;
    property ChangePassNever: WordBool dispid 14;
    property AccountDisable: WordBool dispid 15;
    property AccountLockout: WordBool dispid 16;
    property SimultaneousLogins: WordBool dispid 17;
  end;

// *********************************************************************//
// Interface: IGroups2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {30979B9C-DD5C-4C91-97E9-09A2E714F88F}
// *********************************************************************//
  IGroups2 = interface(IDispatch)
    ['{30979B9C-DD5C-4C91-97E9-09A2E714F88F}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IGroups2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {30979B9C-DD5C-4C91-97E9-09A2E714F88F}
// *********************************************************************//
  IGroups2Disp = dispinterface
    ['{30979B9C-DD5C-4C91-97E9-09A2E714F88F}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IUsers2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B7969588-4C91-4ECD-B66B-156CFB539DFF}
// *********************************************************************//
  IUsers2 = interface(IDispatch)
    ['{B7969588-4C91-4ECD-B66B-156CFB539DFF}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IUsers2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B7969588-4C91-4ECD-B66B-156CFB539DFF}
// *********************************************************************//
  IUsers2Disp = dispinterface
    ['{B7969588-4C91-4ECD-B66B-156CFB539DFF}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IAppActionDefs
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {29CD8C63-D158-4DD1-9E96-7ABF969A03AE}
// *********************************************************************//
  IAppActionDefs = interface(IDispatch)
    ['{29CD8C63-D158-4DD1-9E96-7ABF969A03AE}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IAppActionDefsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {29CD8C63-D158-4DD1-9E96-7ABF969A03AE}
// *********************************************************************//
  IAppActionDefsDisp = dispinterface
    ['{29CD8C63-D158-4DD1-9E96-7ABF969A03AE}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IAppActionDef
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1CC61B2-E70B-45E9-BB63-42974B011E09}
// *********************************************************************//
  IAppActionDef = interface(IDispatch)
    ['{E1CC61B2-E70B-45E9-BB63-42974B011E09}']
    function Get_AppName: WideString; safecall;
    function Get_AppAction: WideString; safecall;
    function Get_ID: Integer; safecall;
    property AppName: WideString read Get_AppName;
    property AppAction: WideString read Get_AppAction;
    property ID: Integer read Get_ID;
  end;

// *********************************************************************//
// DispIntf:  IAppActionDefDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E1CC61B2-E70B-45E9-BB63-42974B011E09}
// *********************************************************************//
  IAppActionDefDisp = dispinterface
    ['{E1CC61B2-E70B-45E9-BB63-42974B011E09}']
    property AppName: WideString readonly dispid 1;
    property AppAction: WideString readonly dispid 2;
    property ID: Integer readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IConfig3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {30200612-288A-459D-923A-39BA7A73F89B}
// *********************************************************************//
  IConfig3 = interface(IDispatch)
    ['{30200612-288A-459D-923A-39BA7A73F89B}']
    function Get_Users: IDispatch; safecall;
    function Get_Groups: IDispatch; safecall;
    function Get_DefaultGroup: IDispatch; safecall;
    function Get_AddUser(const UserName: WideString): IDispatch; safecall;
    procedure DeleteUser(const UserName: WideString); safecall;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindUser(const UserName: WideString): IDispatch; safecall;
    function Get_AddGroup(const GroupName: WideString): IDispatch; safecall;
    procedure DeleteGroup(const GroupName: WideString); safecall;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); safecall;
    function Get_FindGroup(const GroupName: WideString): IDispatch; safecall;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); safecall;
    procedure FileNew; safecall;
    procedure FileOpen(const FileName: WideString); safecall;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); safecall;
    procedure FileSave; safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); safecall;
    function Get_AppActionDefs: IDispatch; safecall;
    function Get_GlobalPolicy: IDispatch; safecall;
    property Users: IDispatch read Get_Users;
    property Groups: IDispatch read Get_Groups;
    property DefaultGroup: IDispatch read Get_DefaultGroup;
    property AddUser[const UserName: WideString]: IDispatch read Get_AddUser;
    property FindUser[const UserName: WideString]: IDispatch read Get_FindUser;
    property AddGroup[const GroupName: WideString]: IDispatch read Get_AddGroup;
    property FindGroup[const GroupName: WideString]: IDispatch read Get_FindGroup;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
    property AppActionDefs: IDispatch read Get_AppActionDefs;
    property GlobalPolicy: IDispatch read Get_GlobalPolicy;
  end;

// *********************************************************************//
// DispIntf:  IConfig3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {30200612-288A-459D-923A-39BA7A73F89B}
// *********************************************************************//
  IConfig3Disp = dispinterface
    ['{30200612-288A-459D-923A-39BA7A73F89B}']
    property Users: IDispatch readonly dispid 1;
    property Groups: IDispatch readonly dispid 2;
    property DefaultGroup: IDispatch readonly dispid 3;
    property AddUser[const UserName: WideString]: IDispatch readonly dispid 4;
    procedure DeleteUser(const UserName: WideString); dispid 5;
    procedure RenameUser(const OldName: WideString; const NewName: WideString); dispid 6;
    property FindUser[const UserName: WideString]: IDispatch readonly dispid 7;
    property AddGroup[const GroupName: WideString]: IDispatch readonly dispid 8;
    procedure DeleteGroup(const GroupName: WideString); dispid 9;
    procedure RenameGroup(const OldName: WideString; const NewName: WideString); dispid 10;
    property FindGroup[const GroupName: WideString]: IDispatch readonly dispid 11;
    procedure AssocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 12;
    procedure DissocUserGroup(const UserName: WideString; const GroupName: WideString); dispid 13;
    procedure FileNew; dispid 14;
    procedure FileOpen(const FileName: WideString); dispid 15;
    procedure FileSaveAs(const FileName: WideString; Overwrite: WordBool); dispid 16;
    procedure FileSave; dispid 17;
    property ServerPublicKey: OleVariant readonly dispid 18;
    procedure AdminLogin(const ComputerName: WideString; const UserName: WideString; 
                         PasswordBlob: OleVariant); dispid 19;
    property AppActionDefs: IDispatch readonly dispid 20;
    property GlobalPolicy: IDispatch readonly dispid 21;
  end;

// *********************************************************************//
// Interface: IPolicyData3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D88A07AA-1BC9-441B-A254-7AF2BE0BBDAE}
// *********************************************************************//
  IPolicyData3 = interface(IDispatch)
    ['{D88A07AA-1BC9-441B-A254-7AF2BE0BBDAE}']
    function Get_Max_password_age: WordBool; safecall;
    procedure Set_Max_password_age(pVal: WordBool); safecall;
    function Get_Pass_never_expire: WordBool; safecall;
    procedure Set_Pass_never_expire(pVal: WordBool); safecall;
    function Get_Pass_days_to_expire: Integer; safecall;
    procedure Set_Pass_days_to_expire(pVal: Integer); safecall;
    function Get_Min_password_age: WordBool; safecall;
    procedure Set_Min_password_age(pVal: WordBool); safecall;
    function Get_Pass_change_immediately: WordBool; safecall;
    procedure Set_Pass_change_immediately(pVal: WordBool); safecall;
    function Get_Pass_days_to_change: Integer; safecall;
    procedure Set_Pass_days_to_change(pVal: Integer); safecall;
    function Get_Min_password_length: WordBool; safecall;
    procedure Set_Min_password_length(pVal: WordBool); safecall;
    function Get_Pass_blank_password: WordBool; safecall;
    procedure Set_Pass_blank_password(pVal: WordBool); safecall;
    function Get_Pass_number_character: Integer; safecall;
    procedure Set_Pass_number_character(pVal: Integer); safecall;
    function Get_Password_uniqueness: WordBool; safecall;
    procedure Set_Password_uniqueness(pVal: WordBool); safecall;
    function Get_Pass_never_keep_history: WordBool; safecall;
    procedure Set_Pass_never_keep_history(pVal: WordBool); safecall;
    function Get_number_passwords: Integer; safecall;
    procedure Set_number_passwords(pVal: Integer); safecall;
    function Get_Auto_logout: WordBool; safecall;
    procedure Set_Auto_logout(pVal: WordBool); safecall;
    function Get_Account_never_timeout: WordBool; safecall;
    procedure Set_Account_never_timeout(pVal: WordBool); safecall;
    function Get_Account_timeout_period: Integer; safecall;
    procedure Set_Account_timeout_period(pVal: Integer); safecall;
    function Get_Account_lockout: WordBool; safecall;
    procedure Set_Account_lockout(pVal: WordBool); safecall;
    function Get_Account_never_lockout: WordBool; safecall;
    procedure Set_Account_never_lockout(pVal: WordBool); safecall;
    function Get_Account_number_bad_attempts: Integer; safecall;
    procedure Set_Account_number_bad_attempts(pVal: Integer); safecall;
    function Get_Account_reset_time: Integer; safecall;
    procedure Set_Account_reset_time(pVal: Integer); safecall;
    function Get_Account_lockout_forever: WordBool; safecall;
    procedure Set_Account_lockout_forever(pVal: WordBool); safecall;
    function Get_Account_lockout_period: Integer; safecall;
    procedure Set_Account_lockout_period(pVal: Integer); safecall;
    function Get_Logout_password: WordBool; safecall;
    procedure Set_Logout_password(pVal: WordBool); safecall;
    function Get_Logout_password_required: WordBool; safecall;
    procedure Set_Logout_password_required(pVal: WordBool); safecall;
    function Get_Password_complexity: WordBool; safecall;
    procedure Set_Password_complexity(pVal: WordBool); safecall;
    function Get_Password_complexity_required: WordBool; safecall;
    procedure Set_Password_complexity_required(pVal: WordBool); safecall;
    property Max_password_age: WordBool read Get_Max_password_age write Set_Max_password_age;
    property Pass_never_expire: WordBool read Get_Pass_never_expire write Set_Pass_never_expire;
    property Pass_days_to_expire: Integer read Get_Pass_days_to_expire write Set_Pass_days_to_expire;
    property Min_password_age: WordBool read Get_Min_password_age write Set_Min_password_age;
    property Pass_change_immediately: WordBool read Get_Pass_change_immediately write Set_Pass_change_immediately;
    property Pass_days_to_change: Integer read Get_Pass_days_to_change write Set_Pass_days_to_change;
    property Min_password_length: WordBool read Get_Min_password_length write Set_Min_password_length;
    property Pass_blank_password: WordBool read Get_Pass_blank_password write Set_Pass_blank_password;
    property Pass_number_character: Integer read Get_Pass_number_character write Set_Pass_number_character;
    property Password_uniqueness: WordBool read Get_Password_uniqueness write Set_Password_uniqueness;
    property Pass_never_keep_history: WordBool read Get_Pass_never_keep_history write Set_Pass_never_keep_history;
    property number_passwords: Integer read Get_number_passwords write Set_number_passwords;
    property Auto_logout: WordBool read Get_Auto_logout write Set_Auto_logout;
    property Account_never_timeout: WordBool read Get_Account_never_timeout write Set_Account_never_timeout;
    property Account_timeout_period: Integer read Get_Account_timeout_period write Set_Account_timeout_period;
    property Account_lockout: WordBool read Get_Account_lockout write Set_Account_lockout;
    property Account_never_lockout: WordBool read Get_Account_never_lockout write Set_Account_never_lockout;
    property Account_number_bad_attempts: Integer read Get_Account_number_bad_attempts write Set_Account_number_bad_attempts;
    property Account_reset_time: Integer read Get_Account_reset_time write Set_Account_reset_time;
    property Account_lockout_forever: WordBool read Get_Account_lockout_forever write Set_Account_lockout_forever;
    property Account_lockout_period: Integer read Get_Account_lockout_period write Set_Account_lockout_period;
    property Logout_password: WordBool read Get_Logout_password write Set_Logout_password;
    property Logout_password_required: WordBool read Get_Logout_password_required write Set_Logout_password_required;
    property Password_complexity: WordBool read Get_Password_complexity write Set_Password_complexity;
    property Password_complexity_required: WordBool read Get_Password_complexity_required write Set_Password_complexity_required;
  end;

// *********************************************************************//
// DispIntf:  IPolicyData3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D88A07AA-1BC9-441B-A254-7AF2BE0BBDAE}
// *********************************************************************//
  IPolicyData3Disp = dispinterface
    ['{D88A07AA-1BC9-441B-A254-7AF2BE0BBDAE}']
    property Max_password_age: WordBool dispid 1;
    property Pass_never_expire: WordBool dispid 2;
    property Pass_days_to_expire: Integer dispid 3;
    property Min_password_age: WordBool dispid 4;
    property Pass_change_immediately: WordBool dispid 5;
    property Pass_days_to_change: Integer dispid 6;
    property Min_password_length: WordBool dispid 7;
    property Pass_blank_password: WordBool dispid 8;
    property Pass_number_character: Integer dispid 9;
    property Password_uniqueness: WordBool dispid 10;
    property Pass_never_keep_history: WordBool dispid 11;
    property number_passwords: Integer dispid 12;
    property Auto_logout: WordBool dispid 13;
    property Account_never_timeout: WordBool dispid 14;
    property Account_timeout_period: Integer dispid 15;
    property Account_lockout: WordBool dispid 16;
    property Account_never_lockout: WordBool dispid 17;
    property Account_number_bad_attempts: Integer dispid 18;
    property Account_reset_time: Integer dispid 19;
    property Account_lockout_forever: WordBool dispid 20;
    property Account_lockout_period: Integer dispid 21;
    property Logout_password: WordBool dispid 22;
    property Logout_password_required: WordBool dispid 23;
    property Password_complexity: WordBool dispid 24;
    property Password_complexity_required: WordBool dispid 25;
  end;

// *********************************************************************//
// Interface: IAcl3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {000F25EB-1347-445C-B6D5-316E07AC2102}
// *********************************************************************//
  IAcl3 = interface(IDispatch)
    ['{000F25EB-1347-445C-B6D5-316E07AC2102}']
    function Get_Name: WideString; safecall;
    function Get_Description: WideString; safecall;
    procedure Set_Description(const pVal: WideString); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): WideString; safecall;
    function Get_Files: IDispatch; safecall;
    function Get_Points: IDispatch; safecall;
    function Get_Stations: IDispatch; safecall;
    function Get_Customs: IDispatch; safecall;
    function Get_AppActions: IDispatch; safecall;
    function Get_HoursOfWeek: IDispatch; safecall;
    function Get_PolicyData: IDispatch; safecall;
    function Get_Administrator: WordBool; safecall;
    procedure Set_Administrator(pVal: WordBool); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pVal: WideString); safecall;
    function Get_ChangePassNext: WordBool; safecall;
    procedure Set_ChangePassNext(pVal: WordBool); safecall;
    function Get_ChangePassNever: WordBool; safecall;
    procedure Set_ChangePassNever(pVal: WordBool); safecall;
    function Get_AccountDisable: WordBool; safecall;
    procedure Set_AccountDisable(pVal: WordBool); safecall;
    function Get_AccountLockout: WordBool; safecall;
    procedure Set_AccountLockout(pVal: WordBool); safecall;
    function Get_SimultaneousLogins: WordBool; safecall;
    procedure Set_SimultaneousLogins(pVal: WordBool); safecall;
    function Get_Alarms: IDispatch; safecall;
    function Get_Domain: WideString; safecall;
    procedure Set_Domain(const pVal: WideString); safecall;
    property Name: WideString read Get_Name;
    property Description: WideString read Get_Description write Set_Description;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: WideString read Get_Item; default;
    property Files: IDispatch read Get_Files;
    property Points: IDispatch read Get_Points;
    property Stations: IDispatch read Get_Stations;
    property Customs: IDispatch read Get_Customs;
    property AppActions: IDispatch read Get_AppActions;
    property HoursOfWeek: IDispatch read Get_HoursOfWeek;
    property PolicyData: IDispatch read Get_PolicyData;
    property Administrator: WordBool read Get_Administrator write Set_Administrator;
    property Password: WideString read Get_Password write Set_Password;
    property ChangePassNext: WordBool read Get_ChangePassNext write Set_ChangePassNext;
    property ChangePassNever: WordBool read Get_ChangePassNever write Set_ChangePassNever;
    property AccountDisable: WordBool read Get_AccountDisable write Set_AccountDisable;
    property AccountLockout: WordBool read Get_AccountLockout write Set_AccountLockout;
    property SimultaneousLogins: WordBool read Get_SimultaneousLogins write Set_SimultaneousLogins;
    property Alarms: IDispatch read Get_Alarms;
    property Domain: WideString read Get_Domain write Set_Domain;
  end;

// *********************************************************************//
// DispIntf:  IAcl3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {000F25EB-1347-445C-B6D5-316E07AC2102}
// *********************************************************************//
  IAcl3Disp = dispinterface
    ['{000F25EB-1347-445C-B6D5-316E07AC2102}']
    property Name: WideString readonly dispid 1;
    property Description: WideString dispid 2;
    property Count: Integer readonly dispid 3;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: WideString readonly dispid 0; default;
    property Files: IDispatch readonly dispid 4;
    property Points: IDispatch readonly dispid 5;
    property Stations: IDispatch readonly dispid 6;
    property Customs: IDispatch readonly dispid 7;
    property AppActions: IDispatch readonly dispid 8;
    property HoursOfWeek: IDispatch readonly dispid 9;
    property PolicyData: IDispatch readonly dispid 10;
    property Administrator: WordBool dispid 11;
    property Password: WideString dispid 12;
    property ChangePassNext: WordBool dispid 13;
    property ChangePassNever: WordBool dispid 14;
    property AccountDisable: WordBool dispid 15;
    property AccountLockout: WordBool dispid 16;
    property SimultaneousLogins: WordBool dispid 17;
    property Alarms: IDispatch readonly dispid 18;
    property Domain: WideString dispid 19;
  end;

// *********************************************************************//
// Interface: IGroups3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DE2F5DF7-A1EE-4C34-8D5F-FA74977EB489}
// *********************************************************************//
  IGroups3 = interface(IDispatch)
    ['{DE2F5DF7-A1EE-4C34-8D5F-FA74977EB489}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IGroups3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DE2F5DF7-A1EE-4C34-8D5F-FA74977EB489}
// *********************************************************************//
  IGroups3Disp = dispinterface
    ['{DE2F5DF7-A1EE-4C34-8D5F-FA74977EB489}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IUsers3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {364840A8-3A73-450F-A2FA-32E6E5DADCFA}
// *********************************************************************//
  IUsers3 = interface(IDispatch)
    ['{364840A8-3A73-450F-A2FA-32E6E5DADCFA}']
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(Index: Integer): IDispatch; safecall;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  IUsers3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {364840A8-3A73-450F-A2FA-32E6E5DADCFA}
// *********************************************************************//
  IUsers3Disp = dispinterface
    ['{364840A8-3A73-450F-A2FA-32E6E5DADCFA}']
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: IGlobalPolicy
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3EEFF3F2-DE2C-447F-8695-595595A7BCD9}
// *********************************************************************//
  IGlobalPolicy = interface(IDispatch)
    ['{3EEFF3F2-DE2C-447F-8695-595595A7BCD9}']
    function Get_SimultaneousLogins: WordBool; safecall;
    procedure Set_SimultaneousLogins(pVal: WordBool); safecall;
    function Get_AllowAutoNTLogin: WordBool; safecall;
    procedure Set_AllowAutoNTLogin(pVal: WordBool); safecall;
    function Get_AllowUserList: WordBool; safecall;
    procedure Set_AllowUserList(pVal: WordBool); safecall;
    function Get_RememberLastLogin: WordBool; safecall;
    procedure Set_RememberLastLogin(pVal: WordBool); safecall;
    function Get_AdvancedMode: WordBool; safecall;
    function Get_OrphanLoginMinutes: Integer; safecall;
    procedure Set_OrphanLoginMinutes(pVal: Integer); safecall;
    function Get_DomainName: WideString; safecall;
    function Get_IsIntegratedNT: WordBool; safecall;
    function Get_NTSyncMinutes: Integer; safecall;
    procedure Set_NTSyncMinutes(pVal: Integer); safecall;
    function Get_CriticalLoginPeriod: Integer; safecall;
    procedure Set_CriticalLoginPeriod(pVal: Integer); safecall;
    function Get_FullNameInEvents: WordBool; safecall;
    procedure Set_FullNameInEvents(pVal: WordBool); safecall;
    function Get_CriticalPoints: IDispatch; safecall;
    property SimultaneousLogins: WordBool read Get_SimultaneousLogins write Set_SimultaneousLogins;
    property AllowAutoNTLogin: WordBool read Get_AllowAutoNTLogin write Set_AllowAutoNTLogin;
    property AllowUserList: WordBool read Get_AllowUserList write Set_AllowUserList;
    property RememberLastLogin: WordBool read Get_RememberLastLogin write Set_RememberLastLogin;
    property AdvancedMode: WordBool read Get_AdvancedMode;
    property OrphanLoginMinutes: Integer read Get_OrphanLoginMinutes write Set_OrphanLoginMinutes;
    property DomainName: WideString read Get_DomainName;
    property IsIntegratedNT: WordBool read Get_IsIntegratedNT;
    property NTSyncMinutes: Integer read Get_NTSyncMinutes write Set_NTSyncMinutes;
    property CriticalLoginPeriod: Integer read Get_CriticalLoginPeriod write Set_CriticalLoginPeriod;
    property FullNameInEvents: WordBool read Get_FullNameInEvents write Set_FullNameInEvents;
    property CriticalPoints: IDispatch read Get_CriticalPoints;
  end;

// *********************************************************************//
// DispIntf:  IGlobalPolicyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3EEFF3F2-DE2C-447F-8695-595595A7BCD9}
// *********************************************************************//
  IGlobalPolicyDisp = dispinterface
    ['{3EEFF3F2-DE2C-447F-8695-595595A7BCD9}']
    property SimultaneousLogins: WordBool dispid 1;
    property AllowAutoNTLogin: WordBool dispid 2;
    property AllowUserList: WordBool dispid 3;
    property RememberLastLogin: WordBool dispid 4;
    property AdvancedMode: WordBool readonly dispid 5;
    property OrphanLoginMinutes: Integer dispid 6;
    property DomainName: WideString readonly dispid 7;
    property IsIntegratedNT: WordBool readonly dispid 8;
    property NTSyncMinutes: Integer dispid 9;
    property CriticalLoginPeriod: Integer dispid 10;
    property FullNameInEvents: WordBool dispid 11;
    property CriticalPoints: IDispatch readonly dispid 12;
  end;

// *********************************************************************//
// Interface: ISECDual2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {980D14E3-4DB8-49A9-A621-05259925F597}
// *********************************************************************//
  ISECDual2 = interface(IDispatch)
    ['{980D14E3-4DB8-49A9-A621-05259925F597}']
    function Get_TestFile(const strItem: WideString): WordBool; safecall;
    function Get_TestOPCItem(const strItem: WideString): WordBool; safecall;
    function Get_TestCustom(const strItem: WideString): WordBool; safecall;
    function Get_TestAppAction(AppItem: Integer): WordBool; safecall;
    function Get_Node: WideString; safecall;
    procedure Set_Node(const pVal: WideString); safecall;
    function Get_LoggedIn: WideString; safecall;
    procedure Logout(const UserName: WideString); safecall;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    function Get_TestCriticalPoint(const strItem: WideString): SecRightsEnum; safecall;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_LastLoginID: WideString; safecall;
    property TestFile[const strItem: WideString]: WordBool read Get_TestFile;
    property TestOPCItem[const strItem: WideString]: WordBool read Get_TestOPCItem;
    property TestCustom[const strItem: WideString]: WordBool read Get_TestCustom;
    property TestAppAction[AppItem: Integer]: WordBool read Get_TestAppAction;
    property Node: WideString read Get_Node write Set_Node;
    property LoggedIn: WideString read Get_LoggedIn;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum read Get_TestCriticalPoint;
    property LastLoginID: WideString read Get_LastLoginID;
  end;

// *********************************************************************//
// DispIntf:  ISECDual2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {980D14E3-4DB8-49A9-A621-05259925F597}
// *********************************************************************//
  ISECDual2Disp = dispinterface
    ['{980D14E3-4DB8-49A9-A621-05259925F597}']
    property TestFile[const strItem: WideString]: WordBool readonly dispid 1;
    property TestOPCItem[const strItem: WideString]: WordBool readonly dispid 2;
    property TestCustom[const strItem: WideString]: WordBool readonly dispid 3;
    property TestAppAction[AppItem: Integer]: WordBool readonly dispid 4;
    property Node: WideString dispid 5;
    property LoggedIn: WideString readonly dispid 6;
    procedure Logout(const UserName: WideString); dispid 7;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); dispid 8;
    property ServerPublicKey: OleVariant readonly dispid 9;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum readonly dispid 10;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); dispid 11;
    property LastLoginID: WideString readonly dispid 12;
  end;

// *********************************************************************//
// Interface: ISECDual3
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {01840A20-562C-4B60-A158-C9FC43F665E2}
// *********************************************************************//
  ISECDual3 = interface(IDispatch)
    ['{01840A20-562C-4B60-A158-C9FC43F665E2}']
    function Get_TestFile(const strItem: WideString): WordBool; safecall;
    function Get_TestOPCItem(const strItem: WideString): WordBool; safecall;
    function Get_TestCustom(const strItem: WideString): WordBool; safecall;
    function Get_TestAppAction(AppItem: Integer): WordBool; safecall;
    function Get_Node: WideString; safecall;
    procedure Set_Node(const pVal: WideString); safecall;
    function Get_LoggedIn: WideString; safecall;
    procedure Logout(const UserName: WideString); safecall;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    function Get_TestCriticalPoint(const strItem: WideString): SecRightsEnum; safecall;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_LastLoginID: WideString; safecall;
    function Get_TestAlarms(const strItem: WideString): SecRightsEnum; safecall;
    property TestFile[const strItem: WideString]: WordBool read Get_TestFile;
    property TestOPCItem[const strItem: WideString]: WordBool read Get_TestOPCItem;
    property TestCustom[const strItem: WideString]: WordBool read Get_TestCustom;
    property TestAppAction[AppItem: Integer]: WordBool read Get_TestAppAction;
    property Node: WideString read Get_Node write Set_Node;
    property LoggedIn: WideString read Get_LoggedIn;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum read Get_TestCriticalPoint;
    property LastLoginID: WideString read Get_LastLoginID;
    property TestAlarms[const strItem: WideString]: SecRightsEnum read Get_TestAlarms;
  end;

// *********************************************************************//
// DispIntf:  ISECDual3Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {01840A20-562C-4B60-A158-C9FC43F665E2}
// *********************************************************************//
  ISECDual3Disp = dispinterface
    ['{01840A20-562C-4B60-A158-C9FC43F665E2}']
    property TestFile[const strItem: WideString]: WordBool readonly dispid 1;
    property TestOPCItem[const strItem: WideString]: WordBool readonly dispid 2;
    property TestCustom[const strItem: WideString]: WordBool readonly dispid 3;
    property TestAppAction[AppItem: Integer]: WordBool readonly dispid 4;
    property Node: WideString dispid 5;
    property LoggedIn: WideString readonly dispid 6;
    procedure Logout(const UserName: WideString); dispid 7;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); dispid 8;
    property ServerPublicKey: OleVariant readonly dispid 9;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum readonly dispid 10;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); dispid 11;
    property LastLoginID: WideString readonly dispid 12;
    property TestAlarms[const strItem: WideString]: SecRightsEnum readonly dispid 13;
  end;

// *********************************************************************//
// Interface: ISECDual4
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD3F34D0-3563-46B0-BD10-B428929C8284}
// *********************************************************************//
  ISECDual4 = interface(IDispatch)
    ['{BD3F34D0-3563-46B0-BD10-B428929C8284}']
    function Get_TestFile(const strItem: WideString): WordBool; safecall;
    function Get_TestOPCItem(const strItem: WideString): WordBool; safecall;
    function Get_TestCustom(const strItem: WideString): WordBool; safecall;
    function Get_TestAppAction(AppItem: Integer): WordBool; safecall;
    function Get_Node: WideString; safecall;
    procedure Set_Node(const pVal: WideString); safecall;
    function Get_LoggedIn: WideString; safecall;
    procedure Logout(const UserName: WideString); safecall;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_ServerPublicKey: OleVariant; safecall;
    function Get_TestCriticalPoint(const strItem: WideString): SecRightsEnum; safecall;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); safecall;
    function Get_LastLoginID: WideString; safecall;
    function Get_TestAlarms(const strItem: WideString): SecRightsEnum; safecall;
    function Get_LoggedInGroups: WideString; safecall;
    property TestFile[const strItem: WideString]: WordBool read Get_TestFile;
    property TestOPCItem[const strItem: WideString]: WordBool read Get_TestOPCItem;
    property TestCustom[const strItem: WideString]: WordBool read Get_TestCustom;
    property TestAppAction[AppItem: Integer]: WordBool read Get_TestAppAction;
    property Node: WideString read Get_Node write Set_Node;
    property LoggedIn: WideString read Get_LoggedIn;
    property ServerPublicKey: OleVariant read Get_ServerPublicKey;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum read Get_TestCriticalPoint;
    property LastLoginID: WideString read Get_LastLoginID;
    property TestAlarms[const strItem: WideString]: SecRightsEnum read Get_TestAlarms;
    property LoggedInGroups: WideString read Get_LoggedInGroups;
  end;

// *********************************************************************//
// DispIntf:  ISECDual4Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BD3F34D0-3563-46B0-BD10-B428929C8284}
// *********************************************************************//
  ISECDual4Disp = dispinterface
    ['{BD3F34D0-3563-46B0-BD10-B428929C8284}']
    property TestFile[const strItem: WideString]: WordBool readonly dispid 1;
    property TestOPCItem[const strItem: WideString]: WordBool readonly dispid 2;
    property TestCustom[const strItem: WideString]: WordBool readonly dispid 3;
    property TestAppAction[AppItem: Integer]: WordBool readonly dispid 4;
    property Node: WideString dispid 5;
    property LoggedIn: WideString readonly dispid 6;
    procedure Logout(const UserName: WideString); dispid 7;
    procedure LogIn(const UserName: WideString; PasswordBlob: OleVariant); dispid 8;
    property ServerPublicKey: OleVariant readonly dispid 9;
    property TestCriticalPoint[const strItem: WideString]: SecRightsEnum readonly dispid 10;
    procedure Logout2(const UserName: WideString; PasswordBlob: OleVariant); dispid 11;
    property LastLoginID: WideString readonly dispid 12;
    property TestAlarms[const strItem: WideString]: SecRightsEnum readonly dispid 13;
    property LoggedInGroups: WideString readonly dispid 14;
  end;

// *********************************************************************//
// The Class CoSECDual provides a Create and CreateRemote method to          
// create instances of the default interface ISECDual exposed by              
// the CoClass SECDual. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECDual = class
    class function Create: ISECDual;
    class function CreateRemote(const MachineName: string): ISECDual;
  end;

// *********************************************************************//
// The Class CoSECConfig provides a Create and CreateRemote method to          
// create instances of the default interface IConfig exposed by              
// the CoClass SECConfig. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECConfig = class
    class function Create: IConfig;
    class function CreateRemote(const MachineName: string): IConfig;
  end;

// *********************************************************************//
// The Class CoPreferences provides a Create and CreateRemote method to          
// create instances of the default interface IPreferences exposed by              
// the CoClass Preferences. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPreferences = class
    class function Create: IPreferences;
    class function CreateRemote(const MachineName: string): IPreferences;
  end;

// *********************************************************************//
// The Class CoSECConfig2 provides a Create and CreateRemote method to          
// create instances of the default interface IConfig2 exposed by              
// the CoClass SECConfig2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECConfig2 = class
    class function Create: IConfig2;
    class function CreateRemote(const MachineName: string): IConfig2;
  end;

// *********************************************************************//
// The Class CoPrivateSession provides a Create and CreateRemote method to          
// create instances of the default interface ISECDual exposed by              
// the CoClass PrivateSession. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPrivateSession = class
    class function Create: ISECDual;
    class function CreateRemote(const MachineName: string): ISECDual;
  end;

// *********************************************************************//
// The Class CoSECConfig3 provides a Create and CreateRemote method to          
// create instances of the default interface IConfig3 exposed by              
// the CoClass SECConfig3. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECConfig3 = class
    class function Create: IConfig3;
    class function CreateRemote(const MachineName: string): IConfig3;
  end;

// *********************************************************************//
// The Class CoSECDual2 provides a Create and CreateRemote method to          
// create instances of the default interface ISECDual2 exposed by              
// the CoClass SECDual2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECDual2 = class
    class function Create: ISECDual2;
    class function CreateRemote(const MachineName: string): ISECDual2;
  end;

// *********************************************************************//
// The Class CoSECDual3 provides a Create and CreateRemote method to          
// create instances of the default interface ISECDual3 exposed by              
// the CoClass SECDual3. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECDual3 = class
    class function Create: ISECDual3;
    class function CreateRemote(const MachineName: string): ISECDual3;
  end;

// *********************************************************************//
// The Class CoSECDual4 provides a Create and CreateRemote method to          
// create instances of the default interface ISECDual4 exposed by              
// the CoClass SECDual4. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSECDual4 = class
    class function Create: ISECDual4;
    class function CreateRemote(const MachineName: string): ISECDual4;
  end;

implementation

uses ComObj;

class function CoSECDual.Create: ISECDual;
begin
  Result := CreateComObject(CLASS_SECDual) as ISECDual;
end;

class function CoSECDual.CreateRemote(const MachineName: string): ISECDual;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECDual) as ISECDual;
end;

class function CoSECConfig.Create: IConfig;
begin
  Result := CreateComObject(CLASS_SECConfig) as IConfig;
end;

class function CoSECConfig.CreateRemote(const MachineName: string): IConfig;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECConfig) as IConfig;
end;

class function CoPreferences.Create: IPreferences;
begin
  Result := CreateComObject(CLASS_Preferences) as IPreferences;
end;

class function CoPreferences.CreateRemote(const MachineName: string): IPreferences;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Preferences) as IPreferences;
end;

class function CoSECConfig2.Create: IConfig2;
begin
  Result := CreateComObject(CLASS_SECConfig2) as IConfig2;
end;

class function CoSECConfig2.CreateRemote(const MachineName: string): IConfig2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECConfig2) as IConfig2;
end;

class function CoPrivateSession.Create: ISECDual;
begin
  Result := CreateComObject(CLASS_PrivateSession) as ISECDual;
end;

class function CoPrivateSession.CreateRemote(const MachineName: string): ISECDual;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_PrivateSession) as ISECDual;
end;

class function CoSECConfig3.Create: IConfig3;
begin
  Result := CreateComObject(CLASS_SECConfig3) as IConfig3;
end;

class function CoSECConfig3.CreateRemote(const MachineName: string): IConfig3;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECConfig3) as IConfig3;
end;

class function CoSECDual2.Create: ISECDual2;
begin
  Result := CreateComObject(CLASS_SECDual2) as ISECDual2;
end;

class function CoSECDual2.CreateRemote(const MachineName: string): ISECDual2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECDual2) as ISECDual2;
end;

class function CoSECDual3.Create: ISECDual3;
begin
  Result := CreateComObject(CLASS_SECDual3) as ISECDual3;
end;

class function CoSECDual3.CreateRemote(const MachineName: string): ISECDual3;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECDual3) as ISECDual3;
end;

class function CoSECDual4.Create: ISECDual4;
begin
  Result := CreateComObject(CLASS_SECDual4) as ISECDual4;
end;

class function CoSECDual4.CreateRemote(const MachineName: string): ISECDual4;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SECDual4) as ISECDual4;
end;

end.
