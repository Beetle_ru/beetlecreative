// AXTestPropPage.cpp: ���������� ������ �������� ������� CAXTestPropPage.

#include "stdafx.h"
#include "AXTest.h"
#include "AXTestPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CAXTestPropPage, COlePropertyPage)



// ����� ���������

BEGIN_MESSAGE_MAP(CAXTestPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(CAXTestPropPage, "AXTEST.AXTestPropPage.1",
	0x322cb7f3, 0xddd6, 0x44de, 0xb2, 0x59, 0xc1, 0x33, 0x79, 0x1d, 0xcf, 0x4f)



// CAXTestPropPage::CAXTestPropPageFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� CAXTestPropPage

BOOL CAXTestPropPage::CAXTestPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AXTEST_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CAXTestPropPage::CAXTestPropPage - �����������

CAXTestPropPage::CAXTestPropPage() :
	COlePropertyPage(IDD, IDS_AXTEST_PPG_CAPTION)
{
}



// CAXTestPropPage::DoDataExchange - ������� ������ ����� ��������� � ����������

void CAXTestPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// ����������� ��������� CAXTestPropPage
