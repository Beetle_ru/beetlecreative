// AXTestCtrl.cpp: ���������� ������ CAXTestCtrl �������� ActiveX.

#include "stdafx.h"
#include "AXTest.h"
#include "AXTestCtrl.h"
#include "AXTestPropPage.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CAXTestCtrl, COleControl)



// ����� ���������

BEGIN_MESSAGE_MAP(CAXTestCtrl, COleControl)
	ON_MESSAGE(OCM_COMMAND, &CAXTestCtrl::OnOcmCommand)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()



// ����� ���������� � ��������

BEGIN_DISPATCH_MAP(CAXTestCtrl, COleControl)
	DISP_FUNCTION_ID(CAXTestCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()



// ����� �������

BEGIN_EVENT_MAP(CAXTestCtrl, COleControl)
END_EVENT_MAP()



// �������� �������

// TODO: ��� ������������� �������� �������������� �������� �������. �� �������� ��������� �������� ��������.
BEGIN_PROPPAGEIDS(CAXTestCtrl, 1)
	PROPPAGEID(CAXTestPropPage::guid)
END_PROPPAGEIDS(CAXTestCtrl)



// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(CAXTestCtrl, "AXTEST.AXTestCtrl.1",
	0xe2a9fa6d, 0x19e5, 0x44aa, 0xb3, 0x4a, 0x9c, 0x21, 0xb8, 0xf, 0x2d, 0x82)



// ������� �� � ������ ����������

IMPLEMENT_OLETYPELIB(CAXTestCtrl, _tlid, _wVerMajor, _wVerMinor)



// �� ����������

const IID IID_DAXTest = { 0xD8E45BE5, 0xC58D, 0x41FE, { 0xA6, 0xEB, 0x6A, 0xBD, 0x1A, 0x38, 0x34, 0xF4 } };
const IID IID_DAXTestEvents = { 0xFD67822, 0x185B, 0x439D, { 0x8D, 0x22, 0x7B, 0x39, 0x21, 0xF6, 0x54, 0xB6 } };


// �������� � ����� ��������� ����������

static const DWORD _dwAXTestOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAXTestCtrl, IDS_AXTEST, _dwAXTestOleMisc)



// CAXTestCtrl::CAXTestCtrlFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� CAXTestCtrl

BOOL CAXTestCtrl::CAXTestCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: ���������, ��� �������� ���������� ������� �������� ������ ������������� �������.
	// �������������� �������� ��. � MFC TechNote 64.
	// ���� ������� ���������� �� ������������� �������� ������ ��������, ��
	// ���������� �������������� ����������� ���� ���, ������� �������� 6-�� ��������� �
	// afxRegApartmentThreading �� 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AXTEST,
			IDB_AXTEST,
			afxRegApartmentThreading,
			_dwAXTestOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CAXTestCtrl::CAXTestCtrl - �����������

CAXTestCtrl::CAXTestCtrl()
{
	InitializeIIDs(&IID_DAXTest, &IID_DAXTestEvents);
	// TODO: ��������������� ����� ������ ���������� �������� ����������.
	MessageBox(L"sukaZZ");
}



// CAXTestCtrl::~CAXTestCtrl - ����������

CAXTestCtrl::~CAXTestCtrl()
{
	// TODO: ��������� ����� ������� ������ ���������� �������� ����������.
}



// CAXTestCtrl::OnDraw - ������� ���������

void CAXTestCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	DoSuperclassPaint(pdc, rcBounds);
}



// CAXTestCtrl::DoPropExchange - ��������� ����������

void CAXTestCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: �������� ������� PX_ ��� ������� ����������� �������������� ��������.
}



// CAXTestCtrl::OnResetState - ����� �������� ���������� � ��������� �� ���������

void CAXTestCtrl::OnResetState()
{
	COleControl::OnResetState();  // ���������� �������� �� ���������, ��������� � DoPropExchange

	// TODO: �������� ����� ��������� ������ ������� �������� ����������.
}



// CAXTestCtrl::AboutBox - ���������� ������������ ������ "� ���������"

void CAXTestCtrl::AboutBox()
{
	CDialogEx dlgAbout(IDD_ABOUTBOX_AXTEST);
	dlgAbout.DoModal();
}



// CAXTestCtrl::PreCreateWindow - �������� ��������� ��� CreateWindowEx

BOOL CAXTestCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszClass = _T("BUTTON");
	return COleControl::PreCreateWindow(cs);
}



// CAXTestCtrl::IsSubclassedControl - ������ ������� ���������� ������ � ���������

BOOL CAXTestCtrl::IsSubclassedControl()
{
	return TRUE;
}



// CAXTestCtrl::OnOcmCommand - ��������� ��������� ���������

LRESULT CAXTestCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
	WORD wNotifyCode = HIWORD(wParam);

	// TODO: �������� ����� wNotifyCode.

	if (wNotifyCode == BN_CLICKED) {
		MessageBox(L"suka");
	}

	return 0;
}



// ����������� ��������� CAXTestCtrl
