// AXTest.cpp: ���������� CAXTestApp � ����������� DLL.

#include "stdafx.h"
#include "AXTest.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CAXTestApp theApp;

const GUID CDECL _tlid = { 0x67ACE68, 0xFAD6, 0x498B, { 0x8C, 0xCC, 0x40, 0xD1, 0xDF, 0xFD, 0x9C, 0x91 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CAXTestApp::InitInstance - ������������� DLL

BOOL CAXTestApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: �������� ����� ���� ��� ������������� ������.
	}

	return bInit;
}



// CAXTestApp::ExitInstance - ���������� DLL

int CAXTestApp::ExitInstance()
{
	// TODO: �������� ����� ���� ��� ���������� ������ ������.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - ��������� ������ � ��������� ������

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - ������� ������ �� ���������� �������

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
