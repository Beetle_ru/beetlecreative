#pragma once

// AXTestCtrl.h: ���������� ������ �������� ���������� ActiveX ��� CAXTestCtrl.


// CAXTestCtrl: ��� ���������� ��. AXTestCtrl.cpp.

class CAXTestCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAXTestCtrl)

// �����������
public:
	CAXTestCtrl();

// ���������������
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// ����������
protected:
	~CAXTestCtrl();

	DECLARE_OLECREATE_EX(CAXTestCtrl)    // ������� ������ � guid
	DECLARE_OLETYPELIB(CAXTestCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAXTestCtrl)     // �� �������� �������
	DECLARE_OLECTLTYPE(CAXTestCtrl)		// ������� ��� � ������������� ���������

	// ��������� ��������� ���������� � ����������
	BOOL IsSubclassedControl();
	LRESULT OnOcmCommand(WPARAM wParam, LPARAM lParam);

// ����� ���������
	DECLARE_MESSAGE_MAP()

// ����� ���������� � ��������
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// ����� �������
	DECLARE_EVENT_MAP()

// ���������� � �������� � �� �������
public:
	enum {
	};
};

