

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon May 26 13:25:16 2014
 */
/* Compiler settings for AXTest.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, LIBID_AXTestLib,0x067ACE68,0xFAD6,0x498B,0x8C,0xCC,0x40,0xD1,0xDF,0xFD,0x9C,0x91);


MIDL_DEFINE_GUID(IID, DIID__DAXTest,0xD8E45BE5,0xC58D,0x41FE,0xA6,0xEB,0x6A,0xBD,0x1A,0x38,0x34,0xF4);


MIDL_DEFINE_GUID(IID, DIID__DAXTestEvents,0x0FD67822,0x185B,0x439D,0x8D,0x22,0x7B,0x39,0x21,0xF6,0x54,0xB6);


MIDL_DEFINE_GUID(CLSID, CLSID_AXTest,0xE2A9FA6D,0x19E5,0x44AA,0xB3,0x4A,0x9C,0x21,0xB8,0x0F,0x2D,0x82);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



