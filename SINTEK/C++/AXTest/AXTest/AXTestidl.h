

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon May 26 13:25:16 2014
 */
/* Compiler settings for AXTest.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __AXTestidl_h__
#define __AXTestidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DAXTest_FWD_DEFINED__
#define ___DAXTest_FWD_DEFINED__
typedef interface _DAXTest _DAXTest;
#endif 	/* ___DAXTest_FWD_DEFINED__ */


#ifndef ___DAXTestEvents_FWD_DEFINED__
#define ___DAXTestEvents_FWD_DEFINED__
typedef interface _DAXTestEvents _DAXTestEvents;
#endif 	/* ___DAXTestEvents_FWD_DEFINED__ */


#ifndef __AXTest_FWD_DEFINED__
#define __AXTest_FWD_DEFINED__

#ifdef __cplusplus
typedef class AXTest AXTest;
#else
typedef struct AXTest AXTest;
#endif /* __cplusplus */

#endif 	/* __AXTest_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __AXTestLib_LIBRARY_DEFINED__
#define __AXTestLib_LIBRARY_DEFINED__

/* library AXTestLib */
/* [control][version][uuid] */ 


EXTERN_C const IID LIBID_AXTestLib;

#ifndef ___DAXTest_DISPINTERFACE_DEFINED__
#define ___DAXTest_DISPINTERFACE_DEFINED__

/* dispinterface _DAXTest */
/* [uuid] */ 


EXTERN_C const IID DIID__DAXTest;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("D8E45BE5-C58D-41FE-A6EB-6ABD1A3834F4")
    _DAXTest : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DAXTestVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DAXTest * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DAXTest * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DAXTest * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DAXTest * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DAXTest * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DAXTest * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DAXTest * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DAXTestVtbl;

    interface _DAXTest
    {
        CONST_VTBL struct _DAXTestVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DAXTest_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DAXTest_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DAXTest_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DAXTest_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DAXTest_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DAXTest_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DAXTest_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DAXTest_DISPINTERFACE_DEFINED__ */


#ifndef ___DAXTestEvents_DISPINTERFACE_DEFINED__
#define ___DAXTestEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DAXTestEvents */
/* [uuid] */ 


EXTERN_C const IID DIID__DAXTestEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("0FD67822-185B-439D-8D22-7B3921F654B6")
    _DAXTestEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DAXTestEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DAXTestEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DAXTestEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DAXTestEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DAXTestEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DAXTestEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DAXTestEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DAXTestEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DAXTestEventsVtbl;

    interface _DAXTestEvents
    {
        CONST_VTBL struct _DAXTestEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DAXTestEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DAXTestEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DAXTestEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DAXTestEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DAXTestEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DAXTestEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DAXTestEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DAXTestEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_AXTest;

#ifdef __cplusplus

class DECLSPEC_UUID("E2A9FA6D-19E5-44AA-B34A-9C21B80F2D82")
AXTest;
#endif
#endif /* __AXTestLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


