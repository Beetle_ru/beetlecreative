

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Mon Jun 02 10:43:56 2014
 */
/* Compiler settings for activex2.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __activex2idl_h__
#define __activex2idl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___Dactivex2_FWD_DEFINED__
#define ___Dactivex2_FWD_DEFINED__
typedef interface _Dactivex2 _Dactivex2;
#endif 	/* ___Dactivex2_FWD_DEFINED__ */


#ifndef ___Dactivex2Events_FWD_DEFINED__
#define ___Dactivex2Events_FWD_DEFINED__
typedef interface _Dactivex2Events _Dactivex2Events;
#endif 	/* ___Dactivex2Events_FWD_DEFINED__ */


#ifndef __activex2_FWD_DEFINED__
#define __activex2_FWD_DEFINED__

#ifdef __cplusplus
typedef class activex2 activex2;
#else
typedef struct activex2 activex2;
#endif /* __cplusplus */

#endif 	/* __activex2_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __activex2Lib_LIBRARY_DEFINED__
#define __activex2Lib_LIBRARY_DEFINED__

/* library activex2Lib */
/* [control][version][uuid] */ 


EXTERN_C const IID LIBID_activex2Lib;

#ifndef ___Dactivex2_DISPINTERFACE_DEFINED__
#define ___Dactivex2_DISPINTERFACE_DEFINED__

/* dispinterface _Dactivex2 */
/* [uuid] */ 


EXTERN_C const IID DIID__Dactivex2;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("835BD739-1502-4DBB-AD7A-0AA1082211F0")
    _Dactivex2 : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _Dactivex2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _Dactivex2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _Dactivex2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _Dactivex2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _Dactivex2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _Dactivex2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _Dactivex2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _Dactivex2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _Dactivex2Vtbl;

    interface _Dactivex2
    {
        CONST_VTBL struct _Dactivex2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _Dactivex2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _Dactivex2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _Dactivex2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _Dactivex2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _Dactivex2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _Dactivex2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _Dactivex2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___Dactivex2_DISPINTERFACE_DEFINED__ */


#ifndef ___Dactivex2Events_DISPINTERFACE_DEFINED__
#define ___Dactivex2Events_DISPINTERFACE_DEFINED__

/* dispinterface _Dactivex2Events */
/* [uuid] */ 


EXTERN_C const IID DIID__Dactivex2Events;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("1AF56996-D497-423C-A8F5-9F92B27776BC")
    _Dactivex2Events : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _Dactivex2EventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _Dactivex2Events * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _Dactivex2Events * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _Dactivex2Events * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _Dactivex2Events * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _Dactivex2Events * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _Dactivex2Events * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _Dactivex2Events * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _Dactivex2EventsVtbl;

    interface _Dactivex2Events
    {
        CONST_VTBL struct _Dactivex2EventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _Dactivex2Events_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _Dactivex2Events_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _Dactivex2Events_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _Dactivex2Events_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _Dactivex2Events_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _Dactivex2Events_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _Dactivex2Events_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___Dactivex2Events_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_activex2;

#ifdef __cplusplus

class DECLSPEC_UUID("185E87CA-CAF3-4289-9A0E-ABAC5D7E8173")
activex2;
#endif
#endif /* __activex2Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


