// activex2PropPage.cpp: ���������� ������ �������� ������� Cactivex2PropPage.

#include "stdafx.h"
#include "activex2.h"
#include "activex2PropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(Cactivex2PropPage, COlePropertyPage)



// ����� ���������

BEGIN_MESSAGE_MAP(Cactivex2PropPage, COlePropertyPage)
END_MESSAGE_MAP()



// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(Cactivex2PropPage, "ACTIVEX2.activex2PropPage.1",
	0x42b69f4b, 0x704d, 0x4f6a, 0x86, 0xd5, 0xbc, 0xc6, 0x7e, 0xf, 0x49, 0xcb)



// Cactivex2PropPage::Cactivex2PropPageFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� Cactivex2PropPage

BOOL Cactivex2PropPage::Cactivex2PropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_ACTIVEX2_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// Cactivex2PropPage::Cactivex2PropPage - �����������

Cactivex2PropPage::Cactivex2PropPage() :
	COlePropertyPage(IDD, IDS_ACTIVEX2_PPG_CAPTION)
{
}



// Cactivex2PropPage::DoDataExchange - ������� ������ ����� ��������� � ����������

void Cactivex2PropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// ����������� ��������� Cactivex2PropPage
