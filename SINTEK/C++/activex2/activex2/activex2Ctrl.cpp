// activex2Ctrl.cpp: ���������� ������ Cactivex2Ctrl �������� ActiveX.

#include "stdafx.h"
#include "activex2.h"
#include "activex2Ctrl.h"
#include "activex2PropPage.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(Cactivex2Ctrl, COleControl)



// ����� ���������

BEGIN_MESSAGE_MAP(Cactivex2Ctrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()



// ����� ���������� � ��������

BEGIN_DISPATCH_MAP(Cactivex2Ctrl, COleControl)
	DISP_FUNCTION_ID(Cactivex2Ctrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(Cactivex2Ctrl, "method1", dispidmethod1, method1, VT_UI4, VTS_NONE)
	DISP_FUNCTION_ID(Cactivex2Ctrl, "method2", dispidmethod2, method2, VT_UI4, VTS_NONE)
	DISP_PROPERTY_EX_ID(Cactivex2Ctrl, "longProp", dispidlongProp, GetlongProp, SetlongProp, VT_I4)
	DISP_FUNCTION_ID(Cactivex2Ctrl, "getBstr", dispidgetBstr, getBstr, VT_BSTR, VTS_NONE)
	DISP_FUNCTION_ID(Cactivex2Ctrl, "setBstr", dispidsetBstr, setBstr, VT_EMPTY, VTS_BSTR)
END_DISPATCH_MAP()



// ����� �������

BEGIN_EVENT_MAP(Cactivex2Ctrl, COleControl)
END_EVENT_MAP()



// �������� �������

// TODO: ��� ������������� �������� �������������� �������� �������. �� �������� ��������� �������� ��������.
BEGIN_PROPPAGEIDS(Cactivex2Ctrl, 1)
	PROPPAGEID(Cactivex2PropPage::guid)
END_PROPPAGEIDS(Cactivex2Ctrl)



// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(Cactivex2Ctrl, "ACTIVEX2.activex2Ctrl.1",
	0x185e87ca, 0xcaf3, 0x4289, 0x9a, 0xe, 0xab, 0xac, 0x5d, 0x7e, 0x81, 0x73)



// ������� �� � ������ ����������

IMPLEMENT_OLETYPELIB(Cactivex2Ctrl, _tlid, _wVerMajor, _wVerMinor)



// �� ����������

const IID IID_Dactivex2 = { 0x835BD739, 0x1502, 0x4DBB, { 0xAD, 0x7A, 0xA, 0xA1, 0x8, 0x22, 0x11, 0xF0 } };
const IID IID_Dactivex2Events = { 0x1AF56996, 0xD497, 0x423C, { 0xA8, 0xF5, 0x9F, 0x92, 0xB2, 0x77, 0x76, 0xBC } };


// �������� � ����� ��������� ����������

static const DWORD _dwactivex2OleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(Cactivex2Ctrl, IDS_ACTIVEX2, _dwactivex2OleMisc)



// Cactivex2Ctrl::Cactivex2CtrlFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� Cactivex2Ctrl

BOOL Cactivex2Ctrl::Cactivex2CtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: ���������, ��� �������� ���������� ������� �������� ������ ������������� �������.
	// �������������� �������� ��. � MFC TechNote 64.
	// ���� ������� ���������� �� ������������� �������� ������ ��������, ��
	// ���������� �������������� ����������� ���� ���, ������� �������� 6-�� ��������� �
	// afxRegApartmentThreading �� 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_ACTIVEX2,
			IDB_ACTIVEX2,
			afxRegApartmentThreading,
			_dwactivex2OleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// Cactivex2Ctrl::Cactivex2Ctrl - �����������

Cactivex2Ctrl::Cactivex2Ctrl()
{
	InitializeIIDs(&IID_Dactivex2, &IID_Dactivex2Events);
	// TODO: ��������������� ����� ������ ���������� �������� ����������.
}



// Cactivex2Ctrl::~Cactivex2Ctrl - ����������

Cactivex2Ctrl::~Cactivex2Ctrl()
{
	// TODO: ��������� ����� ������� ������ ���������� �������� ����������.
}



// Cactivex2Ctrl::OnDraw - ������� ���������

void Cactivex2Ctrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	// TODO: �������� ��������� ��� ����������� ����� ���������.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}



// Cactivex2Ctrl::DoPropExchange - ��������� ����������

void Cactivex2Ctrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: �������� ������� PX_ ��� ������� ����������� �������������� ��������.
}



// Cactivex2Ctrl::OnResetState - ����� �������� ���������� � ��������� �� ���������

void Cactivex2Ctrl::OnResetState()
{
	COleControl::OnResetState();  // ���������� �������� �� ���������, ��������� � DoPropExchange

	// TODO: �������� ����� ��������� ������ ������� �������� ����������.
}



// Cactivex2Ctrl::AboutBox - ���������� ������������ ������ "� ���������"

void Cactivex2Ctrl::AboutBox()
{
	CDialogEx dlgAbout(IDD_ABOUTBOX_ACTIVEX2);
	dlgAbout.DoModal();
}



// ����������� ��������� Cactivex2Ctrl


ULONG Cactivex2Ctrl::method1(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::method1");
	return 0;
}


ULONG Cactivex2Ctrl::method2(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::method2");
	return 0;
}


LONG Cactivex2Ctrl::GetlongProp(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::GetlongProp");
	return 0;
}


void Cactivex2Ctrl::SetlongProp(LONG newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::SetlongProp");
	SetModifiedFlag();
}


BSTR Cactivex2Ctrl::getBstr(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString strResult;

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::getBstr");
	return strResult.AllocSysString();
}


void Cactivex2Ctrl::setBstr(LPCTSTR str)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: �������� ��� ����������� ��������
	MessageBox(L"::setBstr");
}
