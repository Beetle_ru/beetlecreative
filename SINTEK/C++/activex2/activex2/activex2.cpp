// activex2.cpp: ���������� Cactivex2App � ����������� DLL.

#include "stdafx.h"
#include "activex2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


Cactivex2App theApp;

const GUID CDECL _tlid = { 0x74E855D3, 0x2160, 0x4A7F, { 0xB1, 0x43, 0x7D, 0x19, 0xAA, 0x8, 0x64, 0xE1 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// Cactivex2App::InitInstance - ������������� DLL

BOOL Cactivex2App::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: �������� ����� ���� ��� ������������� ������.
	}

	return bInit;
}



// Cactivex2App::ExitInstance - ���������� DLL

int Cactivex2App::ExitInstance()
{
	// TODO: �������� ����� ���� ��� ���������� ������ ������.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - ��������� ������ � ��������� ������

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - ������� ������ �� ���������� �������

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
