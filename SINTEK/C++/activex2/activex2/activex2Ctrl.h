#pragma once

// activex2Ctrl.h: ���������� ������ �������� ���������� ActiveX ��� Cactivex2Ctrl.


// Cactivex2Ctrl: ��� ���������� ��. activex2Ctrl.cpp.

class Cactivex2Ctrl : public COleControl
{
	DECLARE_DYNCREATE(Cactivex2Ctrl)

// �����������
public:
	Cactivex2Ctrl();

// ���������������
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// ����������
protected:
	~Cactivex2Ctrl();

	DECLARE_OLECREATE_EX(Cactivex2Ctrl)    // ������� ������ � guid
	DECLARE_OLETYPELIB(Cactivex2Ctrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(Cactivex2Ctrl)     // �� �������� �������
	DECLARE_OLECTLTYPE(Cactivex2Ctrl)		// ������� ��� � ������������� ���������

// ����� ���������
	DECLARE_MESSAGE_MAP()

// ����� ���������� � ��������
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// ����� �������
	DECLARE_EVENT_MAP()

// ���������� � �������� � �� �������
public:
	enum {
		dispidsetBstr = 5L,
		dispidgetBstr = 4L,
		dispidlongProp = 3,
		dispidmethod2 = 2L,
		dispidmethod1 = 1L
	};
protected:
	ULONG method1(void);
	ULONG method2(void);
	LONG GetlongProp(void);
	void SetlongProp(LONG newVal);
	BSTR getBstr(void);
	void setBstr(LPCTSTR str);
};

