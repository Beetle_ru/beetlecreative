#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *dic = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
int mt[] = {0, 2, 1};

int getB64Len(char *data);
int getB64Len(int dataLen);
int encodeB64(char *data, char *b64);
int encodeB64(char *data, int dataLen, char *b64);
int encodeB64(char *data, int dataLen, char *b64, int b64Len);

int getDataLen(int b64Len);
int getDataLen(char *b64Str);
int decodeB64(char *b64Str, char *data);
int decodeB64(char *b64Str, int b64Len, char *data);
int decodeB64(char *b64Str, int b64Len, char *data, int dataLen);
int getCharIndex(char charter, char *dictionary);

void main() {
	char *data = "test string2";
	char dataRest[1024] = {0};
	char b64[1024] = {0};

	encodeB64(data, b64);
	decodeB64(b64, dataRest);

	printf("* -> %s\n", b64);
	printf("# -> %s\n", dataRest);
} 

int encodeB64(char *data, char *b64) {
	int len = strlen(data);
	return encodeB64(data, ++len, b64);
}

int encodeB64(char *data, int dataLen, char *b64) {
	int b64Len = getB64Len(dataLen);
	return encodeB64(data, dataLen, b64, b64Len);
}

int encodeB64(char *data, int dataLen, char *b64, int b64Len) {
	unsigned int mem = 0;
	int di = 0;
	int b64i = 0;

	memset(b64, 0, b64Len);

	while (di < dataLen) {
		mem = 0;
		for (int i = 2; i >= 0; i--) 
			if (di <= dataLen) mem += data[di++] << (i << 3);

		for (int i = 3; i >= 0; i--) 
			if (b64i <= b64Len) b64[b64i++] = dic[(mem >> (i * 6)) & 0x3F];
	}

	for (int i = 0; i < mt[dataLen % 3]; i++) {
		if (b64i <= b64Len) b64[b64i++] = '=';
	}
	return b64i;
}

int decodeB64(char *b64Str, char *data) {
	int b64Len = strlen(b64Str);
	return decodeB64(b64Str, b64Len, data);
}

int decodeB64(char *b64Str, int b64Len, char *data) {
	int dataLen = getDataLen(b64Len);
	return decodeB64(b64Str, b64Len, data, dataLen);
}

int decodeB64(char *b64Str, int b64Len, char *data, int dataLen) {
	unsigned int mem;
	int b64i = 0;
	int di = 0;
	int res = 0;
	int ci = 0;

	memset(data, 0, dataLen);

	while (b64i < b64Len) {
		mem = 0;
		for (int i = 3; i >= 0; i--) {
			if(b64i < b64Len) {
				if (b64Str[b64i] == '=')
					b64i++;
				else {
					ci = getCharIndex(b64Str[b64i++], dic);

					if (ci >= 0) {
						mem += ci << (i * 6);
					} else {
						b64i++;
						res--;
					}
				}
			}
		}

		for (int i = 2; i >= 0; i--)
			if(di < dataLen) data[di++] = (mem >> (i * 8)) & 0xFF;
	}

	if (res >= 0) res = b64i;

	return res;
}

int getCharIndex(char charter, char *dictionary) {
	int dicLen = strlen(dictionary);
	for (int i = 0; i < dicLen; i++) {
		if (dictionary[i] == charter) return i;
	}
	return -1;
}

int getB64Len(int dataLen) {
	return ((dataLen + 2) / 3) * 4;
}

int getB64Len(char *data) {
	int len = strlen(data);
	return getB64Len(++len);
}

int getDataLen(int b64Len) {
	return ((b64Len / 4) * 3);// - 2;
}

int getDataLen(char *b64Str) {
	int len = strlen(b64Str);
	return getDataLen(len);
}