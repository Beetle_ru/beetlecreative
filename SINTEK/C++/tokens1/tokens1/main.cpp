#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef unsigned char byte;

struct token {
	__int64 pointer : 32;
	__int64 type : 4;
	__int64 count : 28;
};

#define T_TKN 0
#define T_CHR 1
#define T_INT 2
#define T_DBL 3

#define LITTLE_ENDIAN 0
#define BIG_ENDIAN    1

int sizes[] = {
	sizeof(token),  //  T_TKN
	1,              //  T_CHR
	4,              //  T_INT
	8};             //  T_DBL

int getSysSize(int tknType) {
	int sysSize = 0;
	switch (tknType) {
	case T_TKN:
		sysSize = sizeof(token);
		break;
	case T_CHR:
		sysSize = sizeof(char);
		break;
	case T_INT:
		sysSize = sizeof(int);
		break;
	case T_DBL:
		sysSize = sizeof(double);
		break;
	}
	return sysSize;
}

int endian() {
    int i = 1;
    byte *p = (byte *)&i;

    if (p[0] == 1)
        return LITTLE_ENDIAN;
    else
        return BIG_ENDIAN;
}

void cpmem(void *src, void *des, int cnt) {
	byte *cdes = (byte*)des;
	byte *csrc = (byte*)src;

	for (int i = 0; i < cnt; i++) {
		*(cdes + i) = *(csrc + i);
	}
}

void iCpmem(void *src, void *des, int cnt) {
	byte *cdes = (byte*)des;
	byte *csrc = (byte*)src;

	int cEndian = endian();

	for (int i = 0; i < cnt; i++) {
		if(cEndian) *(cdes + cnt - i - 1) = *(csrc + i);
		else *(cdes + i) = *(csrc + i);
	}
}

void changeEndian(void *vp, int size) {
	byte temp;
	byte *cpvp = (byte*)vp;
	int invertI = size - 1;
	for (int i = 0; i < size * 0.5; i++) {
		invertI = size - i - 1;
		temp = cpvp[i];
		cpvp[i] = cpvp[invertI];
		cpvp[invertI] = temp;
	}
}

token *create(void *val, int type, int cnt) {
	byte memSize = sizes[type] * cnt;

	byte *src = (byte*)val;
	byte *des = (byte*)malloc(memSize);

	token *t = (token*)malloc(sizeof(token));
	t->count = cnt;
	t->pointer = (int)des;
	t->type = type;
	
	cpmem(src, des, memSize);
	
	return t;
}

void printToken(token *t, bool isEndLine = true) {
	if (t->count > 0) printf("[");

	for (int i = 0; i < t->count; i++) {
		if (i > 0) printf(", ");

		if (t->type == T_INT) printf("%d", *((int *)t->pointer + i));
		if (t->type == T_CHR) printf("%c", *((char *)t->pointer + i));
		if (t->type == T_DBL) printf("%f", *((double *)t->pointer + i));
		if (t->type == T_TKN) printToken((token *)t->pointer + i, false);
	}

	if (t->count > 0) printf("]");
	if (isEndLine) printf("\n");
		
}

byte *createBuffer(int len) {
	byte *buffer = (byte*)malloc(sizeof(byte) * len);
	for (int i = 0; i < len; i++) {
		buffer[i] = 0;
	}
	return buffer;
}

int pack(token *tp, byte *buffer, int shift = 0) {
	int pos = shift;

	token t = *tp;
	t.pointer = pos += sizes[T_TKN];
	iCpmem(&t, buffer + shift, sizes[T_TKN]);

	if (tp->type == T_TKN) {
		for (int i = 0; i < tp->count; i++) {
			pos = pack(((token*)tp->pointer) + i, buffer, pos);
		}
	} else {
		iCpmem((void*)tp->pointer, buffer + pos, sizes[tp->type] * tp->count);
		pos += sizes[tp->type] * tp->count;
	}

	return pos;
}



int getTTSize(token *tp) {
	int size = sizes[T_TKN];

	if (tp->type == T_TKN) {
		for (int i = 0; i < tp->count; i++) {
			size += getTTSize(((token*)tp->pointer) + i);
		}
	} else {
		size += sizes[tp->type] * tp->count;
	}

	return size;
}

int token2Pkg(token *tp, byte **pkg) {
	int size = getTTSize(tp);
	*pkg = createBuffer(size);

	pack(tp, *pkg);

	return size;
}

token *pkg2Token(byte *pkg, int shift = 0) {
	int pos = 0;
	token *tp = (token*)malloc(sizes[T_TKN]);
	cpmem(pkg + shift + pos, tp, sizes[T_TKN]);
	pos += sizes[T_TKN];

	if (tp->type == T_TKN) {
		void *tknMem = (token*)malloc(getSysSize(tp->type) * tp->count);
		token *currentTkn = (token*)tknMem;
		int localPos = tp->pointer;
		for (int i = 0; i < tp->count; i++) {
			token *src = pkg2Token(pkg, localPos);
			localPos += sizes[T_TKN] + sizes[src->type] * src->count;
			cpmem(src, currentTkn, sizes[tp->type]);
			free(src);
			currentTkn++;
		}
		tp->pointer = (int)tknMem;

	} else {
		void *vp = malloc(getSysSize(tp->type) * tp->count);
		pos = tp->pointer;
		tp->pointer = (int)vp;

		for (int i = 0; i< tp->count; i++) {
			cpmem(pkg + pos, ((byte*)vp) + (i * getSysSize(tp->type)), sizes[tp->type]);
			pos += sizes[tp->type];
		}
	}

	return tp;
}

void freeTT(token *tp) {
	if (!tp) return;
	if (tp->type == T_TKN) {
		for (int i = 0; i < tp->count; i++) {
			freeTT(((token*)tp->pointer) + i);
		}
	} else {
		if (!tp->pointer) return;
		
		free((void*)tp->pointer);
		tp->pointer = NULL;
	}
}

void printPkg(byte *buffer, int len) {
	for (int i = 0; i < len; i++) {
		printf("%0.2X ", (byte)buffer[i]);
	}
	printf("\n");
}

void main() {
	int val = 4294967294;
	int val2 = 3435973836;
	int iArr[] = { 2863311530, 3149642683, 3435973836, 3722304989 };
	char *str = "example string";
	double dArr[] = { 0.1, 0.3, 0.5 };
	token  tArr[] = { *create(&val, T_INT, 1), *create(iArr, T_INT, 4), *create(str, T_CHR, strlen(str)), *create(dArr, T_DBL, 3) };

	token  tArr2[] = { *create(&val, T_INT, 1), *create(&iArr, T_INT, 4)};

	token *t_t_a = create(tArr2, T_TKN, 2);
	printToken(t_t_a);

	byte *pkg = NULL;
	int size = token2Pkg(t_t_a, &pkg);

	printPkg(pkg, size);
	//printf("%d\n", size);

	freeTT(t_t_a);

	token *restore = pkg2Token(pkg);

	
	printToken(restore);

	printf("%d\n", sizeof(unsigned __int64));

	/*token *t_i = create(&val, T_INT, 1);
	printToken(t_i);

	token *t_i_a = create(iArr, T_INT, 4);
	printToken(t_i_a);

	token *t_c_a = create(str, T_CHR, strlen(str));
	printToken(t_c_a);

	token *t_d_a = create(dArr, T_DBL, 3);
	printToken(t_d_a);*/

	//token *t_t_a = create(tArr, T_TKN, 4);
	//printToken(t_t_a);



	return;
}