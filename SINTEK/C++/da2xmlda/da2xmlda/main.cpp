//#include <InitGuid.h>
#include <stdio.h>
#include <ObjBase.h>
#include "OpcDa\opcda.h"

void main() {
	HRESULT hr;
	CLSID clsid;
	IUnknown *pUnkn = 0;
	LPCWSTR progId = L"Sintek.DualSource";

	CoInitialize(NULL);
	

	hr = CLSIDFromProgID((LPCWSTR)progId, &clsid);


	if (!SUCCEEDED(hr)) {
		printf("server \"%s\" is not found", progId);
		return;
	}

	hr = CoCreateInstance(clsid, NULL, CLSCTX_ALL, IID_IUnknown, (LPVOID *)&pUnkn);

	if (!SUCCEEDED(hr)) {
		printf("create instance error = 0x%08X",hr);
		return;
	}

	// Get connection-pointer
	
	IOPCServer *m_pOpcServer;
	IOPCBrowseServerAddressSpace *m_pOpcBrowse;
	IConnectionPointContainer *pCPC;
	//IOPCShutdown *m_pOPCConnPoint;


	hr = pUnkn->QueryInterface(IID_IOPCServer, (LPVOID*)&m_pOpcServer);
	hr = pUnkn->QueryInterface(IID_IOPCBrowseServerAddressSpace, (LPVOID*)&m_pOpcBrowse);
	hr = m_pOpcServer->QueryInterface(IID_IConnectionPointContainer, (void**)&pCPC);
	//hr = pCPC->FindConnectionPoint(IID_IOPCShutdown, &m_pOPCConnPoint);
	//hr = m_pOPCConnPoint->Advise(m_pShutdownCP, &m_dwShutdownConnection);

	LONG  TimeBias=0; 
	FLOAT PercentDeadband=0.0;   
    DWORD AA=0; 
	#define LOCALE_ID 0x409 
	IOPCItemMgt *m_IOPCItemMgt;
	//m_pOpcServer->AddGroup(L"grp1",TRUE,500,1,&TimeBias,&PercentDeadband,LOCALE_ID,NULL,&AA,IID_IOPCItemMgt,(LPUNKNOWN*)&m_IOPCItemMgt);
	//m_pOpcServer->AddGroup(L"grp1",TRUE,500,1,&TimeBias,&PercentDeadband,LOCALE_ID,NULL,&AA,IID_IOPCGroupStateMgt,(LPUNKNOWN*)&m_IOPCItemMgt);

	DWORD dwUpdateRate = 500;
	OPCHANDLE hClientGroup = 0;
	OPCHANDLE hServerGroup; // server handle to the group
	IOPCItemMgt* pIOPCItemMgt = NULL; //pointer to IOPCItemMgt interface

	hr = m_pOpcServer->AddGroup(
		/*szName*/ L"Group1",
		/*bActive*/ FALSE,
		/*dwRequestedUpdateRate*/ dwUpdateRate,
		/*hClientGroup*/ hClientGroup,
		/*pTimeBias*/ 0,
		/*pPercentDeadband*/ 0,
		/*dwLCID*/0,
		/*phServerGroup*/&hServerGroup,
		&dwUpdateRate,
		/*riid*/ IID_IOPCItemMgt,
		/*ppUnk*/ (IUnknown**) &pIOPCItemMgt
	);

	// Array of items to add:
	OPCITEMDEF ItemArray[1] =
	{{
	/*szAccessPath*/ L"",
	/*szItemID*/ L"cr02.ch00.V0",
	/*bActive*/ FALSE,
	/*hClient*/ 1,
	/*dwBlobSize*/ 0,
	/*pBlob*/ NULL,
	/*vtRequestedDataType*/ VT_EMPTY,
	/*wReserved*/0
	}};


	OPCITEMRESULT* pAddResult=NULL;
	HRESULT* pErrors = NULL;
	// Add an Item to the previous Group:
	hr = pIOPCItemMgt->AddItems(1, ItemArray, &pAddResult, &pErrors);
	
	// release memory allocated by the server:
	CoTaskMemFree(pAddResult->pBlob);

	CoTaskMemFree(pAddResult);
	pAddResult = NULL;

	CoTaskMemFree(pErrors);
	pErrors = NULL;

	printf("hello");
}