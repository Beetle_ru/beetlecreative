#include <stdio.h>
//#include <winsock2.h>
//#include <Windows.h>
#include "winbutane.h"
#include "MessageLite.h"

void receiverServer1(receiveInfo *ri);
void receiverServer2(receiveInfo *ri);
void receiverClient (receiveInfo *ri);

int main() {
	WinButane *server1 = new WinButane(); 
	server1->Init();
	server1->Bind(1133, receiverServer1);

	WinButane *server2 = new WinButane(); 
	server2->Init();
	server2->Bind(1135, receiverServer2);

	WinButane *client1 = new WinButane(); 
	client1->Init();

	MessageLite *msg = new MessageLite();
	int len = msg->SetCmd("Request")->SetMsg("Who?")->SetT("string")->Pack();

	client1->SetListener(receiverClient);

	client1->SendAsyncTo("localhost", 1133, msg->GetPkg(), len);
	Sleep(300);
	client1->SendTo("127.0.0.1", 1135, msg->GetPkg(), len);

	getchar();

	delete server1;
	delete server2;
	delete client1;
}

void receiverServer1(receiveInfo *ri) {
	char *address = inet_ntoa(ri->senderAddr->sin_addr);
	printf("[server1] received from %s:%d\n msg:\"%s\"\n", address, ri->senderAddr->sin_port, ri->msg);
	
	MessageLite *msg = new MessageLite();
	msg->SetPkg(ri->msg, ri->len)->Unpack();
	printf("cmd: \"%s\"\nmsg: \"%s\"\n***************************************\n\n", msg->GetCmd(), msg->GetMsg());

	msg->SetCmd("Response")->SetMsg("I'm is [server1]")->Pack();

	RESPONSE(ri, msg->GetPkg(), msg->GetPkgLen());

	return;
}

void receiverServer2(receiveInfo *ri) {
	char *address = inet_ntoa(ri->senderAddr->sin_addr);
	printf("[server2] received from %s:%d\n msg:\"%s\"\n", address, ri->senderAddr->sin_port, ri->msg);

	MessageLite *msg = new MessageLite();
	msg->SetPkg(ri->msg, ri->len)->Unpack();
	printf("cmd: \"%s\"\nmsg: \"%s\"\n***************************************\n\n", msg->GetCmd(), msg->GetMsg());

	msg->SetCmd("Response")->SetMsg("I'm is [server2]")->Pack();

	RESPONSE(ri, msg->GetPkg(), msg->GetPkgLen());

	return;
}

void receiverClient(receiveInfo *ri) {
	char *address = inet_ntoa(ri->senderAddr->sin_addr);
	printf("[client1] received from %s:%d\n msg:\"%s\"\n", address, ri->senderAddr->sin_port, ri->msg);

	MessageLite *msg = new MessageLite();
	msg->SetPkg(ri->msg, ri->len)->Unpack();
	printf("cmd: \"%s\"\nmsg: \"%s\"\n***************************************\n\n", msg->GetCmd(), msg->GetMsg());

	return;
}

