#include "MessageLite.h"


MessageLite::MessageLite(int buffersSize) {
	m_buffersSize = buffersSize;
	Pkg = (char*)malloc(buffersSize);
	Cmd = (char*)malloc(buffersSize);
	Msg = (char*)malloc(buffersSize);
	T   = (char*)malloc(buffersSize);

	for (int i = 0; i < buffersSize; i++) {
		Pkg[i] = Cmd[i] = Msg[i] = T[i] = 0;
	}
}


MessageLite::~MessageLite(void) {
	free(Pkg);
	free(Cmd);
	free(Msg);
	free(T);
}


int MessageLite::Pack() {
	size_t b64CmdLen, b64MsgLen, b64TLen;
	char *b64Cmd = base64_encode((unsigned char*)Cmd, strlen(Cmd), &b64CmdLen);
	char *b64Msg = base64_encode((unsigned char*)Msg, strlen(Msg), &b64MsgLen);
	char *b64T   = base64_encode((unsigned char*)T, strlen(T), &b64TLen);

	int totalLen = b64CmdLen + b64MsgLen + b64TLen;

	if (totalLen + 2 > m_buffersSize) {
		free(b64Cmd);
		free(b64Msg);
		free(b64T);
		return -1;
	}

	memset(Pkg, 0, m_buffersSize);

	int shift = 0;
	shift += cpData(Pkg, shift, b64Cmd, b64CmdLen);
	Pkg[shift++] = ';';
	shift += cpData(Pkg, shift, b64Msg, b64MsgLen);
	Pkg[shift++] = ';';
	shift += cpData(Pkg, shift, b64T,   b64TLen);

	free(b64Cmd);
	free(b64Msg);
	free(b64T);

	return shift;
}

int MessageLite::Unpack() {
	char *buffer = (char*)malloc(m_buffersSize);
	memset(buffer, 0, m_buffersSize);
	char itemCounter = 0;
	int cs = 0;

	for(int i = 0; i < m_buffersSize; i++) {
		if ((Pkg[i] != 0) && (Pkg[i] != ';')) {
			buffer[cs++] = Pkg[i];
		} else {
			size_t resultLen;
			char *item = (char*)base64_decode(buffer, cs, &resultLen);
			
			switch(itemCounter) {
			case 0:
				memset(Cmd, 0, m_buffersSize);
				memcpy(Cmd, item, resultLen);
				break;
			case 1:
				memset(Msg, 0, m_buffersSize);
				memcpy(Msg, item, resultLen);
				break;
			case 2:
				memset(T, 0, m_buffersSize);
				memcpy(T, item, resultLen);
				break;
			}

			free(item);
			cs = 0;
			itemCounter++;
			if ((Pkg[i] == 0)) return 0;
		}
	}

	return 0;
}

char *MessageLite::GetPkg() {
	return Pkg;
}

char *MessageLite::GetCmd() {
	return Cmd;
}

char *MessageLite::GetMsg() {
	return Msg;
}

char *MessageLite::GetT() {
	return T;
}

int MessageLite::GetPkgLen() {
	return strlen(Pkg);
}

MessageLite *MessageLite::SetPkg(char *pkg, int size) {
	memset(Pkg, 0, m_buffersSize);
	memccpy(Pkg, pkg, 0, size);
	return this;
}

MessageLite *MessageLite::SetCmd(char *cmd) {
	int size = strlen(cmd);
	memset(Cmd, 0, m_buffersSize);
	memccpy(Cmd, cmd, 0, size);
	return this;
}

MessageLite *MessageLite::SetMsg(char *str) {
	int size = strlen(str);
	memset(Msg, 0, m_buffersSize);
	memccpy(Msg, str, 0, size);
	return this;
}

MessageLite *MessageLite::SetT(char *type) {
	int size = strlen(type);
	memset(T, 0, m_buffersSize);
	memccpy(T, type, 0, size);
	return this;
}

int cpData(void *dest, int destShift, void *src, int len) {
	char *destb = (char*) dest;
	char *srcb = (char*) src;
	for (int i = 0; i < len; i++) {
		destb[i + destShift] = srcb[i];
	}
	return len;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};


char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length) {

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = (char*)malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}


unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = (unsigned char*)malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}


void build_decoding_table() {

    decoding_table = (char *)malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}


void base64_cleanup() {
    free(decoding_table);
}