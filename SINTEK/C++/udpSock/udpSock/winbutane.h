#pragma once
#define _AFXDLL
#include <Afxwin.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Windows.h>

#pragma comment(lib, "Ws2_32.lib")

#define WINBUTANE(ri) ((WinButane*)ri->self)
#define RESPONSE(ri, msg, len) WINBUTANE(ri)->SendTo(ri->senderAddr, msg, len)

struct receiveInfo {
	sockaddr_in *senderAddr;
	void *self;
	char *msg;
	int len;
	void *params;
};

typedef void (ReceiveCallbackFunct) (receiveInfo *param);

struct connectionInfo {
	SOCKET sock;
	ReceiveCallbackFunct *callBack;
	bool isReceive;
	void *self;
	void *params;
};


UINT ListenProc(PVOID param);
UINT SendProc(PVOID param);

class WinButane {
private:
	static int m_objCnt;
	WSADATA m_wsaData;
	sockaddr_in m_sendAddr;
	ReceiveCallbackFunct *m_receiveCallBack;
	connectionInfo *m_ci;
	CWinThread *m_listenThread;
	SOCKET m_sock;

	int Bind(unsigned short port);
public:
	int Init();
	
	int SetListener(ReceiveCallbackFunct *callBack, void *params = NULL);
	int SendTo(sockaddr_in *addrr, char *buffer, int length);
	int SendTo(const char *ipAddr, unsigned short port, char *buffer, int length);
	int SendAsyncTo(const char *ipAddr, unsigned short port, char *buffer, int length);
	int Bind(unsigned short port, ReceiveCallbackFunct *callBack, void *params = NULL);
	~WinButane();
};

struct sendInfo {
	char *ipAddr;
	unsigned short port;
	char *buffer;
	int length;
	WinButane *self;
};