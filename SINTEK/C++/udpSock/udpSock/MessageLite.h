#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

class MessageLite {
private:
	int m_buffersSize;
private:
	char *Pkg;
	char *Cmd;
	char *Msg;
	char *T;
public:
	MessageLite(int buffersSize = 1024);
	~MessageLite(void);

	int Pack();
	int Unpack();

	char *GetPkg();
	char *GetCmd();
	char *GetMsg();
	char *GetT();
	
	int GetPkgLen();

	MessageLite *SetPkg(char *pkg, int size);
	MessageLite *SetCmd(char *cmd);
	MessageLite *SetMsg(char *str);
	MessageLite *SetT  (char *type);
	
};

int cpData(void *dest, int destShift, void *src, int len);

char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length);
unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length);
void build_decoding_table();
void base64_cleanup();