#include "main.h"

void main() {
	FILE * f = NULL;
	f = fopen("D:\\GIT\\BeetleCreative\\SINTEK\\C++\\CompilerXX\\Debug\\script.txt", "r");

	if (!f) {
		printf("error open file\n");
		return;
	}


	char fileBuf[BUFSIZE] = {0};
	int fsize = 0;
	fsize = fread(fileBuf, sizeof(char), BUFSIZE, f);
	fclose(f);

	init();

	tolower(fileBuf);

	token_t *tokenList = GetTokenList(fileBuf, fsize);

	//printTLS(tokenList, fileBuf);

	//lexTLS(tokenList, fileBuf);
	getNexLex(tokenList);
	node *rootNode = newNode(nodeTypeE_root, 0, NULL);
	createTree(fileBuf, tokenList, rootNode);

	printf("\n\n############################\n\n");
	prinNode(fileBuf, rootNode);

	//printf(operators[operatorsE_comma]);

}

void prinNode(char *buffer, node *root, int nesting) {
	if (root == NULL) return;

	for(int i = 0; i < nesting; i++) {
		printf("..");
	}

	printf("%s => \"", nodeTypeStr[root->nodeType]);

	if (root->lex != NULL) {
		for (int i = root->lex->index; i < root->lex->index + root->lex->length; i++) {
			putchar(buffer[i]);
		}
	}
	printf("\"\n");
	
	nesting++;
	int childI = 0;
	while(true) {
		prinNode(buffer, root->childs[childI++], nesting);
		
		if(root->childs[childI] == NULL) return;
	}
}

token_t *GetTokenList(char *buf, int blen) {
	token_t *result = NULL;
	token_t *lastToken = result;

	TypeTE stt = TypeTE_undef;

	char promB[BUFSIZE] = {0};
	int promBI = 0;

	int i = 0;
	int beginPos;
	while(true) {
		char c = buf[i++];

		TypeTE newStt = getLexemType(c);

		if (newStt != stt) {
			if ((stt == TypeTE_number) && (c == '.')) goto addchr;

			if ((stt != TypeTE_rem) && (stt != TypeTE_undef)) {
				token_t *t = createToken(promB, stt, beginPos - 1, i - beginPos);

				//if (isOperator(t->hash)) t->type = TypeTE_operator;

				if(result != NULL) {
					lastToken->next = t;
				}
				else result = t;

				lastToken = t;
			}

			stt = newStt;

			memset(promB, 0, promBI);
			promBI = 0;

			beginPos = i;
		} 

		addchr:
		promB[promBI++] = c;

		if (c == 0) break;
	}

	return result;
}


void printTL(token_t *tlist, char *buffer) {
	if (tlist == NULL) return;

	while (true) {
		printf("{\n\ttype = %s\n\tindex = %d\n\tlength = %d\n\ttext = \"", TypeTEStr[tlist->type], tlist->index, tlist->length);

		for (int i = tlist->index; i < tlist->index + tlist->length; i++) {
			putchar(buffer[i]);
		}

		printf("\"\n}\n");
		tlist = tlist->next;

		if (tlist == NULL) return;
	}
}

void printTLS(token_t *tlist, char *buffer) {
	if (tlist == NULL) return;

	while (true) {
		printf("%-7s [%10u] -> \"", TypeTEStr[tlist->type], tlist->hash);

		for (int i = tlist->index; i < tlist->index + tlist->length; i++) {
			putchar(buffer[i]);
		}

		printf("\"\n");
		tlist = tlist->next;

		if (tlist == NULL) return;
	}
}

token_t *createToken(char *buf, TypeTE stt, int index, int length) {
	token_t *t = (token_t*)malloc(sizeof(token_t));
	memset(t, 0, sizeof(token_t));

	t->index = index;
	t->length = length;
	t->type = stt;
	t->hash = generateHash(buf, length);
	t->next = NULL;

	return t;
}

void lexTLS(token_t *tlist, char *buffer) {
	if (tlist == NULL) return;
	token_t *lex = tlist;

	//object *root = (object*)malloc(sizeof(object));

	node *rootNode = newNode(nodeTypeE_root, 0, NULL);
	node *currentNode = rootNode;
	node *parentNode = NULL;

	while (true) {
		lex = tlist;

		if (lex->type == TypeTE_word) {
			operatorsE opType = (operatorsE)getOperatorCode(lex->hash);

			if (opType >= 0) { // is operator
				switch(opType) {
				case operatorsE_program:
					parentNode = currentNode;

					tlist = tlist->next;
					if (tlist == NULL) {
						fprintf(stderr, "error: unexpected end of file, chr = %d", lex->index);
						return;
					}

					//currentNode = newNode(nodeTypeE_program, lex->hash);
					printf("PROGRAM\n");
					break;
				}
			} else { // identifier
			}
		}


		lexem(buffer, lex);

		tlist = tlist->next;

		if (tlist == NULL) return;
	}
}

node *createTree(char *buffer, token_t *lex, node *root) {
	if (lex == NULL) return NULL;
	if (root == NULL) return NULL;

	int childI = 0;
	node *newChild = NULL;

	if (lex->type == TypeTE_word) {
			operatorsE opType = (operatorsE)getOperatorCode(lex->hash);

			if (opType >= 0) { // is operator

				switch(opType) {
				case operatorsE_function:
				case operatorsE_function_block:
				case operatorsE_program:

					//root->childs[childI] = newNode(nodeTypeE_block, lex->hash);
					newChild = newNode(nodeTypeE_block, lex->hash, lex);
					apendChild(root, newChild);
					printf("BLOCK\n");
					createTree(buffer, getNexLex(), newChild); // identifier
					break;

				case operatorsE_var :
					printf("VAR\n");
					newChild = newNode(nodeTypeE_vars, lex->hash, lex);
					apendChild(root, newChild);
					createTree(buffer, getNexLex(), newChild);
					break;

				case operatorsE_end_program:
				case operatorsE_end_function:
				case operatorsE_end_function_block:
				case operatorsE_end_var:
				case operatorsE_end_var_input:
				case operatorsE_end_var_output:
				case operatorsE_end_var_in_out:
				case operatorsE_end_var_external:
				case operatorsE_end_var_global:
				case operatorsE_end_var_access:
				case operatorsE_end_constant:
					return root;
					break;
				}

			} else { // identifier
				printf("identifier -> \"");
				for (int i = lex->index; i < lex->index + lex->length; i++) {
					putchar(buffer[i]);
				}
				printf("\"\n");

				newChild = newNode(nodeTypeE_identifier, lex->hash, lex);
				apendChild(root, newChild);

				if (root->nodeType == nodeTypeE_vars) {
					bool isColon = false;
					bool isType = false;

					while(true) {
						lex = lex->next;
						if (lex == NULL) {
							fprintf(stderr, "error: unexpected end of file\n");
							return NULL;
						}
						opType = (operatorsE)getOperatorCode(lex->hash);

						if (isColon && (opType == operatorsE_semicolon)) return root;

						if (isColon && (!isType)) {
							OType t = getTypeByHash(lex->hash);
							if (t < 0) {
								fprintf(stderr, "error: unexpected type of variable chr=%d\n", lex->index);
								return NULL;
							}
							root->type.baseType = t;
							root->type.len = OTypeSize[t];
						}

						if (opType == operatorsE_colon) {
							isColon = true;
						}

						if (!isColon && (opType > 0)) {
							printf("t => %s \n", operators[opType]);
							if (opType != operatorsE_comma) {
								fprintf(stderr, "error: incorrect sequence of statements chr=%d \"%s\"\n", lex->index, operators[opType]);
								return NULL;
							}
						}
						//
					}
				}

				//createTree(buffer, lex->next, root);
			}
		}

		createTree(buffer, getNexLex(), root);

	return root;
}

token_t *getNexLex(token_t * setLex) {
	static token_t *lex = NULL;
	if (setLex == NULL) {
		if (lex != NULL) {
			lex = lex->next;
			return lex;
		}
	} else {
		lex = setLex;
	}
	return NULL;
}

OType getTypeByHash(hash_t hash) {
	for (int i = 0; i < OTypeHashsLen; i++) {
		if (OTypeHashs[i] == hash) return (OType)i;
	}
	return (OType)-1;
}

void apendChild(node *root, node *child) {
	int i = 0;
	while(root->childs[i] != NULL) i++;
	root->childs[i] = child;
}

void lexem(char *buf, token_t *token) { // TODO
	printf("%-7s [%10u] -> \"", TypeTEStr[token->type], token->hash);
	for (int i = token->index; i < token->index + token->length; i++) {
		putchar(buf[i]);
	}
	printf("\"\n");
}

object *newObj(object *root, OCls oclass, hash_t hash) {
	if (getObjectByHash(root, hash)) {
		fprintf(stderr, "object alredy exist");
		return NULL;
	}

	object *o = (object*)malloc(sizeof(object));
	memset(o, 0, sizeof(object));
	
	o->hash = hash;
	o->oClass = oclass;
	o->type = OType_undef;
	o->val = 0;
	o->next = NULL;

	object *oLast = getObjectLast(root);

	if (!oLast) {
		fprintf(stderr, "empty object list");
		return NULL;
	}

	oLast->next = o;

	return o;
}

object *getObjectByHash(object *root, hash_t hash) {
	while(root) {
		if (root->hash == hash) return root;
		root = root->next;
	}
	return NULL;
}

object *getObjectLast(object *root) {
	object *o = NULL;

	while(root) {
		o = root;
		root = root->next;
	}

	return o;
}

TypeTE getLexemType(char c) {
	TypeTE stt = TypeTE_undef;

	if (isContainC(c, remTerms)) {
		stt = TypeTE_rem;
	} else if (isContainC(c, numTerms)) {
		stt = TypeTE_number;
	} else if (isContainC(c, wrdTerms)) {
		stt = TypeTE_word;
	} else if (isContainC(c, spcTerms)) {
		stt = TypeTE_special;
	}

	return stt;
}

bool isContainC(char c, char *dic) {
	for (int i = 0; i < strlen(dic); i++) {
		if (c == dic[i]) return true;
	}

	return false;
}

hash_t generateHash(char *string, int len, int index) {
    hash_t hash = 0;

    for(size_t i = 0; i < len; ++i) {
        hash = 65599 * hash + string[index + i];
	}

    return hash ^ (hash >> 16);
}

hash_t generateTokenHash(char *buffer, token_t *token) {
	return generateHash(buffer, token->length, token->index);
}

void tolower(char *str) {
	for(char *p = str;*p;++p) *p=*p>0x40&&*p<0x5b?*p|0x60:*p;
}

void init() {
	operatorsHashsLen = getDicSize(operators);
	operatorsHashs = (hash_t*)malloc(sizeof(hash_t) * operatorsHashsLen);
	for (int i = 0; i < operatorsHashsLen; i++) {
		operatorsHashs[i] = generateHash(operators[i], strlen(operators[i]));
	}

	OTypeHashsLen = getDicSize(OTypeStr);
	OTypeHashs = (hash_t*)malloc(sizeof(hash_t) * OTypeHashsLen);
	for (int i = 0; i < OTypeHashsLen; i++) {
		OTypeHashs[i] = generateHash(OTypeStr[i], strlen(OTypeStr[i]));
	}
}

int getDicSize(char **dic) {
	int size = 0;
	int i = 0;
	while (true) {
		if (strlen((char *)dic[i++])) size++;
		else break;
	}
	return size;
}

node *newNode(nodeTypeE nodeType, hash_t hash, token_t *lex) {
	const int childCount = 1024;

	node *n = (node*)malloc(sizeof(node));
	memset(n, 0, sizeof(node));

	n->nodeType = nodeType;
	n->hash = hash;
	n->lex = lex;
	n->childs = (node**)malloc(sizeof(node*) * childCount);
	memset(n->childs, 0, sizeof(node*) * childCount);

	return n;
}

int getOperatorCode(hash_t hash) {

	for (int i = 0; i < operatorsHashsLen; i++) {
		if (hash == operatorsHashs[i]) return i;
	}
	return -1;
}