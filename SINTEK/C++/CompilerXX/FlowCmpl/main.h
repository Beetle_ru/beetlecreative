#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024

char remTerms[] = " \n\a\t"; // replase single space
char numTerms[] = "0123456789";
char wrdTerms[] = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_";
char spcTerms[] = "!@#$%^&*()-+=\\*:;\"'?<>.,";

enum operatorsE {
	operatorsE_empty,              // 0
	operatorsE_program,            // 1
	operatorsE_end_program,        // 2
	operatorsE_function,           // 3
	operatorsE_end_function,       // 4
	operatorsE_function_block,     // 5
	operatorsE_end_function_block, // 6
	operatorsE_var,                // 7
	operatorsE_var_input,          // 8
	operatorsE_var_output,         // 9
	operatorsE_var_in_out,         // 10
	operatorsE_var_external,       // 11
	operatorsE_var_global,         // 12
	operatorsE_var_access,         // 13
	operatorsE_end_var,            // 14
	operatorsE_end_var_input,      // 15
	operatorsE_end_var_output,     // 16
	operatorsE_end_var_in_out,     // 17
	operatorsE_end_var_external,   // 18
	operatorsE_end_var_global,     // 19
	operatorsE_end_var_access,     // 20
	operatorsE_retain,             // 21
	operatorsE_constant,           // 22
	operatorsE_end_constant,       // 23
	operatorsE_at,                 // 24
	operatorsE_assign,             // 25
	operatorsE_addition,           // 26
	operatorsE_subtraction,        // 27
	operatorsE_devision,           // 28
	operatorsE_multiplication,     // 29
	operatorsE_power,              // 30
	operatorsE_commetBegin,        // 31
	operatorsE_commentEnd,         // 32
	operatorsE_greater,            // 33
	operatorsE_greaterOrEqual,     // 34
	operatorsE_equal,              // 35
	operatorsE_lessOrEqual,        // 36
	operatorsE_less,               // 37
	operatorsE_notEqual,           // 38
	operatorsE_and,                // 39
	operatorsE_or,                 // 40
	operatorsE_xor,                // 41
	operatorsE_not,                // 42
	operatorsE_if,                 // 43
	operatorsE_then,               // 44
	operatorsE_elseif,             // 45
	operatorsE_else,               // 46
	operatorsE_end_if,             // 47
	operatorsE_case,               // 48
	operatorsE_end_case,           // 49
	operatorsE_for,                // 50
	operatorsE_to,                 // 51
	operatorsE_by,                 // 52
	operatorsE_do,                 // 53
	operatorsE_end_for,            // 54
	operatorsE_while,              // 55
	operatorsE_end_while,          // 56
	operatorsE_exit,               // 57  
	operatorsE_colon,              // 58 
	operatorsE_semicolon,          // 59
	operatorsE_comma               // 60 
};

char *operators[] = {
	"empty",              // 0
	"program",            // 1
	"end_program",        // 2
	"function",           // 3
	"end_function",       // 4
	"function_block",     // 5
	"end_function_block", // 6
	"var",                // 7
	"var_input",          // 8
	"var_output",         // 9
	"var_in_out",         // 10
	"var_external",       // 11
	"var_global",         // 12
	"var_access",         // 13
	"end_var",            // 14
	"end_var_input",      // 15
	"end_var_output",     // 16
	"end_var_in_out",     // 17
	"end_var_external",   // 18
	"end_var_global",     // 19
	"end_var_access",     // 20
	"retain",             // 21
	"constant",           // 22
	"end_constant",       // 23
	"at",                 // 24
	":=",                 // 25
	"+",                  // 26
	"-",                  // 27
	"/",                  // 28
	"*",                  // 29
	"**",                 // 30
	"(*",                 // 31
	"*)",                 // 32
	">",                  // 33
	">=",                 // 34
	"=",                  // 35
	"<=",                 // 36
	"<",                  // 37
	"<>",                 // 38
	"and",                // 39
	"or",                 // 40
	"xor",                // 41
	"not",                // 42
	"if",                 // 43
	"then",               // 44
	"elseif",             // 45
	"else",               // 46
	"end_if",             // 47
	"case",               // 48
	"end_case",           // 49
	"for",                // 50
	"to",                 // 51
	"by",                 // 52
	"do",                 // 53
	"end_for",            // 54
	"while",              // 55
	"end_while",          // 56
	"exit",               // 57
	":",                  // 58
	";",                  // 59
	",",                  // 60
	""  
};

typedef unsigned int hash_t;

hash_t *operatorsHashs = NULL;
int operatorsHashsLen = 0;

enum context { context = 7};

enum TypeTE { TypeTE_undef, TypeTE_word, TypeTE_number, TypeTE_special, TypeTE_rem};
char *TypeTEStr[] = { "undef", "word", "number", "special", "rem"};

enum OCls { OCls_undef, OCls_variable, OCls_constant, OCls_type, OCls_function, OCls_operator};
char *OClsStr[] = {"undef", "variable", "constant", "type", "function", "operator"};

enum OType {
	OType_undef,
	OType_byte, 
	OType_word, 
	OType_dword, 
	OType_lword, 
	OType_sint, 
	OType_int, 
	OType_dint, 
	OType_lint, 
	OType_usint, 
	OType_uint, 
	OType_udint, 
	OType_ulint, 
	OType_bool, 
	OType_real, 
	OType_lreal
};

int OTypeSize[] = { 1, 2, 4, 8, 1, 2, 4, 8, 1, 2, 4, 8, 1, 4, 8};

char *OTypeStr[] = { 
	"undef",
	"byte", 
	"word", 
	"dword", 
	"lword", 
	"sint", 
	"int", 
	"dint", 
	"lint", 
	"usint", 
	"uint", 
	"udint", 
	"ulint", 
	"bool", 
	"real", 
	"lreal",
	""
};

hash_t *OTypeHashs = NULL;
int OTypeHashsLen = 0;

/* 
	OType_time,
	OType_date, 
	OType_time_of_day, 
	OType_date_and_time, 
	OType_string, 
	OType_wstring, 
	OType_type, 
	OType_array, 
	OType_struct, 
	OType_enum
};*/

struct type {
	OType form;
	int len;
	//object fields;
	type *base;
};


struct token_t {
	TypeTE type;
	int index;
	int length;
	hash_t hash;
	token_t *next;
};

struct object {
	hash_t hash;
	OCls oClass;
	OType type;
	unsigned int val;
	object *next;
};

enum nodeTypeE { 
	nodeTypeE_undef, 
	nodeTypeE_root,
	//nodeTypeE_program, 
	//nodeTypeE_function,
	nodeTypeE_block,
	nodeTypeE_operator,
	nodeTypeE_constatns,
	nodeTypeE_vars,
	nodeTypeE_invars,
	nodeTypeE_outvars,
	nodeTypeE_inoutvars,
	nodeTypeE_body,
	nodeTypeE_expression,
	nodeTypeE_identifier,
	nodeTypeE_end
};

char *nodeTypeStr[] = { 
	"undef", 
	"root",
	//nodeTypeE_program, 
	//nodeTypeE_function,
	"block",
	"operator",
	"constatns",
	"vars",
	"invars",
	"outvars",
	"inoutvars",
	"body",
	"expression",
	"identifier",
	"end"
};

struct itype {
	OType baseType;
	int len;
	itype *next;
};

struct node {
	hash_t hash;
	nodeTypeE nodeType;
	itype type;
	token_t *lex;
	node **childs;
};

token_t *GetTokenList(char *buf, int blen);
bool isContainC(char c, char *dic);
TypeTE getLexemType(char c);
token_t *createToken(char *buf, TypeTE stt, int index, int length);
void printTL(token_t *tlist, char *buffer);
void printTLS(token_t *tlist, char *buffer);
hash_t generateHash(char *string, int len, int index = 0);
void lexem(char *buf, token_t *token);
void lexTLS(token_t *tlist, char *buffer);
object *getObjectByHash(object *root, hash_t hash);
void tolower(char *str);
object *getObjectLast(object *root);
hash_t generateTokenHash(char *buffer, token_t *token);
int getDicSize(char **dic);
void init();
int getOperatorCode(hash_t hash);
node *newNode(nodeTypeE nodeType, hash_t hash, token_t *lex);
node *createTree(char *buffer, token_t *lex, node *currentNode);
void apendChild(node *root, node *child);
token_t *getNexLex(token_t * setLex = NULL);
OType getTypeByHash(hash_t hash);
void prinNode(char *buffer, node *root, int nesting = 0);