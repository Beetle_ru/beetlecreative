#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFSIZE 1024

char remTerms[] = " \n\a\t"; // replase single space

char terms[] = "~!@#$%^&*()-+=|\\{}[];\"':.,"; // insert space around this terms

char *keys[] = {"=", "+", "(", ")", ";", ""}; // key word dictionary

void lex(char *stream);
int isSplt(char c);
void norm(char *draft, char *clean, int max = BUFSIZE);
bool isContainC(char c, char *dic);
int getDicSize(char **dic);
int getKeyCode(char *word, char **dic);

void main() {
	FILE * f = NULL;
	f = fopen("D:\\GIT\\BeetleCreative\\SINTEK\\C++\\CompilerXX\\Debug\\script.txt", "r");

	if (!f) {
		printf("error open file\n");
		return;
	}


	char fileBuf[BUFSIZE] = {0};
	int fsize = 0;
	fsize = fread(fileBuf, sizeof(char), BUFSIZE, f);
	fclose(f);

	char normBuf[BUFSIZE] = {0};

	norm(fileBuf, normBuf);

	printf("%s\n", normBuf);

	lex(normBuf);
		
	

	//printf("size -> %d", getDicSize(keys));
}

void norm(char *draft, char *clean, int max) {
	int cleanI = 0;
	bool isPrevTerm = false;
	for (int i = 0; (i < strlen(draft)) && i < max; i++) {
		if (isContainC(draft[i], remTerms)) {
			if (!isPrevTerm) {
				clean[cleanI++] = ' ';
				isPrevTerm = true;
			}
		} else {
			if (isContainC(draft[i], terms) && (!isPrevTerm)) {
				clean[cleanI++] = ' ';
				clean[cleanI++] = draft[i];
				clean[cleanI++] = ' ';
				isPrevTerm = true;
			} else {
				clean[cleanI++] = draft[i];
				isPrevTerm = false;
			}
		}
	}
}

bool isContainC(char c, char *dic) {
	for (int i = 0; i < strlen(dic); i++) {
		if (c == dic[i]) return true;
	}

	return false;
}

void lex(char *stream) {
	char pb[BUFSIZE] = {0};
	int pbIndex = 0;
	char charter = 0;
	int size = strlen(stream);

	for (int i = 0; i < size; i++) {
		charter = stream[i];

		if (charter == ' ') {
			int kc = getKeyCode(pb, keys);
			printf("%d ", kc);

			memset(pb, 0, pbIndex);
			pbIndex = 0;
		} else {
			pb[pbIndex++] = charter;
		}
	}
}

int getKeyCode(char *word, char **dic) {
	int dicSize = getDicSize(dic);

	for (int i = 0; i < dicSize; i++) {
		if (!strcmp(word, dic[i])) return i;
	}

	return -1;
}

int getDicSize(char **dic) {
	int size = 0;
	int i = 0;
	while (true) {
		if (strlen((char *)dic[i++])) size++;
		else break;
	}
	return size;
}


