#include "main.h"

void main() {
	int fileLen = loadFile("D:\\GIT\\BeetleCreative\\SINTEK\\C++\\CompilerXX\\Debug\\script.txt", &fileBuffer);
	parse();
}

int loadFile(char *fileName, char **buffer, int bufSize) {
	*buffer = (char*)malloc(bufSize);
	memset(*buffer, 0, bufSize);

	FILE * f = NULL;
	f = fopen(fileName, "r");

	if (!f) {
		fprintf(stderr, "ERROR: open file \"%s\"\n", fileName);
		return 0;
	}

	int fsize = fread(*buffer, sizeof(char), BUFSIZE, f);

	fclose(f);

	return fsize;
}

bool next() {
	memset(lex, 0, LEXSIZE);
	char c = 0;
	
	TypeTE newType = TypeTE_undef;
	TypeTE prevType = TypeTE_undef;

	int li = 0;
	while(fileBuffer[textIndex++]) {
		c = fileBuffer[textIndex];
		prevType = newType;
		if (isSpace(c)) newType = TypeTE_rem;
		if (isSpec(c)) newType = TypeTE_special;
		if (isDigid(c)) newType = TypeTE_digit;
		if (isLeter(c)) newType = TypeTE_leter;

		if ((newType != TypeTE_rem) && (newType != TypeTE_undef)) {
			lex[li++] = c;
		}
		
		if ((prevType != TypeTE_rem) && (prevType != TypeTE_undef)) {
			if (newType != prevType) return true;
		}
		
	}
	isEOF = true;
	return false;
}

void parse() {
	if (isEOF) return;
	
	next();
		
	if (isProgram(lex)) {
		next();
		if (isIdentifier(lex)) {
			printf("program ident \"%s\"\n", lex);
		}
	}
}

bool isContainChr(char c, char *dic) {
	for (int i = 0; i < strlen(dic); i++) {
		if (c == dic[i]) return true;
	}

	return false;
}

bool isSpec(char c) {
	return isContainChr(c, spcTerms);
}

bool isSpace(char c) {
	return isContainChr(c, remSymbols);
}

//letter ::= �A� | �B� | <...> | �Z� | �a� | �b� | <...> | �z�
bool isLeter(char c) {
	return isContainChr(c, leterSymbols);
}

//digit ::= �0� | �1� | �2� | �3� | �4� | �5� | �6� | �7� | �8� |�9�
bool isDigid(char c) {
	return isContainChr(c, digidSymbols);
}

//octal_digit ::= �0� | �1� | �2� | �3� | �4� | �5� | �6� | �7�
bool isOctalDigid(char c) {
	return isContainChr(c, octalDigidSymbols);
}

//hex_digit ::= digit | �A�| �B� | �C� | �D� | �E� | �F�
bool isHexDigid(char c) {
	return isDigid || isContainChr(c, hexSymbols);
}


//identifier ::= (letter | (�_� (letter | digit))) {[�_�] (letter | digit)}
bool isIdentifier(char *str) {
	int i = 0;
	bool result;
	while (str[i++]) {
		result = isLeter(str[i]) || '_' || (isDigid(str[i]) && (i > 0));
		if ( !result) break;
	}
	return result;
}

bool isProgram(char *str) {
	bool res = !strcmp(str, "PROGRAM");
	return res;
}