#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024
#define LEXSIZE 512

char remSymbols  [] = " \n\a\t"; // replase single space
char digidSymbols[] = "0123456789";
char octalDigidSymbols[] = "01234567";
char hexSymbols[] = "abcdef";
char leterSymbols[] = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
char spcTerms[] = "!@#$%^&*()-+=\\*:;\"'?<>.,";

char *fileBuffer = NULL;
char lex[LEXSIZE] = {0};
int textIndex = 0;
bool isEOF = false;

enum State {State_undef, State_identifier, State_spec, State_string, State_digit, State_comment, State_rem};

enum TypeTE { TypeTE_undef, TypeTE_leter, TypeTE_digit, TypeTE_special, TypeTE_rem};
char *TypeTEStr[] = { "undef", "word", "number", "special", "rem"};

int loadFile(char *fileName, char **buffer, int bufSize = BUFSIZE);
bool next();
void parse();
bool isContainChr(char c, char *dic);
bool isSpec(char c);
bool isSpace(char c);
bool isLeter(char c);
bool isDigid(char c);
bool isOctalDigid(char c);
bool isHexDigid(char c);
bool isIdentifier(char *str);
bool isProgram(char *str);