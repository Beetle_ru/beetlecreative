// getPath.cpp: ������� ���� �������.

#include "stdafx.h"
#include<tchar.h>
#include<stdio.h>

using namespace System;

int GetDir(TCHAR *fullPath, TCHAR *dir) {
	const int buffSize = 1024;

	TCHAR buff[buffSize] = {0};
	int buffCounter = 0;
	int dirSymbolCounter = 0;

	for (int i = 0; i < _tcslen(fullPath); i++) {
		if (fullPath[i] != L'\\') {
			if (buffCounter < buffSize) buff[buffCounter++] = fullPath[i];
			else return -1;
		} else {
			for (int i2 = 0; i2 < buffCounter; i2++) {
				dir[dirSymbolCounter++] = buff[i2];
				buff[i2] = 0;
			}

			dir[dirSymbolCounter++] = fullPath[i];
			buffCounter = 0;
		}
	}

	return dirSymbolCounter;
}

int main(array<System::String ^> ^args) {
	TCHAR * path = L"C:\\Windows\\System32\\cmd.exe";
	TCHAR  dir[1024] = {0};
	GetDir(path, dir);
	wprintf(L"%s\n%s\n", path, dir);

    return 0;
}
