#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int get_file_size(char *filename);
int searchChr(char c, char *chrs);

void main() {
	char *filename = "AgentSec.ini";
	int fileSize = get_file_size(filename);

	FILE *f = fopen(filename, "rt");

	if (f == NULL) {
		printf("error open file\n");
		return;
	}

	char *fileCont = (char*)malloc(fileSize);
	
	fread(fileCont, 1, fileSize, f);

	//printf("%s\n", buffer);

	char *dic = "[]\n\a\315";

	char buff[1024] = {0};
	int buffIndex = 0;
	bool isOpenBkt = false;
	bool isClaster = false;

	for(int i = 0; i <  fileSize; i++) {
		char c = fileCont[i];
		int currenSpec = searchChr(c, dic);
		if (currenSpec + 1) {
			//printf("{%d}", c);
			if (c == 91) isOpenBkt = true;
			if (c == 93) {
				isClaster = !strcmp(buff, "claster");
				isOpenBkt = false;
				//printf("\n***IsClaster***\n");
			}
			if ((c == 10) || (((unsigned char)c) == 205)) {
				if (strlen(buff)) {
					printf("Calls \"%s\"\n", buff);
				}
			}
			memset(buff, 0, buffIndex);
			buffIndex = 0;
		} else {
			if (((unsigned char)c) < 127) {
				//printf("%c", c);
				buff[buffIndex++] = c;
			}
		}
		//printf("\n%d -- %d\n -- %d", (unsigned char)c, i, (unsigned char)'\315');
		//printf("\205");
	}
}

int searchChr(char c, char *chrs) {
	int i = 0;
	while (true) {
		if (c == chrs[i++]) return i;
		if (chrs[i] == 0) break;
	}
	return -1;
}

int get_file_size(char *filename) {
    FILE *p_file = NULL;
    p_file = fopen(filename,"rb");
	if (p_file == NULL) return -1;
    fseek(p_file,0,SEEK_END);
    int size = ftell(p_file);
    fclose(p_file);
    return size;
}
