#include <stdio.h>
#include <curses.h>
#include <sys/time.h>

int main()
{
  int c;
  timeval tv;
  int interval;
  int current_us, previous_us;
  int startSecond;
  int frequency;
  
  gettimeofday (&tv, NULL);
  startSecond = tv.tv_sec;

  initscr();
  cbreak();
  noecho();
   do{
        c=getch();
        gettimeofday (&tv, NULL);
        current_us = (tv.tv_sec - startSecond) * 1000000 + tv.tv_usec;
        interval = current_us - previous_us;
        previous_us = current_us;
        frequency = 1/(interval * 0.000001);
        move(0,0);
        printw("-> %c, %dus, %dHz\n",c,interval,frequency);
   } while(c!=10);
   endwin();
   return 0;
}
