#include <stdio.h>
#include "ext/hookuplib/Include/mainlib.h"

void listener(char *msg, int length, struct CONNHU *conn);
struct CONNHU *server;

int main(int argc, char *argv[]) {
	server = initHu(UDP, SERVER, (char *)"0.0.0.0:32000", listener);
	
	printf("press \"Enter\" for exit\n");
	getchar();

	return 0;
}

void listener(char *msg, int length, struct CONNHU *conn) {
	printf("srv: msg from %s >> \"%s\", length = %d\n", conn->addressRX, msg, length);
}
