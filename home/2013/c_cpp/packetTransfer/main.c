#include<stdio.h>
#include<stdlib.h>

#define COUNT 32

void printData(char *dp, int count);

struct segmentH { // это может быть и токен, т.е. каждый сегмент имеет шапку котороя есть токен -- думать 
	unsigned num : 20;
	unsigned key : 12;
	unsigned length : 16;
	unsigned isToken : 1;
	unsigned byUnused : 15;
};

struct packetH {
	int parcelId;
};

int main(void) {
	unsigned char *packet;
	struct segmentH *s;
	packet = (char*)calloc(COUNT, sizeof(char));
	s = (struct segmentH*)packet;

	printData(packet, COUNT);
	return 0;
}


void printData(char *dp, int count) {
	printf("\n");
	int i; 
	for (i = 0; i < count; i++) {
		printf("%u | ", (unsigned char)dp[i]);
	}
	printf("\n");
}
