#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#define PIPE_NAME "pipeTest"
#define DOTS_STRING "..........................................."

enum ConnType {FIFO};
enum ConnDirection {SERVER, CLIENT};
enum ConnError{OK = 0};

typedef void (*callbackDefinition)(char *msg, int length);

struct CONN {
  ConnType ct;
  ConnError err;
  char *address;
  char *addressRX; 
  char *addressTX;
  int id;
  FILE fifo;
  pthread_t thread;
  callbackDefinition callback;
  char *msgRX;
  int lengthMsgRX;
  char *msgTX;
  int lengthMsgTX;
};


CONN *init(ConnType ct, ConnDirection cd, char *address, callbackDefinition callback);
void callb(char *msg, int length);
void *fifoThread(void *arg);
int fireMsg(CONN *conn, char msg[]);
void *fireFifoThread(void *arg);
void msleep(int ms);
int mkfifoSafe(char *address);

int main()
{
  /*char buffer[1024];
  char wbuf[] = {'h', 'i'};
  int dataSize;
  FILE *fFifo;
  //unlink(PIPE_NAME);
  if (mkfifo(PIPE_NAME, 0777) < 0) {
    printf("error can't create pipe\n");
    //exit(1);
  }
  if ((fFifo = fopen(PIPE_NAME, "r")) == NULL) {
    printf("error can't open pipe\n");
    exit(1);
  }
  dataSize = fread(buffer, sizeof(char), 1024, fFifo);
  printf(">> %d  -- \"%s\"\n", dataSize, buffer);

  fclose(fFifo);*/

  CONN *conn = init(FIFO, SERVER, (char *)"1111", callb);
  CONN *conn2 = init(FIFO, CLIENT, (char *)"1111", callb);

  //printf(" => %d \n", conn->id);
  msleep(1000);
  for(int i = 0; i < 10; i++) {
    fireMsg(conn, (char *)"msg");
    msleep(500);
    fireMsg(conn2, (char *)"msg222");
    msleep(500);
  }
  printf("press \"Enter\" for exit\n");
  getchar();
  return 0;
}

CONN *init(ConnType ct, ConnDirection cd, char *address, callbackDefinition callback) {
  CONN *conn = (CONN*) malloc(sizeof(CONN));
  
  char *addressRX = (char*) malloc(strlen(address)+3);
  char *addressTX = (char*) malloc(strlen(address)+3);
  strcpy(addressRX, address); 
  strcpy(addressTX, address);
  
  //FILE *fp;
  switch (ct) {
    case FIFO:
      printf("FIFO%sOK\n", DOTS_STRING);
      if (cd == SERVER) {
        printf("SERVER%s", DOTS_STRING);
        strcat(addressRX, ".RS");
        strcat(addressTX, ".TS");

        if (mkfifoSafe(addressRX) !=0)
          return NULL;
        if (mkfifoSafe(addressTX) !=0)
          return NULL;
     } else {
        strcat(addressRX, ".TS");
        strcat(addressTX, ".RS");
      }
       /* printf("FIFO TEST%s", DOTS_STRING);
       if (stat(address, &status) != 0) {
        printf("ERROR\nfile %s don't exist\n", address);
        return NULL;
      } else {
        printf("OK\n");
      }
      */

      conn->callback = callback;
      conn->address = address;
      conn->addressRX = addressRX;
      conn->addressTX = addressTX;
      conn->id = 1133;
      
      printf("THREAD CREATE%s", DOTS_STRING);    
      if ( pthread_create(&conn->thread, NULL, fifoThread, conn) != 0) {
        printf("ERROR\n");
      } else {
        printf("OK\n");
      }
      
      //(*callback)();
    break;
    
    default:
      printf("unsupported type connection\n");
      return NULL;
    break;
  }
  return conn;
}

int mkfifoSafe(char *address) {
  struct stat status;

  if (stat(address, &status) != 0) {
    //unlink(address);
    if (mkfifo(address, 0777) < 0) {
      printf("ERROR\ncan't create pipe\n");
      return -1;
    } else {
      printf("OK\n");
    }
  }

  return 0;
}

void callb(char *msg, int length) {
  printf("message [%s], length = %d\n", msg, length);
}

void *fifoThread(void *arg) {
  //callbackDefinition callback = (callbackDefinition) arg; 
  CONN *conn = (CONN*) arg;
  callbackDefinition callback = conn->callback;
  FILE *fFifo;
  int receiveDataLength;
  char buffer[1024];
  char *msg;
  //msg = (char*) malloc(sizeof(char));

  printf("FIFO THREAD%sRUN\n",DOTS_STRING);
 
  while (true) {
    if ((fFifo = fopen(conn->addressRX, "r")) == NULL) {
      printf("error can't open pipe \"%s\"\n", conn->addressRX);
      return NULL;
    }
    
   // printf("!!! %s\n", buffer);
    
    receiveDataLength = fread(buffer, sizeof(char), 1024, fFifo);
   // printf("!!!--- %s\n", buffer);

    msg = (char*) calloc(receiveDataLength, sizeof(char));
   //msg = (char*) realloc(msg, sizeof(char) * receiveDataLength);
    //strncpy(msg, buffer, receiveDataLength);
    for (int i = 0; i<receiveDataLength; i++) {
      msg[i] = buffer[i];
    }
   // printf("!!!*** %s >>> length = %d\n", msg, receiveDataLength);

    (*callback)(msg, receiveDataLength);
    fclose(fFifo);
    free(msg);
   // msleep(10000);
  }
  return NULL;
}

void *fireFifoThread(void *arg) {
  CONN *conn = (CONN*) arg;
  FILE *fFifo;
  
  if ((fFifo = fopen(conn->addressTX, "w")) == NULL) {
    printf("error can't open pipe \"%s\"\n", conn->addressTX);
    return NULL;
  }
  fwrite(conn->msgTX, sizeof(char), conn->lengthMsgTX, fFifo);
  fclose(fFifo);

  return NULL;
}

int fireMsg(CONN *conn, char msg[]) {
  int sizeMsg = sizeof(msg);
  pthread_t thread;
  conn->msgTX = msg;
  conn->lengthMsgTX = sizeMsg;
  if ( pthread_create(&thread, NULL, fireFifoThread, conn) != 0) {
    printf("ERROR send tread dont't created\n");
    return -1;
  } 
  pthread_detach(thread);
  return 0;
}

void msleep(int ms) {
  usleep(ms * 1000);
  return;
}
