#include <stdio.h>

#include <python2.7/Python.h>

int main(int argc, char *argv[]) {
	Py_SetProgramName(argv[0]);
	Py_Initialize();
	PyRun_SimpleString("from time import time,ctime\n"
						"print 'Today is',ctime(time())\n");
	Py_Finalize();
	

	return 0;
}

double compute(double x) { 
	PyObject *pName, *pModule, *pFunc; 
	PyObject *pArgs, *pValue; 
	double result = 0.0; 
	
	Py_Initialize(); 
	
	pName = PyUnicode_FromString("mod"); // convert to python format
	pModule = PyImport_Import(pName); // import module
	
	Py_DECREF(pName); // free
	if (pModule != NULL) { 
		
		pFunc = PyObject_GetAttrString(pModule, "func"); // return function pointer from module
		
		if (pFunc && PyCallable_Check(pFunc)) { 
			
			pArgs = PyTuple_New(1); // кортеж параметров
			
			PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(x)); // fill (tuple, item, value)
			
			pValue = PyObject_CallObject(pFunc, pArgs); // execute
			
			Py_DECREF(pArgs); // free
			if (pValue != NULL) { 
				result = PyFloat_AsDouble(pValue); 
				Py_DECREF(pValue); 
			} 
		} 
		Py_XDECREF(pFunc); // free
		Py_DECREF(pModule); // free
	} 
	Py_Finalize(); 
	return result; 
}
