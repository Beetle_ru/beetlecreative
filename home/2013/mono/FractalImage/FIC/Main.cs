using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;

namespace FIC
{
	class MainClass
	{
		public static int RangeSize = 5;
		public static int DomainSize = 10;
		public static TextWriter MSG = Console.Error;
		
		public static void Main (string[] args)
		{
			if (args.Length == 0) {
				//print help
				return;
			}
			var fileName = args[0];
			if (!File.Exists(fileName)) {
				MSG.WriteLine("ERROR: file {0} is missing", fileName);
				return;
			}
			Console.WriteLine("OPEN: {0}", fileName);
			var img = new Bitmap(fileName);
			var strings = new List<string>();
			
			strings.Add(String.Format("{0};{1};{2};{3}", img.Height, img.Width, RangeSize, DomainSize));
			
			MSG.WriteLine ("Width = {0}", img.Width);
			MSG.WriteLine ("Height = {0}", img.Height);
			MSG.WriteLine ("RangeSize = {0}", RangeSize);
			MSG.WriteLine ("DomainSize = {0}", DomainSize);
			
			var imageBrigthnesAverage = GetAverageBrightness(img);
			MSG.WriteLine ("ImageBrigthnesAverage = {0}", imageBrigthnesAverage);
			
			var rangesList = GetRanges(img, RangeSize);
			var domainsList = GetDomains(img, DomainSize, RangeSize);
			MSG.WriteLine ("RangesCount = {0}", rangesList.Count);
			MSG.WriteLine ("DomainsCount = {0}", domainsList.Count);
			var WidthLost = (img.Width % RangeSize) * img.Height;
			var HeightLost = (img.Height % RangeSize) * img.Width;
			//MSG.WriteLine ("WidthLost = {0}", WidthLost);
			//MSG.WriteLine ("HeightLost = {0}", HeightLost);
			
			MSG.WriteLine ("ImageLost = {0}%", ((double)(WidthLost + HeightLost)) / ((double)(img.Width * img.Height)) * 100);
			
			var rangesHashList = new List<Region>();
			for (var ri = 0; ri < rangesList.Count; ri++) {
				rangesHashList.Add(new Region(rangesList[ri].X, rangesList[ri].Y, BitmapHash2(rangesList[ri].B, 8, imageBrigthnesAverage)));
			}
			
			var domainsHashList = new List<Region>();
			for (var di = 0; di < domainsList.Count; di++) {
				domainsHashList.Add(new Region(domainsList[di].X, domainsList[di].Y, BitmapHash2(domainsList[di].B, 8, imageBrigthnesAverage)));
			}
			
			//var cnt = 0;
			
			for (var ri = 0; ri < rangesHashList.Count; ri++) {
				var best = Int32.MaxValue;
				var bestDomainIndex = 0;
				var bestRot = 0;
				
				for (var di = 0; di < domainsHashList.Count; di++) {
					for (var rot = 0; rot < 4; rot ++) {
						var res = HashCompare2(rangesHashList[ri].B, domainsHashList[di].B);
						if ( best > res) {
							best = res;
							bestDomainIndex = di;
							bestRot = rot;
						}
						rangesHashList[ri].B.RotateFlip(RotateFlipType.Rotate90FlipNone);
					}
				}
				var a = AffineTrans(rangesList[ri], domainsList[bestDomainIndex]);
				a.Rotate = bestRot;
				/*for (var rot = 0; rot < bestRot; rot ++) {
					rangesHashList[ri].B.RotateFlip(RotateFlipType.Rotate90FlipNone);
				}
				rangesHashList[ri].B.Save("res/" + ri + "r.bmp");
				domainsHashList[bestDomainIndex].B.Save("res/" + ri + "d.bmp");*/
				strings.Add(String.Format("{0};{1};{2};{3};{4};{5}", a.RangeX, a.RangeY, a.DomainX, a.DomainY, a.BrightnessShift, a.Rotate));
			}
			File.WriteAllLines("../../../res.csv", strings.ToArray());
		}
		
		public static  List<Region> GetRanges(Bitmap img, int rangeSize) {
			var bl = new List<Region>();
			var skipRanges = 0;
			for (var x = 0; x < (img.Width - rangeSize); x += rangeSize) {
				for (var y = 0; y < (img.Height - rangeSize); y += rangeSize) {
					try {
						var range = img.Clone(new Rectangle(x,y,rangeSize,rangeSize), img.PixelFormat);
						bl.Add(new Region(x,y,range));
					}
					catch {
						skipRanges++;
					}
				}
			}
			//MSG.WriteLine ("SkipRanges = {0}", skipRanges);
			return bl;
		}
		
		public static  List<Region> GetDomains(Bitmap img, int domainSize, int rangeSize) {
			var bl = new List<Region>();
			var skipDomains = 0;
			for (var x = 0; x < (img.Width - domainSize); x += rangeSize) {
				for (var y = 0; y < (img.Height - domainSize); y += rangeSize) {
					try {
						var domain = img.Clone(new Rectangle(x,y,domainSize,domainSize), img.PixelFormat);
						bl.Add(new Region(x,y,domain));
					}
					catch {
						skipDomains++;
					}
				}
			}
			//MSG.WriteLine ("SkipDomains = {0}", skipDomains);
			return bl;
		}
		
		public static Bitmap BitmapHash(Bitmap img, int size, int average) {
			var bh = new Bitmap(img, new Size(size,size));
			var averageB = 0;
			/*for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
					averageB += (bh.GetPixel(x,y).GetBrightness());
				}
			}
			averageB = averageB / (bh.Width * bh.Height);*/
			
			averageB = GetAverageBrightness(bh);
			//averageB =  average;
			
			for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
					if(bh.GetPixel(x,y).G > averageB) {
						bh.SetPixel(x,y, Color.White);
						//Console.WriteLine("$$$");
					}
					else {
						bh.SetPixel(x,y, Color.Black);
						//Console.WriteLine("***");
					}
				}
			}
			
			return bh;
		}
		
		public static Bitmap BitmapHash2(Bitmap img, int size, int average) {
			var bh = new Bitmap(img, new Size(size,size));
			var max = 0;
			for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
						max = Math.Max((int)bh.GetPixel(x,y).G, max);
				}
			}
			int kof = 0;
			double ankof = 0.0;
			if (max > 0) {
				kof = 255 / max;
				ankof = 1 / kof;
			}
			var averageB = GetAverageBrightness(img);
			
			for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
					int currentVal = bh.GetPixel(x,y).G;
					int newCol;
					if( currentVal > averageB) {
						 newCol = currentVal * kof;
						//Console.WriteLine("$$$");
					}
					else {
						newCol = (int)Math.Round((double)currentVal * ankof);
						//Console.WriteLine("***");
					}
					
						newCol = newCol < 0 ? 0 : newCol;
						newCol = newCol > 255 ? 255 : newCol;
						bh.SetPixel(x,y, Color.FromArgb(newCol,newCol,newCol));
				}
			}
			
			return bh;
		}
		public static double Sigmoid(double x)	{
   			return 2 / (1 + Math.Exp(-2 * x)) - 1;
		}
		public static int GetAverageBrightness(Bitmap b) {
			var averageB = 0;
			for (var x = 0; x < b.Width; x++) {
				for (var y = 0; y < b.Height; y++) {
					averageB += (b.GetPixel(x,y).G);
				}
			}
			averageB = averageB / (b.Width * b.Height);
			return averageB;
		}
		
		public static int HashCompare(Bitmap h1, Bitmap h2) {
			if (h1.Size != h2.Size) return -1;
			var diff = 0;
			for (var x = 0; x < h1.Width; x++) {
				for (var y = 0; y < h1.Height; y++) {
					if(h1.GetPixel(x,y).GetBrightness() != h2.GetPixel(x,y).GetBrightness()) {
						diff++;
						//Console.WriteLine("######");
					}
					//Console.WriteLine(h1.GetPixel(x,y).GetBrightness());
				}
			}
			return diff;
		}
		
		public static int HashCompare2(Bitmap h1, Bitmap h2) {
			if (h1.Size != h2.Size) return -1;
			
			//Console.WriteLine(GetAverageBrightness(h1));
			
			var diff = 0;
			for (var x = 0; x < h1.Width; x++) {
				for (var y = 0; y < h1.Height; y++) {
					//Console.WriteLine(p);
					diff += Math.Abs(h1.GetPixel(x,y).G - h2.GetPixel(x,y).G);

				}
				
			}

			return diff;
		}
		
		public static Affine AffineTrans(Region range, Region domain) {
			Affine a = new Affine();
			a.RangeX = range.X;
			a.RangeY = range.Y;
			a.DomainX = domain.X;
			a.DomainY = domain.Y;
			a.BrightnessShift = GetAverageBrightness(range.B) - GetAverageBrightness(domain.B);
			return a;
		}
		

	/*	public static bool[] BitmapHashB(Bitmap img, int size) {
			var bh = new Bitmap(img, new Size(size,size));
			var averageB = 0.0;
			
			var dim = new bool[bh.Width * bh.Height];
			for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
					averageB += (bh.GetPixel(x,y).GetBrightness());
				}
			}
			averageB = averageB / (bh.Width * bh.Height);
			
			for (var x = 0; x < bh.Width; x++) {
				for (var y = 0; y < bh.Height; y++) {
					if(bh.GetPixel(x,y).GetBrightness() > averageB)
						dim[x*y] = true;
					else
						dim[x*y] = false;
				}
			}
			
			return dim;
		}*/
	}
	
	struct Affine {
		public int RangeX;
		public int RangeY;
		public int DomainX;
		public int DomainY;
		public int BrightnessShift;
		public int Rotate;
	}
}

