using System;
using System.Drawing;
using System.Collections.Generic;

namespace FIC
{
	public class Region
	{
		public Region (int x, int y, Bitmap b)
		{
			X = x;
			Y = y;
			B = b;
		}
		
		public int X;
		public int Y;
		public Bitmap B;
	}
}

