using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;


namespace FIDC
{
	class MainClass
	{
		public static int RangeSize = 5;
		public static int DomainSize = 10;
		public static void Main (string[] args)
		{
			
			
			var lines = File.ReadAllLines("../../../res.csv");
			//Bitmap img0 = new Bitmap("img.bmp");
			var initStrs = lines[0].Split(';');
			RangeSize = Int32.Parse(initStrs[2]);
			DomainSize = Int32.Parse(initStrs[3]);
			Bitmap img = new Bitmap(Int32.Parse(initStrs[0]), Int32.Parse(initStrs[1]), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			
			var rnd = new Random();
			for (var x = 0; x < img.Width; x++) {
				for (var y = 0; y < img.Height; y++) {
					var col = rnd.Next(255);
					img.SetPixel(x, y, Color.FromArgb(col, col, col));
				}
			}
			
			
			//Console.WriteLine("width = {0}; rageSize = {1}; ranges = {2}", img.Width, rangeSize, img.Width/rangeSize);
			
			for ( var i = 0; i < 16; i++) {
				for (var li = 1; li < lines.Length; li++) {
					var line = lines[li];
					var values = line.Split(';');
					var a = new Affine();
					/*if(li == 0) {
						img = new Bitmap(Int32.Parse(values[0]), Int32.Parse(values[1]));
						continue;
					}*/ 
					a.RangeX = Int32.Parse(values[0]);
					a.RangeY = Int32.Parse(values[1]);
					a.DomainX = Int32.Parse(values[2]);
					a.DomainY = Int32.Parse(values[3]);
					a.BrightnessShift = Int32.Parse(values[4]);
					a.Rotate = Int32.Parse(values[5]);
					
					Recovery(ref img, a);
					
				}
				//img.Save(i + "recovery.bmp");
			}
			Contrast(ref img, 1.5);
			img.Save("recovery.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
			
		}
		
		public static void Recovery (ref Bitmap img, Affine a) {
			if ((a.DomainY + DomainSize) > img.Height) return;
			if ((a.DomainX + DomainSize) > img.Width) return;
			var domain = img.Clone(new Rectangle(a.DomainX, a.DomainY, DomainSize, DomainSize), img.PixelFormat);
			var range = new Bitmap(domain, new Size(RangeSize, RangeSize));
				
			for (var rot = 0; rot < a.Rotate; rot ++) {
				range.RotateFlip(RotateFlipType.Rotate270FlipNone);
			}
				
			for(var x = 0; x < range.Width; x++) {
				for(var y = 0; y < range.Height; y++) {
					var c = range.GetPixel(x,y);
					var nv = c.R + a.BrightnessShift;
					nv = nv < 0 ? 0 : nv;
					nv = nv > 255 ? 255 : nv;
					var sc = Color.FromArgb(nv, nv, nv);
					var xr = x + a.RangeX;
					var yr = y + a.RangeY;
					xr = xr > img.Width ? img.Width : xr;
					yr = yr > img.Height ? img.Height : yr;
					img.SetPixel(xr, yr, sc);
	
	
				}
			}

		}
		
		public static void Contrast (ref Bitmap img, double val) {
			var shift = (255.0 * val) - 255.0;
			for (var x = 0; x < img.Width; x++) {
				for (var y = 0; y < img.Height; y++) {
					int col = img.GetPixel(x,y).G;
					col = (int)Math.Round(((double)col * val) - shift);
					col = col < 0 ? 0 : col;
					col = col > 255 ? 255 : col;
					img.SetPixel(x, y, Color.FromArgb(col, col, col));
				}
			}
		}
	}
	struct Affine {
		public int RangeX;
		public int RangeY;
		public int DomainX;
		public int DomainY;
		public int BrightnessShift;
		public int Rotate;
	}
}

