using System;
using System.IO;
using System.Collections.Generic;

namespace Serenity {
	class MainClass	{
		public static void Main (string[] args) {
			//Console.WriteLine ("Hello World!");
			if (args.Length <= 0 || args.Length > 1) {
				PrintHelp();
				return;
			}
			if (!File.Exists(args[0])) {
				Console.WriteLine("File {0} not found", args[0]);
				PrintHelp();
				return;
			}
			
			try {
				var cont = File.ReadAllText(args[0]);
				cont = cont.Replace('\t', ' ');
				while(cont.IndexOf("  ") > 0) {
					cont = cont.Replace("  ", " ");
				}
				//Console.WriteLine(cont);
				var worlds = cont.Split(new Char [] {' ', ',', '.', ';', '(', ')', '{', '}', '[', ']', '|', '^', '@', '#', '$', '~', '&', '\"', '\'', ':', '!', '>', '<', '=', '+', '-', '*', '\\', '/', '?', '\t', '\n'});
				var cleanWorlds = new List<string>();
				for (var i = 0; i < worlds.Length; i++) {
					if (worlds[i].Length != 0) {
						cleanWorlds.Add(worlds[i]);
					}
				}
				
				foreach (var w in cleanWorlds) {

					Console.WriteLine("-> {0}", w);
				}
			}
			catch (Exception e) {
				Console.WriteLine("File {0} not read", args[0]);
				Console.WriteLine(e);
				PrintHelp();
			}
		}
		
		public static void PrintHelp() {
			Console.WriteLine("using:");
			Console.WriteLine("Serenity file1");
		}
			
	}
}

