using System;
using System.Collections.Generic;
using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

namespace ironpython_test
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var prefix = "C#: ";
			var engine = Python.CreateEngine();
			var paths = new List<string>();
			
			paths.Add("python/Lib");
			paths.Add("python");
			engine.SetSearchPaths(paths);
			
			var dicIn = new Dictionary<string, object>();
			dicIn.Add("suka",1133);
			var scope =  engine.CreateScope();
			scope.SetVariable("dicIn", dicIn);
			scope.SetVariable("x", 11);
			scope.SetVariable("y", 33);
			
			var source = engine.CreateScriptSourceFromFile("python/_py.py");
			var compiled = source.Compile();
			compiled.Execute(scope); 
			
			var z = scope.GetVariable("z");
			Console.WriteLine(prefix + "res = " + z + " type - " + z.GetType());
			
			var j = scope.GetVariable("j");
			Console.WriteLine(prefix + "j = " + j + " type - " + j.GetType());
			
			var o = scope.GetVariable("o");
			Console.WriteLine(prefix + "o = " + o + " type - " + o.GetType());
			foreach (var i in (IronPython.Runtime.PythonDictionary)o) {
				Console.WriteLine(prefix + "o. " + i.Key + " = " + i.Value);
			}
			
			var vars = scope.GetItems();
			foreach (var v in vars) {
				Console.WriteLine(prefix + "global var name -> " + v.Key);
			}
		}
	}
}
