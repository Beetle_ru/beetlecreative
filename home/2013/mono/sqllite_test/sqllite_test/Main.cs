using System;
using System.IO;
using Mono.Data.Sqlite;

namespace sqllite_test
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var dbname = "test.db";
			var exist = File.Exists(dbname);
			if (!exist)
				SqliteConnection.CreateFile(dbname);
			var conn = new SqliteConnection("Data Source=" + dbname);
			if (!exist) {
				conn.Open();
				var commands = new[] {
	            "CREATE TABLE People (PersonID INTEGER NOT NULL, FirstName ntext, LastName ntext)",
	            "INSERT INTO People (PersonID, FirstName, LastName) VALUES (1, 'First', 'Last')",
	            "INSERT INTO People (PersonID, FirstName, LastName) VALUES (2, 'Dewey', 'Cheatem')",
	            "INSERT INTO People (PersonID, FirstName, LastName) VALUES (3, 'And', 'How')",
	            };
				foreach (var cmd in commands) {
	                using (var c = conn.CreateCommand()) {
	                    c.CommandText = cmd;
	                    //c.CommandType = CommandType.Text;
	                    
						c.ExecuteNonQuery ();
	                }
	            }
	            conn.Close();
			}
			conn.Open();
			var cmd2 = conn.CreateCommand();
			cmd2.CommandText = "SELECT * FROM People";
			var reader = cmd2.ExecuteReader();
			while (reader.Read ()) {
                    Console.Error.Write ("(Row ");
					Console.Error.Write ("{0}", reader[0]);
                    //Write (reader, 0);
                    for (int i = 1; i < reader.FieldCount; ++i) {
                        Console.Error.Write(" ");
						Console.Error.Write ("{0}", reader[i]);
                        //Write (reader, i);
                    }
                    Console.Error.WriteLine(")");
            }
			conn.Close();
			Console.WriteLine ("Hello World!");
		}
	}
}
