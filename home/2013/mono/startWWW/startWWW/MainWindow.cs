using System;
using Gtk;
using System.Net;
using System.IO;
using System.Text;

public partial class MainWindow : Gtk.Window
{
	public MainWindow () : base(Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	protected virtual void OnButton1Clicked (object sender, System.EventArgs e)
	{
		System.Diagnostics.Process.Start("http://www.pulse-cc.org");
	}
	
	protected virtual void OnButton3Clicked (object sender, System.EventArgs e)
	{
		HttpWebRequest httpReq =  (HttpWebRequest)HttpWebRequest.Create(new Uri("http://www.pulse-cc.org"));
		HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
		Stream receiveStream = httpRes.GetResponseStream ();
		
		StreamReader readStream = new StreamReader (receiveStream, Encoding.UTF8);
		
		textview2.Buffer.Text = readStream.ReadToEnd ();
		
	}
	
	protected virtual void OnButton2Clicked (object sender, System.EventArgs e)
	{
		button2.ModifyFg(StateType.Normal, new Gdk.Color(255,0,0));
		button2.ModifyBase(StateType.Normal, new Gdk.Color(255,0,0));
		button2.ModifyBg(StateType.Normal, new Gdk.Color(255,0,0));
		try {
			var d = Double.Parse(entry1.Text);
			label1.Text = Math.Round(d + 0.5).ToString();
		}
		catch {
			label1.Text = "ERROR";
			//label1.ModifyFg(StateType.Normal, new Gdk.Color(255,0,0));
			label1.ModifyBase(StateType.Normal, new Gdk.Color(0,255,0));
			label1.ModifyBg(StateType.Normal, new Gdk.Color(0,0,255));
			//label1.Colormap = new Gdk.Colormap(Raw);
			//label1.LabelProp = @"<span foreground = ""green""></span>";
		}
	}
	
	
	
	
}

