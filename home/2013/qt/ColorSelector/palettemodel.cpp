#include "palettemodel.h"


PaletteModel::PaletteModel(QObject *parent) :
    QAbstractListModel(parent)
{
    int colororStep = 32;
    //RGB
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(i,0,0));
    }
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(0,i,0));
    }
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(0,0,i));
    }
    //CMY
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(0,i,i));
    }
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(i,0,i));
    }
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(i,i,0));
    }
    //gray
    for (int i = 255; i > colororStep; i -= colororStep) {
        colors.append(QColor(i,i,i));
    }
    //other
    colors.append(QColor(0,0,0));

}

int PaletteModel::rowCount(const QModelIndex& ) const
{
    return colors.size();
}

QVariant PaletteModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        return colors.at(index.row());
    }

    if(role == Qt::DecorationRole)
    {
        int row = index.row();
        QColor color = colors.value(row);

        QPixmap pixmap(26, 26);
        pixmap.fill(color);

        QIcon icon(pixmap);

        return icon;
    }

    if(role == Qt::EditRole)
    {
        return colors.at(index.row()).name();
    }

    if(role == Qt::ToolTipRole)
    {
        return "Hex code: " + colors.at(index.row()).name();
    }

    return QVariant();
}

QVariant PaletteModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Color");

        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}
