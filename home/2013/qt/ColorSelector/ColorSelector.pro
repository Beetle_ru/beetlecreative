#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T16:10:53
#
#-------------------------------------------------

QT       += core gui

TARGET = ColorSelector
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    colorlistmodel.cpp \
    palettelistmodel.cpp \
    palettemodel.cpp

HEADERS  += mainwindow.h \
    colorlistmodel.h \
    palettelistmodel.h \
    palettemodel.h

FORMS    += mainwindow.ui
