#include "colorlistmodel.h"
#include <QPixmap>
#include <QIcon>
#include <QDebug>

ColorListModel::ColorListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    //Colors = new QList<QColor>;

}

ColorListModel::ColorListModel(QList<QColor> items, QObject* parent)
    : QAbstractListModel(parent)
{
    colors = items;
}

int ColorListModel::rowCount(const QModelIndex &parent) const {
    //return 10;
    return colors.count();
    //return Colors.count();
}

QVariant ColorListModel::data(const QModelIndex &index, int role) const {

    if(!index.isValid())
        return QVariant();

    if(role == Qt::DecorationRole)
    {
        int row = index.row();
        QColor color = colors.value(row);

        QPixmap pixmap(100, 26);
        pixmap.fill(color);

        QIcon icon(pixmap);

        return icon;
    }

    return QVariant();
}

QVariant ColorListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Color");

        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}

Qt::ItemFlags ColorListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEnabled ;
}

void ColorListModel::addItem(QColor c) {
    colors.append(c);
}
