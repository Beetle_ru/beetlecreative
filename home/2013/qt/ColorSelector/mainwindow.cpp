#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStandardItemModel>
 #include <QBrush>
#include <QColor>
#include "colorlistmodel.h"
#include "palettelistmodel.h"
#include "palettemodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStandardItemModel *model = new QStandardItemModel();
    //ui->listView->setModel(model);
    QStandardItem *item = new QStandardItem();
    item->setData( "StringDescription", Qt::DisplayRole );
    //QColor * c = new QColor(255,125,50);
    //item->setData(c,Qt::BackgroundColorRole);
    //QBrush b;
    //b.setColor(Qt::red);
    //item->setBackground(b);
    //model->setItem(MsgIDlist.size(),i,item);

    //model2->addItem(Qt::red);
    //ui->listView->setModel(model2);

    model->appendRow( item );

    // RGBA
    QColor red = QColor(255, 0, 0, 255);
    QColor green = QColor(0, 255, 0, 255);
    QColor blue = QColor(0, 0, 255, 255);

    QList<QColor> items;
    items.append(red);
    items.append(green);
    items.append(blue);

    int rowCount = 4;
    int columnCount = 6;

    PaletteListModel *model3 = new PaletteListModel(items);
    PaletteModel *pallete = new PaletteModel();

    // Insert at Position 3 -> 5 empty rows
    model3->insertRows(3, 5);

    ColorListModel *model2 = new ColorListModel(items);
    //QColor c = QColor(255,0,0);
    //model2->addItem(c);
    ui->listView->setModel(pallete);
    ui->comboBox->setModel(pallete);
    ui->tableView->setModel(pallete);


}

MainWindow::~MainWindow()
{
    delete ui;
}
