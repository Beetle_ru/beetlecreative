#ifndef COLORLISTMODEL_H
#define COLORLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QVariant>
#include <QModelIndex>
#include <QColor>

class ColorListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ColorListModel(QObject *parent = 0);
    ColorListModel(QList<QColor> colors, QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int selection, Qt::Orientation orientation, int role) const;
    void addItem(QColor c);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    
signals:
    
public slots:
    //;

private:
    QList<QColor> colors;
    
};

#endif // COLORLISTMODEL_H
