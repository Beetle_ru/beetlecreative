#ifndef PALETTEMODEL_H
#define PALETTEMODEL_H

#include <QAbstractListModel>
#include <qstringlist.h>
#include <qcolor.h>
#include <qlist.h>
#include <qpixmap.h>
#include <qicon.h>
#include <qstring.h>

class PaletteModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit PaletteModel(QObject *parent = 0);

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int selection, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    
signals:
    
public slots:
    
private:
    QList<QColor> colors;
};

#endif // PALETTEMODEL_H
