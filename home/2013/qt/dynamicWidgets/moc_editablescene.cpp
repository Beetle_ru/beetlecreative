/****************************************************************************
** Meta object code from reading C++ file 'editablescene.h'
**
** Created: Wed Sep 4 15:55:24 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "editablescene.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'editablescene.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EditableScene[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   15,   14,   14, 0x0a,
      64,   15,   14,   14, 0x0a,
     120,  109,   14,   14, 0x0a,
     177,  162,   14,   14, 0x0a,
     215,   15,   14,   14, 0x0a,
     241,   15,   14,   14, 0x0a,
     288,  274,  269,   14, 0x0a,
     339,  322,  318,   14, 0x0a,
     380,   14,  365,   14, 0x0a,
     408,  403,   14,   14, 0x0a,
     438,  436,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EditableScene[] = {
    "EditableScene\0\0event\0"
    "mousePressEvent(QGraphicsSceneMouseEvent*)\0"
    "mouseReleaseEvent(QGraphicsSceneMouseEvent*)\0"
    "mouseEvent\0mouseMoveEvent(QGraphicsSceneMouseEvent*)\0"
    "mouseWhelEvent\0wheelEvent(QGraphicsSceneWheelEvent*)\0"
    "keyPressEvent(QKeyEvent*)\0"
    "keyReleaseEvent(QKeyEvent*)\0bool\0"
    "watched,event\0eventFilter(QObject*,QEvent*)\0"
    "int\0x,y,xx,yy,parent\0Rect(int,int,int,int,int)\0"
    "QGraphicsItem*\0CreateGraphicsObject()\0"
    "type\0SetCreatingType(CreateType)\0r\0"
    "SetCurrentSvgRenderr(QSvgRenderer*)\0"
};

void EditableScene::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EditableScene *_t = static_cast<EditableScene *>(_o);
        switch (_id) {
        case 0: _t->mousePressEvent((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 1: _t->mouseReleaseEvent((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 2: _t->mouseMoveEvent((*reinterpret_cast< QGraphicsSceneMouseEvent*(*)>(_a[1]))); break;
        case 3: _t->wheelEvent((*reinterpret_cast< QGraphicsSceneWheelEvent*(*)>(_a[1]))); break;
        case 4: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 5: _t->keyReleaseEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 6: { bool _r = _t->eventFilter((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QEvent*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { int _r = _t->Rect((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 8: { QGraphicsItem* _r = _t->CreateGraphicsObject();
            if (_a[0]) *reinterpret_cast< QGraphicsItem**>(_a[0]) = _r; }  break;
        case 9: _t->SetCreatingType((*reinterpret_cast< CreateType(*)>(_a[1]))); break;
        case 10: _t->SetCurrentSvgRenderr((*reinterpret_cast< QSvgRenderer*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData EditableScene::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject EditableScene::staticMetaObject = {
    { &QGraphicsScene::staticMetaObject, qt_meta_stringdata_EditableScene,
      qt_meta_data_EditableScene, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EditableScene::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EditableScene::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EditableScene::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EditableScene))
        return static_cast<void*>(const_cast< EditableScene*>(this));
    return QGraphicsScene::qt_metacast(_clname);
}

int EditableScene::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsScene::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
