#-------------------------------------------------
#
# Project created by QtCreator 2013-08-30T18:47:38
#
#-------------------------------------------------

QT       += core gui svg

TARGET = dynamicWidgets
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    editablescene.cpp \
    imagelibrary.cpp \
    imagelibrarymodel.cpp

HEADERS  += mainwindow.h \
    editablescene.h \
    imagelibrary.h \
    imagelibrarymodel.h

FORMS    += mainwindow.ui \
    imagelibrary.ui

OTHER_FILES += \
    scriptTest.js
