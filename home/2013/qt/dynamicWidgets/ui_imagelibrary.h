/********************************************************************************
** Form generated from reading UI file 'imagelibrary.ui'
**
** Created: Wed Sep 4 14:39:09 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGELIBRARY_H
#define UI_IMAGELIBRARY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QListView>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ImageLibrary
{
public:
    QDialogButtonBox *buttonBox;
    QListView *ViewListAddedImages;
    QPushButton *BtnAddImg;

    void setupUi(QDialog *ImageLibrary)
    {
        if (ImageLibrary->objectName().isEmpty())
            ImageLibrary->setObjectName(QString::fromUtf8("ImageLibrary"));
        ImageLibrary->resize(400, 300);
        buttonBox = new QDialogButtonBox(ImageLibrary);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(200, 260, 181, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        ViewListAddedImages = new QListView(ImageLibrary);
        ViewListAddedImages->setObjectName(QString::fromUtf8("ViewListAddedImages"));
        ViewListAddedImages->setGeometry(QRect(10, 10, 381, 241));
        ViewListAddedImages->setViewMode(QListView::IconMode);
        BtnAddImg = new QPushButton(ImageLibrary);
        BtnAddImg->setObjectName(QString::fromUtf8("BtnAddImg"));
        BtnAddImg->setGeometry(QRect(10, 260, 87, 27));

        retranslateUi(ImageLibrary);
        QObject::connect(buttonBox, SIGNAL(accepted()), ImageLibrary, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ImageLibrary, SLOT(reject()));

        QMetaObject::connectSlotsByName(ImageLibrary);
    } // setupUi

    void retranslateUi(QDialog *ImageLibrary)
    {
        ImageLibrary->setWindowTitle(QApplication::translate("ImageLibrary", "Dialog", 0, QApplication::UnicodeUTF8));
        BtnAddImg->setText(QApplication::translate("ImageLibrary", "add image", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ImageLibrary: public Ui_ImageLibrary {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGELIBRARY_H
