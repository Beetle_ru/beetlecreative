#ifndef EDITABLESCENE_H
#define EDITABLESCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QtSvg>
#include <QGraphicsSvgItem>

class EditableScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit EditableScene(QObject *parent = 0);
    enum CreateType {Empty, Rectangle, Circle, Image, Indicator};
    
signals:
    
public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    void wheelEvent(QGraphicsSceneWheelEvent *mouseWhelEvent);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
    int Rect(int x, int y, int xx, int yy, int parent);
    QGraphicsItem* CreateGraphicsObject();
    void SetCreatingType(CreateType type);
    void SetCurrentSvgRenderr(QSvgRenderer *r);

private:
    QGraphicsItem *SelectedItem;
    bool ItemUnderMouse;
    bool IsScale;
    bool IsRotate;
    CreateType ActiveCreatetingType;
    qreal MousePositionX;
    qreal MousePositionY;
    QSvgRenderer *CurrentSvgRenderr;
};

#endif // EDITABLESCENE_H
