#include "imagelibrary.h"
#include "ui_imagelibrary.h"
#include <QAbstractButton>
#include <QFileDialog>
#include <QDebug>

ImageLibrary::ImageLibrary(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageLibrary)
{
    ui->setupUi(this);
    ui->ViewListAddedImages->setModel(&ImageLibModel);
    ItemIsSelected = false;
}

ImageLibrary::~ImageLibrary()
{
    delete ui;
}

QSvgRenderer* ImageLibrary::Exec() {
    this->show();
    EventLoop.exec();
    //qDebug() << ui->ViewListAddedImages->selectedIndexes().count();
    if (ItemIsSelected)
        return ImageLibModel.GetItem(SelectedIndex);
    else
        return 0;
}

void ImageLibrary::on_buttonBox_accepted()
{
    if (ItemIsSelected) {
        EventLoop.exit();
        this->close();
    }
}

void ImageLibrary::on_buttonBox_rejected()
{
    EventLoop.exit();
    this->close();
}

void ImageLibrary::on_BtnAddImg_clicked()
{
    ImageLibModel.AddItem(QFileDialog::getOpenFileName(this, "open", "./", "SVG Files (*.svg)"));

}


void ImageLibrary::on_ViewListAddedImages_clicked(const QModelIndex &index)
{
    SelectedIndex = index.row();
    ItemIsSelected = true;
}
