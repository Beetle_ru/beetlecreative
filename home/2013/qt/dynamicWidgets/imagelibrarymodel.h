#ifndef IMAGELIBRARYMODEL_H
#define IMAGELIBRARYMODEL_H

#include <QAbstractListModel>
#include <qlist.h>
#include <QtSvg>
#include <qpixmap.h>
#include <qicon.h>
#include <qstring.h>
#include <QPainter>

class ImageLibraryModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ImageLibraryModel(QObject *parent = 0);

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int selection, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    void AddItem(QString file);
    QSvgRenderer *GetItem(int index);
    
signals:
    
public slots:

private:
    QList<QSvgRenderer *> images;
    
};

#endif // IMAGELIBRARYMODEL_H
