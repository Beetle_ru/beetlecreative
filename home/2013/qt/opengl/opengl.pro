#-------------------------------------------------
#
# Project created by QtCreator 2013-07-30T13:22:30
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = opengl
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
