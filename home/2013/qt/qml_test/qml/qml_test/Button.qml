// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: button
    width: 86
    height: 35
    radius: 11
    smooth: true
    property string label: "btn"

        gradient: Gradient {
        GradientStop {
            id: gradientstop1
            position: 0
            color: "#3e0101"
        }

        GradientStop {
            id: gradientstop2
            position: 1
            color: "#973200"
        }
    }

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        radius: 6
        smooth: true
        opacity: 0.600
        anchors.rightMargin: 8
        anchors.leftMargin: 8
        anchors.bottomMargin: 25
        anchors.topMargin: 2
        anchors.fill: parent
    }

    Text {
        id: text1
        color: "#ffd500"
        text: qsTr("text")
        smooth: true
        anchors.fill: parent
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 17
    }

    MouseArea {
        id: mouse_area1
        anchors.fill: parent

        onPressed: button.state = "press"
        onReleased: button.state = "def"
    }
    states: [
        State {
            name: "press"

            PropertyChanges {
                target: gradientstop1
                position: 0
                color: "#81c200"
            }

            PropertyChanges {
                target: gradientstop2
                position: 1
                color: "#973200"
            }
        }
    ]
}
