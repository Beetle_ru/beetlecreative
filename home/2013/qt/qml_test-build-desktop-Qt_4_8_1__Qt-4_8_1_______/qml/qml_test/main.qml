// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: 556
    height: 360
    color: "#9c9c9c"


   function create2() {
        //var newObject = Qt.createQmlObject('import QtQuick 1.0; Rectangle {color: "red"; width: 20; height: 20}',
             //rectangle1, "dynamicSnippet1");
        for (var i = 0; i < 10; i++) {

            Qt.createQmlObject('import QtQuick 1.0; Rectangle {color: "red"; width: 20; height: 20; x:' + (i * 23) + '}',
                     rectangle1, "dynamicSnippet1");
        }
    }
    function _2dig(x) {
      //return x<10?'0'+x:x ;
        return x.length<2?'0'+x:x ;
    }
    function rgb(r,g,b) {
      return '#'+_2dig(r)+_2dig(g)+_2dig(b) ;
    }

    function createSpriteObjects() {

        //for (var i = 0; i < 10; i++) {
         var component = Qt.createComponent("Sprite.qml");
        //var component = Qt.createComponent("Button.qml");

        for (var i = 0; i < 100; i++) {
            //var sprite = component.createObject(grid1, {"x": i * 23, "y": 1});
            var ic = i * 1;
            ic = ic>255?255:ic;

            console.log(ic);
            //var c = rgb("cc","cc",ic.toString(16));
            var c = rgb(ic.toString(16),ic.toString(16),"ff");


            console.log(c);
            var sprite = component.createObject(grid1, {"color": c});
            model.append({name: c, colorCode: c});
            modelTbl.append({name: c, colorCode: c});
        }
        if (sprite == null) {
            // Error Handling
            console.log("Error creating object");
        }
    }



    Button {
        id:btn
        x: 18
        y: 15
        MouseArea {
            anchors.fill: parent
            onPressed: createSpriteObjects()
        }
    }

    /*Grid  {
        id: table
        x: 20
        y: 189
        width: 0
        height: 0
        anchors.verticalCenterOffset: 62
        anchors.horizontalCenterOffset: 11
           anchors.horizontalCenter: parent.horizontalCenter
           anchors.verticalCenter: parent.verticalCenter
           columns: 4
           spacing: 6

           Rectangle  { x: 0; color: "#aa6666"; width: 50; height: 50 }
           Rectangle  { color: "#aaaa66"; width: 50; height: 50 }
           Rectangle  { color: "#9999aa"; width: 50; height: 50 }
           Rectangle  { color: "#6666aa"; width: 50; height: 50 }

           Rectangle  { color: "#aa6666"; width: 50; height: 50 }
           Rectangle  { color: "#aaaa66"; width: 50; height: 50 }
           Rectangle  { color: "#9999aa"; width: 50; height: 50 }
           Rectangle  { color: "#6666aa"; width: 50; height: 50 }
       }*/



       Rectangle {
           id: rectangle1
           x: 34
           y: 226
           width: 498
           height: 104
           color: "#ffffff"

           GridView {
               id: grid_view1
               anchors.rightMargin: 5
               anchors.leftMargin: 5
               anchors.bottomMargin: 5
               anchors.topMargin: 5
               flickableDirection: Flickable.HorizontalAndVerticalFlick
               flow: GridView.TopToBottom
               clip: true
               boundsBehavior: Flickable.StopAtBounds
               snapMode: GridView.NoSnap
               highlightRangeMode: GridView.ApplyRange
               keyNavigationWraps: true
               anchors.fill: parent
               cellHeight: 80
               cellWidth: 85
               delegate: Item {
                   x: 5
                   height: 50
                   Column {
                       spacing: 5
                       Rectangle {
                           width: 70
                           height: 30
                           color: colorCode
                           anchors.horizontalCenter: parent.horizontalCenter
                           Text {
                               text: name
                               anchors.horizontalCenter: parent.horizontalCenter
                               font.bold: true
                           }
                       }
                       Rectangle {
                           width: 70
                           height: 30
                           color: "#ffcccc"
                           anchors.horizontalCenter: parent.horizontalCenter
                           Text {
                               text: "value"
                               anchors.horizontalCenter: parent.horizontalCenter
                               font.bold: true
                           }
                       }

                       Rectangle {
                           width: 70
                           height: 30
                           color: "#ccffcc"
                           anchors.horizontalCenter: parent.horizontalCenter
                           Text {
                               text: "value"
                               anchors.horizontalCenter: parent.horizontalCenter
                               font.bold: true
                           }
                       }


                   }
               }
               model: ListModel {
                   id: modelTbl
               }
               /*model: ListModel {
                   ListElement {
                       name: "Grey"
                       colorCode: "grey"
                   }

                   ListElement {
                       name: "Red"
                       colorCode: "red"
                   }

                   ListElement {
                       name: "Blue"
                       colorCode: "blue"
                   }

                   ListElement {
                       name: "Green"
                       colorCode: "green"
                   }*/
               //}
           }
       }

       Rectangle {
           id: rectangle2
           x: 342
           y: 15
           width: 127
           height: 200
           color: "#ffffff"

           ListView {
               id: list_view1
               anchors.rightMargin: 5
               anchors.leftMargin: 5
               anchors.bottomMargin: 5
               anchors.topMargin: 5
               anchors.fill: parent
               clip: true
               spacing: 3
               highlightFollowsCurrentItem: true
               preferredHighlightEnd: 662
               preferredHighlightBegin: 716
               highlightResizeSpeed: 459
               highlightResizeDuration: 743
               highlightMoveSpeed: 419
               highlightMoveDuration: 445
               snapMode: ListView.NoSnap
               keyNavigationWraps: true
               interactive: true
               flickDeceleration: 999
               delegate: Item {
                   x: 5
                   height: 40
                   Row {
                       id: row1
                       spacing: 10
                       Rectangle {
                           width: 70
                           height: 30
                           color: colorCode
                           Text {
                               text: name
                               anchors.verticalCenter: parent.verticalCenter
                               font.bold: true
                           }
                       }


                   }
               }
               model: ListModel {
                   id: model
               }
               /*model: ListModel {
               id: model
               ListElement {
                   name: "Grey"
                   colorCode: "grey"
               }

               ListElement {
                   name: "Red"
                   colorCode: "red"
               }

               ListElement {
                   name: "Blue"
                   colorCode: "blue"
               }

               ListElement {
                   name: "Green"
                   colorCode: "green"
               }
           }*/
           }
       }

       Rectangle {
           id: rectangle3
           x: 119
           y: 15
           width: 200
           height: 200
           color: "#ffffff"

           Flickable {
               id: flickable1
               anchors.rightMargin: 5
               anchors.leftMargin: 5
               anchors.bottomMargin: 5
               anchors.topMargin: 5
               anchors.fill: parent
               flickDeceleration: 100
               clip: true
               boundsBehavior: Flickable.DragAndOvershootBounds
               contentHeight: grid1.height
               contentWidth: grid1.width

               Grid {
                   id: grid1
                   anchors.fill: parent
                   flow: Grid.TopToBottom
                   spacing: 3
                   columns: 8
               }
           }
       }



}
