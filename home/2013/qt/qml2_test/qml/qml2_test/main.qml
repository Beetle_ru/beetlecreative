import QtQuick 2.0

Rectangle {
    width: 360
    height: 360
    Text {
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }
    MouseArea {
        id: mousearea1
        anchors.fill: parent
        onClicked: {
            colored.color = "#"+((1<<24)*Math.random()|0).toString(16)
        }
        onDoubleClicked: {
            //Qt.quit();
        }


    }
    Rectangle {
        id: colored
        x: 72
        y: 205
        width: 200
        height: 119
        color: "#fea0ff"
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Row {
        id: row1
        x: 8
        y: 8
        width: 344
        height: 83
        spacing: 10

        Button { text: "Apple" }
        Button { text: "Orange" }
        Button { text: "Pear" }
        Button { text: "Grape" }
    }
}
