import QtQuick 2.0

Rectangle {
    id: mr
    width: 70
    height: 30
    property alias text: txtItem.text
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#ff73ca"
        }

        GradientStop {
            position: 0.5
            color: "#66004e"
        }

        GradientStop {
            position: 1
            color: "#f562cc"
        }
    }

    Text {
        id: txtItem
        x: 16
        y: -16
        width: 38
        height: 21
        color: "#ffffff"
        text: qsTr("Text")
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 14
        font.pixelSize: 12
    }
}
