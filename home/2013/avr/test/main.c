#include <avr/io.h>

#include <stdio.h>
#include <util/delay.h>

const unsigned char p1 = 1 << 0;
const unsigned char p2 = 1 << 1;
const unsigned char p3 = 1 << 2;
const unsigned char p4 = 1 << 3;
const unsigned char p5 = 1 << 4;


int main(void) {
	DDRA=0xff;
        
        const unsigned char odd = p1 | p3;
        const unsigned char even = p2 | p4;
        unsigned char state = 0;
        
        int injectionTime = 50;
        int flashTime = 10;
        int wait = 100;

	while (1) {
		//state |= p5;
		//state |= p3;
		//state |= even;
		//PORTA = state;
		//_delay_ms(100);
		
		//PORTA=0x00;
		//state &= p5;
		//state &= p3;
		//state &= even;
		
		state &= odd;
		PORTA = state;
		_delay_ms(wait);
		
		state |= odd;
		PORTA = state;
		_delay_ms(injectionTime);
		
		state &= p5;
		PORTA = state;
		_delay_ms(wait);
		
		state |= p5;
		PORTA = state;
		_delay_ms(flashTime);
		
		state &= even;
		PORTA = state;
		_delay_ms(wait);
		
		state |= even;
		PORTA = state;
		_delay_ms(injectionTime);
		
		state &= p5;
		PORTA = state;
		_delay_ms(wait);
		
		state |= p5;
		PORTA = state;
		_delay_ms(flashTime);
	}
}
