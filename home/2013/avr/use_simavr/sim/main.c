#include <simavr/sim_avr.h>
#include <simavr/avr_twi.h>
#include <simavr/sim_elf.h>
#include <simavr/sim_gdb.h>
#include <simavr/sim_vcd_file.h>
#include <simavr/avr_ioport.h>
//#include <simavr/i2c_eeprom.h>
#include <string.h>
#include <pthread.h>

avr_t * avr = NULL;
unsigned char portval = 0;
unsigned char port[8];
avr_vcd_t vcd_file;

void pin_changed_hook(struct avr_irq_t * irq, uint32_t value, void * param) {

	uint8_t pin_state = value;
	
	//if (!irq->irq) printf("\n");
	//printf("[%d]", pin_state);
	
	//printf("[%d] ==>> %d\n", pin_state, irq->irq);
	if (value)
		portval |= (1 << irq->irq);
	else
		portval &= ~(1 << irq->irq);
	
	//printf("-->> %d; irq = %d\n", portval, irq->irq);
	port[irq->irq] = value;
	for (int i = 0; i < 8; i++) {
		printf("[%d]", port[i]);
	}
	printf("\n");
}

static void * avr_run_thread(void * param) {
	while (1) {
		avr_run(avr);
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	elf_firmware_t f;
	const char * fname =  "main.elf";
	
	printf("Firmware pathname is %s\n", fname);
	elf_read_firmware(fname, &f);

	printf("firmware %s f=%d mmcu=%s\n", fname, (int)f.frequency, f.mmcu);

	avr = avr_make_mcu_by_name(f.mmcu);
	if (!avr) {
		fprintf(stderr, "%s: AVR '%s' not known\n", argv[0], f.mmcu);
		//exit(1);
		return 1;
	}

	avr_init(avr);
	avr_load_firmware(avr, &f);
	

	// connect all the pins on port A to our callback
	for (int i = 0; i < 8; i++)
		avr_irq_register_notify(
			avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'), i),
			pin_changed_hook, 
			NULL);
	
	avr_vcd_init(avr, "gtkwave_output.vcd", &vcd_file, 100000 /* usec */);
	avr_vcd_add_signal(&vcd_file, 
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ('A'), IOPORT_IRQ_PIN_ALL), 8 /* bits */ ,
		"portb" );
	avr_vcd_start(&vcd_file);
	
	pthread_t run;
	pthread_create(&run, NULL, avr_run_thread, NULL);
	
	printf("press \"Enter\" for exit\n");
	getchar();
	
	avr_vcd_stop(&vcd_file);
	
	
	return 0;
}

