consoleSetter = function () {
  this.reset = function () {
    process.stdout.write('\033[0m');
  }
  this.setfg = function (color) {
    switch (color) {
      case 'black':
        process.stdout.write('\033[30m');
      break;

      case 'red':
        process.stdout.write('\033[31m');
      break;

      case 'green':
        process.stdout.write('\033[32m');
      break;

      case 'yellow':
        process.stdout.write('\033[33m');
      break;

      case 'blue':
        process.stdout.write('\033[34m');
      break;

      case 'purple':
        process.stdout.write('\033[35m');
      break;

      case 'cygan':
        process.stdout.write('\033[36m');
      break;

      case 'gray':
        process.stdout.write('\033[36m');
      break;

      default:
        console.error('undefined color');
      break;
    }
  }

  this.setbg = function (color) {
    switch (color) {
      case 'black':
        process.stdout.write('\033[40m');
      break;

      case 'red':
        process.stdout.write('\033[41m');
      break;

      case 'green':
        process.stdout.write('\033[42m');
      break;

      case 'yellow':
        process.stdout.write('\033[43m');
      break;

      case 'blue':
        process.stdout.write('\033[44m');
      break;

      case 'purple':
        process.stdout.write('\033[45m');
      break;

      case 'cygan':
        process.stdout.write('\033[46m');
      break;

      case 'gray':
        process.stdout.write('\033[46m');
      break;

      default:
        console.error('undefined color');
      break;
    }
  }

  this.setatt = function (color) {
    switch (color) {
      case 'bold':
        process.stdout.write('\033[1m');
      break;

      case 'shadow':
        process.stdout.write('\033[2m');
      break;

      case 'underline':
        process.stdout.write('\033[4m');
      break;

      case 'flash':
        process.stdout.write('\033[5m');
      break;

      case 'reverse':
        process.stdout.write('\033[7m');
      break;

      case 'noshadow':
        process.stdout.write('\033[22m');
      break;

      case 'nounderline':
        process.stdout.write('\033[24m');
      break;

      case 'noflash':
        process.stdout.write('\033[25m');
      break;

      case 'noreverse':
        process.stdout.write('\033[25m');
      break;

      default:
        console.error('undefined attribute');
      break;
    }
  }
}

cs = new consoleSetter();

cs.setfg('yellow');
cs.setbg('blue');
cs.setatt('underline');
process.stdout.write('testo');
cs.reset();
process.stdout.write('\n');
//console.log('testo');
cs.reset();