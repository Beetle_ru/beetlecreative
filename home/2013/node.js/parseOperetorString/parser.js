var cmdLine = 'operator1(1, 2, 3) operator2 (1 2)'



function prser(commandLine, operators) {
	function operatorModel(operatorCallbacks) {
		this.operators = operatorCallbacks;
		this.operationLine = '';
		this.argsLine = '';
		
		this.appendArgStr = function (chr) {
			this.argsLine += chr;
		}
		
		this.appendName = function (chr) {
			this.operationLine += chr;
		}
		
		function wash(str, NotAllowChrs) {
			var res = '';
			for (var i = 0; i < str.length; i++) {
				var ok = true;
				for (var j = 0; j < NotAllowChrs.length; j++) {
					if (str[i] === NotAllowChrs[j]) {
						ok = false;
						break;
					}
				}
				if (ok)	{ res += str[i]; };
			}
			return res;
		}
		
		this.execute = function () {
			operation = wash(this.operationLine, '()[]{}"\'\\/|!@#$%^&*+=-:;,.?<>\n\t ');
			this.argsLine = wash(this.argsLine, '()[]{}"\'\\/|!@#$%^&*+=-:;?<>');

			var splited = this.argsLine.split(/[\s,]/);
			var args = [];

			for (var i = 0; i < splited.length; i++) {
				var val = parseInt(splited[i], 10);
				if (val === val) { args.push(val); }
			}

			for (var key in this.operators) {
				if (key == operation) {
					this.operators[key](args);
				}
			}
		}
	}
	
	
	var bkt = 0; // bracket index
	var transporter = [new operatorModel(operators)];
	// main loop
	for (var i = 0; i < commandLine.length; i++) {
		var c = commandLine[i];
		
		if (c === '(') {
			bkt++;
			continue;
		}
		if (c === ')') {
			bkt--;
			transporter.push(new operatorModel(operators));
			continue;
		}
		
		var operator = transporter[transporter.length - 1];
		
		switch (bkt) {
			case 0:	// operator
				operator.appendName(c);
			break;
			case 1:	// arguments
				operator.appendArgStr(c);
			break;
			default: // error
				throw 'Bad format the command line';
			break;
		}
	}

	for (var i in transporter) {
		transporter[i].execute();
	}
}

var o = {};
o.operator1 = function (args) {
	console.log('operator1 ' + args);
};

o.operator2 = function (args) {
	console.log('operator2 ' + args);
};

prser(cmdLine, o);

