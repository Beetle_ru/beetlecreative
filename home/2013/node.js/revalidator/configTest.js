var revalidator = require('revalidator');

var schema = {

  type: 'object',
  additionalProperties: false,
  properties: {
    name:             { type: 'string',   required: false },
    css_prefix_text:  { type: 'string',   required: true  },
    css_use_suffix:   { type: 'boolean',  required: true  },
    hinting:          { type: 'boolean',  required: false },
    units_per_em:     { type: 'integer',  required: false, minimum: 10 },
    ascent:           { type: 'integer',  required: false, minimum: 10 },
    glyphs: {
      type: 'array',
      minItems: 1,
      items: //[
        // embedded glyph schema
        {
          type: 'object',
          additionalProperties: false,
          properties: {
            uid:  { type: 'string',  required: true },
            css:  { type: 'string',  required: true },
            code: { type: 'integer', required: true, minimum: 1 },
            src:  { type: 'string',  required: true },
          }
        },
        // custom icon schema
        /*{
          type: 'object',
          additionalProperties: false,
          properties: {
            uid:  { type: 'string',  required: true },
            css:  { type: 'string',  required: true },
            code: { type: 'integer', required: true,  minimum: 1 },
            src:  { type: 'string',  required: true,  pattern: /^custom_icons$/ },
            svg: {
              type: 'object',
              required: true,
              additionalProperties: false,
              properties: {
                path:   { type: 'string',  required: true },
                width:  { type: 'integer', required: true,  minimum: 10 },
              }
            }
          }
        }*/
      //]
    }
  }
};

function uid() {
  /*jshint bitwise: false*/
  return 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
    return ((Math.random()*16)|0).toString(16);
  });
}

var confing = {
	 name:             'text',
     css_prefix_text:  'text',
     css_use_suffix:   false,
     hinting:          false,
     units_per_em:     100,
     ascent:           100
};

confing['glyphs'] = [];
confing.glyphs.push('1133')
for (var i = 0; i < 1; i++) {
	var o = {
		uid:  uid(),
        css:  'text',
        code: 1133,
        src:  'text'
	}
	confing.glyphs.push(o);
}
/*
for (var i = 0; i < 10; i++) {
	var o = {
		uid:  uid(),
        css:  'text',
        code: 1133,
        src:  'text',
        svg: {
            path:   'qweqweqweqweqwe',
            width:  100,
        }
	}
	confing.glyphs.push(o);
}*/


console.log(revalidator.validate(confing, schema));
