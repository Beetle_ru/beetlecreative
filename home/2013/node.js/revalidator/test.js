var revalidator = require('revalidator');

object = { url : 'www.some.com', challenge : '12345', arr : [1,2,3]};

schema = {
	properties: {
	  url: {
		description: 'the url the object should be stored at',
		type: 'string',
		//pattern: '^/[^#%&*{}\\:<>?\/+]+$',
		required: true
	  },
	  challenge: {
		description: 'a means of protecting data (insufficient for production, used as example)',
		type: 'string',
		minLength: 5
	  },
	  arr: {
		type: 'array',
		required: true,
		properties: {
			str : {
				type: 'string',
				required: true
			},
		},
	  },
	  body: {
		description: 'what to store at the url',
		type: 'any',
		default: null
	  }
	}
}

console.log(revalidator.validate(object, schema));
