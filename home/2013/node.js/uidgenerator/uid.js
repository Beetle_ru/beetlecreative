function uidGen() {
  return 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[x]/g, function(c) {
    return (Math.random()*16|0).toString(16);
  });
}

console.log(uidGen());
