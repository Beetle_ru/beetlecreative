# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 18:53:09 2013

@author: beetle
"""
from pysimavr.avr import Avr
avr=Avr(mcu='atmega48',f_cpu=8000000)
firmware = Firmware('lcd.elf')
avr.load_firmware(firmware)