# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 13:41:20 2013

@author: beetle
"""

import json
import pickle

favorite_color = { "lion": "yellow", "kitty": "red" } # dictionary

print "object:"
print favorite_color
print "##################################################\n"

picklestr = pickle.dumps(favorite_color)

print "serialize pikle:"
print picklestr

print "deserialize pikle:"
print pickle.loads(picklestr)

print "##################################################\n"
jsonstr = json.dumps(favorite_color)

print "serialize json:"
print jsonstr

print "deserialize json:"
print json.loads(jsonstr)