import Image
import ImageDraw
import random

rdSize = (20,40) # range size, domain size
 
inImg = Image.open("in.jpg")
img = Image.new( 'RGB', inImg.size, "black") # create a new black image
img.paste(inImg, (0,0, inImg.size[0], inImg.size[1]))
draw = ImageDraw.Draw(inImg)
#draw.text((100, 100), '%d' % 123)
#box = (430,230,470,270)

#img.paste(inImg.crop(box), box)
#pixels = img.load() # create the pixel map

def diffImg2(img1, img2):
    if img1.size != img2.size:
        return
    sx = img1.size[0]
    sy = img1.size[1]
    
    pix1 = img1.load()
    pix2 = img2.load()


    steep = 5

    y = steep
    ax = 0
    i = 0
    while y + steep < sy:
        x = steep
        
        while x + steep <  sx:
            #print 'x = ', x, 'y = ', y, 'data = ', pix1[x,y]
            ax += abs(pix1[x,y][0] - pix2[x,y][0])
            ax += abs(pix1[x,y][1] - pix2[x,y][1])
            ax += abs(pix1[x,y][2] - pix2[x,y][2])
            i += 3
            x += steep
        y += steep
    return (int)(ax / i)

def diffImg(img1, img2):
    if img1.size != img2.size:
        return
    sx = img1.size[0]
    sy = img1.size[1]
    
    pix1 = img1.load()
    pix2 = img2.load()


    steep = 5

    y = steep
    ax = 0
    while y + steep < sy:
        x = 0
        rs = 0
        gs = 0
        bs = 0
        while x <  sx:
            #print 'x = ', x, 'y = ', y, 'data = ', pix1[x,y]
            rs += abs(pix1[x,y][0] - pix2[x,y][0])
            gs += abs(pix1[x,y][1] - pix2[x,y][1])
            bs += abs(pix1[x,y][2] - pix2[x,y][2])
            x += 1
        y += steep
        ax += ((rs + gs + bs) * 0.333)
    ax = ax / (sy / steep)

    x = steep
    ay = 0
    while x + steep < sx:
        y = 0
        rs = 0
        gs = 0
        bs = 0
        while y <  sy:
            #print 'x = ', x, 'y = ', y, 'data = ', pix1[x,y]
            rs += abs(pix1[x,y][0] - pix2[x,y][0])
            gs += abs(pix1[x,y][1] - pix2[x,y][1])
            bs += abs(pix1[x,y][2] - pix2[x,y][2])
            y += 1
        x += steep
        ay += ((rs + gs + bs) * 0.333)
    ay = ay / (sx / steep)
    
    return (int)((ax + ay) * 0.5)

def averageColor(img):
    a = [0, 0, 0]
    pix = img.load()
    y = 0
    while y < img.size[1]:
        x = 0
        while x < img.size[0]:
            a[0] += pix[x, y][0]
            a[1] += pix[x, y][1]
            a[2] += pix[x, y][2]
            x += 1
        y += 1
    
    d = 1.0 / (img.size[0] * img.size[1])
    return ((int)(a[0] * d), (int)(a[1] * d), (int)(a[2] * d))

ranges = []
firstSubImg = inImg.crop((0, 0, rdSize[0], rdSize[0]))
ranges.append({'x' : 0, 'y' : 0, 'b' : firstSubImg})

treshold = 10

y = 0
while y + rdSize[0] < inImg.size[1]:
    x = 0
    while x + rdSize[0] < inImg.size[0]:
        area = (x, y, x + rdSize[0], y + rdSize[0])
        subImg = inImg.crop(area)
        #diff = diffImg(ranges[len(ranges) - 1]['b'], subImg)
        diff = 0
        ifFound = False
        for rang in ranges:
            diff = diffImg2(rang['b'], subImg)
            if diff < treshold:
                #draw.ellipse((rang['x'], rang['y'], rang['x'] + 20, rang['y'] + 20))
                #draw.line((x + 10, y + 10, rang['x'] + 10, rang['y'] + 10))
                #draw.bitmap((0,0),rang['b'])
                img.paste(rang['b'],area)
                #draw.rectangle(area, outline=(150,150,0))
                #draw.text((x, y), '%d' % rang['x'])
                #draw.text((x, y + 10), '%d' % rang['y'])
                ifFound = True
                break
        
        if not ifFound:
            #draw.ellipse((x + 5, y + 5, x + 15, y + 15), averageColor(subImg))
            #draw.rectangle(area, outline=(150,150,0))
            #draw.text((x, y), '%d' % diff)
            ranges.append({'x' : x, 'y' : y, 'b' : subImg})
            
        x += rdSize[0]
    y += rdSize[0]
print len(ranges)
 
img.save("out.bmp")
inImg.save("deb.bmp")


