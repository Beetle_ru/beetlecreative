import Image
import random
 
img = Image.new( 'RGB', (255,255), "black") # create a new black image
pixels = img.load() # create the pixel map
 
for y in range(img.size[1]):    # for every pixel:
	liner = random.randint(0,255)
	for x in range(img.size[0]):
		pixels[x,y] = (liner, liner, liner) # set the colour accordingly

img.show()
img.save("b.bmp")
