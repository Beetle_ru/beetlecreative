#!/bin/bash
clear
rm menu/*
path='/usr/share/menu/'
files=`ls $path`
echo $files
for file in $files
do
	#echo "------------$file"
	item=`cat $path$file`
	#item=`cat $file`
	if [[ $item == "?"* ]];
	then
		title=${item##*'title="'}
		title=${title%%'"'*}
		command=${item##*'command="'}
		command=${command%%'"'*}
		section=${item##*'section="'}
		section=${section%%'"'*}
		section=`echo $section | sed "s/.\//_/g"`
		echo "$file:$command" >> menu/$section
		#echo "$section:9menu -file $section" >> menu/index
	else
		echo "error file format"
	fi
done

files=`ls menu/`
for file in $files
do
	echo "$file:9menu -file $file" >> menu/index
done


exit 0
