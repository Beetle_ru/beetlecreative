#-------------------------------------------------
#
# Project created by QtCreator 2012-04-18T19:41:56
#
#-------------------------------------------------

QT       += core gui

TARGET = qt_design_test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
