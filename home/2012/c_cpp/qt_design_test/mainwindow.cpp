#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    lm = new QStringListModel();
    lm2 = new QStringListModel();
    ui->listView->setModel(lm2);
    ui->listView_3->setModel(lm2);
    ui->listView_2->setModel(lm);
    textList.append(QString::fromUtf8("Имя профиля"));
    textList.append(QString::fromUtf8("Описание профиля"));
    textList.append(QString::fromUtf8("Цель использования профиля"));
    textList.append(QString::fromUtf8("Преимущества данного профиля"));
    textList.append(QString::fromUtf8("Недостатки данного профиля"));

    textList2.append(QString::fromUtf8("Indicator 1"));
    textList2.append(QString::fromUtf8("Indicator 2"));
    textList2.append(QString::fromUtf8("Indicator 3"));
    textList2.append(QString::fromUtf8("Indicator 4"));
    textList2.append(QString::fromUtf8("Indicator 5"));
    textList2.append(QString::fromUtf8("Indicator 6"));
    textList2.append(QString::fromUtf8("Indicator 7"));
    textList2.append(QString::fromUtf8("Indicator 8"));
    textList2.append(QString::fromUtf8("Indicator 9"));
    lm->setStringList(textList);
    lm2->setStringList(textList2);

    ui->pushButton->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}
