#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //QwtPlot *myPlot = new QwtPlot(this);
    myPlot = new QwtPlot(this);
    // Создается первая линия
   // QwtPlotCurve *curve1 = new QwtPlotCurve("H ot T");
    curve1 = new QwtPlotCurve("H ot T");
    curve1->setRenderHint(QwtPlotItem::RenderAntialiased); // Устанавливается параметр сглаживания для этой кривой

    // Создается вторая линия
//    QwtPlotCurve *curve2 = new QwtPlotCurve("Curve 2");
    curve2 = new QwtPlotCurve("Curve 2");

    // В области рисования обозначаются оси координат
    myPlot->setAxisTitle(QwtPlot::xBottom, "data 1/seconds"); // Подпись горизонтальной оси
    myPlot->setAxisScale(QwtPlot::xBottom, 0, 1000); // Масштаб горизонтальной оси
    myPlot->setAxisTitle(QwtPlot::yLeft, "data 2/seconds"); // Подпись вертикальной оси

    // Устанавливается цвет для второй линии
    curve2->setPen(QPen(Qt::blue));

    // Задается геометрия линий, согласно заранее подготовленным данным
    double *X0;
    double *X1;
    double *X2;
    int Size = 1;
    //curve1->setData(X0,X1,Size);
    //curve2->setData(X0,X2,Size);
  //  curve1->setData(0.0,0.0,1.1);
 //   curve2->setData(1,1,1);

    // Линии размещаются на области рисования
    curve1->attach(myPlot);
    curve2->attach(myPlot);

    // И наконец, обновляется область рисования
    myPlot->resize(200, 200); // Устанавливается размер области рисования

    myPlot->replot(); // Область рисования перерисовывается
}

MainWindow::~MainWindow()
{
    delete ui;
}
