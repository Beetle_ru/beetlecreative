#-------------------------------------------------
#
# Project created by QtCreator 2012-04-11T12:44:00
#
#-------------------------------------------------

QT       += core gui

TARGET = qt_testqwt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

#unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../usr/local/qwt-6.0.2-svn/lib/ -lqwt

#INCLUDEPATH += $$PWD/../../../../../../usr/local/qwt-6.0.2-svn/include
#DEPENDPATH += $$PWD/../../../../../../usr/local/qwt-6.0.2-svn/include

unix:!macx:!symbian: LIBS += -L/usr/local/qwt-6.0.2-svn/lib/ -lqwt

INCLUDEPATH += /usr/local/qwt-6.0.2-svn/include
DEPENDPATH += /usr/local/qwt-6.0.2-svn/include
