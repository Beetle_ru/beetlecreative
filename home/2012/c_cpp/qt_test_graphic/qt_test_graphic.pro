#-------------------------------------------------
#
# Project created by QtCreator 2012-04-11T11:56:44
#
#-------------------------------------------------

QT       += core gui

TARGET = qt_test_graphic
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
