#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //manager = new QNetworkAccessManager(this);
}

MainWindow::~MainWindow()
{
    delete ui;
    //delete manager;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButton_clicked()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    //QString url = "http://api.twitter.com/1/users/show.xml?screen_name=";
    //QString url = "http://api.twitter.com/1/users/show.json?screen_name=";
    QString url = "http://api.twitter.com/1/statuses/user_timeline.xml?count=200&screen_name=";
    //QString url = "http://api.twitter.com/1/statuses/user_timeline.json?count=200&screen_name=";
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
    url.append(ui->leUserName->text());

    manager->get(QNetworkRequest(QUrl(url)));


}

void MainWindow::replyFinished(QNetworkReply *reply) {

    ui->teStream->setText( QString::fromUtf8(reply->readAll()));
    //reply->deleteLater();
}
