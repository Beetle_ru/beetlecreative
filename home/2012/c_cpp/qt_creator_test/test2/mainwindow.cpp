#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButton_clicked()
{

    QStringList list;
    QStringListModel *listModel  = new QStringListModel();
    QAbstractTableModel *listTableModel = new QAbstractTableModel();
    list << "one" << "two" << "three";
    listModel->setStringList(list);
    listTableModel->insertColumn(1,listModel);
    ui->listView->setModel(listModel);
}
