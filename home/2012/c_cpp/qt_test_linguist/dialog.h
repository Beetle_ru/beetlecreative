#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTranslator>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0, QTranslator * qt_translator = 0);
    ~Dialog();
    
private slots:
    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::Dialog *ui;
    QTranslator * qt_trans;
};

#endif // DIALOG_H
