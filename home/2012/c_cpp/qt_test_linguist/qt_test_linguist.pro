#-------------------------------------------------
#
# Project created by QtCreator 2012-04-21T10:18:00
#
#-------------------------------------------------

QT       += core gui

TARGET = qt_test_linguist
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui

TRANSLATIONS += ru.ts
TRANSLATIONS += en.ts
