#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
//#include "trans.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void changeEvent(QEvent* event);

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTranslator  *qt_translator;
};

#endif // MAINWINDOW_H
