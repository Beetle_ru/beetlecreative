<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>главное окно</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>PushButton</source>
        <translatorcomment>вот так</translatorcomment>
        <translation>тыкай кнопку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="41"/>
        <source>en</source>
        <translation>англ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>ru</source>
        <translation>рус</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="21"/>
        <source>Enter the text</source>
        <translatorcomment>см http://borisnote.wordpress.com/2012/04/07/rus-qt-translete/</translatorcomment>
        <translation>введите текст</translation>
    </message>
</context>
</TS>
