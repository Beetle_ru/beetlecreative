#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"
//#include "trans.h"

Dialog::Dialog(QWidget *parent, QTranslator * qt_translator) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    qt_trans = qt_translator;
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_comboBox_currentIndexChanged(int index)
{
    QString file_translate;// = QApplication::applicationDirPath () + "/en.qm";

    if(index == 2) {
        file_translate = QApplication::applicationDirPath () + "/ru.qm";
    }
    if(index == 1) {
        file_translate = QApplication::applicationDirPath () + "/en.qm";
    }
    qt_trans->load(file_translate);
    this->close();
}
