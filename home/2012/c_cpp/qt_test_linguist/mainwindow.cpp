#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QtDebug>
#include "dialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qt_translator = new QTranslator;
    qApp->installTranslator( qt_translator );

    ui->setupUi(this);
    //QMessageBox mbox;
    //mbox.setText(tr("Enter the text"));  // <== ТЕКСТ ДЛЯ ПЕРЕВОДА
    //mbox.exec();
    //Dialog *dialog = new Dialog(0,qt_translator);
    //dialog->show();

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange) {
        ui->retranslateUi(this);
        qDebug() << "Language Change ";
    }
}

void MainWindow::on_pushButton_clicked()
{
    Dialog *dialog = new Dialog(0,qt_translator);
    dialog->show();
}
