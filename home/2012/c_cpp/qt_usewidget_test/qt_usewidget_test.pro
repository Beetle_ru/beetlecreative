#-------------------------------------------------
#
# Project created by QtCreator 2012-04-23T20:59:22
#
#-------------------------------------------------

QT       += core gui

TARGET = qt_usewidget_test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

unix:!macx:!symbian: LIBS += -L$$PWD/../qt_widget_test/ -ltestplugin

INCLUDEPATH += $$PWD/../qt_widget_test
DEPENDPATH += $$PWD/../qt_widget_test
