#-------------------------------------------------
#
# Project created by QtCreator 2012-04-28T10:53:09
#
#-------------------------------------------------

QT       += core gui

TARGET = qwt_test_useqwt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    canvaspicker.cpp

HEADERS  += mainwindow.h \
    canvaspicker.h

FORMS    += mainwindow.ui


unix:!macx:!symbian: LIBS += -L$$PWD/qwt-6.0/lib/ -lqwt

INCLUDEPATH += $$PWD/qwt-6.0/src
DEPENDPATH += $$PWD/qwt-6.0/src
