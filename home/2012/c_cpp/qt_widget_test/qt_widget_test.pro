CONFIG      += designer plugin debug_and_release
TARGET      = $$qtLibraryTarget(testplugin)
TEMPLATE    = lib

HEADERS     = testplugin.h \
    form.h
SOURCES     = testplugin.cpp \
    form.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(test.pri)

FORMS += \
    form.ui
