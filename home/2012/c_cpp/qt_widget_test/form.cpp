#include "form.h"
#include "ui_form.h"

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
}

Form::~Form()
{
    delete ui;
}

void Form::on_pushButton_clicked()
{
    ui->lineEdit->setText("left button");
}

void Form::on_pushButton_2_clicked()
{
    ui->lineEdit->setText("right button");
}
