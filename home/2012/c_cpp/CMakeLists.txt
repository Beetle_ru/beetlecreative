cmake_minimum_required (VERSION 2.6)
 
set (PROJECT ИМЯ_ПРОЕКТА)
  
project (${PROJECT})
   
include_directories (../)
    
set (LIBRARIES
     ИМЯ_БИБЛИОТЕКИ_1
     ИМЯ_БИБЛИОТЕКИ_2)
     
foreach (LIBRARY ${LIBRARIES})
add_subdirectory (../${LIBRARY}/build bin/${LIBRARY})
endforeach ()
       
set (HEADERS
     ../ЗАГОЛОВОЧНЫЕ_ФАЙЛЫ)
set (SOURCES 
     ../ФАЙЛЫ_С_РЕАЛИЗАЦИЕЙ)
        
add_executable (${PROJECT} ${HEADERS} ${SOURCES})
#add_library (${PROJECT} SHARED ${HEADERS} ${SOURCES}) 
target_link_libraries (${PROJECT} ${LIBRARIES})
