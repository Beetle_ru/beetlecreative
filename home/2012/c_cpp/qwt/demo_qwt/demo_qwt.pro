#-------------------------------------------------
#
# Project created by QtCreator 2010-08-17T09:04:07
#
#-------------------------------------------------

QT  += core gui


TARGET  = demo_qwt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS += mainwindow.h

FORMS   += mainwindow.ui


#INCLUDEPATH += C:/Qt/qwt-5.2.1/include
#LIBS    += C:/Qt/qwt-5.2.1/lib/libqwt5.a

INCLUDEPATH += /usr/local/qwt-6.0.2-svn/include
DEPENDPATH += /usr/local/qwt-6.0.2-svn/include
