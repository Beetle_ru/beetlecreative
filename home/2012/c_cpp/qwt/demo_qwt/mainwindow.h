#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <qwt_plot_canvas.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    QwtLegend *leg;             // �������
    QwtPlotGrid *grid;          // ������������ �����
    QwtPlotCurve *curv1,*curv2; // ������, ������������ �� �������

#if QWT_VERSION >= 0x060000
    QwtSymbol *symbol1;         // ������� ��� ����� ������ ������
#endif

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
