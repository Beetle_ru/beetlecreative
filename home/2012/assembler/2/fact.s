#Листинг 2
.globl main
main:
     movl $5, %eax
     movl $1, %ebx
L1:  cmpl $0, %eax          # // сравнить содержимое регистра eax с 0
     je L2                  # // переход на L2 если 0==eax (je - jump if equa l, перейти если равно)
     imull %eax, %ebx       # // ebx = ebx*eax
     decl %eax              # // decrement eax (уменьшить на 1)
     jmp L1                 # // безусловный переход на L1
L2:  ret
