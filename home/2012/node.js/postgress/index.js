var http = require('http');
var pg = require('pg');

//var conString = "postgres://YOURUSER:YOURPASSWORD@localhost/databaseName";
//var connectionString = "pg://phone:book@server:5432/phone";
var connectionString = "pg://panax:p1panax@localhost:5432/panaxbd";
pg.connect(connectionString, function(err, client) {
  if(err) {
	console.log("errrrr");
    console.log(err);
  }
  else {
    http.createServer(function(request, response){
      response.writeHead(200, {'Content-Type': 'text/html'});
      response.write('<h1>Phone Book</h1>');

      //client.query('SELECT name, number FROM phonebook', function(err, result) {
		client.query('SELECT amname, amsupport FROM pg_am', function(err, result) {
        if(err) {
         console.log(err);
        }
        else {
          response.write('<table>');
          for (var i=0; i<result.rows.length; i++){
            response.write('<tr><td>' + result.rows[i].amname + '</td><td>' + result.rows[i].amsupport + '</td></tr>');
          }
          response.write('</table>');
          response.end();
        }
      });
    }).listen(8080, "localhost");
    console.log('Server running at http://127.0.0.1:8080/');
  }
});
