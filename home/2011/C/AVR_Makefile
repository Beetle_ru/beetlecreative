# Makefile
# //////////////////
#   ИСПОЛЬЗОВАНИЕ
# //////////////////
# Наберите в консоли
#
# make all = собрать (можно просто make)
#
# make clean = Очистить проект (удаляет все, кроме исходиников)
#
# make coff = Конвертировать ELF в AVR COFF
#
# make extcoff = Конвертировать ELF в AVR Extended COFF
#
# make program = Загрузить hex файл в память микроконтроллера при помощи программатора
#                Желательно сначала настроить параметры программатора
#
# make program-read = Прочесть прошивку из памяти микроконтроллера в hex-файл.
#
# make debug = Запустить simulavr или avarice для отладки
#              в качестве фронтэнда используется avr-gdb или avr-insight
#
# make filename.s = Откомпилировать filename.c в ассемблерный код
#
# make filename.i = Создать препроцессированный исходник для отравки
#                   багрепортов в разработчикам GCC
#
# Для полной пересборки проекта используйте "make clean", затем "make all".
#----------------------------------------------------------------------------


# Имя микроконтроллера
MCU = atmega16


# Частота работы микроконтроллера
#     Если объявлено, это заменит все вхождения F_CPU в исходном коде на указанную ниже частоту
#     Не добавляйте в конце симовлы 'UL' для того, чтобы получить 32-х битное число, это
#     делается автоматически.
# Примеры:
#         F_CPU =  1000000
#         F_CPU =  1843200
#         F_CPU =  2000000
#         F_CPU =  3686400
#         F_CPU =  4000000
#         F_CPU =  7372800
#         F_CPU =  8000000
#         F_CPU = 11059200
#         F_CPU = 14745600
#         F_CPU = 16000000
#         F_CPU = 18432000
#         F_CPU = 20000000
F_CPU = 4000000


# Выходной формат. Может быть srec, ihex, binary
FORMAT = ihex


# Имя файла, с которого начинается выполнение программы (без расширения)
TARGET = main


# Папка с объектными файлами
#     В указанную папку будут сложены объектные файлы. Обычно используется текущий каталог (.)
#     Эта переменная НЕ должна быть пустой
OBJDIR = .


# Перечислите здесь файлы с исходным кодом на Си. Зависимости разрешаются автоматически
SRC = $(TARGET).c


# Перечислите здесь файлы с исходным кодом на C++. Зависимости разрешаются автоматически
CPPSRC = 


# Перечислите здесь исходные файла на ассемблере.
#     Их имена ВСЕГДА должны заканчиваться на .S (заглавная буква!). Если файл заканчивается
#     на .s, он будет считаться не исходником, а файлом с выводом ассемблерного компилятора,
#     в результате чего будет удален по команде "make clean"!
ASRC =


# Уровень оптимизации, может быть [0, 1, 2, 3, s].
#     0 = выключить оптимизацию. s = оптимизировать до минимального размера
#     (Примечание: 3 не всегда означает самый высокий уровень оптимизации. Смотри avr-libc FAQ.)
OPT = 0


# Формат отладки.
#     Родные форматы для AVR-GCC это dwarf-2 [по умолчанию] или stabs.
#     AVR Studio 4.10 нужен dwarf-2.
#     AVR [Extended] COFF формат требует stabs и запуск avr-objcopy.
DEBUG = dwarf-2


# Перечислите дополнительные папки для поиска заголовочных файлов.
#     Папки разделяются пробелами
#     Если имя папки содержит пробелы, используйте кавычки
EXTRAINCDIRS = 


# Флаг для компилятора, чтобы выставить стандартный вид языка Си
#     c89   = "ANSI" C
#     gnu89 = c89 плюс расширения GCC
#     c99   = ISO C99 standard (до конца до сих пор не реализован)
#     gnu99 = c99 плюс расширения GCC
CSTANDARD = -std=gnu99


# Добавляет UL к макросу F_CPU для C
CDEFS = -DF_CPU=$(F_CPU)UL


# Добавляет UL к макросу F_CPU для ASM
ADEFS = -DF_CPU=$(F_CPU)


# Добавляет UL к макросу F_CPU для C++
CPPDEFS = -DF_CPU=$(F_CPU)UL
#CPPDEFS += -D__STDC_LIMIT_MACROS
#CPPDEFS += -D__STDC_CONSTANT_MACROS


# Имя hex-файла, содержащего считанные из микроконтроллера данные:
READ_NAME = read_from_mcu.hex



#---------------- Опции компилятора C ----------------
#  -g*:          создать отладочную информацию
#  -O*:          уровень оптимизации
#  -f...:        tuning, смотри GCC manual и документацию avr-libc
#  -Wall...:     уровень предупреждений (warning)
#  -Wa,...:      объяснить GCC, что нужно передать это ассемблеру
#    -adhlns...: сделать листинг кода ассемблера
CFLAGS = -g$(DEBUG)
CFLAGS += $(CDEFS)
CFLAGS += -O$(OPT)
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -fshort-enums
CFLAGS += -Wall
CFLAGS += -Wstrict-prototypes
#CFLAGS += -mshort-calls
#CFLAGS += -fno-unit-at-a-time
#CFLAGS += -Wundef
#CFLAGS += -Wunreachable-code
#CFLAGS += -Wsign-compare
CFLAGS += -Wa,-adhlns=$(<:%.c=$(OBJDIR)/%.lst)
CFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))
CFLAGS += $(CSTANDARD)


#---------------- Опции компилятора C++ ----------------
#  -g*:          создать отладочную информацию
#  -O*:          уровень оптимизации
#  -f...:        tuning, смотри GCC manual и документацию avr-libc
#  -Wall...:     уровень предупреждений (warning)
#  -Wa,...:      объяснить GCC, что нужно передать это ассемблеру
#    -adhlns...: сделать листинг кода ассемблера
CPPFLAGS = -g$(DEBUG)
CPPFLAGS += $(CPPDEFS)
CPPFLAGS += -O$(OPT)
CPPFLAGS += -funsigned-char
CPPFLAGS += -funsigned-bitfields
CPPFLAGS += -fpack-struct
CPPFLAGS += -fshort-enums
CPPFLAGS += -fno-exceptions
CPPFLAGS += -Wall
CFLAGS += -Wundef
#CPPFLAGS += -mshort-calls
#CPPFLAGS += -fno-unit-at-a-time
#CPPFLAGS += -Wstrict-prototypes
#CPPFLAGS += -Wunreachable-code
#CPPFLAGS += -Wsign-compare
CPPFLAGS += -Wa,-adhlns=$(<:%.cpp=$(OBJDIR)/%.lst)
CPPFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))
#CPPFLAGS += $(CSTANDARD)


#---------------- Опции Assembler ----------------
#  -Wa,...:   объяснить GCC, что нужно передать это ассемблеру
#  -adhlns:   сделать листинг
#  -gstabs:   нужно ли ассемблеру создавать информацию о нумерации строк; помните, что это
#             требуется для использования в файлах COFF, дополнительная информация об именах файлов
#             и функций также должна присутствовать в исходном коде ассемблера.
#             -- см. документацию avr-libc
#  -listing-cont-lines: Sets the maximum number of continuation lines of hex
#       dump that will be displayed for a given single line of source input.
ASFLAGS = $(ADEFS) -Wa,-adhlns=$(<:%.S=$(OBJDIR)/%.lst),-gstabs,--listing-cont-lines=100


#---------------- Опции библиотек ----------------
# Minimalistic printf version
PRINTF_LIB_MIN = -Wl,-u,vfprintf -lprintf_min

# Floating point printf version (requires MATH_LIB = -lm below)
PRINTF_LIB_FLOAT = -Wl,-u,vfprintf -lprintf_flt

# If this is left blank, then it will use the Standard printf version.
PRINTF_LIB = 
#PRINTF_LIB = $(PRINTF_LIB_MIN)
#PRINTF_LIB = $(PRINTF_LIB_FLOAT)


# Minimalistic scanf version
SCANF_LIB_MIN = -Wl,-u,vfscanf -lscanf_min

# Floating point + %[ scanf version (requires MATH_LIB = -lm below)
SCANF_LIB_FLOAT = -Wl,-u,vfscanf -lscanf_flt

# If this is left blank, then it will use the Standard scanf version.
SCANF_LIB = 
#SCANF_LIB = $(SCANF_LIB_MIN)
#SCANF_LIB = $(SCANF_LIB_FLOAT)


# прилинковать библиотеку математики
MATH_LIB = -lm


# Перечислите здесь все дополнительные папки с библиотеками
#     Папки разделяются пробелами
#     Если имя папки содержит пробелы, используйте кавычки
EXTRALIBDIRS = 



#---------------- Опции внешней памяти ----------------

# 64 KB внешней ОЗУ (RAM), начинающейся после внутренней RAM (ATmega128!),
# используеются для переменных (.data/.bss) и динамической памяти (malloc()).
#EXTMEMOPTS = -Wl,-Tdata=0x801100,--defsym=__heap_end=0x80ffff

# 64 KB внешней ОЗУ (RAM), начинающейся после внутренней RAM (ATmega128!),
# Используется только для динамической памяти (malloc())
#EXTMEMOPTS = -Wl,--section-start,.data=0x801100,--defsym=__heap_end=0x80ffff

EXTMEMOPTS =



#---------------- Опции Сборщика (Linker Options) ----------------
#  -Wl,...:     Объяснить GCC, что это нужно передать линкеру
#    -Map:      создать map file
#    --cref:    add cross reference to  map file
LDFLAGS = -Wl,-Map=$(TARGET).map,--cref
LDFLAGS += $(EXTMEMOPTS)
LDFLAGS += $(patsubst %,-L%,$(EXTRALIBDIRS))
LDFLAGS += $(PRINTF_LIB) $(SCANF_LIB) $(MATH_LIB)
#LDFLAGS += -T linker_script.x



#---------------- Опции программатора ----------------
#
# Опции AVReAl (он используется в данном Makefile):
#      +mega8:      Прошиваем AtMega8
#      -o1000000Hz: Использовать частоту 1МГц
#      -p1:         Использовать LPT порт №1
#      -e:          Стереть содержимое кристалла
#      -as:         STK200/300 адаптер
#      -az:         адаптер "5 проводков"
#      -ab:         адаптер ByteBlaster
#      -%:          показывать дополнительную информацию
#      -n:          использовать счетчик стираний
#      -r:          прочесть из микроконтроллера в hex-файл

PROGRAMMER_FLAGS = +$(MCU) -o$(F_CPU)Hz -p1 -e -w -as -% $(TARGET).hex -n
PROGRAMMER_FLAGS_READ = +$(MCU) -o$(F_CPU)Hz -p1 -r -as -% $(READ_NAME)

#---------------- Опции отладки ----------------

# Только для simulavr - частота MCU
DEBUG_MFREQ = $(F_CPU)

# Укажите, какой фронтэнд использовать. Может быть gdb или insight.
# DEBUG_UI = gdb
DEBUG_UI = insight

# Какой back-end использовать для отладки. Может avarice или simulavr.
DEBUG_BACKEND = avarice
#DEBUG_BACKEND = simulavr

# GDB Init Filename.
GDBINIT_FILE = __avr_gdbinit

# Настройки JTAG при использовании avarice
JTAG_DEV = /dev/com1

# Порт отладки, используемый при отладки для взаимодействия между GDB / avarice / simulavr.
DEBUG_PORT = 4242

# Хост, используемый для взаимодейсвия между GDB / avarice / simulavr
#     Если запущено на локальном компьютере, просто пропишите localhost
DEBUG_HOST = localhost



#============================================================================


# Объявления программ и команд
SHELL = sh
CC = avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
SIZE = avr-size
AR = avr-ar rcs
NM = avr-nm
PROGRAMMER = avreal
REMOVE = rm -f
REMOVEDIR = rm -rf
COPY = cp
WINSHELL = cmd


# Пропишите здесь сообщений
# Русские сообщения:
MSG_ERRORS_NONE = Ошибки: нет ошибок
MSG_BEGIN = -------- НАЧАЛО --------
MSG_END = -------- КОНЕЦ  --------
MSG_SIZE_BEFORE = Размер в начале:
MSG_SIZE_AFTER = Размер в конце:
MSG_COFF = Конвертация в AVR COFF:
MSG_EXTENDED_COFF = Конвертация в AVR Extended COFF:
MSG_FLASH = Создание файла загрузки для Flash:
MSG_EEPROM = Создание файла загрузки для EEPROM:
MSG_EXTENDED_LISTING = Создание расширенного листинга:
MSG_SYMBOL_TABLE = Создание Symbol Table:
MSG_LINKING = Линковка:
MSG_COMPILING = Компиляция C:
MSG_COMPILING_CPP = Компиляция C++:
MSG_ASSEMBLING = Assembling:
MSG_CLEANING = Очистка проекта:
MSG_CREATING_LIBRARY = Создание библиотеки:



# Объявить все объектные файлы. Сопоставляет объектники исходникам
OBJ = $(SRC:%.c=$(OBJDIR)/%.o) $(CPPSRC:%.cpp=$(OBJDIR)/%.o) $(ASRC:%.S=$(OBJDIR)/%.o) 

# Объявляет файлы листингов
LST = $(SRC:%.c=$(OBJDIR)/%.lst) $(CPPSRC:%.cpp=$(OBJDIR)/%.lst) $(ASRC:%.S=$(OBJDIR)/%.lst) 


# Флаги компилятора для генерации файлов зависимостей
GENDEPFLAGS = -MMD -MP -MF .dep/$(@F).d


# Объединить все необходимые флаги и необязательные
# Добавить указание модели микроконтроллера к флагам
ALL_CFLAGS = -mmcu=$(MCU) -I. $(CFLAGS) $(GENDEPFLAGS)
ALL_CPPFLAGS = -mmcu=$(MCU) -I. -x c++ $(CPPFLAGS) $(GENDEPFLAGS)
ALL_ASFLAGS = -mmcu=$(MCU) -I. -x assembler-with-cpp $(ASFLAGS)





# Цель по умолчанию
all: begin gccversion sizebefore build sizeafter end

# Выберите цель: создание hex файла (первая строка) или библиотеки (вторая строка)
build: elf hex eep lss sym
#build: lib

# Имена файлов
elf: $(TARGET).elf
hex: $(TARGET).hex
eep: $(TARGET).eep
lss: $(TARGET).lss
sym: $(TARGET).sym
LIBNAME=lib$(TARGET).a
lib: $(LIBNAME)


# Пишет сообщения в начале и конце сборки
begin:
	@echo
	@echo $(MSG_BEGIN)

end:
	@echo $(MSG_END)
	@echo


# Показывает размеры файла
HEXSIZE = $(SIZE) --target=$(FORMAT) $(TARGET).hex
ELFSIZE = $(SIZE) -B $(TARGET).elf

sizebefore:
	@if test -f $(TARGET).elf; then echo; echo $(MSG_SIZE_BEFORE); $(ELFSIZE); \
	2>/dev/null; echo; fi

sizeafter:
	@if test -f $(TARGET).elf; then echo; echo $(MSG_SIZE_AFTER); $(ELFSIZE); \
	2>/dev/null; echo; fi

# Информация о версии компилятора
gccversion :
	@$(CC) --version


# Прошить программу в память микроконтроллера
program: $(TARGET).hex $(TARGET).eep
	$(PROGRAMMER) $(PROGRAMMER_FLAGS)


# Прочитать программу из памяти микроконтроллера в файл $(READ_NAME)
program-read:
	$(PROGRAMMER) $(PROGRAMMER_FLAGS_READ)


# Сгенерировать avr-gdb config/init файл, который делает следующее:
#     определяет сигнал сброса, загружает нужный файл для отладки, подключается к устройству
#     и ставит breakpoint на функции main()
gdb-config: 
	@$(REMOVE) $(GDBINIT_FILE)
	@echo define reset >> $(GDBINIT_FILE)
	@echo SIGNAL SIGHUP >> $(GDBINIT_FILE)
	@echo end >> $(GDBINIT_FILE)
	@echo file $(TARGET).elf >> $(GDBINIT_FILE)
	@echo target remote $(DEBUG_HOST):$(DEBUG_PORT)  >> $(GDBINIT_FILE)
ifeq ($(DEBUG_BACKEND),simulavr)
	@echo load  >> $(GDBINIT_FILE)
endif
	@echo break main >> $(GDBINIT_FILE)

debug: gdb-config $(TARGET).elf
ifeq ($(DEBUG_BACKEND), avarice)
	@echo Starting AVaRICE - Press enter when "waiting to connect" message displays.
	@avarice --jtag $(JTAG_DEV) --erase --program --file $(TARGET).elf $(DEBUG_HOST):$(DEBUG_PORT)

else
	@simulavr --gdbserver --device $(MCU) --clock-freq $(DEBUG_MFREQ) --port $(DEBUG_PORT)
endif
	@avr-$(DEBUG_UI) --command=$(GDBINIT_FILE)




# Convert ELF to COFF for use in debugging / simulating in AVR Studio or VMLAB.
COFFCONVERT = $(OBJCOPY) --debugging
COFFCONVERT += --change-section-address .data-0x800000
COFFCONVERT += --change-section-address .bss-0x800000
COFFCONVERT += --change-section-address .noinit-0x800000
COFFCONVERT += --change-section-address .eeprom-0x810000



coff: $(TARGET).elf
	@echo
	@echo $(MSG_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-avr $< $(TARGET).cof


extcoff: $(TARGET).elf
	@echo
	@echo $(MSG_EXTENDED_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-ext-avr $< $(TARGET).cof



# Создает финальные выходные файлы (.hex, .eep) из файлов ELF
%.hex: %.elf
	@echo
	@echo $(MSG_FLASH) $@
	$(OBJCOPY) -O $(FORMAT) -R .eeprom $< $@

%.eep: %.elf
	@echo
	@echo $(MSG_EEPROM) $@
	-$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" \
	--change-section-lma .eeprom=0 --no-change-warnings -O $(FORMAT) $< $@ || exit 0

# Создать расширенный файл листинга из ELF
%.lss: %.elf
	@echo
	@echo $(MSG_EXTENDED_LISTING) $@
	$(OBJDUMP) -h -S $< > $@

# Создать symbol table из ELF
%.sym: %.elf
	@echo
	@echo $(MSG_SYMBOL_TABLE) $@
	$(NM) -n $< > $@



# Создать библиотеку из объектных файлов
.SECONDARY : $(TARGET).a
.PRECIOUS : $(OBJ)
%.a: $(OBJ)
	@echo
	@echo $(MSG_CREATING_LIBRARY) $@
	$(AR) $@ $(OBJ)


# Линковка: создать ELF файл из объектных файлов
.SECONDARY : $(TARGET).elf
.PRECIOUS : $(OBJ)
%.elf: $(OBJ)
	@echo
	@echo $(MSG_LINKING) $@
	$(CC) $(ALL_CFLAGS) $^ --output $@ $(LDFLAGS)


# Компиляция: создать объектные файлы из исходников Си
$(OBJDIR)/%.o : %.c
	@echo
	@echo $(MSG_COMPILING) $<
	$(CC) -c $(ALL_CFLAGS) $< -o $@ 


# Компиляция: создать объектные файлы из исходников С++
$(OBJDIR)/%.o : %.cpp
	@echo
	@echo $(MSG_COMPILING_CPP) $<
	$(CC) -c $(ALL_CPPFLAGS) $< -o $@ 


# Компиляция: создать ассемблерные файлы из исходного кода Си
%.s : %.c
	$(CC) -S $(ALL_CFLAGS) $< -o $@


# Компиляция: создать ассемблерные файлы из исходного кода С++
%.s : %.cpp
	$(CC) -S $(ALL_CPPFLAGS) $< -o $@


# Ассемблирование: создать объектные файлы из ассемблерного исходного кода
$(OBJDIR)/%.o : %.S
	@echo
	@echo $(MSG_ASSEMBLING) $<
	$(CC) -c $(ALL_ASFLAGS) $< -o $@


# Создать препроцессированный исходник для отправления багрепорта
%.i : %.c
	$(CC) -E -mmcu=$(MCU) -I. $(CFLAGS) $< -o $@ 


# Очистка проекта
clean: begin clean_list end

clean_list :
	@echo
	@echo $(MSG_CLEANING)
	$(REMOVE) $(TARGET).c~
	$(REMOVE) $(TARGET).hex
	$(REMOVE) $(TARGET).eep
	$(REMOVE) $(TARGET).cof
	$(REMOVE) $(TARGET).elf
	$(REMOVE) $(TARGET).map
	$(REMOVE) $(TARGET).sym
	$(REMOVE) $(TARGET).lss
	$(REMOVE) $(SRC:%.c=$(OBJDIR)/%.o)
	$(REMOVE) $(SRC:%.c=$(OBJDIR)/%.lst)
	$(REMOVE) $(SRC:.c=.s)
	$(REMOVE) $(SRC:.c=.d)
	$(REMOVE) $(SRC:.c=.i)
	$(REMOVEDIR) .dep


# Создает папку для объектных файлов
$(shell mkdir $(OBJDIR) 2>/dev/null)


# Включает файлы зависимостей
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)


# Listing of phony targets.
.PHONY : all begin finish end sizebefore sizeafter gccversion \
build elf hex eep lss sym coff extcoff \
clean clean_list program debug gdb-config



















