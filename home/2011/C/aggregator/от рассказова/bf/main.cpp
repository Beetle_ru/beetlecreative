#include "soapH.h"
#include "soapBFGlobalServiceProxy.h"
#include "BFGlobalService.nsmap"
#include <iostream>

using namespace std;


/*
class ns2__LoginReq
{ public:
    // Element ipAddress of type xs:string.
    std::string                          ipAddress                      1;      ///< Required element.
    // Element locationId of type xs:int.
    int                                  locationId                     1;      ///< Required element.
    // Element password of type xs:string.
    std::string                          password                       1;      ///< Required element.
    // Element productId of type xs:int.
    int                                  productId                      1;      ///< Required element.
    // Element username of type xs:string.
    std::string                          username                       1;      ///< Required element.
    // Element vendorSoftwareId of type xs:int.
    int                                  vendorSoftwareId               1;      ///< Required element.
    // A handle to the soap struct that manages this instance (automatically set)
    struct soap                         *soap                          ;
};

class _ns1__login
{ public:
   // Element request of type "http://www.betfair.com/publicapi/types/global/v3/":LoginReq.
   ns2__LoginReq*                       request                        1;      ///< Required element.
   // A handle to the soap struct that manages this instance (automatically set)
   struct soap                         *soap                          ;
};
//        
*/

int main(int argc, char** argv) {

	BFGlobalServiceProxy svc;
	soap_ssl_init(); 

	if (soap_ssl_client_context(
	   &svc,
	   SOAP_SSL_DEFAULT,
	   NULL, /* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
	   NULL, /* password to read the key file (not used with GNUTLS) */
	   "betfair.pem", /* cacert file to store trusted certificates (needed to verify server) */    
	   NULL, /* capath to directory with trusted certificates */
	   NULL /* if randfile!=NULL: use a file with random data to seed randomness */
	))
	{
	   soap_print_fault(&svc, stderr);
	   exit(1);
	} 

	_ns1__login login;
	_ns1__loginResponse login_res;
	ns2__LoginReq req;

	login.request = &req;

	req.ipAddress = "0";
        req.locationId = 0;
        req.password = "Jzt,eKjiflrb100500";
        req.productId = 82;
        req.username = "tonyrasskazov";
        req.vendorSoftwareId = 0;
	
	int res = svc.login(&login, &login_res);
	
	cout << "login return " << res << endl;
	
	return 0;
} 
