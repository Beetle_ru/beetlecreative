#include <curl/curl.h>
#include<QString>

//объявляем буфер, для хранения возможной ошибки, размер определяется в самой библиотеке
static char errorBuffer[CURL_ERROR_SIZE];
//объялвяем буфер принимаемых данных
static QString buffer;

static int writer(char *data, size_t size, size_t nmemb, QString *buffer) {
    //переменная - результат, по умолчанию нулевая
    int result = 0;
    //проверяем буфер
    if (buffer != NULL) {
        //добавляем к буферу строки из data, в количестве nmemb
        //buffer->append(data, size * nmemb);
        buffer->append(data);
        //вычисляем объем принятых данных
        result = size * nmemb;
    }
    //вовзращаем результат
    return result;
}

int init(CURL *&curl, const char *&url, char *&postthis) {
    //инициализируем curl
    curl = curl_easy_init();
    if (curl) {
        //задаем все необхо	мые опции
        //определяем, куда выводить ошибки
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
        //задаем опцию - получить страницу по адресу http://google.com
        curl_easy_setopt(curl, CURLOPT_URL, "google.com");
        //указываем прокси сервер
        //curl_easy_setopt(curl, CURLOPT_PROXY, url);
        //задаем опцию отображение заголовка страницы
        curl_easy_setopt(curl, CURLOPT_HEADER, 0);
        //указываем функцию обратного вызова для записи получаемых данных
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
        //указываем куда записывать принимаемые данные
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
        //указываем переменную с пост данными
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);
        //длинна пост данных - ВНИМАНИЕ НУЖНО ПРОИЗВОДИТЬ ПЕРЕД КАЖДОЙ ПЕРЕДАЧЕЙ ДАННЫХ
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long) strlen(postthis));
    }
    return 0;
}

int tr(void) {
    //необходимые CURL объекты
    CURL *curl;
    CURLcode result;
    const char *url = "http://google.com";
    const char *requestf = "BFGlobalService.login.req.xml";

    char *postthis;




    printf("%s", postthis);
    //проводим инициализацию
    init(curl, url, postthis);
    //запускаем выполнение задачи
    /*result = curl_easy_perform(curl);
    //проверяем успешность выполнения операции
    if (result == CURLE_OK)
            //выводим полученные данные на стандартный вывод (консоль)
            cout << buffer << "\n";
    else
            //выводим сообщение об ошибке
            cout << "Ошибка! " << errorBuffer << endl;
    //завершаем сессию*/
    curl_easy_cleanup(curl);
    return 0;
}