/*
 * File:   main.cpp
 * Author: beetle
 *
 * Created on 14 Июль 2011 г., 18:58
 */

#include <QtGui/QApplication>
#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QTextEdit>
#include <curl/curl.h>

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);

    QWidget window;
    window.resize(500, 500);
    QPushButton quit("Quit", &window);
    quit.setGeometry(250, 350, 180, 40);
    //QObject::connect(&quit, SIGNAL(clicked()), &app, SLOT(quit()));
    QTextEdit te("hi",&window);
    te.setGeometry(10,10,300,300);
    QString str("hi hi hi");
    
    te.setText(str);
    window.show();


    return app.exec();
}
