/* 
 * File:   BFRT.h
 * Author: beetle
 *
 * Created on 18 Июль 2011 г., 23:50
 */

#ifndef BFRT_H
#define	BFRT_H

class BFRT {
public:
    BFRT();
    BFRT(const BFRT& orig);
    
    int tr(void);
    QString *buffer;
    virtual ~BFRT();
private:
        //необходимые CURL объекты
    CURL *curl;
    CURLcode result;
    const char *url;
    const char *requestf;
    int writer(char *data, size_t size, size_t nmemb, QString *buffer);
    //объявляем буфер, для хранения возможной ошибки, размер определяется в самой библиотеке
   // static char errorBuffer[CURL_ERROR_SIZE];
    char *errorBuffer;
    //объялвяем буфер принимаемых данных
    

};

#endif	/* BFRT_H */

