/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.cc
 * Copyright (C) beetle 2011 <beetle@>
 * 
 * libcurl_test2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libcurl_test2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <QString.h>
#include <curl/curl.h>

//подключаем стандартное пространство имен
using namespace std;
//объявляем буфер, для хранения возможной ошибки, размер определяется в самой библиотеке
static char errorBuffer[CURL_ERROR_SIZE];
//объялвяем буфер принимаемых данных
static string buffer;

//функция обратного вызова
static int writer(char *data, size_t size, size_t nmemb, string *buffer)
{
  //переменная - результат, по умолчанию нулевая
  int result = 0;
  //проверяем буфер
  if (buffer != NULL)
  {
    //добавляем к буферу строки из data, в количестве nmemb
    buffer->append(data, size * nmemb);
    //вычисляем объем принятых данных
    result = size * nmemb;
  }
  //вовзращаем результат
  return result;
}
int init(CURL *&curl,const char *&url, char *&postthis)
{
	//инициализируем curl
	curl = curl_easy_init();
	if (curl)
	{
		//задаем все необхо	мые опции
		//определяем, куда выводить ошибки
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
		//задаем опцию - получить страницу по адресу http://google.com
		curl_easy_setopt(curl, CURLOPT_URL, "google.com");
		//указываем прокси сервер
		//curl_easy_setopt(curl, CURLOPT_PROXY, url);
		//задаем опцию отображение заголовка страницы
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
		//указываем функцию обратного вызова для записи получаемых данных
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
		//указываем куда записывать принимаемые данные
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
		//указываем переменную с пост данными
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);
		//длинна пост данных - ВНИМАНИЕ НУЖНО ПРОИЗВОДИТЬ ПЕРЕД КАЖДОЙ ПЕРЕДАЧЕЙ ДАННЫХ
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
	}
	return 0;
}

int main()
{
	//необходимые CURL объекты
	CURL *curl;
	CURLcode result;
	const char *url = "http://google.com";
	const char *requestf = "BFGlobalService.login.req.xml";

	char *postthis;




	printf("%s",postthis);
	//проводим инициализацию
	init(curl,url,postthis);
	//запускаем выполнение задачи
	/*result = curl_easy_perform(curl);
	//проверяем успешность выполнения операции
	if (result == CURLE_OK)
		//выводим полученные данные на стандартный вывод (консоль)
		cout << buffer << "\n";
	else
		//выводим сообщение об ошибке
		cout << "Ошибка! " << errorBuffer << endl;
	//завершаем сессию*/
	curl_easy_cleanup(curl);
	return 0;
}
