/*
 * File:   main.cpp
 * Author: beetle
 *
 * Created on 26 Июль 2011 г., 23:50
 */

#include <QtGui/QApplication>
#include <string>
#include <iostream>
#include <QPushButton>
#include <QWidget>
#include <QTextEdit>
//#include <QString.h>
#include "curl/curl.h"
//подключаем стандартное пространство имен
using namespace std;
//объявляем буфер, для хранения возможной ошибки, размер определяется в самой библиотеке
static char errorBuffer[CURL_ERROR_SIZE];
//объялвяем буфер принимаемых данных
static string buffer;
//функция обратного вызова

static int writer(char *data, size_t size, size_t nmemb, string *buffer) {
    //переменная - результат, по умолчанию нулевая
    int result = 0;
    //проверяем буфер
    if (buffer != NULL) {
        //добавляем к буферу строки из data, в количестве nmemb
        buffer->append(data, size * nmemb);
        //вычисляем объем принятых данных
        result = size * nmemb;
    }
    //вовзращаем результат
    return result;
}

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);
    QWidget window;
    window.resize(500, 500);
    //QPushButton quit("Quit", &window);
    //    quit.setGeometry(250, 350, 180, 40);
    //QObject::connect(&quit, SIGNAL(clicked()), &app, SLOT(quit()));

    QTextEdit te("hi", &window);
    te.setGeometry(5, 5, 490, 400);
    QString str("hi hi hi");

    te.setText(str);
    window.show();
    //необходимые CURL объекты
    CURL *curl;
    CURLcode result;
    //инициализируем curl
    curl = curl_easy_init();
    //проверяем результат инициализации
    if (curl) {
        //задаем все необходимые опции
        //определяем, куда выводить ошибки
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
        //задаем опцию - получить страницу по адресу http://google.com
        curl_easy_setopt(curl, CURLOPT_URL, "google.com");
        //указываем прокси сервер
        //curl_easy_setopt(curl, CURLOPT_PROXY, "proxy:8080");
        //задаем опцию отображение заголовка страницы
        curl_easy_setopt(curl, CURLOPT_HEADER, 1);
        //указываем функцию обратного вызова для записи получаемых данных
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
        //указываем куда записывать принимаемые данные
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
        //запускаем выполнение задачи
        result = curl_easy_perform(curl);
        //проверяем успешность выполнения операции
        if (result == CURLE_OK) {
            //выводим полученные данные на стандартный вывод (консоль)
            //  cout << buffer << "\n";
            te.setText(QString::fromStdString(buffer));
            window.repaint();
        } else {
            //выводим сообщение об ошибке
            //  cout << "Ошибка! " << errorBuffer << endl;
            te.setText(QString::fromAscii(errorBuffer, CURL_ERROR_SIZE));
            window.repaint();
        }
    }
    //завершаем сессию
    curl_easy_cleanup(curl);

    return app.exec();
}
