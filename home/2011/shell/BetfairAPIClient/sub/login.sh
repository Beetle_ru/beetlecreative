#!/bin/bash

request=`cat ./data/requests/loginrequest.xml`
headers='SOAPAction: "login" User-Agent: Jakarta Commons-HttpClient/3.1 Host: api.betfair.com'
connecttimeout=5

#recived=`cat ./testrecive/recived_invalid` 
recived=`curl  -v --connect-timeout "$connecttimeout" -H "$headers" -d "$request"  https://api.betfair.com/global/v3/BFGlobalService`

echo "-------------------------------------------------------------------"

case $recived in
	*'<errorCode xsi:type="n2:APIErrorEnum">OK</errorCode>'*)
		case $recived in
			*'<errorCode xsi:type="n2:LoginErrorEnum">OK</errorCode>'*)
				echo "login --> ok"			
			;;
			*'<errorCode xsi:type="n2:LoginErrorEnum">INVALID_USERNAME_OR_PASSWORD</errorCode>'*)
				echo "login --> invalid username or password"
				exit 1
			;;
			*)
				echo "login --> unknown login error"
			;;
		esac
	;;
	*)
		echo "login --> unknown API error"
		exit 1
	;;
esac

sessionToken=${recived##*'<sessionToken xsi:type="xsd:string">'}
sessionToken=${sessionToken%%</sessionToken>*}

echo "sessionToken = $sessionToken"
echo "$sessionToken" > ./data/sessionToken

exit 0
