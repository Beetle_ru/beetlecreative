#!/bin/bash
OPTIONS="Hello Quit"
select opt in $OPTIONS; do
    case $opt in
    "Quit")
	exit 0
    ;;
    "Hello")
	echo "hello world"
    ;;
    *)
	echo "bad option"
    ;;
    esac
done