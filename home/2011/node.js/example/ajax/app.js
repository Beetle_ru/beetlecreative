
/**
 * Module dependencies.
 */

var express = require('express');
 // , routes = require('./routes')

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Tools check
var common = require('jsTools/common')
for (var i = 0; i < 10; i++) {
    console.log("RANDOM %d:%d", i, common.rndFromTo(0, 9))
}
var md5 = require('jsTools/md5')
var strTime = common.msTime().toString()
console.log("Current ms time:%s [md5 %s]", strTime, md5(strTime))

// Routes

//app.get('/', routes.index);
require('./routes/index').set(app);

app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
