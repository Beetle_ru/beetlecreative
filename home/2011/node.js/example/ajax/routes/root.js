exports.get = function(req, res) {
   res.render('root', {title: 'index'});
   global.i = 0;
   console.log("root");
}

exports.ajax = function(req, res) {
   console.log(req.body.test); // пример получения value через ajax 
   res.writeHead(200, {'content-type': 'text/html' });// заголовок для отправки
   res.write('test <BR> click - ' + ++global.i);// само сообщение
   res.end('\n');// конец сообщения
   console.log("rootAjax");
   console.log("     clk = " + global.i);
}
