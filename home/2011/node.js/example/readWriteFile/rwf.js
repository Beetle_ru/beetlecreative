var fs = require("fs"),
    sys = require("sys");
/*  * r — только чтение, указатель в начале файла
    * r+ — чтение и запись, указатель в начале файла
    * w — только запись, указатель в начале файла
    * w+ — запись и чтение, указатель в начале файла
    * a — запись, указатель в конце файла
    * a+ — запись и чтение, указатель в конце файла
*/

function fileOpen(err, file_handle) {
	if(!err){
		sys.puts("file open")
		fs.read(file_handle, 10000, null, 'ascii', fileRead)
		fs.write(file_handle, 'Copyrighted by Me \n', null, 'ascii',fileWrite)
		fs.close(file_handle)
	} else {
		sys.puts("ошибка открытия файла: " + err)
	}

}

function fileRead(err, data) {
	if(!err) {
		sys.puts("прочитано из файла -->" + data)
	} else {
		sys.puts("ошибка чтения файла: " + err)
	}
}

function fileWrite(err, writen) {
	if(!err) {
		sys.puts("в файл записано --> " + writen + " символов")
	} else {
		sys.puts("ошибка записи файла: " + err)
	}
}

fs.open("log.txt", "r+", 0644, fileOpen);
