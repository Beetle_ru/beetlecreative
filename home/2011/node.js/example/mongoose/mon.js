//require.paths.push('/usr/local/lib/node_modules');
var mongoose = require('mongoose');
//var db = mongoose.connect('mongodb://localhost/my_database');
var db = mongoose.connect('mongodb://localhost/my_database:2323');


var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

var BlogPost = new Schema({
    author    : ObjectId
  , title     : String
  , body      : String
  , date      : Date
});

var MyModel = mongoose.model('ModelName', BlogPost);


var instance = new MyModel();
instance.title = 'hello';
instance.body = 'hw hw hw hw hw';
instance.save(function (err) {
  //
});

MyModel.find({}, function (err, docs) {
  // docs.forEach
  //console.log(docs);
  for(var i = 0; i < docs.length; i++)
  {
	  console.log('________________________________________');
	  console.log("ID       ==>> " + docs[i]._id);
	  console.log("Title    ==>> " + docs[i].title);
	  console.log("Message  ==>> " );
	  console.log("[[");
	  console.log(docs[i].body);
	  console.log("[[");
	}
});

MyModel.findOne({}, function (err, doc){
  // doc is a Document
  //console.log(doc);
});
