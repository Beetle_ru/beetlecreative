
/**
 * Module dependencies.
 */
//require.paths.push('/usr/local/lib/node_modules');
var express = require('express')
   //routes = require('./routes')
//require('express/plugins');



//mongoose = require('mongoose').Mongoose
//db = mongoose.connect('mongodb://localhost/nodepad')

//Document = require('./models').Document(db);

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  
  app.use(express.cookieParser());                   // для поддержки сессий -> очень важно место положение этого кода
  app.use(express.session({ secret: 'secret'}));     // для поддержки сессий <== this work great
  
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
  
  //var MemStore = express.session.MemoryStore;
 
  //app.use(express.cookieParser());
  //app.use(express.session({ secret: "qqq", store: MemStore({reapInterval: 60000 * 10})}));
   // app.use(express.session());       // для поддержки сессий
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// Routes

require('./routes/index').set(app);

/*app.get('/', routes.index);
app.get('/test', routes.test);
app.post('/test2', routes.test2P);
app.get('/test2', routes.test2);*/


app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
