import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    width: 360
    height: 360
    Text {
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }

        Button {
            id: button1
            x: 48
            y: 208
            text: "Button"
        }

        CheckBox {
            id: check_box1
            x: 48
            y: 255
            text: "Check Box"
        }

        ComboBox {
            id: combo_box1
            x: 48
            y: 293
        }
    }
}
