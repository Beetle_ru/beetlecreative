program variant_r;

type
  DataType = (t_i, t_b, t_a);

  type1_R = record
    case integer of
    0 : (fieldI : integer);
    1 : (fieldBl : byte; fieldBR : byte);
    2 : (fieldA : array[0..1] of byte);
    3 : (field1 : 0..2; field2 : boolean);
  end;

function GetBit(var a : array of byte; n : longint) : byte;
var
  cbyte, cbit, mask : byte;
begin
  cbyte := n div 8;
  cbit := n mod 8;
  mask := 1 shl cbit;
  if a[cbyte] and mask = mask then GetBit := 1;
  if a[cbyte] and mask = 0 then GetBit := 0;
end;

var
  t : type1_R;
  i : integer;
begin
  //t.fieldI := 32767;
  t.fieldI := 0;
  t.field2 := true;
  writeln(t.fieldI);

  write(t.fieldBr);
  write(' ');
  writeln(t.fieldBl);

  for i := 15 downto 0 do
  begin
    write(GetBit(t.fieldA, i));
    write(' ');
  end;

end.