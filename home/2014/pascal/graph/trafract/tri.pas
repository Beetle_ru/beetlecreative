Program TriaFractal;
//Uses ptcGraph, ptcCrt;
uses sdlGraph;

Type
  TTriaPoints = array[0..3] of PointType; // Координаты для рисования треугольника

// Рекурсивная процедура для рисования треугольников
// внутри заданного треугольника
// Параметры:
//    Points - координаты внешнего треугольника
//    N - уровень вложенности процедуры
Procedure Triangle1(Points: TTriaPoints; N: Integer);
Var
  Points1: TTriaPoints;
Begin
  If N>0 Then
  Begin
    //Delay(200);
    SetColor(Random(14)+1); // Случайный цвет рисования. Чёрный нам, естественно, не нужен

    // Вычисление новых координат треугольника
    Points1[0].X:=(Points[1].X+Points[0].X) div 2;
    Points1[0].Y:=(Points[1].Y+Points[0].Y) div 2;
    Points1[1].X:=(Points[2].X+Points[0].X) div 2;
    Points1[1].Y:=(Points[2].Y+Points[0].Y) div 2;
    Points1[2].X:=(Points[1].X+Points[2].X) div 2;
    Points1[2].Y:=(Points[1].Y+Points[2].Y) div 2;
    Points1[3].X:=Points1[0].X;
    Points1[3].Y:=Points1[0].Y;

    DrawPoly(4, Points1);    // Рисование треугольника

    Triangle1(Points1, N-1); // Рекурсивный вызов для рисования внутри
  End;
End;

Var
  Driver: SmallInt;  // Номер драйвера
  Mode  : SmallInt;  // Номер графического режима
  Points: TTriaPoints;

Begin
  Randomize;
  // Установка графического режима
  //Driver:=VGA;
  //Mode:=VGAHi;
  DetectGraph(Driver, Mode);
  InitGraph(Driver, Mode, '');

  // Большой треугольник
  Points[0].X:=0;
  Points[0].Y:=400;
  Points[1].X:=600;
  Points[1].Y:=400;
  Points[2].X:=300;
  Points[2].Y:=0;
  Points[3].X:=Points[0].X;
  Points[3].Y:=Points[0].Y;
  DrawPoly(4, Points);
  Triangle1(Points, 6);

  // Верхний треугольник
  Points[0].X:=150;
  Points[0].Y:=200;
  Points[1].X:=450;
  Points[1].Y:=200;
  Points[2].X:=300;
  Points[2].Y:=0;
  Points[3].X:=Points[0].X;
  Points[3].Y:=Points[0].Y;
  Triangle1(Points, 6);

  // Левый треугольник
  Points[0].X:=0;
  Points[0].Y:=400;
  Points[1].X:=300;
  Points[1].Y:=400;
  Points[2].X:=150;
  Points[2].Y:=200;
  Points[3].X:=Points[0].X;
  Points[3].Y:=Points[0].Y;
  Triangle1(Points, 6);

  // Правый треугольник
  Points[0].X:=300;
  Points[0].Y:=400;
  Points[1].X:=600;
  Points[1].Y:=400;
  Points[2].X:=450;
  Points[2].Y:=200;
  Points[3].X:=Points[0].X;
  Points[3].Y:=Points[0].Y;
  Triangle1(Points, 6);

  ReadLn;
  CloseGraph;
End.
