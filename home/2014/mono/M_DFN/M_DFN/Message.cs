using System;
using Newtonsoft.Json;

namespace M_DFN {
	[Serializable]
	public class Message {
		public string Cmd;
		public string Data;
		
		public Message () {}
		public Message (Token cmd, Token data) {
			Cmd = cmd == null ? "" : cmd.serialize();
			Data = data == null ? "" : data.serialize();
		}
		
		static public Message Parse(string json) {
			try {
				var messg = JsonConvert.DeserializeObject<Message>(json);
				return messg;
			} catch {}
			return null;
		}
		
		public string Serialize() {
			return JsonConvert.SerializeObject(this);
		}
		
		public Token GetCmd() {
			return Token.deSerialize(Cmd);
		}
		
		public Token GetData() {
			return Token.deSerialize(Data);
		}
	}
}

