using System;
using System.Collections.Generic;

namespace M_DFN {
	static public class Base {
		
		static private Dictionary<string, InterfaceFunction> _ieDic;
		
		static public void Init() {
			if (_ieDic != null) return;
			
			_ieDic = new Dictionary<string, InterfaceFunction>();
			
			//########################################################
			_ieDic.Add("DFN.GETIE", DFN_GetIe);
			_ieDic.Add("DFN.QUERYHDBK", DFN_QueryHDBK);
			
			//########################################################
			foreach(var ie in _ieDic) {
				DFN.RegisterInterface(ie.Key, ie.Value);
			}
		}
		
		static public Token DFN_GetIe(Token cmd, Token data) {
			Console.WriteLine("calling DFN.GETIE");
			return new TokenStr("Take interfaces");
		}
		
		static public Token DFN_QueryHDBK(Token cmd, Token data) {
			Console.WriteLine("calling DFN.QUERYHDBK");
			return new TokenStr("HDBK is Alive");
		}

	}
}

