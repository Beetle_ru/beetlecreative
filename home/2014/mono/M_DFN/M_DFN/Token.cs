using System;
using Newtonsoft.Json;


namespace M_DFN {
	public abstract class Token {
		public abstract string ttype { get; set; }
		
		public virtual string serialize() {
			return JsonConvert.SerializeObject(this);
		}
		
		public static Token deSerialize(string json) {
			if(json.IndexOf("TokenInt") > -1) {
				Console.WriteLine("TokenInt");
				try {
					return JsonConvert.DeserializeObject<TokenInt>(json);
				} catch {}
			} else if(json.IndexOf("TokenStr") > -1) {
				Console.WriteLine("TokenStr");
				try {
					return JsonConvert.DeserializeObject<TokenStr>(json);
				} catch {}
			} else if(json.IndexOf("TokenCmd") > -1) {
				Console.WriteLine("TokenCmd");
				try {
					return JsonConvert.DeserializeObject<TokenCmd>(json);
				} catch {}
			}
			
			Console.WriteLine("Unknown Token Format");
			return null;
		}
	}
	
	[Serializable]
	public class TokenCmd : Token {
		public override string ttype { get;	set; }
		
		private void SetTtype() {
			ttype = "TokenCmd";
		}
		
		public string Id;
		public string Argument;
		public bool IsRequest;
		
		public TokenCmd() {
			SetTtype();
			Argument = "";
			IsRequest = true;
			Id = Guid.NewGuid().ToString();
		}
		
		public TokenCmd(string str, bool isRequest) {
			SetTtype();
			Argument = str;
			IsRequest = isRequest;
			Id = Guid.NewGuid().ToString();
		}
		
		public TokenCmd(Token generic) {
			SetTtype();
			if (generic.GetType() == typeof(TokenCmd)) {
				Argument = ((TokenCmd)generic).Argument;
				Id = ((TokenCmd)generic).Id;
				IsRequest = ((TokenCmd)generic).IsRequest;
			} else {
				throw new Exception(String.Format("Can't create '{0}' from Token", this.GetType()));
			}
		}
		
		public Token MakeResponse() {
			var respCmd = new TokenCmd(this);
			respCmd.IsRequest = false;
			return respCmd;
		}
	}
	
	[Serializable]
	public class TokenStr : Token {
		//public string ttype = "TokenStr";
		public override string ttype { get;	set; }
		
		private void SetTtype() {
			ttype = "TokenStr";
		}
		
		public string Argument;
		
		public TokenStr() {
			SetTtype();
			Argument = "";
		}
		
		public TokenStr(string str) {
			SetTtype();
			Argument = str;
		}
		
		public TokenStr(Token generic) {
			SetTtype();
			if (generic.GetType() == typeof(TokenStr)) {
				Argument = ((TokenStr)generic).Argument;
			} else {
				throw new Exception(String.Format("Can't create '{0}' from Token", this.GetType()));
			}
		}
	}
	
	[Serializable]
	public class TokenInt : Token {
		//public string ttype = "TokenInt";
		public override string ttype { get;	set; }
		
		private void SetTtype() {
			ttype = "TokenInt";
		}
		public int Argument;
		
		public TokenInt() {
			SetTtype();
			Argument = 0;
		}
		
		public TokenInt(int val) {
			SetTtype();
			Argument = val;
		}
		
		public TokenInt(Token generic) {
			SetTtype();
			if (generic.GetType() == this.GetType()) {
				Argument = ((TokenInt)generic).Argument;
			} else {
				throw new Exception(String.Format("Can't create '{0}' from Token", this.GetType()));
			}
		}
	}
}

