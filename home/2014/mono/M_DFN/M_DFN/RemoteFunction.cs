using System;
using System.Net;
using System.Net.Sockets;

namespace M_DFN
{
	public class RemoteFunction	{
		public IPEndPoint EP;
		public string Operation;
		
		public RemoteFunction (string operation, IPEndPoint ep) {
			EP = ep;
			Operation = operation;
		}
		
		public RemoteFunction (string operation, string ipAddress, int port) {
			EP = new IPEndPoint(IPAddress.Parse(ipAddress), port);
			Operation = operation;
		}
		
		public bool ContainsOperation(string operation) {
			return Operation == operation;
		}
	}
}

