using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;


namespace M_DFN {
	public delegate Token InterfaceFunction(Token cmd, Token data);
	
	static public class DFN {
		private const int HDBKPORT = 1133;
		static private Dictionary<string, InterfaceFunction> _InterfaceFunction = new Dictionary<string, InterfaceFunction>();
		static private List<RemoteFunction> _remoteFunctions = new List<RemoteFunction>();
		
		static private UdpClient _udpHDBK;
		static private Socket _udpSocket;
		static private IPEndPoint _groupEP;
		static private Thread _udpHDBKListenThread;
		static private Thread _udpSocketListenThread;
		static private Timer StartHDBKTimer;
		static private int _queryHDBKCounter = 9;
		
		static public void RegisterInterface(string cmd, InterfaceFunction callback) {
			if (!_InterfaceFunction.ContainsKey(cmd)) {
				_InterfaceFunction.Add(cmd, callback);
			} else {
				throw new Exception(String.Format("Interface '{0}' alredy exist", cmd));
			}
		}
		
		static public Token Call(Token cmd, Token data) {
			Base.Init();
			if (cmd.GetType() != typeof(TokenCmd)) return null;
			var tCmd = (TokenCmd)cmd;
			var key = tCmd.Argument;
			if (_InterfaceFunction.ContainsKey(key)) {
				return _InterfaceFunction[key](cmd, data);
			} else {
				var operation = ((TokenCmd)cmd).Argument;
				var remoteFunction = SearchRemoteFunction(operation);
				if (remoteFunction != null) {
					////////////////////!!!!!!!!!!!!!!!!!!
				}
			}
			return null;
		}
		
		static public RemoteFunction SearchRemoteFunction(string operation) {
			foreach (var rf in _remoteFunctions) {
				if (rf.ContainsOperation(operation)) return rf;
			}
			return null;
		}
		
		static public void ExportInterface() {
			_udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			
			_udpSocketListenThread = new Thread(new  ThreadStart(UdpKListenProc));
			_udpSocketListenThread.IsBackground = true;
			_udpSocketListenThread.Start();
			
			_remoteFunctions.Add(new RemoteFunction("DFN.QUERYHDBK", "127.0.0.1", 1133));
			StartHDBKTimer = new Timer(QueryRemoteHDBK, null, 0, new Random().Next(100, 500));
		}
		
		static public void QueryRemoteHDBK(Object stateInfo) {
			if (_queryHDBKCounter > 0) {
				_queryHDBKCounter--;
				
				var ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), HDBKPORT);
				var cmd = new TokenCmd("DFN.GETIE", true);			
				var message = new Message(cmd, new TokenStr("Query HDBK"));
				UdpRequest(message.Serialize(), ep);
			} else {
				StartHDBK();
				QueryHDBKStop();
			}
		}
		
		static public void QueryHDBKStop() {
			StartHDBKTimer.Change(-1,-1);
		}
		
		static private void StartHDBK() {
			Console.WriteLine("HDBK Exec");
			_udpHDBK = new UdpClient(HDBKPORT);
			_groupEP = new IPEndPoint(IPAddress.Any, HDBKPORT);
			
			_udpHDBKListenThread = new Thread(new  ThreadStart(UdpHDBKListenProc));
			_udpHDBKListenThread.IsBackground = true;
			_udpHDBKListenThread.Start();
			
		}
		
		static private void UdpRequest(string msg, IPEndPoint endPoint) {
			byte[] buffer = Encoding.ASCII.GetBytes(msg);
			_udpSocket.SendTo(buffer,endPoint);
		}
		
		static private void UdpHDBKRequest(string msg, IPEndPoint endPoint) {
			byte[] buffer = Encoding.ASCII.GetBytes(msg);
			_udpHDBK.Send(buffer, buffer.Length, endPoint);
		}
		
		static private void ResponseHandler(Token cmd, Token data) {
			Console.WriteLine("has response");
		}
		
		static private void UdpKListenProc() {
			try {
	            while (true) {
	                Console.WriteLine("...");
	                byte[] buffer = new byte[65507];

					var iep = new IPEndPoint(IPAddress.Any, 0);
					EndPoint ep = (EndPoint)iep;
				
					_udpSocket.ReceiveFrom(buffer, ref ep);
				
	                Console.WriteLine("Received broadcast from {0} :\n {1}\n",
	                    ep.ToString(),
	                    Encoding.ASCII.GetString(buffer,0,buffer.Length));
					var message = Message.Parse(Encoding.ASCII.GetString(buffer,0,buffer.Length));
					if (message != null) {
						QueryHDBKStop(); // не лучшее место
						var cmd = message.GetCmd();
						var data = message.GetData();
						if (((TokenCmd)cmd).IsRequest) {
							var res = Call(cmd, data);			
							message = new Message(((TokenCmd)cmd).MakeResponse(), res);
							UdpRequest(message.Serialize(), (IPEndPoint)ep);
						} else {
							ResponseHandler(cmd, data);
						}
					}
	            }
	            
	        } 
	        catch (Exception e) {
	            Console.WriteLine(e.ToString());
	        }
	        finally {
	            _udpSocket.Close();
	        }
		}
		
		static private void UdpHDBKListenProc() {
			try {
	            while (true) {
	                Console.WriteLine("Waiting for broadcast");
	                byte[] buffer = _udpHDBK.Receive( ref _groupEP);
	
	                Console.WriteLine("Received broadcast from {0} :\n {1}\n",
	                    _groupEP.ToString(),
	                    Encoding.ASCII.GetString(buffer,0,buffer.Length));
					var message = Message.Parse(Encoding.ASCII.GetString(buffer,0,buffer.Length));
					if (message != null) {
						var cmd = message.GetCmd();
						var data = message.GetData();
						if (((TokenCmd)cmd).IsRequest) {
							var res = Call(cmd, data);			
							message = new Message(((TokenCmd)cmd).MakeResponse(), res);
							UdpHDBKRequest(message.Serialize(), _groupEP);
						} else {
							ResponseHandler(cmd, data);
						}
					}
	            }
	            
	        } 
	        catch (Exception e) {
	            Console.WriteLine(e.ToString());
	        }
	        finally {
	            _udpHDBK.Close();
	        }
		}
	}
}

