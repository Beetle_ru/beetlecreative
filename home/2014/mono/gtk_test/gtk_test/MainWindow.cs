using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{	
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void OnBtnCloredClicked (object sender, System.EventArgs e)
	{
		Btn_clored.ModifyBg(StateType.Normal, new Gdk.Color(255,0,0));
		//throw new System.NotImplementedException ();
	}
}
